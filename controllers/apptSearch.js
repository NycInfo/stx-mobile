var config = require('../common/config');
var sendResponse = require('../common/response').sendResponse;
var logger = require('../common/logger');
var execute = require('../common/dbConnection');

module.exports.controller = function(app) {

    //--- Start of API to get Appointment search results ---//
    app.post('/api/appointment/v2/search', function(req, res) {
        //--- Input Param ---//var dbName = req.headers['db'];
        var inputParms = req.body;
        var dbName = req.headers['db'];
        inputParms.durations = [];
        inputParms.serviceData = [];
        inputParms.bookEvery = [];
        inputParms.resources = [];
        inputParms.resFilter = [];
        var index = 0,
            apptPrfData = [],
            cmpHrsData = [],
            cstHrsData = [],
            wrkSrvData = [],
            resourcesData = [];
        //--- Appointments settings data for Duration and Interval ---//
        getDursrvData(0, dbName, inputParms, function(durData) {
            index++;
            inputParms = durData;
            // inputParms.durations.push(durData.durations);
            // inputParms.serviceData.push(durData.serviceData);
            // inputParms.bookEvery.push(durData.bookEvery);
            finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, resourcesData, inputParms, res);
        });
        getExpressBookingServices(0, dbName, inputParms, function(resData) {
            index++;
            // inputParms = resData;
            inputParms.resources.push(resData.resources);
            inputParms.resFilter.push(resData.resFilter);
            finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, resourcesData, inputParms, res);
        });
        getApptPrf(dbName, inputParms, function(data) {
            index++;
            apptPrfData = data;
            finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, resourcesData, inputParms, res);
        });
        //--- All days all input workers working days (Company and Custom Hours) ---//
        getWrkHrs(dbName, inputParms, function(data1, data2) {
            index++;
            //--- Sorting Company Hours according to input Ids ---//
            for (var i = 0; i < inputParms['worker_id'].length; i++) {
                cmpHrsData.push(data1.filter(function(a) {
                    return a['Id'] === inputParms['worker_id'][i]
                })[0]);
            }
            cstHrsData = data2;
            finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, resourcesData, inputParms, res);
        });
        //--- All services done by input workers for the next 2 weeks ---//
        getWrkSrvData(dbName, inputParms, function(data) {
            index++;
            wrkSrvData = data;
            finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, resourcesData, inputParms, res);
        });
        //--- All resources used for the next 2 weeks ---//
        getResourcesData(dbName, inputParms, function(data) {
            index++;
            resourcesData = data;
            finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, resourcesData, inputParms, res);
        });
    });
    //--- End of API to get Appointment search results ---//
};

//--- Start of function to get data for Appointments settings ---//
function getApptPrf(dbName, inputParms, callback) {
    var apptPrfQry1 = ''
    var apptPrfQry = 'SELECT JSON__c FROM Preference__c WHERE Name = "Appt Booking" and IsDeleted=0';
    if (inputParms.Booked_Online__c) {
        apptPrfQry1 = 'SELECT JSON__c FROM Preference__c WHERE Name = "Appt Booking" and IsDeleted=0';
    }
    execute.query(dbName, apptPrfQry + ';' + apptPrfQry1, '', function(err, data) {
        if (err) {
            logger.error('Error in getApptPrf: ', err);
            callback(null);
        } else {
            if (inputParms.Booked_Online__c) {
                var resData = JSON.parse(data[0][0].JSON__c);
                resData.onlineMaximumAvailableToShow = JSON.parse(data[1][0].JSON__c.replace('\\', '\\\\')).maximumAvailableToShow;
                callback(resData);
            } else {
                callback(JSON.parse(data[0].JSON__c));
            }

        }
    });
}

function getDursrvData(i, dbName, inputParms, callback) {
    if (i < inputParms.worker_id.length) {
        var sql = 'SELECT IFNULL(Duration_1__c, 0) Duration_1__c, IFNULL(Duration_2__c, 0) Duration_2__c, IFNULL(Duration_3__c, 0) Duration_3__c, IFNULL(Buffer_After__c, 0) Buffer_After__c, Duration_1_Available_for_Other_Work__c, Duration_2_Available_for_Other_Work__c, Duration_3_Available_for_Other_Work__c FROM Worker_Service__c WHERE Worker__c = "' + inputParms.worker_id[i] + '" and Service__c = "' + inputParms.service_id[i] + '" and IsDeleted=0';
        var sql1 = 'SELECT IFNULL(Book_Every__c, 0) Book_Every__c, Service_Level__c FROM User__c WHERE Id = "' + inputParms.worker_id[i] + '"';
        var sql2 = 'SELECT Levels__c FROM Service__c WHERE Id = "' + inputParms.service_id[i] + '" and IsDeleted=0';
        execute.query(dbName, sql, '', function(err, data) {
            if (err) {
                logger.error('Error in getApptPrf: ', err);
            } else {
                if (data.length > 0 && (data[0].Buffer_After__c != 0 || data[0].Duration_1__c != 0 ||
                        data[0].Duration_2__c != 0 || data[0].Duration_3__c != 0)) {
                    inputParms.durations.push(data[0].Buffer_After__c + data[0].Duration_1__c +
                        data[0].Duration_2__c + data[0].Duration_3__c);
                    inputParms.serviceData.push({
                        'duration1': data[0].Duration_1__c,
                        'duration2': data[0].Duration_2__c,
                        'duration3': data[0].Duration_3__c,
                        'bufferafter': data[0].Buffer_After__c,
                        'available1': data[0].Duration_1_Available_for_Other_Work__c,
                        'available2': data[0].Duration_2_Available_for_Other_Work__c,
                        'available3': data[0].Duration_3_Available_for_Other_Work__c
                    });
                    execute.query(dbName, sql1, '', function(err, userData) {
                        inputParms.bookEvery.push(userData[0].Book_Every__c);
                        getDursrvData(i + 1, dbName, inputParms, callback);
                    });
                } else {
                    var temp = 0;
                    execute.query(dbName, sql1, '', function(err, userData) {
                        if (err) {
                            logger.error('Error in getApptPrf: ', err);
                        } else {
                            execute.query(dbName, sql2, '', function(err, durations) {
                                var levels = JSON.parse(durations[0].Levels__c)
                                inputParms.bookEvery.push(userData[0].Book_Every__c);
                                levels.filter(function(level) {
                                    if (userData[0].Service_Level__c == level.levelNumber) {
                                        temp++;
                                        inputParms.durations.push(level.totalDuration);
                                        inputParms.serviceData.push({
                                            'duration1': level.duration1,
                                            'duration2': level.duration2,
                                            'duration3': level.duration3,
                                            'bufferafter': level.bufferAfter,
                                            'available1': level.duration1AvailableForOtherWork,
                                            'available2': level.duration2AvailableForOtherWork,
                                            'available3': level.duration3AvailableForOtherWork
                                        });
                                    }
                                });
                                if (temp === 0) {
                                    inputParms.durations.push(levels[0].totalDuration);
                                    inputParms.serviceData.push({
                                        'duration1': levels[0].duration1,
                                        'duration2': levels[0].duration2,
                                        'duration3': levels[0].duration3,
                                        'bufferafter': levels[0].bufferAfter,
                                        'available1': levels[0].duration1AvailableForOtherWork,
                                        'available2': levels[0].duration2AvailableForOtherWork,
                                        'available3': levels[0].duration3AvailableForOtherWork,
                                    });
                                }
                                getDursrvData(i + 1, dbName, inputParms, callback);
                            });
                        }
                    });
                }
            }
        });
    } else {
        callback(inputParms);
    }
}

function getExpressBookingServices(i, dbName, inputParms, done) {
    try {
        if (i < inputParms.worker_id.length) {
            var sql = ' SELECT GROUP_CONCAT(IFNULL(sr.Priority__c,0)) as priority,' +
                ' s.Resource_Filter__c as filters,' +
                ' IFNULL(GROUP_CONCAT(r.Name," #1" ),"#1") as resourceName,' +
                ' IFNULL(ws.Price__c,0) as Price__c ,' +
                ' IFNULL(s.Buffer_After__c,0) as serviceBuffer, IFNULL(ws.Duration_1__c,0) as workerDuration1, ' +
                ' IFNULL(s.Duration_1__c,0) as serviceDuration1, IFNULL(ws.Duration_2__c,0) as workerDuration2, ' +
                ' IFNULL(s.Duration_2__c,0) as serviceDuration2, IFNULL(ws.Duration_3__c,0) as workerDuration3, ' +
                ' IFNULL(s.Duration_3__c,0) as serviceDuration3, ws.Duration_1_Available_for_Other_Work__c as workerAvail1,' +
                ' ws.Duration_2_Available_for_Other_Work__c as workerAvail2,' +
                ' ws.Duration_3_Available_for_Other_Work__c as workerAvail3,' +
                ' (IFNULL(ws.Duration_1__c,0)+IFNULL(ws.Duration_2__c,0)+IFNULL(ws.Duration_3__c,0)+IFNULL(ws.Buffer_After__c,0)) as sumDurationBuffer ' +
                ' FROM Worker_Service__c as ws' +
                ' left join Service__c as s on ws.Service__c = s.Id ' +
                ' left join Service_Resource__c as sr on sr.Service__c = s.Id ' +
                ' left join Resource__c as r on sr.Resource__c = r.Id ' +
                ' and r.IsDeleted =0 ' +
                ' and s.IsDeleted = 0 ' +
                ' and sr.IsDeleted = 0 ' +
                ' and ws.IsDeleted= 0 ' +
                ' where ws.Worker__c = "' + inputParms.worker_id[i] + '" ' +
                ' and ws.Service__c = "' + inputParms.service_id[i] + '" ' +
                ' and s.Is_Class__c=0 ' +
                ' GROUP by s.Id order by s.Service_Group__c, s.Name;'
            execute.query(dbName, sql, '', function(err, data) {
                if (err) {
                    logger.error('Error in appoitments dao - getExpressBookingServices:', err);
                    done(err, { statusCode: '9999' });
                } else {
                    if (data.length > 0) {
                        inputParms.resources.push(data[0].resourceName.replace(/#1/g, ''));
                        inputParms.resFilter.push(data[0].filters);
                    }
                    getExpressBookingServices(i + 1, dbName, inputParms, done)
                }
            });
        } else {
            done(inputParms);
        }
    } catch (err) {
        logger.error('Unknown error in apptBooking dao - getExpressBookingServices:', err);
        return (err, { statusCode: '9999' });
    }
}
//--- End of function to get data for Appointments settings ---//

//--- Start of function to get data for all days and all input workers working days (Company and Custom Hours) ---//
function getWrkHrs(dbName, parms, callback) {
    var cmpHrsQry = `SELECT u.Id, ch.Id as compHrsId, ch.SundayStartTime__c as SundayStartTime__c, 
        ch.SundayEndTime__c as SundayEndTime__c, ch.MondayStartTime__c as MondayStartTime__c, 
        ch.MondayEndTime__c as MondayEndTime__c, ch.TuesdayStartTime__c as TuesdayStartTime__c, 
        ch.TuesdayEndTime__c as TuesdayEndTime__c, ch.WednesdayStartTime__c as WednesdayStartTime__c, 
        ch.WednesdayEndTime__c as WednesdayEndTime__c, ch.ThursdayStartTime__c as ThursdayStartTime__c, 
        ch.ThursdayEndTime__c as ThursdayEndTime__c, ch.FridayStartTime__c as FridayStartTime__c, 
        ch.FridayEndTime__c as FridayEndTime__c, ch.SaturdayStartTime__c as SaturdayStartTime__c, 
        ch.SaturdayEndTime__c as SaturdayEndTime__c 
        FROM User__c as u`
    if (parms.Booked_Online__c) {
        cmpHrsQry += ` LEFT JOIN Company_Hours__c as ch on ch.Id = u.Online_Hours__c`
    } else {
        cmpHrsQry += ` LEFT JOIN Company_Hours__c as ch on ch.Id = u.Appointment_Hours__c`
    }
    cmpHrsQry += ` WHERE ch.IsDeleted = 0 AND u.Id IN ` + getInQryStr(parms['worker_id']);
    execute.query(dbName, cmpHrsQry, '', function(err, cmpHrsData) {
        if (err) {
            logger.error('Error in getWrkHrs:', err);
            callback(null, null);
        } else {
            if (cmpHrsData && cmpHrsData.length > 0) {
                var compHrsIds = '(';
                for (var i = 0; i < cmpHrsData.length; i++) {
                    compHrsIds += '\'' + cmpHrsData[i].compHrsId + '\','
                }
                compHrsIds = compHrsIds.slice(0, -1);
                compHrsIds += ')';
                //--- Adding custom hours ---//
                getCstHrs(dbName, compHrsIds, parms['date'], function(cstHrsData) {
                    callback(cmpHrsData, cstHrsData);
                });
            } else {
                callback(null, null);
            }
        }
    });
}
//--- End of function to get data for all days and all input workers working days (Company and Custom Hours) ---//

//--- Start of function to get data for Custom Hours for the next 2 weeks from the selected date ---//
function getCstHrs(dbName, compHrsIds, date, callback) {
    compHrsIds = compHrsIds.replace(/-OLB/g, '');
    var cstHrsQry = "SELECT Date__c, All_Day_Off__c, StartTime__c, EndTime__c, Company_Hours__c FROM CustomHours__c WHERE " +
        "Date__c >= '" + date + "' AND Date__c <= ('" + date + "' + INTERVAL 14 DAY) AND Company_Hours__c IN " + compHrsIds + " AND IsDeleted=0";
    execute.query(dbName, cstHrsQry, '', function(err, data) {
        if (err) {
            callback(null);
        } else {
            callback(data);
        }
    });
}
//--- End of function to get data for Custom Hours for the next 2 weeks from the selected date ---//

//--- Start of function to get data for all services for selected workers ---//
function getWrkSrvData(dbName, param, callback) {
    var wrkSrvQry = `SELECT 
            ts.Service_Date_Time__c, 
            ts.Duration__c, 
            ts.Worker__c,
            ts.Duration_1_Available_for_Other_Work__c dur1Available,
            ts.Duration_2_Available_for_Other_Work__c dur2Available, 
            ts.Duration_3_Available_for_Other_Work__c dur3Available,
            ts.Duration_1__c,
            IFNULL(ts.Duration_2__c, 0) Duration_2__c,
            IFNULL(ts.Duration_3__c, 0) Duration_3__c,
            IFNULL(ts.Buffer_After__c, 0) Buffer_After__c
        FROM 
            Ticket_Service__c ts,
            Appt_Ticket__c a
        WHERE 
            ts.Appt_Ticket__c = a.Id AND
            a.Status__c NOT IN ('Canceled') AND
            ts.IsDeleted = 0 AND 
            ts.Service_Date_Time__c IS NOT NULL AND 
            ts.Service_Date_Time__c >= '` + param['date'] + `' AND 
            ts.Service_Date_Time__c <= ('` + param['date'] + `' + INTERVAL 14 DAY) AND 
            ts.Worker__c IN ` + getInQryStr(param['worker_id']);
    execute.query(dbName, wrkSrvQry, '', function(err, data) {
        if (err) {
            callback(null);
        } else {
            callback(data);
        }
    });
}
//--- End of function to get data for all services for selected workers ---//

//--- Start of function to get all resources used ---//
function getResourcesData(dbName, param, callback) {
    var rsrcs = param.resources,
        resources = '',
        resrAry = [];
    for (var i = 0; i < rsrcs.length; i++) {
        if (rsrcs[i]) {
            if (rsrcs[i].indexOf(',') > -1) {
                var tempRsr = rsrcs[i].split(',');
                tempRsr.forEach(function(obj) {
                    resources += (obj + '|');
                    resrAry.push(obj);
                });
            } else {
                resources += (rsrcs[i] + '|');
                resrAry.push(rsrcs[i]);
            }
        }
    }
    if (resources.length > 0) {
        resources = resources.slice(0, -1);
        var resourcesQry = `SELECT 
            ts.Service_Date_Time__c, 
            ts.Duration__c,
            ts.Duration_1_Available_for_Other_Work__c dur1Available,
            ts.Duration_2_Available_for_Other_Work__c dur2Available, 
            ts.Duration_3_Available_for_Other_Work__c dur3Available,
            ts.Duration_1__c,
            ts.Duration_2__c,
            ts.Duration_3__c,
            ts.Buffer_After__c,
            ts.Resources__c,
            r.Number_Available__c,
            r.Name
        FROM 
            Ticket_Service__c ts,
            Appt_Ticket__c a,
            Resource__c r
        WHERE 
            ts.Appt_Ticket__c = a.Id AND
            a.Status__c NOT IN ('Canceled') AND
            ts.IsDeleted = 0 AND 
            ts.Service_Date_Time__c IS NOT NULL AND 
            ts.Service_Date_Time__c >= '` + param['date'] + `' AND 
            ts.Service_Date_Time__c <= ('` + param['date'] + `' + INTERVAL 14 DAY) AND 
            ts.Resources__c LIKE CONCAT('%', r.Name, '%') AND
            ts.Resources__c REGEXP '` + resources + `'`;
        execute.query(dbName, resourcesQry, '', function(err, data) {
            if (err) {
                callback(null);
            } else {
                callback(data);
            }
        });
    } else {
        callback(null);
    }

}
//--- End of function to get all resources used ---//

//--- Start of main function to generate output for the request ---//
function finalMethod(index, apptPrfData, cmpHrsData, cstHrsData, wrkSrvData, resourcesData, inputParms, res) {
    if (index == 6) {
        //--- Generation of time slots ---//
        var bookEvery = lcm_more_than_two_numbers(inputParms.bookEvery, apptPrfData);
        var srtEndAry = gntSrvcRslt(bookEvery, cmpHrsData, cstHrsData, inputParms);
        var stdrn = 0;
        var subParamsAry = {
            'id': [],
            'durations': []
        };
        //--- Start of filtering time slots by considering existing worker serives ---//
        for (var i = 0; i < inputParms['durations'].length; i++) {
            if (srtEndAry && srtEndAry.length > 0) {
                // if (wrkSrvData && wrkSrvData.length > 0) {
                //     var tempWrkSrvData = wrkSrvData.filter(function (a) { return a.Worker__c == inputParms['worker_id'][i] });
                //     srtEndAry = filterWrkSrv(srtEndAry, tempWrkSrvData, stdrn, inputParms['durations'][i]);
                // }
                if (resourcesData && resourcesData.length > 0 && inputParms['resources'][i]) {
                    srtEndAry = filterResources(srtEndAry, resourcesData, stdrn, inputParms['durations'][i], inputParms['resources'][i].split(','), inputParms['resFilter'][i]);
                }
            }
            stdrn += parseInt(inputParms['durations'][i], 10);
            //--- Start of code to divide durations based on avilability --//
            var duration1 = 0;
            if (+inputParms['serviceData'][i]['duration1']) {
                duration1 = +inputParms['serviceData'][i]['duration1'];
            }
            var duration2 = 0;
            if (+inputParms['serviceData'][i]['duration2']) {
                duration2 = +inputParms['serviceData'][i]['duration2'];
            }
            var duration3 = 0;
            if (+inputParms['serviceData'][i]['duration3']) {
                duration3 = +inputParms['serviceData'][i]['duration3'];
            }
            var bufferafter = 0;
            if (+inputParms['serviceData'][i]['bufferafter']) {
                bufferafter = +inputParms['serviceData'][i]['bufferafter'];
            }
            if (!inputParms['serviceData'][i]['available1'] &&
                !inputParms['serviceData'][i]['available2'] &&
                !inputParms['serviceData'][i]['available3']) {
                subParamsAry['durations'].push(duration1 + duration2 + duration3 + bufferafter);
                subParamsAry['id'].push(inputParms['worker_id'][i]);
            } else if (!inputParms['serviceData'][i]['available1'] &&
                !inputParms['serviceData'][i]['available2'] &&
                inputParms['serviceData'][i]['available3']) {
                subParamsAry['durations'].push(duration1 + duration2 + bufferafter);
                subParamsAry['durations'].push(duration3 + bufferafter);
                subParamsAry['id'].push(inputParms['worker_id'][i]);
                subParamsAry['id'].push(null);
            } else if (!inputParms['serviceData'][i]['available1'] &&
                inputParms['serviceData'][i]['available2'] &&
                !inputParms['serviceData'][i]['available3']) {
                subParamsAry['durations'].push(duration1 + bufferafter);
                subParamsAry['durations'].push(duration2 + bufferafter);
                subParamsAry['durations'].push(duration3 + bufferafter);
                subParamsAry['id'].push(inputParms['worker_id'][i]);
                subParamsAry['id'].push(null);
                subParamsAry['id'].push(inputParms['worker_id'][i]);
            } else if (inputParms['serviceData'][i]['available1'] &&
                !inputParms['serviceData'][i]['available2'] &&
                !inputParms['serviceData'][i]['available3']) {
                subParamsAry['durations'].push(duration1 + bufferafter);
                subParamsAry['durations'].push(duration2 + duration3 + bufferafter);
                subParamsAry['id'].push(null);
                subParamsAry['id'].push(inputParms['worker_id'][i]);
            } else if (!inputParms['serviceData'][i]['available1'] &&
                inputParms['serviceData'][i]['available2'] &&
                inputParms['serviceData'][i]['available3']) {
                subParamsAry['durations'].push(duration1 + bufferafter);
                subParamsAry['durations'].push(duration2 + duration3 + bufferafter);
                subParamsAry['id'].push(inputParms['worker_id'][i]);
                subParamsAry['id'].push(null);
            } else if (inputParms['serviceData'][i]['available1'] &&
                !inputParms['serviceData'][i]['available2'] &&
                inputParms['serviceData'][i]['available3']) {
                subParamsAry['durations'].push(duration1 + bufferafter);
                subParamsAry['durations'].push(duration2 + bufferafter);
                subParamsAry['durations'].push(duration3 + bufferafter);
                subParamsAry['id'].push(null);
                subParamsAry['id'].push(inputParms['worker_id'][i]);
                subParamsAry['id'].push(null);
            } else if (inputParms['serviceData'][i]['available1'] &&
                inputParms['serviceData'][i]['available2'] &&
                !inputParms['serviceData'][i]['available3']) {
                subParamsAry['durations'].push(duration1 + duration2 + bufferafter);
                subParamsAry['durations'].push(duration3 + bufferafter);
                subParamsAry['id'].push(null);
                subParamsAry['id'].push(inputParms['worker_id'][i]);
            } else if (inputParms['serviceData'][i]['available1'] &&
                inputParms['serviceData'][i]['available2'] &&
                inputParms['serviceData'][i]['available3']) {
                subParamsAry['durations'].push(duration1 + duration2 + duration3 + bufferafter);
                subParamsAry['id'].push(null);
            }
            //--- End of code to divide durations based on avilability --//
        }
        stdrn = 0;
        for (var i = 0; i < subParamsAry['durations'].length; i++) {
            if (wrkSrvData && wrkSrvData.length > 0 && subParamsAry['durations'][i]) {
                var tempWrkSrvData = wrkSrvData.filter(function(a) { return a.Worker__c == subParamsAry['id'][i] });
                srtEndAry = filterWrkSrv(srtEndAry, tempWrkSrvData, stdrn, subParamsAry['durations'][i]);
            }
            stdrn += subParamsAry['durations'][i];
        }
        //--- End of filtering time slots by considering existing worker serives ---//
        //--- Start of generation of ranks ---//
        if (srtEndAry.length > 0) {
            srtEndAry.forEach(function(item, index) {
                if (srtEndAry[index][2]) {
                    srtEndAry[index] = { 'date': srtEndAry[index][0], 'value': srtEndAry[index][1], 'rank': srtEndAry[index][2] };
                } else {
                    srtEndAry[index] = { 'date': srtEndAry[index][0], 'value': srtEndAry[index][1], 'rank': 0 };
                }
            });
            //--- Ranks generation ---//
            srtEndAry = generateRanks(srtEndAry, inputParms, cmpHrsData, cstHrsData, wrkSrvData);
            if (apptPrfData['availabilityOrder'] === 'Ranking Order' && !inputParms.Booked_Online__c) {
                //--- Sorting array based on ranks ---//
                srtEndAry = sortJSONAry(srtEndAry, 'rank', 'desc');
            }
            var maximumAvailable = 0
            if (inputParms.Booked_Online__c) {
                maximumAvailable = apptPrfData['onlineMaximumAvailableToShow']
            } else {
                maximumAvailable = apptPrfData['maximumAvailableToShow']
            }
            srtEndAry = srtEndAry.slice(0, maximumAvailable);
            //--- Delete date key from all output objects ---//
            srtEndAry = deleteDateKey(srtEndAry);
        }
        //--- End of generation of ranks ---//
        //--- Final response ---//
        if (srtEndAry && srtEndAry.length > 0) {
            sendResponse(res, true, srtEndAry);
        } else {
            sendResponse(res, false, '011');
        }

    }
}
//--- End of main function to generate output for the request ---//

//--- Start of function to generate Time slots ---//
function gntSrvcRslt(bookEvery, cmpHrsData, cstHrsData, params) {
    var tempDtObj = params['date'].split('-');
    var selDt = new Date(tempDtObj[0], (parseInt(tempDtObj[1], 10) - 1), tempDtObj[2]);
    //--- Generates default start and end times of workers first worker company hours ---//
    // var srtEndAry = getDefStEdAry(cmpHrsData[0], selDt);
    var tempsrtEndAry = [];
    for (var i = 0; i < cmpHrsData.length; i++) {
        tempsrtEndAry.push(getDefStEdAry(cmpHrsData[i], selDt));
    }
    var srtEndAry = [];
    for (var i = 0; i < tempsrtEndAry.length; i++) {
        if (i === 0) {
            srtEndAry = tempsrtEndAry[i];
        } else {
            for (var j = 0; j < tempsrtEndAry[i].length; j++) {
                if (!tempsrtEndAry[i][j][1]) {
                    srtEndAry[j] = tempsrtEndAry[i][j];
                }
            }
        }
    }
    //--- First worker custom hours ---//
    // var firstWrkCstHrs = cstHrsData.filter(function (a) { return a['Company_Hours__c'] === cmpHrsData[0]['Id'] });
    // //--- Start of adding custom hours (Considering custom hours and all day off) ---//
    // for (var i = 0; i < srtEndAry.length; i++) {
    //     if (firstWrkCstHrs && firstWrkCstHrs.length > 0) {
    //         for (var j = 0; j < firstWrkCstHrs.length; j++) {
    //             tempDtObj = firstWrkCstHrs[j]['Date__c'].split('-');
    //             var cstDt = new Date(tempDtObj[0], (parseInt(tempDtObj[1], 10) - 1), tempDtObj[2]);
    //             if (srtEndAry[i] && cstDt.getTime() === srtEndAry[i][0].getTime()) {
    //                 if (firstWrkCstHrs[j]['All_Day_Off__c'] == 1) {
    //                     srtEndAry[i] = null;
    //                 } else {
    //                     srtEndAry[i] = [cstDt, firstWrkCstHrs[j]['StartTime__c'], firstWrkCstHrs[j]['EndTime__c']];
    //                 }
    //             }
    //         }
    //     }
    //     selDt.setDate(selDt.getDate() + 1);
    // }
    // //--- End of adding custom hours (Considering custom hours and all day off) ---//
    // //--- Removing null values from output array ---//
    // srtEndAry = srtEndAry.filter(function (a) { return a !== null && a[1] && a[2] });

    //--- Start of code to remove dayoff days from the list ---//
    for (var i = 0; i < cstHrsData.length; i++) {
        var dayOffObj = cstHrsData[i]['Date__c'].split('-');
        var dayOffDate = new Date(dayOffObj[0], (parseInt(dayOffObj[1], 10) - 1), dayOffObj[2]);
        if (cstHrsData[i]['All_Day_Off__c'] == 1) {
            srtEndAry = srtEndAry.filter(obj => obj[0].getTime() !== dayOffDate.getTime());
        } else {
            srtEndAry.forEach(obj => {
                if (obj[0].getTime() === dayOffDate.getTime()) {
                    obj[1] = cstHrsData[i]['StartTime__c'];
                    obj[2] = cstHrsData[i]['EndTime__c'];
                }
            });
        }
    }
    //--- End of code to remove dayoff days from the list ---//
    //--- Generation of time slots ---//
    return getChrnOdrAry(srtEndAry, bookEvery, params, cmpHrsData, cstHrsData);
}
//--- End of function to generate Time slots ---//

//--- Start of function to genetate start and end times of the days ---//
function getDefStEdAry(cmpHrsData, selDt) {
    var tempDt = new Date(selDt.getFullYear(), selDt.getMonth(), selDt.getDate());
    var rtnAry = [];
    for (var i = 0; i < 1; i++) {
        switch (tempDt.getDay()) {
            case 0:
                rtnAry.push([new Date(tempDt), cmpHrsData['SundayStartTime__c'], cmpHrsData['SundayEndTime__c']]);
                break;
            case 1:
                rtnAry.push([new Date(tempDt), cmpHrsData['MondayStartTime__c'], cmpHrsData['MondayEndTime__c']]);
                break;
            case 2:
                rtnAry.push([new Date(tempDt), cmpHrsData['TuesdayStartTime__c'], cmpHrsData['TuesdayEndTime__c']]);
                break;
            case 3:
                rtnAry.push([new Date(tempDt), cmpHrsData['WednesdayStartTime__c'], cmpHrsData['WednesdayEndTime__c']]);
                break;
            case 4:
                rtnAry.push([new Date(tempDt), cmpHrsData['ThursdayStartTime__c'], cmpHrsData['ThursdayEndTime__c']]);
                break;
            case 5:
                rtnAry.push([new Date(tempDt), cmpHrsData['FridayStartTime__c'], cmpHrsData['FridayEndTime__c']]);
                break;
            case 6:
                rtnAry.push([new Date(tempDt), cmpHrsData['SaturdayStartTime__c'], cmpHrsData['SaturdayEndTime__c']]);
                break;
            default:
                break;
        }
        tempDt.setDate(tempDt.getDate() + 1);
    }
    return rtnAry;
}
//--- End of function to genetate start and end times of the days ---//

//--- Start of function to genetate time slots based on chronological order ---//
function getChrnOdrAry(srtEndAry, intvl, params, cmpHrsData, cstHrsData) {
    var tmpAry = params['mindate'].split(' ');
    var temAry2 = tmpAry[0].split('-');
    var temAry3 = tmpAry[1].split(':');
    var minDtObj = new Date(temAry2[0], parseInt(temAry2[1], 10) - 1, temAry2[2], temAry3[0], temAry3[1], temAry3[2]);
    var rtnObj = [];
    for (var i = 0; i < srtEndAry.length; i++) {
        var srtDate = new Date(srtEndAry[i][0]);
        var stHrsMin = getHrsMin(srtEndAry[i][1]);
        srtDate.setHours(stHrsMin[0]);
        srtDate.setMinutes(stHrsMin[1]);
        var endDate = new Date(srtEndAry[i][0]);
        var endHrsMin = getHrsMin(srtEndAry[i][2]);
        endDate.setHours(endHrsMin[0]);
        endDate.setMinutes(endHrsMin[1]);
        var workHours = getWorkerHours(params['worker_id'], cmpHrsData, cstHrsData, new Date(srtEndAry[i][0]));
        while (srtDate.getTime() < endDate.getTime()) {
            if (minDtObj.getTime() < srtDate.getTime() &&
                valiWorkerStEdTime(srtDate, workHours, params['durations'])) {
                var temp = getDateStr(srtDate, params['dateformat']);
                rtnObj.push([new Date(srtDate), temp]);
            }
            srtDate.setMinutes(srtDate.getMinutes() + intvl);
        }
    }
    return rtnObj;
}
//--- End of function to genetate time slots based on chronological order ---//

//--- Start of function to validate time slot based on all workers start and end time including custom hours of that day ---//
function valiWorkerStEdTime(stTime, workHours, durations) {
    var serviceStart = new Date(stTime);
    var serviceEnd = new Date(stTime);
    serviceEnd.setMinutes(serviceEnd.getMinutes() + durations[0]);
    for (var i = 0; i < durations.length; i++) {
        if (i !== 0) {
            serviceStart.setMinutes(serviceStart.getMinutes() + durations[i - 1]);
            serviceEnd.setMinutes(serviceEnd.getMinutes() + durations[i]);
        }
        var workerStart = new Date(stTime);
        var stHrsMin = getHrsMin(workHours[i][0]);
        workerStart.setHours(stHrsMin[0]);
        workerStart.setMinutes(stHrsMin[1]);
        var workerEnd = new Date(stTime);
        var endHrsMin = getHrsMin(workHours[i][1]);
        workerEnd.setHours(endHrsMin[0]);
        workerEnd.setMinutes(endHrsMin[1]);
        if (!(workerStart <= serviceStart && serviceEnd <= workerEnd)) {
            return false;
        }
    }
    return true;
}
//--- End of function to validate time slot based on all workers start and end time including custom hours of that day ---//

//--- Start of function to get Worker related data ---//
function getWorkerHours(workerIdAry, cmpHrsData, cstHrsData, srvDate) {
    var rtnObj = [];
    for (var i = 0; i < workerIdAry.length; i++) {
        switch (srvDate.getDay()) {
            case 0:
                rtnObj[i] = [cmpHrsData[i]['SundayStartTime__c'], cmpHrsData[i]['SundayEndTime__c']]
                break;
            case 1:
                rtnObj[i] = [cmpHrsData[i]['MondayStartTime__c'], cmpHrsData[i]['MondayEndTime__c']]
                break;
            case 2:
                rtnObj[i] = [cmpHrsData[i]['TuesdayStartTime__c'], cmpHrsData[i]['TuesdayEndTime__c']]
                break;
            case 3:
                rtnObj[i] = [cmpHrsData[i]['WednesdayStartTime__c'], cmpHrsData[i]['WednesdayEndTime__c']]
                break;
            case 4:
                rtnObj[i] = [cmpHrsData[i]['ThursdayStartTime__c'], cmpHrsData[i]['ThursdayEndTime__c']]
                break;
            case 5:
                rtnObj[i] = [cmpHrsData[i]['FridayStartTime__c'], cmpHrsData[i]['FridayEndTime__c']]
                break;
            case 6:
                rtnObj[i] = [cmpHrsData[i]['SaturdayStartTime__c'], cmpHrsData[i]['SaturdayEndTime__c']]
                break;
            default:
                break;
        }
        var customHrsAry = cstHrsData.filter(obj => obj['Company_Hours__c'] === cmpHrsData[i]['Id']);
        for (var j = 0; j < customHrsAry.length; j++) {
            tempDtObj = customHrsAry[j]['Date__c'].split('-');
            var cstDt = new Date(tempDtObj[0], (parseInt(tempDtObj[1], 10) - 1), tempDtObj[2]);
            if (cstDt.getTime() === srvDate.getTime()) {
                rtnObj[i] = [customHrsAry[j]['StartTime__c'], customHrsAry[j]['EndTime__c']];
            }
        }
    }
    return rtnObj;
}
//--- End of function to get Worker related data ---//

//--- Start of function to get hours and minutes from string ---//
function getHrsMin(timeStr) {
    var hrs = 0;
    var tempAry = timeStr.split(' ');
    var hrsMinAry = tempAry[0].split(':');
    hrs = parseInt(hrsMinAry[0], 10);
    if (tempAry[1] == 'AM' && hrs == 12) {
        hrs = 0;
    } else if (tempAry[1] == 'PM' && hrs != 12) {
        hrs += 12;
    }
    return [hrs, parseInt(hrsMinAry[1], 10)];
}
//--- End of function to get hours and minutes from string ---//

//--- Start of function to generate date time string based on the request ---//
function getDateStr(dtObj, dateformat) {
    var datStr = '';
    if (dateformat === 'MM/DD/YYYY hh:mm:ss a') {
        datStr = ('0' + (dtObj.getMonth() + 1)).slice(-2) + '/' +
            ('0' + dtObj.getDate()).slice(-2) + '/' +
            dtObj.getUTCFullYear() + ' ' + formatAMPM(dtObj);
    }
    return datStr;
}
//--- End of function to generate date time string based on the request ---//

//--- Start of function to generate time string based on the date object ---//
function formatAMPM(dtObj) {
    var hours = dtObj.getHours();
    var minutes = dtObj.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12;
    var strTime = ('0' + hours).slice(-2) + ':' + ('0' + minutes).slice(-2) + ' ' + ampm;
    return strTime;
}
//--- End of function to generate time string based on the date object ---//

//--- Start of function to generate string from array for IN query parameters ---//
function getInQryStr(arryObj) {
    var rtnStr = '';
    if (arryObj && arryObj.length > 0) {
        rtnStr += '(';
        for (var i = 0; i < arryObj.length; i++) {
            rtnStr += '\'' + arryObj[i] + '\',';
        }
        rtnStr = rtnStr.slice(0, -1);
        rtnStr += ')';
    }
    return rtnStr;
}
//--- End of function to generate string from array for IN query parameters ---//

//--- Start of function to check time slots are available or not? based on worker existing services ---//
function filterWrkSrv(srtEndAry, wrkSrvData, subDur, srvDur) {
    for (var i = 0; i < srtEndAry.length; i++) {
        for (var j = 0; j < wrkSrvData.length; j++) {
            if (srtEndAry[i]) {
                var newSrvStDt = new Date(srtEndAry[i][0]);
                newSrvStDt.setMinutes(newSrvStDt.getMinutes() + parseInt(subDur, 10));
                var newSrvEndDt = new Date(newSrvStDt);
                newSrvEndDt.setMinutes(newSrvEndDt.getMinutes() + parseInt(srvDur, 10));
                var tempDtTmAr = wrkSrvData[j]['Service_Date_Time__c'].split(' ');
                var tempDtAr = tempDtTmAr[0].split('-');
                var tempTmAr = tempDtTmAr[1].split(':');
                //--- First Duration ---//
                var srvStDt = new Date(tempDtAr[0], (parseInt(tempDtAr[1], 10) - 1), tempDtAr[2], tempTmAr[0], tempTmAr[1], tempTmAr[2]);
                var srvEndDt = new Date(srvStDt);
                srvEndDt.setMinutes(srvEndDt.getMinutes() + parseInt(wrkSrvData[j]['Duration_1__c'], 10));
                if (parseInt(wrkSrvData[j]['Duration_1__c'], 10) > 0 && (newSrvStDt.getTime() === srvStDt.getTime() ||
                        newSrvEndDt.getTime() === srvEndDt.getTime() ||
                        (newSrvStDt.getTime() > srvStDt.getTime() && newSrvStDt.getTime() < srvEndDt.getTime()) ||
                        (newSrvEndDt.getTime() > srvStDt.getTime() && newSrvEndDt.getTime() < srvEndDt.getTime()) ||
                        (newSrvStDt.getTime() < srvStDt.getTime() && newSrvEndDt.getTime() > srvEndDt.getTime()))) {
                    if (wrkSrvData[j]['dur1Available'] != 1) {
                        srtEndAry[i] = null;
                    } else if (srtEndAry[i]) {
                        if (srtEndAry[i][2]) {
                            srtEndAry[i][2] += 1;
                        } else {
                            srtEndAry[i][2] = 1;
                        }
                    }
                }
                //--- Second Duration ---//
                srvStDt = new Date(srvEndDt);
                srvEndDt.setMinutes(srvEndDt.getMinutes() + parseInt(wrkSrvData[j]['Duration_2__c'], 10));
                if (parseInt(wrkSrvData[j]['Duration_2__c'], 10) > 0 && (newSrvStDt.getTime() === srvStDt.getTime() ||
                        newSrvEndDt.getTime() === srvEndDt.getTime() ||
                        (newSrvStDt.getTime() > srvStDt.getTime() && newSrvStDt.getTime() < srvEndDt.getTime()) ||
                        (newSrvEndDt.getTime() > srvStDt.getTime() && newSrvEndDt.getTime() < srvEndDt.getTime()) ||
                        (newSrvStDt.getTime() < srvStDt.getTime() && newSrvEndDt.getTime() > srvEndDt.getTime()))) {
                    if (wrkSrvData[j]['dur2Available'] != 1) {
                        srtEndAry[i] = null;
                    } else if (srtEndAry[i]) {
                        if (srtEndAry[i][2]) {
                            srtEndAry[i][2] += 1;
                        } else {
                            srtEndAry[i][2] = 1;
                        }
                    }
                }
                //--- Third Duration ---//
                srvStDt = new Date(srvEndDt);
                srvEndDt.setMinutes(srvEndDt.getMinutes() + parseInt(wrkSrvData[j]['Duration_3__c'], 10));
                if (parseInt(wrkSrvData[j]['Duration_3__c'], 10) > 0 && (newSrvStDt.getTime() === srvStDt.getTime() ||
                        newSrvEndDt.getTime() === srvEndDt.getTime() ||
                        (newSrvStDt.getTime() > srvStDt.getTime() && newSrvStDt.getTime() < srvEndDt.getTime()) ||
                        (newSrvEndDt.getTime() > srvStDt.getTime() && newSrvEndDt.getTime() < srvEndDt.getTime()) ||
                        (newSrvStDt.getTime() < srvStDt.getTime() && newSrvEndDt.getTime() > srvEndDt.getTime()))) {
                    if (wrkSrvData[j]['dur3Available'] != 1) {
                        srtEndAry[i] = null;
                    } else if (srtEndAry[i]) {
                        if (srtEndAry[i][2]) {
                            srtEndAry[i][2] += 1;
                        } else {
                            srtEndAry[i][2] = 1;
                        }
                    }
                }
                //--- Fourth Duration ---//
                srvStDt = new Date(srvEndDt);
                srvEndDt.setMinutes(srvEndDt.getMinutes() + parseInt(wrkSrvData[j]['Buffer_After__c'], 10));
                if (parseInt(wrkSrvData[j]['Buffer_After__c'], 10) > 0 && (newSrvStDt.getTime() === srvStDt.getTime() ||
                        newSrvEndDt.getTime() === srvEndDt.getTime() ||
                        (newSrvStDt.getTime() > srvStDt.getTime() && newSrvStDt.getTime() < srvEndDt.getTime()) ||
                        (newSrvEndDt.getTime() > srvStDt.getTime() && newSrvEndDt.getTime() < srvEndDt.getTime()) ||
                        (newSrvStDt.getTime() < srvStDt.getTime() && newSrvEndDt.getTime() > srvEndDt.getTime()))) {
                    srtEndAry[i] = null;
                }
                //--- Adding ranks based on previous services start and end times ---//
                if ((newSrvStDt.getTime() === srvEndDt.getTime() ||
                        newSrvEndDt.getTime() === srvStDt.getTime()) && srtEndAry[i]) {
                    if (srtEndAry[i][2]) {
                        srtEndAry[i][2] += 1;
                    } else {
                        srtEndAry[i][2] = 1;
                    }
                }
            }
        }
    }
    srtEndAry = srtEndAry.filter(function(a) { return a !== null });
    return srtEndAry;
}
//--- End of function to check time slots are available or not? based on worker existing services ---//

//--- Start of function to delete date key from JSON array ---//
function deleteDateKey(jsonArry) {
    for (var i = 0; i < jsonArry.length; i++) {
        delete jsonArry[i]['date'];
    }
    return jsonArry;
}
//--- End of function to delete date key from JSON array ---//

//--- Start of function to generate ranks based on following rules ---//
/*
 *	Ranking Rules:
 *	If beginning of appt is beginning of worker’s hours - 1 pt
 *	If the end of the appt is at the end of worker’s hours - 1 pt
 *	If beginning of appt is touching the end of a previous appt / book out time - 1 pt
 *	If the end of the appt is touching the beginning of the next appt  / book out time - 1 pt
 *	If the appt is ‘meshable’ (fits inside 'available for other work' interval) with another appt - 1 pt
 */
function generateRanks(srtEndAry, inputParms, cmpHrsData, cstHrsData, wrkSrvData) {
    srtEndAry = srtEndAry.filter(function(a) { return a !== null });
    var tempDtObj = inputParms['date'].split('-');
    var wrkDayStEnd = [];
    for (var i = 0; i < inputParms['worker_id'].length; i++) {
        var selDt = new Date(tempDtObj[0], (parseInt(tempDtObj[1], 10) - 1), tempDtObj[2]);
        //--- Generates default start and end times of worker company hours ---//

        var defStEndTms = getDefStEdAry(cmpHrsData.filter(function(a) { return a['Id'] === inputParms['worker_id'][i] })[0], selDt);
        //--- Worker custom hours ---//
        var wrkCstHrs = cstHrsData.filter(function(a) { return a['Company_Hours__c'] === cmpHrsData[i]['Id'] });
        //--- Start of adding custom hours (Considering custom hours and all day off) ---//
        for (var j = 0; j < defStEndTms.length; j++) {
            if (wrkCstHrs && wrkCstHrs.length > 0) {
                for (var k = 0; k < wrkCstHrs.length; k++) {
                    tempDtObj = wrkCstHrs[k]['Date__c'].split('-');
                    var cstDt = new Date(tempDtObj[0], (parseInt(tempDtObj[1], 10) - 1), tempDtObj[2]);
                    if (defStEndTms[j] && cstDt.getTime() === defStEndTms[j][0].getTime()) {
                        if (wrkCstHrs[k]['All_Day_Off__c'] == 1) {
                            defStEndTms[j] = null;
                        } else {
                            defStEndTms[j] = [cstDt, wrkCstHrs[k]['StartTime__c'], wrkCstHrs[k]['EndTime__c']];
                        }
                    }
                }
            }
            selDt.setDate(selDt.getDate() + 1);
        }
        //--- End of adding custom hours (Considering custom hours and all day off) ---//
        //--- Removing null values from output array ---//
        defStEndTms = defStEndTms.filter(function(a) { return a !== null });
        //--- Start of creating start and end times of a day date objects ---//
        for (var j = 0; j < defStEndTms.length; j++) {
            var wrkStDtTime = new Date(defStEndTms[j][0]);
            var tempWrkStHrsMinAry = getHrsMin(defStEndTms[j][1]);
            wrkStDtTime.setHours(wrkStDtTime.getHours() + tempWrkStHrsMinAry[0]);
            wrkStDtTime.setMinutes(wrkStDtTime.getMinutes() + tempWrkStHrsMinAry[1]);
            var wrkEndDtTime = new Date(defStEndTms[j][0]);
            var tempWrkEndHrsMinAry = getHrsMin(defStEndTms[j][2]);
            wrkEndDtTime.setHours(wrkEndDtTime.getHours() + tempWrkEndHrsMinAry[0]);
            wrkEndDtTime.setMinutes(wrkEndDtTime.getMinutes() + tempWrkEndHrsMinAry[1]);
            defStEndTms[j] = { 'start': wrkStDtTime, 'end': wrkEndDtTime };
        }
        //--- End of creating start and end times of a day date objects ---//
        wrkDayStEnd.push({ 'id': inputParms['worker_id'][i], 'dayStEnd': defStEndTms });
    }
    srtEndAry = filterByDur(srtEndAry, wrkDayStEnd, inputParms['durations']);
    for (var i = 0; i < srtEndAry.length; i++) {
        var stDur = 0;
        var endDur = parseInt(inputParms['durations'][0], 10);
        for (var j = 0; j < inputParms['worker_id'].length; j++) {
            var tempWrkDayStEnd = wrkDayStEnd.filter(function(a) { return a['id'] == inputParms['worker_id'][j] })[0]['dayStEnd'];
            for (var k = 0; k < tempWrkDayStEnd.length; k++) {
                // If beginning of appt is beginning of worker’s hours
                var temStrDateObj = new Date(srtEndAry[i]['date']);
                temStrDateObj.setMinutes(temStrDateObj.getMinutes() + stDur);
                if (temStrDateObj.getTime() === tempWrkDayStEnd[k]['start'].getTime()) {
                    srtEndAry[i]['rank'] += 1;
                }
                // If the end of the appt is at the end of worker’s hours
                var temEndDateObj = new Date(srtEndAry[i]['date']);
                temEndDateObj.setMinutes(temEndDateObj.getMinutes() + endDur);
                if (temEndDateObj.getTime() === tempWrkDayStEnd[k]['end'].getTime()) {
                    srtEndAry[i]['rank'] += 1;
                }
            }
            stDur += parseInt(inputParms['durations'][j], 10);
            if (inputParms['durations'][j + 1]) {
                endDur += parseInt(inputParms['durations'][j + 1], 10);
            }
        }
    }
    return srtEndAry;
}
//--- End of function to generate ranks ---//

//--- Start of function to filter time slots based on service end time and end time of worker day ---//
function filterByDur(srtEndAry, wrkDayStEnd, durations) {
    for (var i = 0; i < srtEndAry.length; i++) {
        var tempDate = new Date(srtEndAry[i]['date']);
        for (var j = 0; j < wrkDayStEnd.length; j++) {
            if (srtEndAry[i]) {
                tempDate.setMinutes(tempDate.getMinutes() + parseInt(durations[j], 10));
                var tempEndDate = wrkDayStEnd[j]['dayStEnd'].filter(function(a) { return a['start'].getDate() === tempDate.getDate() });
                if (tempEndDate && tempEndDate.length > 0) {
                    var endDate = tempEndDate[0]['end'];
                    if (tempDate.getTime() > endDate.getTime()) {
                        srtEndAry[i] = null;
                    }
                }
            }
        }
    }
    srtEndAry = srtEndAry.filter(function(a) { return a !== null });
    return srtEndAry;
}
//--- End of function to filter time slots based on service end time and end time of worker day ---//

//--- Start of function to sort JSON Array ---//
function sortJSONAry(JSONAry, JSONAttrb, order) {
    var leng = JSONAry.length;
    for (var i = 0; i < leng; i++) {
        for (var j = i + 1; j < leng; j++) {
            if ((order === 'asc' && JSONAry[i][JSONAttrb] > JSONAry[j][JSONAttrb]) ||
                (order === 'desc' && JSONAry[i][JSONAttrb] < JSONAry[j][JSONAttrb])) {
                var tempObj = JSONAry[i];
                JSONAry[i] = JSONAry[j];
                JSONAry[j] = tempObj;
            }
        }
    }
    return JSONAry;
}
//--- End of function to sort JSON Array ---//

//--- Start of function to filter array based on resources ---//
function filterResources(srtEndAry, resourcesData, subDur, srvDur, rescs, resFil) {
    for (var i = 0; i < srtEndAry.length; i++) {
        if (srtEndAry[i]) {
            var newSrvStDt = new Date(srtEndAry[i][0]);
            newSrvStDt.setMinutes(newSrvStDt.getMinutes() + parseInt(subDur, 10));
            var newSrvEndDt = new Date(newSrvStDt);
            newSrvEndDt.setMinutes(newSrvEndDt.getMinutes() + parseInt(srvDur, 10));
            for (var j = 0; j < rescs.length; j++) {
                var resAry = resourcesData.filter(function(obj) {
                    return obj['Name'] === rescs[j];
                });
                if (resAry && resAry.length > 0) {
                    var numAvb = resAry[0]['Number_Available__c'] - 1;
                    for (k = 0; k < resAry.length; k++) {
                        var tempDtTmAr = resAry[k]['Service_Date_Time__c'].split(' ');
                        var tempDtAr = tempDtTmAr[0].split('-');
                        var tempTmAr = tempDtTmAr[1].split(':');
                        var srvStDt = new Date(tempDtAr[0], (parseInt(tempDtAr[1], 10) - 1), tempDtAr[2], tempTmAr[0], tempTmAr[1], tempTmAr[2]);
                        var srvEndDt = new Date(srvStDt);
                        srvEndDt.setMinutes(srvEndDt.getMinutes() + parseInt(resAry[k]['Duration__c'], 10));
                        if (newSrvStDt.getTime() === srvStDt.getTime() ||
                            newSrvEndDt.getTime() === srvEndDt.getTime() ||
                            (newSrvStDt.getTime() > srvStDt.getTime() && newSrvStDt.getTime() < srvEndDt.getTime()) ||
                            (newSrvEndDt.getTime() > srvStDt.getTime() && newSrvEndDt.getTime() < srvEndDt.getTime()) ||
                            (newSrvStDt.getTime() < srvStDt.getTime() && newSrvEndDt.getTime() > srvEndDt.getTime())) {
                            if (numAvb !== 0) {
                                numAvb -= 1;
                            } else {
                                srtEndAry[i] = null;
                                break;
                            }
                        }
                    }
                }
            }
            if (resFil === 'All') {

            } else if (resFil === 'Any') {

            }
        }
    }
    srtEndAry = srtEndAry.filter(function(a) { return a !== null });
    return srtEndAry;
}
//--- End of function to filter array based on resources ---//

function lcm_more_than_two_numbers(input_array, apptPrfData) {
    input_array = input_array.filter(function(obj) { return obj && obj != 0 });
    if (input_array.length > 0) {
        if (toString.call(input_array) !== "[object Array]")
            return false;
        var r1 = 0,
            r2 = 0;
        var l = input_array.length;
        for (i = 0; i < l; i++) {
            r1 = input_array[i] % input_array[i + 1];
            if (r1 === 0) {
                input_array[i + 1] = (input_array[i] * input_array[i + 1]) / input_array[i + 1];
            } else {
                r2 = input_array[i + 1] % r1;
                if (r2 === 0) {
                    input_array[i + 1] = (input_array[i] * input_array[i + 1]) / r1;
                } else {
                    input_array[i + 1] = (input_array[i] * input_array[i + 1]) / r2;
                }
            }
        }
        return input_array[l - 1];
    } else {
        return apptPrfData.bookingIntervalMinutes;
    }
}
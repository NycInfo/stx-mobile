var uniqid = require('uniqid');
var fs = require('fs');

var dateFns = require('../common/dateFunctions');
var CommonSRVC = require('../common/tokenService');
var mail = require('../common/sendMail');
var sms = require('../common/sendSMS');
var execute = require('../common/dbConnection');
var logger = require('../common/logger');
var async = require('async');
var sendResponse = require('../common/response').sendResponse;
var sendResponseWithMsg = require('../common/response').sendResponseWithMsg;
var config = require('../common/config');
var multer = require('multer');

var AWS = require('aws-sdk');
AWS.config.update({
    accessKeyId: config.AWSAccessKey,
    secretAccessKey: config.AWSSecretKey,
    region: config.S3Region
});
var s3 = new AWS.S3();

module.exports.controller = function(app) {
    app.get('/api/appointment/getclientnames/:id?', function(req, res) {
        var dbName = req.headers['db'];
        query = `SELECT
        DISTINCT Id,
            FirstName,
            LastName,
            Sms_Consent__c,
            IFNULL(Client_Pic__c,"") AS imagepath,
                IFNULL(MobilePhone, "") AS MobilePhone,
                    IFNULL(Phone, "") AS Phone,
                        Email AS Email,
                            IFNULL(Mobile_Carrier__c, "") AS Mobile_Carrier__c,
                            IFNULL(REPLACE(REPLACE(REPLACE(Phone, "(", ""), ")", ""), "-", ""),'') AS modiPhone,                                        
                            REPLACE(REPLACE(REPLACE(MobilePhone, "(", ""), ")", ""), "-", "") AS modfMobilePhone,
                                        IsDeleted, IFNULL(Booking_Restriction_Type__c,'') as Booking_Restriction_Type__c,
                                        IF(BR_Reason_No_Email__c = 1, 'No Email', '') as noEmail,
                                        IF(BR_Reason_Account_Charge_Balance__c = 1, 'Account Charge Balance', '') as acctCharge,
                                        Current_Balance__c,
                                        IF(BR_Reason_Deposit_Required__c =1, 'Deposit Required', '' ) as depReq,
                                        IF(BR_Reason_No_Show__c = 1, 'Persistant No Show', '') as noShow,
                                        IFNULL(BR_Reason_Other_Note__c ,'') as note,
                                        IF(BR_Reason_Other__c = 1, 'Other', '') as other
        FROM
        Contact__c
        WHERE IsDeleted = 0 AND CONCAT(FirstName, ' ', LastName) != 'NO CLIENT'
        AND CONCAT(FirstName, ' ', LastName) != 'CLASS CLIENT'
        HAVING
        CONCAT(FirstName, " ", LastName) LIKE "%` + req.params.id + `%" OR
        MobilePhone LIKE "%` + req.params.id + `%" OR
        Phone LIKE "%` + req.params.id + `%" OR
        Email LIKE "%` + req.params.id + `%" OR
        modiPhone LIKE "%` + req.params.id + `%" OR
        modfMobilePhone LIKE "%` + req.params.id + `%"
        ORDER BY
        Active__c desc,
            FirstName,
            LastName,
            Phone
        LIMIT 100`;
        execute.query(dbName, query, function(error, results) {
            if (error) {
                logger.error('Error in Client Search: ', error);
                sendResponse(res, false, '000');
            } else {
                for (var i = 0; i < results.length; i++) {
                    results[i].image = config.awss3imagepath + results[i].imagepath;
                }
                sendResponse(res, true, results);
            }
        });
    });

    app.put('/api/client/quick/:id', function(req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpName = req.headers['cname'];
        var dateTime = req.headers['dt'];
        var birthDate = '';
        if (req.body.isNewClient === true) {
            if (req.body.birthMonth && req.body.birthDay && req.body.birthYear) {
                birthDate = req.body.birthMonth + '-' + req.body.birthDay + '-' + req.body.birthYear;
            } else {
                birthDate = '';
            }

            var quickAddClientData = {
                Id: uniqid(),
                OwnerId: loginId,
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                FirstName: req.body.firstname,
                // MiddleName: req.body.middlename,
                Gender__c: req.body.gender,
                LastName: req.body.lastname,
                Phone: req.body.primaryPhone,
                Email: req.body.email,
                Secondary_Email__c: req.body.secondaryEmail,
                MobilePhone: req.body.mobilePhone,
                BirthYearNumber__c: req.body.birthYear,
                BirthDateNumber__c: req.body.birthDay,
                BirthMonthNumber__c: req.body.birthMonth,
                Birthdate: birthDate,
                MailingStreet: req.body.clientInfoMailingStreet,
                MailingCity: req.body.clientInfoMailingCity,
                MailingState: req.body.clientInfoMailingState,
                MailingPostalCode: req.body.clientInfoPostalCode,
                MailingCountry: req.body.clientInfoMailingCountry,
                Active_Rewards__c: 1,
                Notification_Primary_Email__c: req.body.notificationPrimaryEmail,
                Reminder_Primary_Email__c: req.body.reminderPrimaryEmail,
                Marketing_Primary_Email__c: req.body.marketingPrimaryEmail,
                No_Email__c: req.body.clientInfoNoEmail,
                Active__c: 1,
                Allow_Online_Booking__c: 1,
                Sms_Consent__c: req.body.Sms_Consent__c
            };
            if (req.body.Sms_Consent__c === 1) {
                quickAddClientData.Marketing_Mobile_Phone__c = 1;
                quickAddClientData.Notification_Mobile_Phone__c = 1;
                quickAddClientData.Reminder_Mobile_Phone__c = 1;
            }
            if (req.body.clientInfoNoEmail) {
                quickAddClientData.Marketing_Primary_Email__c = 0;
                quickAddClientData.Notification_Primary_Email__c = 0;
                quickAddClientData.Reminder_Primary_Email__c = 0;
                quickAddClientData.Marketing_Secondary_Email__c = 0;
                quickAddClientData.Notification_Secondary_Email__c = 0;
                quickAddClientData.Reminder_Secondary_Email__c = 0;
            }
            if (req.body.type === 'checkout') {
                clientQuickAdd(quickAddClientData, dbName, loginId, dateTime, cmpName, function(status, data) {
                    if (!status) {
                        sendResponse(res, status, data);
                    } else {
                        quickAddClientData.Client__c = data.clientId;
                        createAppt(quickAddClientData, dbName, loginId, dateTime, function(status, data) {
                            sendResponse(res, status, data);
                        });
                    }
                });
            } else {
                clientQuickAdd(quickAddClientData, dbName, loginId, dateTime, cmpName, function(status, data) {
                    sendResponse(res, status, data);
                });
            }
        } else {
            var sqlQuery = 'UPDATE Contact__c' +
                ' SET Email = "' + req.body.email +
                '", MobilePhone = "' + req.body.mobilePhone +
                '", Phone = "' + req.body.primaryPhone +
                '", LastModifiedDate = "' + dateTime +
                '", LastModifiedById = "' + loginId +
                '" WHERE Id = "' + req.params.id + '"';
            execute.query(dbName, sqlQuery, function(error, results) {
                if (error) {
                    logger.error('Error in edit Client: ', error);
                    sendResponse(res, false, '000');
                } else {
                    sendResponse(res, true, results);
                }
            });
        }
    });

    app.put('/api/client/:id', function(req, res) {
        var clientId = '';
        if (req.params['id'] && req.params['id'] !== 'undefined') {
            clientId = req.params['id'];
        } else {
            clientId = uniqid();
        }
        uploadLogo(req, res, function(err) {
            var uplLoc = config.uploadsPath + req.headers['cid'] +
                '/' + config.clientPictureLocation + '/' + clientId;
            var options = {
                Bucket: config.S3Name,
                Key: uplLoc
            };
            if (req.file) {
                s3.deleteObject(options, function(err) {
                    options.Body = fs.readFileSync(uplLoc);
                    s3.putObject(options, function(err) {
                        if (err) {
                            logger.info('unable to upload client img to s3', uplLoc)
                        } else {
                            fs.unlink(uplLoc, function() {})
                        }
                    });
                });
            }
            var image = '';
            var dbName = req.headers['db'];
            var loginId = req.headers['id'];
            var cmpyId = req.headers['cid'];
            var cmpName = req.headers['cname'];
            var dateTime = req.headers['dt'];
            var updateObj = req.body;
            var ticketServiceObj = updateObj.ApptServiceData;
            var clientRewardsUpdateData = updateObj.clientRewardsData;
            var birthDate = '';
            if (updateObj.BirthMonthNumber && updateObj.BirthDateNumber && updateObj.BirthYearNumber) {
                if (updateObj.BirthMonthNumber == "0" || updateObj.BirthDateNumber == "0" || updateObj.BirthYearNumber == "0") {
                    birthDate = '';
                } else {
                    birthDate = updateObj.BirthMonthNumber + '-' + updateObj.BirthDateNumber + '-' + updateObj.BirthYearNumber;
                }
            } else {
                birthDate = '';
            }
            if (updateObj.marketingOptOut === 1) {
                updateObj.marketingMobilePhone = 0;
                updateObj.marketingPrimaryEmail = 0;
                updateObj.marketingSecondaryEmail = 0;
            }
            if (updateObj.notificationOptOut === 1) {
                updateObj.notificationMobilePhone = 0;
                updateObj.notificationPrimaryEmail = 0;
                updateObj.notificationSecondaryEmail = 0;
            }
            if (updateObj.reminderOptOut === 1) {
                updateObj.reminderMobilePhone = 0;
                updateObj.reminderPrimaryEmail = 0;
                updateObj.reminderSecondaryEmail = 0;
            }
            updateObj.Active__c = 1;
            var editQuery = 'UPDATE Contact__c' +
                ' SET Gender__c = "' + updateObj.Gender__c
                // client Info
                +
                '", FirstName = "' + updateObj.FirstName +
                '", LastName = "' + updateObj.LastName +
                '", Phone = "' + updateObj.Phone +
                '", MobilePhone = "' + updateObj.MobilePhone +
                '", Email = "' + updateObj.Email +
                '", Active__c = "' + updateObj.Active__c +
                '", Birthdate = "' + birthDate +
                '", BirthYearNumber__c = "' + updateObj.BirthYearNumber +
                '", BirthDateNumber__c = "' + updateObj.BirthDateNumber +
                '", BirthMonthNumber__c = "' + updateObj.BirthMonthNumber +
                '", Notes__c = "' + updateObj.Notes__c +
                '", Title = "' + updateObj.designation +
                '", Client_Flag__c = "' + updateObj.Client_Flag__c
            if (image !== '') {
                editQuery += '", Client_Pic__c = "' + image
            }
            editQuery += '", BR_Reason_Other_Note__c = "' + updateObj.BR_Reason_Other_Note__c +
                '", Booking_Restriction_Type__c = "' + updateObj.Booking_Restriction_Type__c +
                '", Sms_Consent__c = "' + updateObj.Sms_Consent__c
            if (updateObj.Membership_ID__c && updateObj.Membership_ID__c !== '') {
                editQuery += '", Membership_ID__c = "' + updateObj.Membership_ID__c + '", ';
            } else {
                editQuery += '", Membership_ID__c = null, ';
            }
            editQuery += 'LastModifiedDate = "' + dateTime +
                '", LastModifiedById = "' + loginId +
                '" WHERE Id = "' + req.params.id + '"';
            execute.query(dbName, editQuery, function(err, results) {
                if (err !== null) {
                    if (err.sqlMessage.indexOf('Membership_ID__c') > 0) {
                        sendResponse(res, false, '009');
                    } else if (err.sqlMessage.indexOf('FirstNameLastNameEmail') > 0) {
                        sendResponse(res, false, '004');
                    } else {
                        logger.error('Error in edit Client dao - quickAddClientData:', err);
                        sendResponse(res, false, '000');
                    }
                } else {
                    sendResponse(res, true, results);
                }
            });
        });
    });
    app.get('/api/client/:id', function(req, res) {
        var dbName = req.headers['db'];
        var dateTime = req.headers['dt'];
        var query = `SELECT 
                        Id,FirstName,Active__c, LastName,Email,IFNULL(MobilePhone, "") AS MobilePhone,IFNULL(Phone, "") AS Phone, 
                        Sms_Consent__c,IFNULL(IF(Gender__c='','No Gender',Gender__c), 'No Gender') Gender__c,IF(Birthdate ='null-null-null', '',IF(Birthdate ='undefined-undefined-undefined', '', IF(IFNULL(Birthdate,'NO DOB')='','No DOB',Birthdate))) Birthdate,
                        IFNULL(Membership_ID__c, '') Membership_ID__c,
                        IFNULL(BirthDateNumber__c, 0) BirthDateNumber,IFNULL(BirthMonthNumber__c, 0) BirthMonthNumber,
                        IFNULL(BirthYearNumber__c, 0) BirthYearNumber,
                        IFNULL(IF(Booking_Restriction_Type__c='','None',Booking_Restriction_Type__c), 'None') Booking_Restriction_Type__c,IFNULL(IF(Title='','No Occupation',Title), 'No Occupation') designation,
                        IFNULL(IF(Client_Flag__c='','None',Client_Flag__c), 'None') Client_Flag__c, IFNULL(Notes__c, '') Notes__c,
                        IFNULL(BR_Reason_Other_Note__c, '') BR_Reason_Other_Note__c, IFNULL(Client_Pic__c, '') Client_Pic__c
                    FROM Contact__c c 
                    WHERE 
                        c.Id = '` + req.params.id + `' and c.isDeleted =0`
        var lastvisitsql = 'SELECT ts.Service_Date_Time__c lastAppointment, s.Name FROM Ticket_Service__c as ts JOIN Service__c as s on s.Id = ts.Service__c ' +
            ' WHERE Client__c="' + req.params.id + '" and Service_Date_Time__c <= "' + dateTime + '" ORDER BY Service_Date_Time__c desc LIMIT 1';
        var nextvisitsql = 'SELECT ts.Service_Date_Time__c nextAppointment, s.Name FROM Ticket_Service__c as ts JOIN Service__c as s on s.Id = ts.Service__c ' +
            ' WHERE Client__c="' + req.params.id + '" and Status__c != "Canceled" and Service_Date_Time__c >= "' + dateTime + '" ORDER BY Service_Date_Time__c asc LIMIT 1';
        execute.query(dbName, query, function(error, results) {
            if (error) {
                logger.error('Error in getting getClientSearch: ', error);
                sendResponse(res, false, '000');
            } else {
                execute.query(dbName, lastvisitsql, function(error1, results1) {
                    if (error1) {
                        logger.error('Error in getting getClientSearch: ', error1);
                        sendResponse(res, false, '000');
                    } else {
                        execute.query(dbName, nextvisitsql, function(error2, results2) {
                            if (error2) {
                                logger.error('Error in getting getClientSearch: ', error2);
                                sendResponse(res, false, '000');
                            } else {
                                if (results1[0]) {
                                    results[0]['lastAppointment'] = results1[0]['lastAppointment'];
                                } else {
                                    results[0]['lastAppointment'] = 'Not Available';
                                }
                                if (results2[0]) {
                                    results[0]['nextAppointment'] = results2[0]['nextAppointment'];
                                } else {
                                    results[0]['nextAppointment'] = 'Not Available';
                                }
                                sendResponse(res, true, results);
                            }
                        });
                    }
                });
            }
        });
    });
    app.get('/api/client/productlog/:id', function(req, res) {
        var dbName = req.headers['db'];
        var Id = req.params.id;
        var sqlQuery = "select tp.id, at.Client__c, at.Name as apptName, at.Appt_Date_Time__c," +
            " at.Status__c,  CAST(tp.Price__c AS CHAR) Price__c, tp.Promotion__c, p.Name as promotionName,CAST( tp.Net_Price__c AS CHAR) Net_Price__c," +
            " tp.Qty_Sold__c, tp.Product__c, tp.Appt_Ticket__c, pd.Id, pd.Name as productName, pd.Size__c, pd.Unit_of_Measure__c, pd.Product_Line__c," +
            " pl.Name as productLineName, tp.Worker__c, u.Username , CONCAT(u.FirstName, ' ', u.LastName)as workername from Ticket_Product__c as tp" +
            " left JOIN Appt_Ticket__c as at on at.Id = tp.Appt_Ticket__c" +
            " left JOIN Promotion__c as p on p.Id = tp.Promotion__c" +
            " left JOIN Product__c as pd on pd.Id = tp.Product__c" +
            " left JOIN User__c as u on u.Id = tp.Worker__c" +
            " left JOIN Product_Line__c as pl on pl.Id = pd.Product_Line__c" +
            " where at.Client__c = '" + Id + "' and tp.IsDeleted = 0 order by at.Appt_Date_Time__c desc limit 100";
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in getting getProductLog:', err);
                sendResponse(res, false, '000');
            } else {
                if (result.length > 0) {
                    sendResponseWithMsg(res, true, result, "");
                } else {
                    sendResponseWithMsg(res, true, result, "Logs are not available");
                }

            }
        });
    });
    app.get('/api/client/packages/:id', function(req, res) {
        var dbName = req.headers['db'];
        query = 'SELECT Cp.Package_Details__c, Apt.Name as TicketName, P.Name FROM `Client_Package__c` as Cp ' +
            'LEFT JOIN Package__c as P on P.Id = Cp.Package__c ' +
            'LEFT JOIN Appt_Ticket__c as Apt on Cp.Ticket__c = Apt.Id ' +
            'WHERE Cp.Client__c= "' + req.params.id + '" and Cp.isDeleted =0'
        execute.query(dbName, query, function(error, results) {
            var serviceIds = "'";
            for (i = 0; i < results.length; i++) {
                for (let k = 0; k < JSON.parse(results[i].Package_Details__c).length; k++) {
                    serviceIds += JSON.parse(results[i].Package_Details__c)[k].serviceId + "','";
                }
            }
            if (error) {
                logger.error('Error in getting getClientPackages: ', error);
                sendResponse(res, false, '000');
            } else {
                if (results.length > 0 && serviceIds.slice(0, serviceIds.length - 2)) {
                    query1 = 'SELECT Id, Name as ServiceName FROM Service__c WHERE 	IsDeleted = 0 AND Id IN (' + serviceIds.slice(0, serviceIds.length - 2) + ');'
                    execute.query(dbName, query1, function(error1, results1) {
                        if (error1) {
                            logger.error('Error in getting getClientServices: ', error1);
                            sendResponse(res, false, '000');
                        } else {
                            sendResponse(res, true, { 'ClientPackageData': results, 'ServiceData': results1 });
                        }
                    });
                } else {
                    sendResponse(res, true, { 'ClientPackageData': results, 'ServiceData': [] });
                }
            }
        });
    });
    app.get('/api/client/servicelog/:id', function(req, res) {
        var dbName = req.headers['db'];
        var Id = req.params.id
        var sqlQuery = `select a.Id apptId, ts.id, IFNULL(ts.Booked_Package__c,"") Booked_Package__c,
                            CONCAT(c.FirstName, " ", c.LastName) clientName, a.Client__c, a.Name apptNumber, 
                            a.Status__c, ts.Service_Date_Time__c, s.Name serviceName, 
                            CAST(ts.Price__c AS CHAR) Price__c, IFNULL(ts.Promotion__c, "") Promotion__c, 
                            IFNULL(p.Name,"") promotion, ts.Status__c, ts.Notes__c, 
                            CAST(ts.Net_Price__c AS CHAR) Net_Price__c , ts.Worker__c, 
                            CONCAT(u.FirstName," ", u.LastName) workerName, 
                            IFNULL(ts.Client_Package__c,"") Client_Package__c 
                        from Ticket_Service__c as ts 
                        LEFT JOIN Appt_Ticket__c as a on a.Id = ts.Appt_Ticket__c 
                        LEFT JOIN Service__c as s on s.Id = ts.Service__c 
                        LEFT JOIN Contact__c as c on c.Id = a.Client__c 
                        LEFT JOIN User__c as u on u.Id = ts.Worker__c 
                        LEFT JOIN Promotion__c as p on p.Id = ts.Promotion__c 
                            where a.Client__c ='` + Id + `' and a.isTicket__c = 1 
                            and a.Status__c != "Canceled"  
                            AND ts.IsDeleted = 0 order by ts.Service_Date_Time__c desc limit 200`;
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in getting getServiceLog:', err);
                sendResponse(res, false, '000');
            } else {
                if (result.length > 0) {
                    sendResponseWithMsg(res, true, result, "");
                } else {
                    sendResponseWithMsg(res, true, result, "Logs are not available");
                }

            }
        });
    });
    app.get('/api/client/rewards/:id', function(req, res) {
        var dbName = req.headers['db'];
        var query = 'select cr.Id, cr.Name, c.Id as clientId, sum(cr.Points_Balance__c) as Points_Balance__c, cr.Reward__c, r.Name, r.Active__c from ' +
            ' Client_Reward__c cr LEFT JOIN Contact__c c on c.Id = cr.Client__c LEFT JOIN Reward__c r on r.Id = cr.Reward__c ' +
            ' where cr.Client__c =  "' + req.params.id + '" and cr.isDeleted =0 GROUP BY r.Id'
        execute.query(dbName, query, function(error, results) {
            if (error) {
                logger.error('Error in getting getClientSearch: ', error);
                sendResponse(res, false, '000');
            } else {
                sendResponse(res, true, results);
            }
        });
    });
    app.get('/api/client/quick/get', function(req, res) {
        var createJson = {
            'firstname': '',
            'lastname': '',
            'primaryPhone': '',
            'mobilePhone': '',
            'email': '',
            'isNewClient': true,
            'clientInfoMailingCountry': '',
            'marketingPrimaryEmail': 1,
            'Sms_Consent__c': 0
        }
        sendResponse(res, true, createJson);
    });
    app.get('/api/client/memberships/:id', function(req, res) {
        var dbName = req.headers['db'];
        query = 'SELECT IFNULL(Cm.Next_Bill_Date__c,"") Next_Bill_Date__c,IFNULL(Cm.Membership_Price__c, "") Membership_Price__c, M.Name FROM Client_Membership__c as Cm ' +
            ' JOIN Membership__c as M on Cm.Membership__c = M.Id WHERE Cm.Client__c= "' + req.params.id + '" and Cm.isDeleted =0'
        execute.query(dbName, query, function(error, results) {
            if (error) {
                logger.error('Error in getting getClientSearch: ', error);
                sendResponse(res, false, '000');
            } else {
                sendResponse(res, true, results);
            }
        });
    });
    app.get('/api/client/accounts/:id', function(req, res) {
        var dbName = req.headers['db'];
        var Id = req.params.id
        var currentBalance = 0;
        var accountList = [];
        var sqlQuery = 'SELECT Tp.*, Pt.Name as debitType, ' +
            ' Apt.Appt_Date_Time__c as dateTime, Apt.Name as TicketName FROM `Ticket_Payment__c` as Tp ' +
            ' LEFT JOIN Appt_Ticket__c as Apt on Apt.Id = Tp.Appt_Ticket__c' +
            ' LEFT JOIN Payment_Types__c as Pt ON Pt.Id = Tp.Payment_Type__c ' +
            ' WHERE Apt.Client__c = "' + Id + '" and Apt.isDeleted =0 AND Pt.Name = "Account Charge" GROUP BY Tp.Id  ORDER BY Apt.CreatedDate Desc';
        var crdSql = `select ot.id, ot.Ticket__c as Appt_Ticket__c, at.Client__c
                , at.Name as TicketName, at.Appt_Date_Time__c as dateTime, at.Status__c, 
				(ot.Amount__c * -1) Amount_Paid__c, ot.Transaction_Type__c creditType
				from Ticket_Other__c ot
                LEFT JOIN Appt_Ticket__c as at on at.Id=ot.Ticket__c
				where at.Client__c ='` + Id + `'
				  and at.Status__c ='Complete'
                  and (ot.Transaction_Type__c = 'Received on Account' OR 
                  ot.Transaction_Type__c = 'Deposit' OR 
                  ot.Transaction_Type__c = 'Pre Payment') 
                  and ot.isDeleted = 0
                order by at.CreatedDate DESC`;
        var clientSql = 'SELECT c.Starting_Balance__c, c.Current_Balance__c currentBalance, at.Name TicketName, at.Appt_Date_Time__c dateTime FROM Contact__c c ' +
            ' LEFT JOIN Appt_Ticket__c as at on at.Client__c = c.Id' +
            ' WHERE c.Id="' + Id + '" GROUP by c.id ORDER by at.CreatedDate desc';
        execute.query(dbName, sqlQuery + ';' + crdSql + ';' + clientSql, '', function(err, result) {
            accountList = accountList.concat(result[0], result[1]);
            if (result[2][0]) {
                currentBalance = (result[2][0].Starting_Balance__c == null ? 0 : result[2][0].Starting_Balance__c);
            }
            for (var i = accountList.length - 1; i >= 0; i--) {
                currentBalance += parseFloat(accountList[i].Amount_Paid__c);
                accountList[i]['currentBalance'] = currentBalance;
            }
            if (err) {
                logger.error('Error in getting getClientAccounts:', err);
                sendResponse(res, false, '000');
            } else {
                sendResponse(res, true, accountList);
            }
        });
    });
    app.get('/api/setup/clientpreferences/clientflags', function(req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT * FROM Preference__c WHERE Name = "Client Flags"';
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in getting getClientAccounts:', err);
                sendResponse(res, false, '000');
            } else if (result && result.length > 0) {
                var JSON__c_str = JSON.parse(result[0].JSON__c);
                JSON__c_str = JSON__c_str.filter(obj => obj.active === true);
                sendResponse(res, true, JSON__c_str);
            } else {
                sendResponse(res, true, []);
            }
        });
    });
    app.get('/api/setup/clientpreferences/occupations', function(req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT * FROM Preference__c WHERE Name = "Client Occupations"';
        execute.query(dbName, sqlQuery, '', function(err, result) {
            if (err) {
                logger.error('Error in getting getClientAccounts:', err);
                sendResponse(res, false, '000');
            } else if (result && result.length > 0) {
                var JSON__c_str = JSON.parse(result[0].JSON__c);
                sendResponse(res, true, JSON__c_str);
            } else {
                sendResponse(res, true, []);
            }
        });
    });
    app.put('/api/clientProfilePic/:id/:status', function(req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpyId = req.headers['cid'];
        var dateTime = req.headers['dt'];
        uploadLogo(req, res, function(err) {
            var uplLoc = config.uploadsPath + req.headers['cid'] +
                '/' + config.clientPictureLocation + '/' + req.params['id'];
            var options = {
                Bucket: config.S3Name,
                Key: uplLoc
            };
            if (req.file) {
                s3.deleteObject(options, function(err) {
                    if (req.params.status === 'upload') {
                        options.Body = fs.readFileSync(uplLoc);
                        s3.putObject(options, function(err) {
                            if (err) {
                                logger.info('unable to upload client img to s3', uplLoc)
                            } else {
                                fs.unlink(uplLoc, function() {})
                            }
                        });
                    }
                });
            }
            if (req.file) {
                filepath = config.uploadsPath + cmpyId + '/' + config.clientPictureLocation + '/' + req.params.id;
                image = filepath;
                fs.rename(config.uploadsPath + cmpyId + '/' + config.clientPictureLocation + '/' + req.file.filename, filepath, function(err) {});
            } else {
                image = '';
            }
            var sqlQuery = 'UPDATE Contact__c'
            if (req.params.status === 'remove') {
                sqlQuery += ' SET Client_Pic__c = ' + null +
                    ', LastModifiedDate = "' + dateTime +
                    '", LastModifiedById = "' + loginId +
                    '" WHERE Id = "' + req.params.id + '"';
            } else if (req.params.status === 'upload') {
                sqlQuery += ' SET Client_Pic__c = "' + image +
                    '", LastModifiedDate = "' + dateTime +
                    '", LastModifiedById = "' + loginId +
                    '" WHERE Id = "' + req.params.id + '"';
            }
            execute.query(dbName, sqlQuery, function(error, results) {
                if (error) {
                    logger.error('Error in clientProPIc: ', error);
                    sendResponse(res, false, '000');
                } else {
                    sendResponse(res, true, results);
                }
            });
        });
    });
}
var storage = multer.diskStorage({
    destination: function(req, file, callback) {
        var cmpyId = req.headers['cid'];
        var uplLoc = config.uploadsPath + cmpyId
        if (!fs.existsSync(uplLoc)) {
            fs.mkdirSync(uplLoc);
        }
        uplLoc += '/' + config.clientPictureLocation;
        if (!fs.existsSync(uplLoc)) {
            fs.mkdirSync(uplLoc);
        }
        callback(null, uplLoc);
    },
    filename: function(req, file, callback) {
        callback(null, file.originalname);
    }
});
var uploadLogo = multer({ storage: storage }).single('clientPictureFile');

function clientQuickAdd(quickAddClientData, dbName, loginId, dateTime, cmpName, callback) {
    var val = Math.floor(1000 + Math.random() * 9000);
    quickAddClientData.Pin__c = val;
    var sqlQuery = 'INSERT INTO Contact__c SET ?';
    execute.query(dbName, sqlQuery, quickAddClientData, function(err, data) {
        if (err !== null) {
            if (err.sqlMessage.indexOf('FirstNameLastNameEmail') > 0) {
                callback(false, '004');
            } else {
                logger.error('Error in clientQuickAdd dao - clientQuickAdd:', err);
                callback(false, '000');
            }
        } else {
            var email_c;
            var cmpCity;
            var cmpState;
            var cmpPhone;
            var clientPin;
            execute.query(dbName, 'SELECT * from Company__c where isDeleted = 0; ' +
                ' SELECT * FROM Preference__c' +
                ' WHERE Name = "Appt Online Booking"',
                function(err, cmpresult) {
                    if (err) {
                        logger.error('Error in postal code dao - postal code:', err);
                        callback(false, '000');
                    } else {
                        email_c = cmpresult[0][0]['Email__c'];
                        cmpState = cmpresult[0][0]['State_Code__c'];
                        cmpPhone = cmpresult[0][0]['Phone__c'];
                        cmpCity = cmpresult[0][0]['City__c'];
                        clientPin = JSON.parse(cmpresult[1][0].JSON__c.replace('\\', '\\\\')).clientPin;
                    }

                    if (clientPin === true) {
                        fs.readFile(config.clientCreateHTML, function(err, data) {
                            if (err) {
                                logger.error('Error in reading HTML template:', err);
                                callback(false, '000');
                            } else {
                                var subject = 'Online Booking for ' + cmpName
                                var emailTempalte = data.toString();
                                emailTempalte = emailTempalte.replace("{{clientName}}", quickAddClientData.FirstName + " " + quickAddClientData.LastName);
                                emailTempalte = emailTempalte.replace("{{pin}}", val);
                                emailTempalte = emailTempalte.replace(/{{cmpName}}/g, cmpName);
                                emailTempalte = emailTempalte.replace("{{clientemail}}", quickAddClientData.Email);
                                emailTempalte = emailTempalte.replace("{{cmpCity}}", cmpCity);
                                emailTempalte = emailTempalte.replace("{{cmpState}}", cmpState);
                                emailTempalte = emailTempalte.replace("{{cmpPhone}}", cmpPhone);
                                emailTempalte = emailTempalte.replace("{{cmpEmail}}", email_c);
                                emailTempalte = emailTempalte.replace("{{action_url}}", config.bseURL + config.clientLink + dbName);
                                emailTempalte = emailTempalte.replace("{{cli_login}}", config.bseURL + config.clientLink + dbName);
                                emailTempalte = emailTempalte.replace("{{address_book}}", email_c);
                                CommonSRVC.getCompanyEmail(dbName, function(email) {
                                    if (quickAddClientData.Email) {
                                        mail.sendemail(quickAddClientData.Email, email, subject, emailTempalte, '', function(err, result) {
                                            if (err) {
                                                logger.error('Error in send reminder email - insert into Email_c table:', err);
                                                callback(false, '000');
                                            } else {
                                                var insertData = {
                                                    Id: uniqid(),
                                                    Appt_Ticket__c: '',
                                                    Client__c: quickAddClientData.Id,
                                                    Sent__c: dateTime,
                                                    Type__c: 'Send PIN Email',
                                                    Name: 'Send PIN Email',
                                                    OwnerId: loginId,
                                                    IsDeleted: 0,
                                                    CreatedDate: dateTime,
                                                    CreatedById: loginId,
                                                    LastModifiedDate: dateTime,
                                                    LastModifiedById: loginId,
                                                    SystemModstamp: dateTime,
                                                    LastModifiedDate: dateTime,
                                                }
                                                var sqlQuery = 'INSERT INTO Email__c SET ?';
                                                execute.query(dbName, sqlQuery, insertData, function(err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in send reminder email - insert into Email_c table:', err1);
                                                        callback(false, '000');
                                                    }
                                                });
                                                if (quickAddClientData.Sms_Consent__c && quickAddClientData.Email) {
                                                    var textSms = 'Hi, {{FirstName}} {{LastName}}. A 4-digit PIN has been assigned to you and is as follows: {{pin}}.';
                                                    textSms = textSms.replace(/{{FirstName}}/g, quickAddClientData.FirstName);
                                                    textSms = textSms.replace(/{{LastName}}/g, quickAddClientData.LastName);
                                                    textSms = textSms.replace(/{{pin}}/g, val);
                                                    sms.sendsms(quickAddClientData.MobilePhone, textSms, function(err, data) {
                                                        if (err) {
                                                            logger.info('Sms not Sent to :' + data);
                                                        }
                                                    });
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                        callback(true, {
                            'clientId': quickAddClientData.Id,
                            'clientName': quickAddClientData.FirstName + ' ' + quickAddClientData.LastName,
                            'clientPrimaryEmail': quickAddClientData.Email,
                            'clientPhone': quickAddClientData.Phone,
                            'clientSecondaryEmail': quickAddClientData.Secondary_Email__c
                        });
                    } else {
                        callback(true, {
                            'clientId': quickAddClientData.Id,
                            'clientName': quickAddClientData.FirstName + ' ' + quickAddClientData.LastName,
                            'clientPrimaryEmail': quickAddClientData.Email,
                            'clientPhone': quickAddClientData.Phone,
                            'clientSecondaryEmail': quickAddClientData.Secondary_Email__c
                        });
                    }

                }
            );
        }
    });
}

function createAppt(quickAddClientData, dbName, loginId, dateTime, callback) {
    var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
    execute.query(dbName, selectSql, '', function(err, result) {
        if (err) {
            callback(false, '000');
        } else {
            apptName = ('00000' + result[0].Name).slice(-6);
        }
        var serviceDur;
        if (quickAddClientData && quickAddClientData.serviceDur) {
            serviceDur = quickAddClientData.serviceDur;
        } else {
            serviceDur = 0;
        }
        var apptObjData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            Name: apptName,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            LastModifiedDate: dateTime,
            Appt_Date_Time__c: dateTime,
            Client_Type__c: null,
            Client__c: quickAddClientData.Client__c,
            Duration__c: serviceDur,
            Status__c: 'Checked In',
            isRefund__c: 0,
            isTicket__c: 1,
            Is_Booked_Out__c: 0,
            New_Client__c: 1,
            isNoService__c: 1
        }
        var insertQuery = 'INSERT INTO Appt_Ticket__c SET ?';
        execute.query(dbName, insertQuery, apptObjData, function(apptDataErr, apptDataResult) {
            if (apptDataErr) {
                logger.error('Error in CheckOut dao - createAppt:', apptDataErr);
                callback(false, '000');
            } else {
                callback(true, { 'apptId': apptObjData.Id, 'clientId': apptObjData.Client__c });
            }
        });
    });
}
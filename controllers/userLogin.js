var sendResponse = require('../common/response').sendResponse;
var logger = require('../common/logger');
var cfg = require('../common/config');
var execute = require('../common/dbConnection');
var bcrypt = require('bcrypt');
var CommonSRVC = require('../common/tokenService');
var mail = require('../common/sendMail');
var fs = require('fs');

// --- Start of Controller
module.exports.controller = function(app) {
    // --- Start of code to login into application
    app.post('/api/users/login', function(req, res) {
        if (req.body.userName && req.body.password) {
            var userName = req.body.userName;
            var qryStr = `SELECT DB_NAME FROM USER_SCHEMA WHERE IS_DELETED = 0 AND USER_ACCOUNT = '` + userName + `'`;
            var cmpySql = 'SELECT Id, Name, Package FROM Company__c'
            execute.query(cfg.globalDBName, qryStr, function(error, results) {
                if (error) {
                    logger.error('Error in getting userLogin: ', error);
                    sendResponse(res, false, '000');
                } else if (results.length == 0) {
                    sendResponse(res, false, '005');
                } else {
                    var dbName = results[0]['DB_NAME'];
                    query = 'SELECT ps.Id,u.UserType workerRole, ps.Authorized_Pages__c,IFNULL(u.image,"") workerImage, u.* FROM User__c as u left JOIN Permission_Set__c as ps on ps.Id = u.Permission_Set__c WHERE u.Username = "' + userName + '"';
                    execute.query(dbName, query, function(error, results) {
                        if (error) {
                            logger.error('Error in getting userLogin: ', error);
                            sendResponse(res, false, '005');
                        } else {
                            if (results[0] && !results[0]['IsActive']) {
                                sendResponse(res, false, '006');
                            } else {
                                execute.query(dbName, cmpySql, function(error, cmpresult) {
                                    if (results.length !== 0) {
                                        var passwordHash = results[0].Password__c;
                                        var userPassword = req.body.password;
                                    } else {
                                        var passwordHash = '';
                                        var userPassword = '';
                                    }
                                    bcrypt.compare(userPassword, passwordHash, function(err, res1) {
                                        if (res1 === true) {
                                            results[0]['db'] = dbName;
                                            results[0]['cid'] = cmpresult[0].Id;
                                            results[0]['cname'] = cmpresult[0].Name;
                                            results[0]['package'] = cmpresult[0].Package;
                                            var tokenData = {
                                                'id': results[0].Id,
                                                'userName': results[0].Username,
                                                'firstName': results[0].FirstName,
                                                'lastName': results[0].LastName,
                                                'db': dbName,
                                                'cid': cmpresult[0].Id,
                                                'cname': cmpresult[0].Name,
                                                'package': cmpresult[0].Package,
                                                'active': results[0].IsActive,
                                                'workerRole': results[0].workerRole
                                            };
                                            var rigthData = {
                                                'permissions': results[0].Authorized_Pages__c,
                                            }
                                            CommonSRVC.generateToken(tokenData, function(err, tkn) {
                                                if (err) {
                                                    sendResponse(res, false, '000');
                                                } else {
                                                    res.header('token', tkn);
                                                    CommonSRVC.generateToken(rigthData, function(err, rigths) {
                                                        if (err) {
                                                            sendResponse(res, false, '000');
                                                        } else {
                                                            var sqlQuery = 'UPDATE User__c SET Device_Token__c = "' + req.body.Pushtoken + '" WHERE Username = "' + userName + '"';
                                                            execute.query(dbName, sqlQuery, function(pushError, resObj) {
                                                                var result = {};
                                                                result['token'] = tkn;
                                                                result['rights'] = rigths;
                                                                result['id'] = results[0].Id;
                                                                result['userName'] = results[0].Username;
                                                                result['firstName'] = results[0].FirstName;
                                                                result['lastName'] = results[0].LastName;
                                                                result['image'] = results[0].workerImage;
                                                                result['View_Only_My_Appointments__c'] = results[0].View_Only_My_Appointments__c;
                                                                result['Hide_Client_Contact_Info__c'] = results[0].Hide_Client_Contact_Info__c;
                                                                result['Pushtoken'] = '';
                                                                sendResponse(res, true, result);
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        } else if (res1 === false || res1 === undefined || res1 === 'undefined') {
                                            sendResponse(res, false, '005');
                                        }
                                    });
                                });
                            }
                        }
                    });
                }
            });
        } else {
            logger.error('Error in UserLoginController - User Login: ' +
                'Missing mandatory field data');
            sendResponse(res, false, '007');
        }
    });
    // --- End of code to login into application
    // --- Start of code to update User Password ---//
    app.put('/api/users/password/:id', function(req, res) {
        if (req.params.id && req.body.password && req.headers.token) {
            UserLoginService.updatePassword(req, res);
        } else {
            logger.error('Error in UserLoginController - User Password Update: ' +
                'Missing mandatory field data');
            utils.sendResponse(res, 400, '9995', {});
        }
    });
    // --- End of code to update User Password ---//

    // --- Start of code to Forgot Password ---//
    app.post('/api/users/forgot_password', function(req, res) {
        if (req.body.username) {
            userName = req.body.username;
            var qryStr = `SELECT DB_NAME FROM USER_SCHEMA WHERE IS_DELETED = 0 AND USER_ACCOUNT = '` + userName + `'`;
            var cmpySql = 'SELECT Id, Name, Package FROM Company__c'
            execute.query(cfg.globalDBName, qryStr, function(error, result) {
                if (error) {
                    logger.error('Error in getting userLogin: ', error);
                    var response = {};
                    response.status = false;
                    response.result = "An unexpected error occurred on the server side";
                    response.message = "An unexpected error occurred on the server side";
                } else {
                    if (result.length > 0) {
                        var dbName = result[0]['DB_NAME'];
                        query = 'SELECT * FROM User__c WHERE Username = "' + userName + '"';
                        execute.query(dbName, query, function(error, results) {
                            if (error) {
                                logger.error('Error in validateUserName: ', error);
                                sendResponse(res, false, '000');
                                var response = {};
                                response.status = false;
                                response.result = "An unexpected error occurred on the server side";
                                response.message = "An unexpected error occurred on the server side";
                                res.json(response);
                            } else {
                                execute.query(dbName, cmpySql, function(error, cmpresult) {
                                    results[0]['cid'] = cmpresult[0].Id;
                                    results[0]['cname'] = cmpresult[0].Name;
                                    results[0]['package'] = cmpresult[0].Package;
                                    if (results.length == 0) {
                                        sendResponse(res, false, '013');
                                        var response = {};
                                        response.status = false;
                                        response.result = "Invalid Username";
                                        response.message = "Invalid Username";
                                        res.json(response);
                                    } else {
                                        results[0]['db'] = dbName;
                                    }
                                    sendForgotMail(results[0], dbName, function(status, data) {
                                        var response = {};
                                        response.status = true;
                                        response.result = "Mail sent Successfully";
                                        response.message = "Mail sent Successfully";
                                        res.json(response);
                                    });
                                });
                            }
                        });
                    } else {
                        var response = {};
                        response.status = false;
                        response.result = [];
                        response.message = "Username Invalid";
                        res.json(response);
                    }
                }
            });
        } else {
            logger.error('Error in UserLoginController - User Password Update: ' +
                'Missing mandatory field data');
            sendResponse(res, false, '014');
        }
    });
    // --- End of code to Forgot Password ---//

    // --- Start of code to Rest Password ---//
    app.post('/api/users/restpassword', function(req, res) {
        if (req.body.password && req.headers.token) {
            CommonSRVC.validateToken(req.headers.token, res, function(err, decodedToken) {
                if (err) {
                    sendResponse(res, false, '002');
                } else {
                    const saltRounds = 10;
                    bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                        query = 'UPDATE User__c SET Password__c="' + hash + '" WHERE Id="' + decodedToken.data.id + '"';
                        execute.query(decodedToken.data.db, query, function(error, result) {
                            if (error) {
                                logger.error('Error in getting userLogin: ', error);
                                sendResponse(res, false, '002');
                            } else {
                                sendResponse(res, true, result);
                            }
                        });
                    });
                }
            });
        } else {
            logger.error('Error in UserLoginController - Rest Password: ' +
                'Missing mandatory field data');
            sendResponse(res, false, '014');
        }
    });
    // --- End of code to Rest Password ---//
    app.post('/api/users/logout', function(req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'UPDATE User__c SET Device_Token__c="" WHERE Id="' + req.body.worker_id + '"';
        execute.query(dbName, sqlQuery, function(error, results) {
            if (error) {
                logger.error('Error in getting userLogin: ', error);
                sendResponse(res, false, '002');
            } else {
                sendResponse(res, true, results);
            }
        })
    })
};

function sendForgotMail(userObj, dbName, callback) {
    fs.readFile(cfg.forgotHTML, function(err, data) {
        if (err) {
            logger.error('Error in reading HTML template:', err);
            callback(false, '000');
        } else {
            var emailTempalte = data.toString();
            emailTempalte = emailTempalte.replace("{{name}}", userObj.FirstName + " " + userObj.LastName);
            var tokenData = {
                'id': userObj.Id,
                'userName': userObj.Username,
                'firstName': userObj.FirstName,
                'lastName': userObj.LastName,
                'db': userObj.db,
                'cid': userObj.cid,
                'cname': userObj.cname,
                'package': userObj.package,
                'workerRole': userObj.workerRole
            }
            CommonSRVC.generateToken(tokenData, function(err2, token) {
                emailTempalte = emailTempalte.replace("{{action_url}}", cfg.bseURL + cfg.resetLink + token);
                CommonSRVC.getCompanyEmail(dbName, function(email) {
                    mail.sendemail(userObj.Email, email, cfg.forgotSubject, emailTempalte, '', function(err, result) {
                        if (err) {
                            callback(false, '000');
                        } else {
                            callback(true, 'Mail sent Successfully');
                        }
                    });
                });
            });
        }
    });
}
var uniqid = require('uniqid');
var fs = require('fs');
var mysql = require('mysql');
var dateFns = require('../common/dateFunctions');
var mail = require('../common/sendMail');
var sms = require('../common/sendSMS');
var moment = require('moment');
var execute = require('../common/dbConnection');
var logger = require('../common/logger');
var sendResponse = require('../common/response').sendResponse;
var sendResponseWithMsg = require('../common/response').sendResponseWithMsg;
var FCM = require('fcm-push');
var Color = require('color');

module.exports.controller = function (app) {
    app.get('/api/appointment/workerList/:date', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var usrSql = 'select View_Only_My_Appointments__c from User__c where Id = "' + loginId + '" ';
        execute.query(dbName, usrSql, '', function (err, data) {
            if (err) {
                logger.error('Error in personCalender ', err);
                sendResponse(res, false, '000');
            } else if (data[0]['View_Only_My_Appointments__c'] === 1) {
                var sqlQuery1 = 'SELECT DISTINCT ' +
                    ' concat(UPPER(LEFT(us.FirstName,1)),LOWER(SUBSTRING(us.FirstName,2,LENGTH(us.FirstName)))," ", ' +
                    ' concat(UPPER(LEFT(us.LastName,1)),LOWER(SUBSTRING(us.LastName,2,LENGTH(us.LastName)))) )as names,' +
                    ' us.Id as workerId,(us.FirstName +" " + us.LastName) as fw,  us.View_Only_My_Appointments__c, ' +
                    ' concat(us.FirstName," ",us.LastName) as workerName,  IFNULL(us.Book_Every__c, 0) as Book_Every__c, ' +
                    ' IFNULL(us.image,"") as image ,' +
                    ' us.StartDay ' +
                    ' from User__c as us ' +
                    ' WHERE us.Id="' + loginId + '" ' +
                    ' order by case when Display_Order__c is null then 1 else 0 end,' +
                    ' Display_Order__c,CONCAT(FirstName, " ", LastName), CreatedDate asc';
                execute.query(dbName, sqlQuery1, '', function (err, result) {
                    if (err) {
                        logger.error('Error in appoitments: ', err);
                        sendResponse(res, false, '000');
                    } else {
                        sendResponse(res, true, result);
                    }
                });
            } else {
                var sqlQuery = 'SELECT DISTINCT ' +
                    ' concat(UPPER(LEFT(users.FirstName,1)),LOWER(SUBSTRING(users.FirstName,2,LENGTH(users.FirstName)))," ", ' +
                    ' concat(UPPER(LEFT(users.LastName,1)),LOWER(SUBSTRING(users.LastName,2,LENGTH(users.LastName)))) )as names,' +
                    ' users.Id as workerId,concat(users.FirstName," ",users.LastName) as workerName,users.Book_Every__c, ' +
                    ' users.StartDay ' +
                    ' FROM Worker_Service__c as service ' +
                    ' join Service__c as groups on groups.Id = service.Service__c ' +
                    ' join User__c as users on users.Id = service.Worker__c ' +
                    ' and users.StartDay IS NOT NULL' +
                    ' and users.StartDay <= "' + req.params.date + '" ' +
                    ' where service.Service__c IS NOT NULL ' +
                    ' and users.FirstName IS NOT NULL and users.IsActive =1 ' +
                    ' order by case when users.Display_Order__c is null then 1 else 0 end,' +
                    ' users.Display_Order__c,' +
                    ' CONCAT(users.FirstName, " ", users.LastName),' +
                    ' users.CreatedDate';
                execute.query(dbName, sqlQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in appoitments: ', err);
                        sendResponse(res, false, '000');
                    } else {
                        sendResponse(res, true, result);
                    }
                });
            }
        });
    });
    app.get('/api/appointments/expressbookingservices/:id/:date', function (req, res) {
        var dbName = req.headers['db'];
        var sql = 'SELECT CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName,IFNULL(ws.Price__c,0) Price__c,u.Service_Level__c,s.Guest_Charge__c, s.Id as serviceId, s.Levels__c, s.Name as serviceName, u.Id,u.Id workername, ' +
            ' IF(ws.Price__c = null, IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, IFNULL(s.Price__c,0))) as Price, ' +
            ' CONCAT(u.FirstName," " , u.LastName) as workerName, u.StartDay,u.Book_Every__c, s.Taxable__c,' +
            'IFNULL(ws.Duration_1__c, 0) as Duration_1,' +
            'IFNULL(ws.Duration_2__c, 0) as Duration_2,' +
            'IFNULL(ws.Duration_3__c,0) as Duration_3,' +
            'IFNULL(ws.Buffer_After__c,0) as Buffer_After,' +
            'IFNULL(ws.Duration_1_Available_For_Other_Work__c, 0) as Duration_1_Available_For_Other_Work__c,' +
            'IFNULL(ws.Duration_2_Available_For_Other_Work__c,0) as Duration_2_Available_For_Other_Work__c,' +
            'IFNULL(ws.Duration_3_Available_For_Other_Work__c,0) as Duration_3_Available_For_Other_Work__c,' +
            ' IFNULL(s.Duration_1__c,0) as Duration_1__c,' +
            ' IFNULL(s.Duration_2__c,0) as Duration_2__c,' +
            ' IFNULL(s.Duration_3__c,0) as Duration_3__c,s.Service_Group__c,' +
            ' IFNULL(s.Buffer_After__c,0) as Buffer_After__c,' +
            ' s.Duration_1_Available_For_Other_Work__c as SDuration_1_Available_For_Other_Work__c,' +
            ' s.Duration_2_Available_For_Other_Work__c as SDuration_2_Available_For_Other_Work__c,' +
            ' s.Duration_3_Available_For_Other_Work__c as SDuration_3_Available_For_Other_Work__c,' +
            ' IFNULL(GROUP_CONCAT(r.Name," #1" ),"#1") Resources__c, s.Resource_Filter__c' +
            ' FROM Worker_Service__c as ws' +
            ' RIGHT JOIN Service__c as s on s.Id = ws.Service__c ' +
            ' LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id and sr.IsDeleted=0' +
            ' LEFT JOIN Resource__c as r on r.Id = sr.Resource__c' +
            ' JOIN User__c as u on u.Id =ws.Worker__c WHERE ws.Worker__c = "' + req.params.id + '" '
        sql += ' and ws.isDeleted =0 and u.IsActive =1 and s.Is_Class__c = 0 GROUP BY s.Id ORDER BY s.Name ASC';
        var sqlQuery = ' SELECT u.Id as workerId,Ts.Service__c as serviceId,Ts.Service_Date_Time__c,' +
            ' Ts.Resources__c, ap.*,' +
            ' Ts.Service_Date_Time__c as Booking_Date_Time,' +
            ' Ts.Duration__c as Service_Duration, ' +
            ' CONCAT(u.FirstName, " ", LEFT(u.LastName, 1)) as FullName ' +
            ' FROM Appt_Ticket__c as ap ' +
            ' JOIN Ticket_Service__c as Ts on Ts.Appt_Ticket__c = ap.Id ' +
            ' left join User__c as u on u.Id = Ts.Worker__c ' +
            ' and isNoService__c = false ' +
            ' WHERE DATE(Appt_Date_Time__c) = "' + req.params.date + '" ' +
            ' and Ts.Status__c <> "Canceled" ' +
            ' order by ap.Appt_Date_Time__c asc';
        execute.query(dbName, sql, '', function (err, data) {
            if (err) {
                logger.error('Error in appoitments dao - getExpressBookingServices:', err);
                sendResponse(res, false, '000');
            } else {
                var result = data;
                var appList;
                if (result && result.length > 0) {
                    var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE (Name = "Service Groups" OR Name = "Sales Tax") and IsDeleted=0 Order By Name';
                    execute.query(dbName, sql2, '', function (err2, result2) {
                        if (err2) {
                            return (err, { statusCode: '9999' });
                        } else {
                            var salesTaxJSON = result2[0]['JSON__c'];
                            const colorList = JSON.parse(result2[1].JSON__c);
                            for (var i = 0; i < result.length; i++) {
                                if (result[i]['Taxable__c'] == 1) {
                                    result[i]['serviceTax'] = salesTaxJSON;
                                }
                                if (result[i]['Price__c'] === null || result[i]['Price__c'] === 'null' ||
                                    result[i]['Price__c'] === 0) {
                                    for (var j = 0; j < JSON.parse(result[i].Levels__c).length; j++) {
                                        if (result[i]['Service_Level__c'] === JSON.parse(result[i].Levels__c)[j]['levelNumber']) {
                                            result[i]['Price'] = JSON.parse(result[i].Levels__c)[j]['price'];
                                        }
                                    }
                                }
                                if ((result[i]['Duration_1'] === null || result[i]['Duration_1'] === 'null' ||
                                    result[i]['Duration_1'] === 0)) {
                                    for (var j = 0; j < JSON.parse(result[i].Levels__c).length; j++) {
                                        if (result[i]['Service_Level__c'] === JSON.parse(result[i].Levels__c)[j]['levelNumber']) {
                                            result[i]['Duration_1'] = JSON.parse(result[i].Levels__c)[j]['duration1'] ? JSON.parse(result[i].Levels__c)[j]['duration1'] : 0;
                                            result[i]['Duration_2'] = JSON.parse(result[i].Levels__c)[j]['duration2'] ? JSON.parse(result[i].Levels__c)[j]['duration2'] : 0;
                                            result[i]['Duration_3'] = JSON.parse(result[i].Levels__c)[j]['duration3'] ? JSON.parse(result[i].Levels__c)[j]['duration3'] : 0;
                                            result[i]['Buffer_After'] = JSON.parse(result[i].Levels__c)[j]['bufferAfter'] ? JSON.parse(result[i].Levels__c)[j]['bufferAfter'] : 0;

                                            result[i]['Duration_1__c'] = JSON.parse(result[i].Levels__c)[j]['duration1'] ? JSON.parse(result[i].Levels__c)[j]['duration1'] : 0;
                                            result[i]['Duration_2__c'] = JSON.parse(result[i].Levels__c)[j]['duration2'] ? JSON.parse(result[i].Levels__c)[j]['duration2'] : 0;
                                            result[i]['Duration_3__c'] = JSON.parse(result[i].Levels__c)[j]['duration3'] ? JSON.parse(result[i].Levels__c)[j]['duration3'] : 0;
                                            result[i]['Buffer_After__c'] = JSON.parse(result[i].Levels__c)[j]['bufferAfter'] ? JSON.parse(result[i].Levels__c)[j]['bufferAfter'] : 0;

                                            result[i]['Duration_1_Available_For_Other_Work__c'] = JSON.parse(result[i].Levels__c)[j]['duration1AvailableForOtherWork'] ? 1 : 0;
                                            result[i]['Duration_2_Available_For_Other_Work__c'] = JSON.parse(result[i].Levels__c)[j]['duration2AvailableForOtherWork'] ? 1 : 0;
                                            result[i]['Duration_3_Available_For_Other_Work__c'] = JSON.parse(result[i].Levels__c)[j]['duration3AvailableForOtherWork'] ? 1 : 0;
                                        } else {
                                            result[i]['Duration_1'] = result[i]['Duration_1__c'];
                                            result[i]['Duration_2'] = result[i]['Duration_2__c'];
                                            result[i]['Duration_3'] = result[i]['Duration_3__c'];
                                            result[i]['Buffer_After'] = result[i]['Buffer_After__c'];
                                            result[i]['Duration_1__c'] = result[i]['Duration_1__c'];
                                            result[i]['Duration_2__c'] = result[i]['Duration_2__c'];
                                            result[i]['Duration_3__c'] = result[i]['Duration_3__c'];
                                            result[i]['Buffer_After__c'] = result[i]['Buffer_After__c'];
                                            result[i]['Duration_1_Available_For_Other_Work__c'] = result[i]['SDuration_1_Available_For_Other_Work__c'];
                                            result[i]['Duration_2_Available_For_Other_Work__c'] = result[i]['SDuration_2_Available_For_Other_Work__c'];
                                            result[i]['Duration_3_Available_For_Other_Work__c'] = result[i]['SDuration_3_Available_For_Other_Work__c'];
                                        }
                                    }
                                } else {
                                    result[i]['Duration_1__c'] = result[i]['Duration_1'];
                                    result[i]['Duration_2__c'] = result[i]['Duration_2'];
                                    result[i]['Duration_3__c'] = result[i]['Duration_3'];
                                    result[i]['Buffer_After__c'] = result[i]['Buffer_After'];
                                    result[i]['Duration_1_Available_For_Other_Work__c'] = result[i]['Duration_1_Available_For_Other_Work__c'];
                                    result[i]['Duration_2_Available_For_Other_Work__c'] = result[i]['Duration_2_Available_For_Other_Work__c'];
                                    result[i]['Duration_3_Available_For_Other_Work__c'] = result[i]['Duration_3_Available_For_Other_Work__c'];
                                }
                                for (var j = 0; j < colorList.length; j++) {
                                    if (colorList[j].serviceGroupName === result[i].Service_Group__c) {
                                        result[i].color = colorList[j].serviceGroupColor;
                                    }
                                }
                            }
                        }
                        var serviceList = result;
                        execute.query(dbName, sqlQuery, '', function (error, appList) {
                            if (error) {
                                sendResponse(res, false, '000');
                            } else {
                                appList = appList;
                                sendResponse(res, true, { serviceList, appList });
                            }
                        });
                    });
                } else {
                    var serviceList = result;
                    sendResponse(res, true, { serviceList, appList });
                }
            }
        });
    });
    app.post('/api/appointment/existingExpressBooking', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var id = uniqid();
        var expressBookingObj = req.body;
        var records = [];
        var apptName;
        // var apptDate1 = moment(expressBookingObj.bookingDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
        var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
        execute.query(dbName, selectSql, '', function (err, result) {
            if (err) {
                sendResponse(res, false, '000');
            } else {
                apptName = ('00000' + result[0].Name).slice(-6);
            }
            var tempServices;
            for (var i = 0; i < expressBookingObj.service.length; i++) {
                tempServices = expressBookingObj.service[i].service.serviceId;
            }
            reBookedChecked(dbName, expressBookingObj.client_person_id, expressBookingObj.bookingDate, tempServices, function (err, redata) {
                if (redata.length >= 1) {
                    var Rebooked_Rollup_Max__c = 1;
                    var Rebooked__c = 1;
                } else {
                    var Rebooked_Rollup_Max__c = 0;
                    var Rebooked__c = 0;
                }
                if (expressBookingObj.client_person_id !== '') {
                    var apptDate1 = new Date(expressBookingObj.bookingDate);
                    var apptObjData = {
                        Id: id,
                        OwnerId: loginId,
                        IsDeleted: 0,
                        Name: apptName,
                        CreatedDate: dateTime,
                        CreatedById: loginId,
                        LastModifiedDate: dateTime,
                        LastModifiedById: loginId,
                        SystemModstamp: dateTime,
                        LastModifiedDate: dateTime,
                        Appt_Date_Time__c: apptDate1,
                        Client_Type__c: expressBookingObj.visitType,
                        Client__c: expressBookingObj.client_person_id,
                        Duration__c: expressBookingObj.sumDuration,
                        Status__c: 'Booked',
                        Is_Booked_Out__c: 0,
                        New_Client__c: 0,
                        Notes__c: expressBookingObj.textArea,
                        Service_Tax__c: expressBookingObj.totalServiceTax,
                        Service_Sales__c: expressBookingObj.totalPrice,
                        Rebooked_Rollup_Max__c: Rebooked_Rollup_Max__c
                    }
                    var insertQuery = 'INSERT INTO Appt_Ticket__c SET ?';
                    execute.query(dbName, insertQuery, apptObjData, function (error, data2) {
                        if (error) {
                            logger.error('Error in getting exprsssbokking1: ', error);
                            sendResponse(res, false, '000');
                        } else {
                            var insertQuery1 = "update Contact__c SET Active__c=1,Mobile_Carrier__c='" + expressBookingObj.mobileCarrier + "', " +
                                "  Email='" + expressBookingObj.primaryEmail + "',MobilePhone='" + expressBookingObj.mobileNumber + "', " +
                                " LastModifiedDate = '" + dateTime + "',LastModifiedById = '" + loginId + "' " +
                                " ,Sms_Consent__c = " + expressBookingObj.Sms_Consent__c + " "
                            if (expressBookingObj.Sms_Consent__c === 1) {
                                insertQuery1 += ',Marketing_Mobile_Phone__c = ' + 1 +
                                    ', Notification_Mobile_Phone__c = ' + 1 +
                                    ', Reminder_Mobile_Phone__c = ' + 1
                            } else if (expressBookingObj.Sms_Consent__c === 0) {
                                insertQuery1 += ',Marketing_Mobile_Phone__c = ' + 0 +
                                    ', Notification_Mobile_Phone__c = ' + 0 +
                                    ', Reminder_Mobile_Phone__c = ' + 0
                            }
                            if (expressBookingObj.primaryEmail === '' || expressBookingObj.primaryEmail === undefined ||
                                expressBookingObj.primaryEmail === null) {
                                insertQuery1 += ',Marketing_Primary_Email__c = ' + 0 +
                                    ', Notification_Primary_Email__c = ' + 0 +
                                    ', Reminder_Primary_Email__c = ' + 0
                            } else if (expressBookingObj.primaryEmail !== '') {
                                insertQuery1 += ',Marketing_Primary_Email__c = ' + 1 +
                                    ', Notification_Primary_Email__c = ' + 1 +
                                    ', Reminder_Primary_Email__c = ' + 1
                            }
                            insertQuery1 += "  where Id='" + expressBookingObj.client_person_id + "' ";
                            execute.query(dbName, insertQuery1, apptObjData, function (error5, data3) {
                                if (error5) {
                                    logger.error('Error in getting while update contact_c tabel for making active=1 : ', error5);
                                    sendResponse(res, false, '000');
                                }
                            });
                            if (data2 && data2.affectedRows > 0) {
                                var servicePriceAll = 0;
                                var workerAndServiceDuration1 = 0;
                                var workerAndServiceDuration2 = 0;
                                var workerAndServiceDuration3 = 0;
                                var sumOfAllDuration = 0;
                                for (var i = 0; i < expressBookingObj.service.length; i++) {
                                    var avail1 = expressBookingObj.service[i].service.avaiable1;
                                    var avail2 = expressBookingObj.service[i].service.avaiable2;
                                    var avail3 = expressBookingObj.service[i].service.avaiable3;
                                    var buffer = expressBookingObj.service[i].service.Buffer_after__c;
                                    if (expressBookingObj.service[i].service.Price__c === null || expressBookingObj.service[i].service.Price__c === "") {
                                        servicePriceAll = expressBookingObj.service[i].service.pcsergrp;
                                    } else {
                                        servicePriceAll = expressBookingObj.service[i].service.Price__c;
                                    }
                                    if ((expressBookingObj.service[i].service.workerDuration1 === null || expressBookingObj.service[i].service.workerDuration1 === 0) &&
                                        (expressBookingObj.service[i].service.workerDuration2 === null || expressBookingObj.service[i].service.workerDuration2 === 0) &&
                                        (expressBookingObj.service[i].service.workerDuration3 === null || expressBookingObj.service[i].service.workerDuration3 === 0)) {
                                        workerAndServiceDuration1 = expressBookingObj.service[i].service.serviceDuration1;
                                        workerAndServiceDuration2 = expressBookingObj.service[i].service.serviceDuration2;
                                        workerAndServiceDuration3 = expressBookingObj.service[i].service.serviceDuration3;
                                    } else {
                                        workerAndServiceDuration1 = expressBookingObj.service[i].service.workerDuration1;
                                        workerAndServiceDuration2 = expressBookingObj.service[i].service.workerDuration2;
                                        workerAndServiceDuration3 = expressBookingObj.service[i].service.workerDuration3;
                                    }
                                    if (expressBookingObj.service[i].service.sumDurationBuffer === null || expressBookingObj.service[i].service.sumDurationBuffer === 0) {
                                        sumOfAllDuration = expressBookingObj.service[i].service.dursergrp;
                                    } else {
                                        sumOfAllDuration = expressBookingObj.service[i].service.sumDurationBuffer;
                                    }
                                    records.push([
                                        uniqid(),
                                        0,
                                        dateTime,
                                        loginId,
                                        dateTime,
                                        loginId,
                                        dateTime,
                                        apptObjData.Id,
                                        expressBookingObj.visitType,
                                        expressBookingObj.client_person_id,
                                        expressBookingObj.service[i].worker,
                                        apptDate1,
                                        expressBookingObj.service[i].service.color, // service group color
                                        workerAndServiceDuration1, // duration 1
                                        workerAndServiceDuration2, // duration 2
                                        workerAndServiceDuration3, // duration 3
                                        sumOfAllDuration, // sum of duations
                                        0,
                                        servicePriceAll, // net price
                                        servicePriceAll, // price
                                        0,
                                        Rebooked__c,
                                        expressBookingObj.service[i].service.serviceId,
                                        // expressBookingObj.textArea,
                                        'Booked',
                                        avail1 ? avail1 : 0,
                                        avail2 ? avail2 : 0,
                                        avail3 ? avail3 : 0,
                                        buffer ? buffer : 0,
                                        expressBookingObj.service[i].service.Taxable__c,
                                        expressBookingObj.service[i].service.Service_Tax__c,
                                        expressBookingObj.service[i].service.resName
                                    ]);
                                    if (expressBookingObj.service[i].service.sumDurationBuffer) {
                                        apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);
                                    } else {
                                        apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);
                                    }
                                    var insertQuery1 = 'INSERT INTO Ticket_Service__c ' +
                                        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                        ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                                        ' Worker__c, Service_Date_Time__c,Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c,' +
                                        ' Is_Booked_Out__c,Price__c,Net_Price__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Status__c,' +
                                        ' Duration_1_Available_for_Other_Work__c,Duration_2_Available_for_other_Work__c, ' +
                                        ' Duration_3_Available_for_other_Work__c,Buffer_After__c,Taxable__c,Service_Tax__c,Resources__c) VALUES ?';
                                }
                                execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                    if (err1) {
                                        logger.error('Error in express1 dao - updateExpress:', err1);
                                        sendResponse(res, false, '000');
                                    } else {
                                        sendResponseWithMsg(res, true, apptObjData.Id, "Appointment updated successfully");
                                    }
                                });
                            } else {
                                sendResponseWithMsg(res, true, apptObjData.Id, "Appointment booked successfully");
                            }
                        }
                    });
                } else {
                    sendResponse(res, false, '000');
                }
            });
        });
    });
    app.get('/api/appointment/getServices/:id/:workerId?', function (req, res) {
        var dbName = req.headers['db'];
        var userSql = '';
        var Active = 0;
        var sql = 'SELECT ap.Id appiId, ts.Id as tsid,IFNULL(ap.Notes__c,"") as Notes__c, ap.Booked_Online__c,ap.Is_Standing_Appointment__c as standing,ts.Is_Booked_Out__c,IFNULL(ap.Status__c,"") as status,' +
            ' IFNULL(cont.FirstName,"") FirstName, IFNULL(cont.LastName,"") LastName,' +
            ' ap.New_Client__c, IFNULL(ser.Name, "") as serviceName,ap.Booked_Online__c as Booked_Online__c, ts.Appt_Ticket__c,ts.Service_Date_Time__c,' +
            ' ts.Worker__c, ts.Service__c, IFNULL(ts.Service_Group_Color__c," ") as serviceGroupColor,' +
            ' IFNULL(ts.Client__c,"") as clientID,' +
            ' ts.Duration_1_Available_for_Other_Work__c,' +
            'ts.Duration_2_Available_for_other_Work__c,' +
            'ts.Duration_3_Available_for_other_Work__c,' +
            'ts.Buffer_After__c,' +
            'ts.Duration_1__c,' +
            'ts.Duration_2__c,' +
            'ts.Duration_3__c,' +
            'ts.Duration__c ' +
            ' FROM Ticket_Service__c as ts' +
            ' left JOIN Service__c as ser on ser.Id = ts.Service__c ' +
            ' left JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id ' +
            ' left JOIN Contact__c as cont on cont.Id = ts.Client__c ' +
            ' where DATE(ts.Service_Date_Time__c) = "' + req.params.id + '" '
        if (req.params.workerId) {
            sql += 'and ts.Worker__c ="' + req.params.workerId + '" '
        }
        sql += ' and ap.Status__c <>"Canceled" ' +
            ' and ap.isRefund__c = 0 ' +
            '  and ts.IsDeleted=0 ';
        var sqlQuery = 'SELECT * FROM Preference__c WHERE Name = "Appt Booking"';
        if (req.params.workerId) {

            userSql += 'SELECT IsActive FROM User__c WHERE Id = "' + req.params.workerId + '"';
        }
        if (userSql.length > 0) {
            execute.query(dbName, userSql, function (error, useresult) {
                if (useresult.length > 0) {
                    Active = useresult[0]['IsActive'];
                }
            });
        }
        execute.query(dbName, sql, function (error, result) {
            if (error) {
                logger.error('Error in getting getSerives: ', error);
                sendResponse(res, false, '000');
            } else {
                if (result && result.length > 0) {
                    for (var i = 0; i < result.length; i++) {
                        if (result[i]['FirstName']) {
                            result[i]['FirstName'] = result[i]['FirstName'][0].toUpperCase() + result[i]['FirstName'].slice(1);
                        }
                        if (result[i]['LastName']) {
                            result[i]['LastName'] = result[i]['LastName'][0].toUpperCase() + result[i]['LastName'].slice(1);
                        }
                        if (!result[i]['FirstName']) {
                            result[i]['Name'] = '';
                        } else {
                            result[i]['Name'] = result[i]['FirstName'] + ' ' + result[i]['LastName'];
                        }
                    }
                    execute.query(dbName, sqlQuery, function (error1, data) {
                        var JSON__c_str = JSON.parse(data[0].JSON__c);
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].status === 'Complete') {
                                result[i].statusColor = JSON__c_str.completeStatusColor;
                            } else if (result[i].status === 'Booked') {
                                result[i].statusColor = JSON__c_str.bookedStatusColor;
                            } else if (result[i].status === 'Checked In') {
                                result[i].statusColor = JSON__c_str.checkedInStatusColor;
                            } else if (result[i].status === 'Conflicting') {
                                result[i].statusColor = JSON__c_str.conflictingStatusColor;
                            } else if (result[i].status === 'Reminder Sent') {
                                result[i].statusColor = JSON__c_str.reminderSentStatusColor;
                            } else if (result[i].status === 'Called') {
                                result[i].statusColor = JSON__c_str.calledStatusColor;
                            } else if (result[i].status === 'Confirmed') {
                                result[i].statusColor = JSON__c_str.confirmedStatusColor;
                            } else if (result[i].status === 'Canceled') {
                                result[i].statusColor = JSON__c_str.canceledStatusColor;
                            } else if (result[i].status === 'No Show') {
                                result[i].statusColor = JSON__c_str.noShowStatusColor;
                            } else if (result[i].status === 'Pending Deposit') {
                                result[i].statusColor = JSON__c_str.pendingDepositStatusColor;
                            }
                            if (result[i].Is_Booked_Out__c === 1 || !result[i].serviceGroupColor) {
                                result[i].serviceGroupColor = '#3a87ad';
                            }
                            if (srchAry(result[i].Appt_Ticket__c, i, result) !== null) {
                                result[i].Appt_Icon = 'asterix';
                            }

                        }
                        var dataObject = avlDurColor(result);
                        if (req.params.workerId) {
                            getWrkHrs(dbName, req.params.workerId, req.params.id, function (data1, data2) {
                                var startDate = '';
                                var endDate = '';
                                if (data2.length > 0 && Active === 1) {
                                    if (data2[0]['All_Day_Off__c'] === 1) {
                                        Active = 0;
                                    } else {
                                        Active = 1;
                                        if (data2[0]['StartTime__c'] != '') {
                                            startDate = formatAMPM(data2[0]['StartTime__c'], new Date(req.params.id));
                                            endDate = formatAMPM(data2[0]['EndTime__c'], new Date(req.params.id));
                                        }
                                    }
                                } else if (data1.length === 0 && Active === 1) {
                                    Active = 0;
                                } else {
                                    if (Active === 1)
                                        Active = 1;
                                    var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                    var tempAry = new Date(req.params.id);
                                    var day = weekday[tempAry.getDay()];
                                    var splitArray = data1[0][day].split('|')
                                    if (splitArray[0] != '') {
                                        startDate = formatAMPM(splitArray[0], new Date(req.params.id));
                                        endDate = formatAMPM(splitArray[1], new Date(req.params.id));
                                    }
                                }
                                sendResponse(res, true, { 'result': dataObject, 'Active': Active, wrkrStartDate: startDate, wrkrEndDate: endDate });
                            });
                        } else {
                            sendResponse(res, true, { 'result': dataObject, 'Active': 0, wrkrStartDate: '', wrkrEndDate: '' });
                        }
                    });
                } else {
                    if (req.params.workerId) {
                        getWrkHrs(dbName, req.params.workerId, req.params.id, function (data1, data2) {
                            var startDate = '';
                            var endDate = '';
                            if (data2.length > 0) {
                                if (data2[0]['All_Day_Off__c'] === 1) {
                                    Active = 0;
                                } else {
                                    Active = 1;
                                    if (data2[0]['StartTime__c'] != '') {
                                        startDate = formatAMPM(data2[0]['StartTime__c'], new Date(req.params.id));
                                        endDate = formatAMPM(data2[0]['EndTime__c'], new Date(req.params.id));
                                    }
                                }
                            } else if (data1.length === 0) {
                                Active = 0;
                            } else {
                                Active = 1;
                                var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                                var tempAry = new Date(req.params.id);
                                var day = weekday[tempAry.getDay()];
                                var splitArray = data1[0][day].split('|')
                                if (splitArray[0] != '') {
                                    startDate = formatAMPM(splitArray[0], new Date(req.params.id));
                                    endDate = formatAMPM(splitArray[1], new Date(req.params.id));
                                }
                            }
                            res.status(200).send({ 'status': true, 'result': { result: [], 'Active': Active, wrkrStartDate: startDate, wrkrEndDate: endDate }, 'message': 'There are no appointments on this date. Please select another date' });
                        });
                    } else {
                        res.status(200).send({ 'status': true, 'result': { result: [], 'Active': Active, wrkrStartDate: '', wrkrEndDate: '' }, 'message': 'There are no appointments on this date. Please select another date' });
                    }
                    // sendResponse(res, true, '','yes');
                }

            }
        });
    });
    app.get('/api/appointments/activeMembers/:day/:date', function (req, res) {
        var dbName = req.headers['db'];
        var date = req.params['date'];
        var tempAry = date.split('-');
        var tempDt = new Date(tempAry[0], tempAry[1] - 1, tempAry[2], 1);
        var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var day = weekday[tempDt.getDay()];
        var min;
        var loginId = req.headers['id'];
        var usrSql = 'select View_Only_My_Appointments__c from User__c where Id = "' + loginId + '" ';
        execute.query(dbName, usrSql, '', function (err, data) {
            if (err) {
                logger.error('Error in personCalender ', err);
                sendResponse(res, false, '000');
            } else {
                checkDefaultAppId(dbName, loginId, function (err, defaultAppId) {
                    if (data[0]['View_Only_My_Appointments__c'] === 1) {
                        var sqlQuery1 = 'SELECT DISTINCT us.View_Only_My_Appointments__c,' +
                            ' us.Id as workerId, ' +
                            ' us.FirstName as names, ' +
                            ' ch.' + day + 'StartTime__c' + ' as start,' +
                            ' ch.' + day + 'EndTime__c' + ' as end,' +
                            ' us.image as image from User__c as us' +
                            ' left join Company_Hours__c as ch on ch.Id = "' + defaultAppId + '" ' +
                            ' WHERE  us.Id="' + loginId + '" ';
                        execute.query(dbName, sqlQuery1, '', function (err, result) {
                            if (err) {
                                logger.error('Error in appoitments dao - getApptBookingData:', err);
                                sendResponse(res, false, '000');
                            } else {
                                result[0]['min'] = moment(result[0]['start'], 'hh:mm A').format('HH:mm');
                                result[0]['max'] = moment(result[0]['end'], 'hh:mm A').format('HH:mm');
                                sendResponse(res, true, result);
                            }
                        });
                    } else {
                        var sqlQuery = 'SELECT' +
                            ' DISTINCT us.Appointment_Hours__c,us.Id as workerId,' +
                            ' IFNULL(us.Display_Order__c,0) as Display_Order__c,' +
                            ' concat(UPPER(LEFT(us.FirstName,1)),  LOWER(SUBSTRING(us.FirstName,2,LENGTH(us.FirstName)))," ", UPPER(LEFT(us.LastName,1)),"." ) as names, ' +
                            ' IF(cs.All_Day_Off__c = 1, "", IF(cs.StartTime__c, cs.StartTime__c, ch.' + day + 'StartTime__c' + ')) as start,' +
                            ' IF(cs.All_Day_Off__c = 1, "", IF(cs.EndTime__c, cs.EndTime__c, ch.' + day + 'EndTime__c' + ')) as end,' +
                            ' cs.StartTime__c,' +
                            ' cs.EndTime__c,' +
                            ' cs.Date__c,' +
                            ' cs.All_Day_Off__c,' +
                            ' us.image as image ' +
                            ' from' +
                            ' User__c as us' +
                            ' left join Worker_Service__c as ws on ws.Worker__c=us.Id ' +
                            ' left join Company_Hours__c as ch on ch.Id = us.Appointment_Hours__c ' +
                            ' left join Service__c as s on s.Id = ws.Service__c ' +
                            ' left outer JOIN CustomHours__c as cs on cs.Company_Hours__c =us.Appointment_Hours__c ' +
                            ' AND cs.Date__c = "' + req.params.date + '" AND cs.IsDeleted =0  ' +
                            ' WHERE us.StartDay <= "' + req.params.date + '" ' +
                            ' and ws.Service__c IS NOT NULL' +
                            ' and us.IsActive=1 ' +
                            ' and s.Is_Class__c= 0' +
                            ' or cs.StartTime__c <>"" order by  ch.' + day + 'StartTime__c' + ' asc';
                        execute.query(dbName, sqlQuery, '', function (err, result) {
                            if (err) {
                                logger.error('Error in appoitments dao - getApptBookingData:', err);
                                sendResponse(res, false, '000');
                            } else {
                                var finalDatesStart = [];
                                var finalDatesEnd = [];
                                var defStart = '';
                                var defEnd = '';
                                var defAppt = [];
                                var tempStart = result.filter(function (obj) { return obj['defaultStart'] });
                                var tempEnd = result.filter(function (obj) { return obj['defaultEnd'] });
                                if (tempStart.length > 0) {
                                    defStart = tempStart[0]['defaultStart'];
                                    defEnd = tempStart[0]['defaultEnd'];
                                }
                                for (var i = 0; i < result.length; i++) {
                                    finalDatesStart.push(moment(result[i].start, 'hh:mm A').format('HH'));
                                    finalDatesEnd.push(moment(result[i].end, 'hh:mm A').format('HH'));
                                    if (result[i]['Default__c']) {
                                        if (result[i].Default__c.split('-')[0] === '1') {
                                            result[i]['Appointment_Hours__c'] = result[i]['Default__c'].split('-')[1];
                                        }
                                    }
                                    if (result[i].Appointment_Hours__c === "" || result[i].Appointment_Hours__c === null) {
                                        result[i]['start'] = defStart;
                                        result[i]['end'] = defEnd;
                                    }
                                }
                                var toRemove = new Set(['Invalid date']);
                                const difference = new Set([...finalDatesStart].filter((x) => !toRemove.has(x)));
                                const difference1 = new Set([...finalDatesEnd].filter((x) => !toRemove.has(x)));
                                var mainValues1 = Array.from(difference);
                                var mainValues2 = Array.from(difference1);
                                var merg = mainValues1.concat(mainValues2);
                                var minHrs = Math.min.apply(null, merg);
                                var maxHrs = Math.max.apply(null, merg);
                                var result = result.filter(function (a) { return (a.start !== null && a.start !== '' && a.end !== null && a.end !== '') });
                                if (result && result.length >= 0) {
                                    var sql1 = ' SELECT DISTINCT concat(UPPER(LEFT(us.FirstName,1)), ' +
                                        'LOWER(SUBSTRING(us.FirstName,2,LENGTH(us.FirstName)))," ", UPPER(LEFT(us.LastName,1)),"." ) as names, ' +
                                        ' ts.Worker__c as workerId, us.image as image,IFNULL(us.Display_Order__c,0) as Display_Order__c, time(ts.Service_Date_Time__c) as start, ts.Duration__c ' +
                                        ' FROM Ticket_Service__c as ts ' +
                                        ' left join User__c as us on us.Id = ts.Worker__c ' +
                                        ' left JOIN Appt_Ticket__c as ap on ts.Appt_Ticket__c = ap.Id ' +
                                        ' where DATE(ts.Service_Date_Time__c) = "' + req.params.date + '" ' +
                                        ' and ts.IsDeleted=0 and ap.Status__c <> "Canceled" ' +
                                        '  order by ts.Service_Date_Time__c asc';
                                    execute.query(dbName, sql1, '', function (err1, result1) {
                                        if (err1) {
                                            logger.error('Error in appoitments dao - workerList2:', err1);
                                            sendResponse(res, false, '000');
                                        } else {
                                            if (result1 && result1.length > 0) {
                                                var minHrs1 = 0;
                                                var maxHrs1 = 0;
                                                var start = moment(result1[0].start, 'HH:mm').format('HH');
                                                var addDur = parseInt(result1[result1.length - 1].Duration__c);
                                                if ((parseInt(result1[result1.length - 1].Duration__c) + parseInt(result1[result1.length - 1].start.split(':')[1])) % 60 != 0) {
                                                    addDur += 60;
                                                }
                                                var end = moment(result1[result1.length - 1].start, 'HH:mm').add(addDur, 'minutes').format('HH');
                                            }
                                            result = result1.concat(result);
                                            if (result) {
                                                var temp = removeDuplicates(result, 'workerId');
                                                result = temp;
                                            }
                                            var sd = sortJSONAry(result, 'Display_Order__c', 'asc');
                                            var nonZero = sd.filter(function (a) { return a.Display_Order__c !== 0 }).sort((a, b) => a.Display_Order__c - b.Display_Order__c);
                                            var zero = sd.filter(function (a) { return a.Display_Order__c === 0 }).sort(function (a, b) {
                                                //  return a.names === 0
                                                var nameA = '';
                                                var nameB = '';
                                                if (a.names) {
                                                    nameA = a.names.toLowerCase();
                                                }
                                                if (b.names) {
                                                    nameB = b.names.toLowerCase();
                                                }
                                                if (nameA < nameB) //sort string ascending
                                                    return -1
                                                if (nameA > nameB)
                                                    return 1
                                                return 0
                                            });
                                            result = nonZero.concat(zero);
                                            if (result.length === 0 && result1.length === 0) {
                                                let data;
                                                data = 0;
                                                sendResponse(res, false, '015');
                                            } else if (start === undefined && end === undefined) {
                                                result[0]['min'] = moment(minHrs, 'HH:mm').format('HH');
                                                result[0]['max'] = moment(maxHrs, 'HH:mm').format('HH');
                                            } else if (minHrs === undefined || maxHrs === undefined) {
                                                result[0]['min'] = moment(start, 'HH:mm').format('HH');
                                                result[0]['max'] = moment(end, 'HH:mm').format('HH');
                                            } else if (minHrs === '00:00' || maxHrs === '00:00') {
                                                result[0]['min'] = moment(start, 'HH:mm').format('HH');
                                                result[0]['max'] = moment(end, 'HH:mm').format('HH');
                                            } else if (result.length === 0 && result1.length === 0) {
                                                sendResponse(res, false, '015');
                                            } else {
                                                if (start > minHrs) {
                                                    result[0]['min'] = minHrs;
                                                } else {
                                                    result[0]['min'] = start;
                                                }
                                                if (end > maxHrs) {
                                                    result[0]['max'] = end;
                                                } else {
                                                    result[0]['max'] = maxHrs;
                                                }
                                            }
                                            sendResponse(res, true, result);
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });
    });
    app.post('/api/clientSearch/appointmentbooking', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var date = req.headers['dt'];
        var appointmentbookingObj = req.body;
        appointmentbookingObj.Client__c = req.body.client_id;
        appointmentbookingObj.Appt_Date_Time__c = req.body.date;
        appointmentbookingObj.apptCreatedDate = req.body.mindate;
        appointmentbookingObj.durations = [];
        appointmentbookingObj.serviceData = [];
        appointmentbookingObj.daleteArray = [];
        var index = 0
        getDursrvData(0, dbName, appointmentbookingObj, function (durData) {
            index++;
            appointmentbookingObj = durData;
            apptbookingMethod(index, req, res, loginId, date, dbName, appointmentbookingObj);
        });
    });
    app.get('/api/clientSearch/appointmentbooking', function (req, res) {
        var createApptJson = {
            "Client_Type__c": "",
            "Client__c": "",
            "Duration__c": 0,
            "Appt_Date_Time__c": "",
            "servicesData": [{
                "Id": "",
                "serviceGroupColour": "",
                "workerName": "",
                "Duration_1__c": 0,
                "Net_Price__c": 0.0,
                "Duration_2__c": 0,
                "Duration_3__c": 0,
                "Buffer_After__c": 0,
                "Duration_1_Available_for_Other_Work__c": 0,
                "Duration_2_Available_for_Other_Work__c": 0,
                "Duration_3_Available_for_Other_Work__c": 0,
                "Guest_Charge__c": '0',
                "Taxable__c": 0,
                "Duration__c": 0,
                "Service_Tax__c": 0.0,
                "Resources__c": ""
            },
            {
                "Id": "",
                "serviceGroupColour": "",
                "workerName": "",
                "Duration_1__c": 0,
                "Net_Price__c": 0.0,
                "Duration_2__c": 0,
                "Duration_3__c": 0,
                "Buffer_After__c": 0,
                "Duration_1_Available_for_Other_Work__c": 0,
                "Duration_2_Available_for_Other_Work__c": 0,
                "Duration_3_Available_for_Other_Work__c": 0,
                "Taxable__c": 0,
                "Duration__c": 0,
                "Service_Tax__c": 0.0,
                "Resources__c": ""
            }
            ],
            "apptId": "",
            "Notes__c": "",
            "daleteArray": [],
            "IsPackage": 0,
            "Service_Tax__c": 0.0,
            "Service_Sales__c": 0.0,
            "apptCreatedDate": "",
            "bookingType": "findappt"
        }
        sendResponse(res, true, createApptJson);
    });
    app.get('/api/appointment/booking_slot', function (req, res) {
        var dbName = req.headers['db'];
        try {
            var sqlQuery = 'SELECT * FROM Preference__c WHERE Name = "Appt Booking"';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                if (result && result.length > 0) {
                    var JSON__c_str = JSON.parse(result[0].JSON__c);
                    sendResponse(res, true, JSON__c_str);
                } else {
                    logger.error('Error in apptBooking dao - getApptBookingData:', err);
                    sendResponseWithMsg(res, false, [], err)
                }
            });
        } catch (err) {
            logger.error('Unknown error in apptBooking dao - getApptBookingData:', err);
            return (err, { statusCode: '9999' });
        }
    });
    app.put('/api/appointment/detail/:id', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var apptId = req.params.id;
        var sqlQuery = 'UPDATE Appt_Ticket__c SET Status__c = "' + req.body.status +
            '", Notes__c = "' + req.body.notes +
            '", Client_Type__c = "' + req.body.visittype +
            '", LastModifiedDate = "' + dateTime +
            '", LastModifiedById = "' + loginId +
            '" WHERE Id = "' + apptId + '";';
        sqlQuery += 'UPDATE `Ticket_Service__c` SET `Status__c` = "' + req.body.status + '" WHERE `Appt_Ticket__c` = "' + apptId + '";'
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in appoitments dao - getApptBookingData:', err);
                sendResponse(res, false, '000');
            } else {
                sendResponse(res, true, result);
            }
        });
    });
    app.put('/api/ticketservice/:tsid', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var sqlQuery = 'UPDATE Ticket_Service__c SET Notes__c = "' + req.body.notes +
            '", LastModifiedDate = "' + dateTime +
            '", LastModifiedById = "' + loginId +
            '" WHERE Id = "' + req.params.tsid + '";';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in appoitments dao - getApptBookingData:', err);
                sendResponse(res, false, '000');
            } else {
                sendResponse(res, true, result);
            }
        });
    });
    app.get('/api/appointments/services/:id/:apptid', function (req, res) {
        var dbName = req.headers['db'];
        var cleintId = req.params.id
        var sql = "SELECT IFNULL(CONCAT('scale:',pckg.Id),'0')as pckgId,U.StartDay, U.Book_Every__c, IFNULL(pckg.Name,'0')as packageName,at.Appt_Date_Time__c, at.Id as apptId,at.Name as ticketNumber,CAST(Ts.Service_Tax__c AS CHAR) Service_Tax__c, Ts.Id as tsId, CONCAT(IFNULL(S.Id, ''), '$',IFNULL(S.Duration_1__c,0), '$', IFNULL(S.Duration_2__c,0)," +
            "  '$',IFNULL(S.Duration_3__c,0),'$',  IFNULL(S.Buffer_After__c,0), '$', CAST(IFNULL(Ts.Guest_Charge__c, 0) AS CHAR), '$',S.Price__c) as serviceName,S.Duration_1_Available_for_Other_Work__c sDuration1Available, S.Duration_2_Available_for_Other_Work__c sDuration2Available," +
            " S.Duration_3_Available_for_Other_Work__c sDuration3Available, S.Name as Name, at.Status__c,S.Id,Ts.Service_Date_Time__c,  CAST(Ts.Net_Price__c AS CHAR) Price__c,IFNULL(Ts.Notes__c,'') Notes__c, Ts.Resources__c, Ts.Rebooked__c, IFNULL(Ts.Booked_Package__c,'') Booked_Package__c , " +
            " Ts.Taxable__c, IFNULL(Ts.Visit_Type__c,'') as visttype,Ts.Worker__c as workerName,Ts.Duration__c, Ts.Preferred_Duration__c,  Ts.Duration_1__c, Ts.Duration_2__c,Ts.Duration_3__c, Ts.Buffer_After__c, Ts.Duration_1_Available_for_Other_Work__c, Ts.Duration_2_Available_for_Other_Work__c, " +
            " Ts.Duration_3_Available_for_Other_Work__c,  CONCAT(S.Service_Group__c, '$', Ts.Service_Group_Color__c) as serviceGroup,CONCAT(U.FirstName, ' ', U.LastName) as name, " +
            " Ts.Service_Group_Color__c as serviceGroupColour, S.Service_Group__c as serviceGroupName  FROM Ticket_Service__c as Ts  LEFT JOIN Service__c as S ON S.Id = Ts.Service__c  LEFT JOIN User__c as U ON U.Id = Ts.Worker__c  " +
            " LEFT Join Appt_Ticket__c as at on at.Id = Ts.Appt_Ticket__c  LEFT JOIN Worker_Service__c as ws on ws.Service__c =S.Id LEFT Join Package__c as pckg on pckg.Id = Ts.Booked_Package__c WHERE Ts.isDeleted = 0 And "
        if (cleintId && cleintId != 'null' && cleintId != 'noClientid') {
            sql += " Ts.Client__c ='" + cleintId + "' And "
        }
        sql += " at.Id ='" + req.params.apptid + "' " +
            " GROUP BY Ts.Id order by Ts.Service_Date_Time__c asc ";
        execute.query(dbName, sql, '', function (err, srvcresult) {
            if (err) {
                logger.error('Error in appoitments dao - getExpressBookingServices:', err);
                sendResponse(res, false, '000');
            } else if (srvcresult.length > 0) {
                var apptrst = [];
                var srvgResult = [];
                var pckgResult = [];
                var nextapptresult = [];
                var indexParam = 0;
                var servList = [];
                var srvGroupList = [];
                var workerList = [];
                // if (srvcresult && srvcresult.length > 0) {
                for (var i = 0; i < srvcresult.length; i++) {
                    servList.push(srvcresult[i].Id);
                    srvGroupList.push(srvcresult[i].serviceGroupName);
                    workerList.push(srvcresult[i].Id)
                    srvcresult[i]['servList'] = [];
                    srvcresult[i]['workerList'] = [];
                }
                // }
                if (servList.length > 0) {
                    var servPar = '(';
                    for (var i = 0; i < servList.length; i++) {
                        servPar = servPar + '"' + servList[i] + '",'
                    }
                    servPar = servPar.substr(0).slice(0, -2);
                    servPar = servPar + '")';
                    var srvGrpPar = '(';
                    for (var i = 0; i < srvGroupList.length; i++) {
                        srvGrpPar = srvGrpPar + '"' + srvGroupList[i] + '",'
                    }
                    srvGrpPar = srvGrpPar.substr(0).slice(0, -2);
                    srvGrpPar = srvGrpPar + '")';

                    var wrkrPar = '(';
                    for (var i = 0; i < workerList.length; i++) {
                        wrkrPar = wrkrPar + '"' + workerList[i] + '",'
                    }
                    wrkrPar = wrkrPar.substr(0).slice(0, -2);
                    wrkrPar = wrkrPar + '")';
                    var srvGrpQuery = ' SELECT s.Id, s.Name, s.Taxable__c, s.Service_Group__c as serviceGroupName, IFNULL(s.Duration_1__c, 0) Duration_1__c,' +
                        ' IFNULL(s.Duration_2__c, 0) Duration_2__c,IFNULL(s.Duration_3__c, 0) Duration_3__c,IFNULL(s.Buffer_After__c, 0) Buffer_After__c, ' +
                        ' s.Duration_1_Available_for_Other_Work__c,IFNULL(s.Client_Facing_Name__c,"") Client_Facing_Name__c, s.Duration_2_Available_for_Other_Work__c, s.Duration_3_Available_for_Other_Work__c,' +
                        ' CAST(IFNULL(s.Price__c,0) AS CHAR) Net_Price__c,CAST(IFNULL(s.Guest_Charge__c, 0) AS CHAR) Guest_Charge__c,IFNULL(GROUP_CONCAT(DISTINCT r.Name),"") Resources__c' +
                        ' FROM  Service__c as s ' +
                        ' LEFT JOIN Service_Resource__c sr ON sr.Service__c=s.Id ' +
                        ' LEFT JOIN Resource__c r on r.Id = sr.Resource__c' +
                        ' JOIN (SELECT ws.Service__c as serviceId, "" as Resources__c  FROM Worker_Service__c as ws join User__c as u ON u.Id = ws.Worker__c'
                        // + ' WHERE u.startDay <= "' + srvcresult[0].Appt_Date_Time__c.split(" ")[0] + '" AND u.IsActive=1 UNION '
                        +
                        ' WHERE u.IsActive=1 '
                    if (req.headers['type']) {
                        srvGrpQuery += ' AND ws.Self_Book__c=1 '
                    }
                    srvGrpQuery += ' UNION ' +
                        ' SELECT ts.Service__c as serviceId, ts.Resources__c as Resources__c  from Ticket_Service__c  as ts' +
                        ' WHERE ' +
                        'ts.Appt_Ticket__c = "' + req.params.apptid + '" ) as uni on s.Id = uni.serviceId' +
                        ' WHERE s.isDeleted = 0 and  s.Service_Group__c IN ' + srvGrpPar + ' GROUP BY s.Name';
                    var servicesQuery = 'SELECT u.StartDay, u.Book_Every__c,CONCAT(u.FirstName, " ", u.LastName) as name, ws.Service__c as sId, ws.Worker__c as workername, IFNULL(ws.Duration_1__c,0) wduration1,IFNULL(ws.Duration_2__c,0) wduration2,IFNULL(ws.Duration_3__c,0) wduration3,IFNULL(ws.Buffer_After__c,0) wbuffer,' +
                        ' s.Levels__c,u.Service_Level__c,s.Duration_1__c sduration1,s.Duration_2__c sduration2,s.Duration_3__c sduration3,s.Buffer_After__c sbuffer,CAST(IFNULL(s.Guest_Charge__c, 0) AS CHAR) Guest_Charge__c, CAST(IF(ws.Price__c = null,' +
                        ' IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, IFNULL(s.Price__c,0))) AS CHAR) Net_Price__c,' +
                        ' s.Taxable__c, IFNULL(ws.Duration_1_Available_for_Other_Work__c,0) Duration_1_Available_for_Other_Work__c, IFNULL(ws.Duration_2_Available_for_Other_Work__c,0) Duration_2_Available_for_Other_Work__c,IFNULL(ws.Duration_3_Available_for_Other_Work__c,0) Duration_3_Available_for_Other_Work__c,' +
                        ' s.Duration_1_Available_for_Other_Work__c sDuration_1_Available_for_Other_Work__c, s.Duration_2_Available_for_Other_Work__c sDuration_2_Available_for_Other_Work__c, s.Duration_3_Available_for_Other_Work__c sDuration_3_Available_for_Other_Work__c' +
                        ' FROM Worker_Service__c as ws right join Service__c as s on s.Id=ws.Service__c join User__c as u ON u.Id = ws.Worker__c' +
                        ' WHERE ws.Service__c IN ' + wrkrPar + ' and u.IsActive = 1 and s.isDeleted =0 '
                    if (req.headers['type']) {
                        servicesQuery += ' AND ws.Self_Book__c=1 '
                    }
                    servicesQuery += 'GROUP BY name, sId, workername';
                    // + ' WHERE ws.Service__c IN ' + wrkrPar + ' and u.StartDay <= "' + srvcresult[0].Appt_Date_Time__c.split(" ")[0] + '" and u.IsActive = 1 and s.isDeleted =0 GROUP BY name, sId, workername';
                    execute.query(dbName, srvGrpQuery, '', function (srvGrpErr, srvGrpData) {
                        if (srvGrpData && srvGrpData.length > 0) {
                            for (var i = 0; i < srvGrpData.length; i++) {
                                for (var j = 0; j < srvcresult.length; j++) {
                                    if (srvGrpData[i].serviceGroupName == srvcresult[j].serviceGroupName) {
                                        srvcresult[j].servList.push(srvGrpData[i]);
                                    }
                                }
                            }
                            indexParam++;
                            if (indexParam == 4) {
                                sendResponse(res, true, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            }
                        } else {
                            indexParam++;
                            if (indexParam == 4) {
                                sendResponse(res, true, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            }
                        }
                    });
                    execute.query(dbName, servicesQuery, '', function (srvcErr, srvcData) {
                        if (srvcData && srvcData.length > 0) {
                            for (var i = 0; i < srvcData.length; i++) {
                                for (var j = 0; j < srvcresult.length; j++) {
                                    if (srvcData[i].sId == srvcresult[j].Id) {
                                        srvcresult[j].workerList.push(srvcData[i]);
                                    }
                                }
                            }
                            indexParam++;
                            if (indexParam == 4) {
                                sendResponse(res, true, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            }
                        } else {
                            indexParam++;
                            if (indexParam == 4) {
                                sendResponse(res, true, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            }
                        }
                    });
                } else {
                    indexParam += 2;
                    if (indexParam == 4) {
                        sendResponse(res, true, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                    }
                }
                var apptSql = `SELECT 
                                 ts.Rebooked__c,ts.Resources__c,c.id as clientId,CAST(IFNULL(c.Current_Balance__c,0) AS CHAR) Current_Balance__c 
                                ,pref.JSON__c as SalesTax, IFNULL(GROUP_CONCAT(IF (ts.Booked_Package__c = "", NULL, ts.Booked_Package__c)),'') as Booked_Package__c,
                                apt.Id as apptId,apt.isTicket__c ,apt.Status__c as apstatus, apt.Name as ticketNumber, apt.Booked_Online__c, 
                                IFNULL(apt.Client_Type__c,'')as visttype, apt.Appt_Date_Time__c, CONCAT(c.FirstName, " ", c.LastName) as clientName,
                                c.Sms_Consent__c,IFNULL(c.Reminder_Mobile_Phone__c,"") Reminder_Mobile_Phone__c,
                                IFNULL(c.Reminder_Primary_Email__c,"") Reminder_Primary_Email__c,IFNULL(c.MobilePhone,"") as mbphone, 
                                IFNULL(apt.Notes__c,"") Notes__c, apt.Duration__c, IFNULL(c.No_Email__c,"") No_Email__c, 
                                IFNULL(c.Email,"") as cltemail, apt.New_Client__c as newclient, IFNULL(c.Phone,"") as cltphone, 
                                apt.Is_Standing_Appointment__c as standingappt, apt.Has_Booked_Package__c as pkgbooking, 
                                IFNULL(c.Client_Pic__c, "") as clientpic, apt.CreatedDate as creadate,apt.LastModifiedDate as lastmofdate, 
                                CONCAT(us.FirstName," ", us.LastName) CreatedBy,CONCAT(u.FirstName," ", u.LastName) LastModifiedBy,
                                CAST(IFNULL(apt.Client_Type__c,'')AS CHAR) Client_Type__c
                            FROM 
                                Appt_Ticket__c as apt 
                            LEFT JOIN Contact__c as c on c.Id = apt.Client__c and c.IsDeleted = 0 
                            LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c= apt.Id 
                            JOIN Preference__c as pref LEFT JOIN User__c as us on us.Id = apt.CreatedById 
                            LEFT JOIN User__c as u on u.Id = apt.LastModifiedById  WHERE pref.Name = "Sales Tax" 
                            AND apt.Id = '` + req.params.apptid + `' AND `
                if (cleintId && cleintId != 'null' && cleintId != 'noClientid') {
                    apptSql += ` apt.Client__c ='` + cleintId + `' And `
                }
                apptSql += ` apt.isDeleted =0 GROUP BY c.Id`;
                execute.query(dbName, apptSql, '', function (err, apptresult) {
                    if (err) {
                        logger.error('Error in appoitments dao - getExpressBookingServices:', err);
                    } else {
                        var sqlQuery = `SELECT s.Service_Group__c serviceGroupName FROM Service__c as s
                WHERE s.Is_Class__c = 0  AND s.IsDeleted = 0 AND s.Id IN
                    (SELECT ws.Service__c wsService FROM Worker_Service__c AS ws
                                        JOIN User__c as u on u.Id = ws.Worker__c and u.StartDay <= '` + srvcresult[0].Appt_Date_Time__c.split(" ")[0] + `' and u.IsActive = 1`
                        if (req.headers['type']) {
                            sqlQuery += ` AND ws.Self_Book__c = 1 `
                        }
                        sqlQuery += ` UNION
                            SELECT ts.Service__c wsService from Ticket_Service__c as ts WHERE ts.Appt_Ticket__c = '` + req.params.apptid + `')
                GROUP BY s.Service_Group__c`;
                        var srgpQuery = 'SELECT * FROM Preference__c WHERE Name = "Service Groups"';
                        var query = "SELECT * from Package__c where Active__c=" + 1 + ' and isDeleted=0';
                        execute.query(dbName, sqlQuery + ';' + srgpQuery + ';' + query, '', function (err, result) {
                            pckgResult = result[2];
                            pckgResult.forEach(element => {
                                if (element.Client_Facing_Name__c === null) {
                                    element.Client_Facing_Name__c = "";
                                }
                                element.Deposit_Amount__c = element.Deposit_Amount__c.toString();
                                element.Deposit_Required__c = element.Deposit_Required__c.toString();
                                if (element.Description__c === null) {
                                    element.Description__c = "";
                                }
                                element.Discounted_Package__c = element.Discounted_Package__c.toString();
                                element.Package_value_before_discounts__c = element.Package_value_before_discounts__c.toString();
                                element.Tax_Percent__c = element.Tax_Percent__c.toString();
                                element.Tax__c = element.Tax__c.toString();

                            });
                            if (result[1] && result[1].length > 0) {
                                var JSON__c_str = JSON.parse(result[1][0].JSON__c);
                                result[1][0].JSON__c = JSON__c_str.sort(function (a, b) {
                                    return a.sortOrder - b.sortOrder
                                });
                                var result = result[1][0].JSON__c.filter((obj) => (obj.active && !obj.isSystem) && result[0].findIndex((ser) => ser.serviceGroupName === obj.serviceGroupName) !== -1);
                                result.forEach(element => {
                                    if (element.isSystem === null) {
                                        element.isSystem = false;
                                    }
                                });
                                apptrst = apptresult;
                                srvgResult = result;
                                indexParam++;
                                sendResponse(res, true, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            } else {
                                logger.error('Error in SetupServiceGroup dao - getServiceGroups:', err);
                                sendResponse(res, false, '000');
                            }
                        });

                    }
                    if (apptresult && apptresult.length > 0) {
                        var nextserviceSql = 'SELECT ts.Id, s.Name, ap.Id, ap.Appt_Date_Time__c , GROUP_CONCAT(s.Name) serviceName' +
                            ' from Appt_Ticket__c ap' +
                            ' LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = ap.Id' +
                            ' LEFT JOIN Service__c as s on s.Id = ts.Service__c' +
                            ' WHERE ap.Client__c="' + cleintId + '" and  ap.Appt_Date_Time__c > "' + apptresult[0].Appt_Date_Time__c + '" and ap.isNoService__c = 0 and ap.Status__c != "CANCELED" ' +
                            ' GROUP BY ap.Id ORDER BY ap.Appt_Date_Time__c asc limit 1'
                        execute.query(dbName, nextserviceSql, '', function (err, nextappt) {
                            nextapptresult = nextappt;
                            if (err) {
                                logger.error('Error in appoitments dao - getExpressBookingServices:', err);
                            }
                            indexParam++;
                            if (indexParam == 4) {
                                sendResponse(res, true, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                            }
                        });
                    } else {
                        indexParam++;
                        if (indexParam == 4) {
                            sendResponse(res, true, { srvcresult, apptrst, nextapptresult, srvgResult, pckgResult });
                        }
                    }
                });
            } else {
                sendResponse(res, true, []);
            }
        });
    });
    app.get('/api/appointments/apptstatus', function (req, res) {
        var apptStatus = [
            { 'status': 'Called' },
            { 'status': 'Canceled' },
            { 'status': 'Conflicting' },
            { 'status': 'Confirmed' },
            { 'status': 'Booked' },
            { 'status': 'No Show' },
            { 'status': 'Reminder Sent' },
        ];
        sendResponse(res, true, apptStatus);
    });
    app.post('/api/appointment/skipBooking', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var date = new Date();
        var id = uniqid();
        var indexParm = 0;
        var expressBookingObj = req.body;
        var sglength = [];
        var colorCode = [];
        var records = [];
        var apptName;
        let taxSerValue;
        let taxSerValue1;
        var Client_Type;
        var apptDate1 = moment(expressBookingObj.bookingDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');
        var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
        execute.query(dbName, selectSql, '', function (err, result) {
            if (err) {
                sendResponse(res, false, result);
            } else {
                apptName = ('00000' + result[0].Name).slice(-6);
            }
            if (expressBookingObj.service[0].service.serviceTax) {
                const tax = expressBookingObj.service[0].service.serviceTax.split(':')[1];
                taxSerValue = tax.split(',')[0].replace(/"/g, '');
            } else {
                taxSerValue = 0;
            }
            var apptDate1 = new Date(expressBookingObj.bookingDate);
            if (expressBookingObj.visitType && expressBookingObj.visitType.length > 0) {
                Client_Type = expressBookingObj.visitType;
            } else {
                Client_Type = "";
            }
            var apptObjData = {
                Id: id,
                OwnerId: loginId,
                IsDeleted: 0,
                Name: apptName,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                Appt_Date_Time__c: apptDate1,
                Client_Type__c: Client_Type,
                Client__c: null,
                Duration__c: expressBookingObj.duration,
                Status__c: 'Booked',
                Is_Booked_Out__c: 0,
                New_Client__c: 0,
                Notes__c: expressBookingObj.textArea,
                Service_Tax__c: expressBookingObj.totalServiceTax,
                Service_Sales__c: expressBookingObj.totalPrice
            }
            var insertQuery = 'INSERT INTO Appt_Ticket__c SET ?';
            execute.query(dbName, insertQuery, apptObjData, function (error, data2) {
                if (error) {
                    // logger.error('Error in getting exprsssbokking1: ', error);
                    sendResponse(res, false, data2);
                } else {
                    if (data2 && data2.affectedRows > 0) {
                        var servicePriceAll = 0;
                        var workerAndServiceDuration1 = 0;
                        var workerAndServiceDuration2 = 0;
                        var workerAndServiceDuration3 = 0;
                        var sumOfAllDuration = 0;
                        for (var i = 0; i < expressBookingObj.service.length; i++) {
                            if (expressBookingObj.service[i].service.Price__c === null || expressBookingObj.service[i].service.Price__c === "") {
                                servicePriceAll = expressBookingObj.service[i].service.pcsergrp;
                            } else {
                                servicePriceAll = expressBookingObj.service[i].service.Price__c;
                            }
                            if ((expressBookingObj.service[i].service.workerDuration1 === null || expressBookingObj.service[i].service.workerDuration1 === 0) &&
                                (expressBookingObj.service[i].service.workerDuration2 === null || expressBookingObj.service[i].service.workerDuration2 === 0) &&
                                (expressBookingObj.service[i].service.workerDuration3 === null || expressBookingObj.service[i].service.workerDuration3 === 0)) {
                                workerAndServiceDuration1 = expressBookingObj.service[i].service.Duration_1__c;
                                workerAndServiceDuration2 = expressBookingObj.service[i].service.Duration_2__c;
                                workerAndServiceDuration3 = expressBookingObj.service[i].service.Duration_3__c;
                            } else {
                                workerAndServiceDuration1 = expressBookingObj.service[i].service.workerDuration1;
                                workerAndServiceDuration2 = expressBookingObj.service[i].service.workerDuration2;
                                workerAndServiceDuration3 = expressBookingObj.service[i].service.workerDuration3;
                            }

                            if (expressBookingObj.service[i].service.sumDurationBuffer === null || expressBookingObj.service[i].service.sumDurationBuffer === 0) {
                                sumOfAllDuration = expressBookingObj.service[i].service.dursergrp;
                            } else {
                                sumOfAllDuration = expressBookingObj.service[i].service.sumDurationBuffer;
                            }
                            var avail1 = expressBookingObj.service[i].service.avaiable1;
                            var avail2 = expressBookingObj.service[i].service.avaiable2;
                            var avail3 = expressBookingObj.service[i].service.avaiable3;
                            var buffer = expressBookingObj.service[i].service.Buffer_after__c;

                            records.push([
                                uniqid(),
                                0,
                                dateTime, loginId,
                                dateTime, loginId,
                                dateTime,
                                apptObjData.Id,
                                apptObjData.Client_Type__c,
                                null,
                                expressBookingObj.service[i].worker, // worker 
                                apptDate1,
                                expressBookingObj.service[i].service.color, // service group color
                                workerAndServiceDuration1, // duration 1
                                workerAndServiceDuration2, // duration 2
                                workerAndServiceDuration3, // duration 3
                                sumOfAllDuration, // sum of duations
                                0,
                                servicePriceAll, // net price
                                servicePriceAll, // price
                                0,
                                0,
                                expressBookingObj.service[i].service.serviceId,
                                // expressBookingObj.textArea,
                                'Booked',
                                avail1 ? avail1 : 0,
                                avail2 ? avail2 : 0,
                                avail3 ? avail3 : 0,
                                buffer ? buffer : 0,
                                expressBookingObj.service[i].service.Taxable__c,
                                expressBookingObj.service[i].service.Service_Tax__c,
                                expressBookingObj.service[i].service.resName
                            ]);
                            if (expressBookingObj.service[i].service.sumDurationBuffer) {

                                apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);

                            } else {
                                apptDate1 = new Date(apptDate1.getTime() + expressBookingObj.service[i].service.sumDurationBuffer * 60000);
                            }
                            var insertQuery1 = 'INSERT INTO Ticket_Service__c' +
                                ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                                ' Worker__c, Service_Date_Time__c,Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c,' +
                                ' Is_Booked_Out__c,Price__c,Net_Price__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Status__c,' +
                                ' Duration_1_Available_for_Other_Work__c,Duration_2_Available_for_other_Work__c, ' +
                                ' Duration_3_Available_for_other_Work__c,Buffer_After__c,Taxable__c,Service_Tax__c,Resources__c) VALUES ?';
                        }
                        execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                            if (err1) {
                                logger.error('Error in express1 dao - updateExpress:', err1);
                                sendResponse(res, false, result1);

                            } else {
                                sendResponseWithMsg(res, true, apptObjData.Id, "Appointment booked succesfully");
                            }
                        });
                    } else {
                        sendResponse(res, false, result1);
                    }
                }
            });
        });

    });
    app.get('/api/appointment/skipBooking', function (req, res) {
        var createJson = {
            "bookingDate": "",
            "textArea": "",
            "duration": 0,
            "visitType": "",
            "service": [{
                "service": {
                    "pcsergrp": 0.0,
                    "dursergrp": 0,
                    "serviceId": "",
                    "Taxable__c": 0,
                    "Price__c": 0,
                    "workerDuration1": 0,
                    "serviceDuration1": 0,
                    "workerDuration2": 0,
                    "serviceDuration2": 0,
                    "workerDuration3": 0,
                    "serviceDuration3": 0,
                    "Guest_Charge__c": '0',
                    "sumDurationBuffer": 0,
                    "Duration_1__c": 0,
                    "Duration_2__c": 0,
                    "Duration_3__c": 0,
                    "avaiable1": 0,
                    "avaiable2": 0,
                    "avaiable3": 0,
                    "Buffer_after__c": 0,
                    "color": "",
                    "Service_Tax__c": 0.0,
                    "resName": ""
                },
                "worker": ""
            }],
            "totalServiceTax": 0.0,
            "totalPrice": 0.0,
        }
        sendResponse(res, true, createJson);
    });
}

function getDursrvData(i, dbName, inputParms, callback) {
    if (i < inputParms.service_id.length) {
        var sql = 'SELECT IFNULL(Duration_1__c, 0) Duration_1__c, IFNULL(Duration_2__c, 0) Duration_2__c, IFNULL(Duration_3__c, 0) Duration_3__c, IFNULL(Buffer_After__c, 0) Buffer_After__c, Duration_1_Available_for_Other_Work__c, Duration_2_Available_for_Other_Work__c, Duration_3_Available_for_Other_Work__c, Price__c FROM Worker_Service__c WHERE Worker__c = "' + inputParms.worker_id[i] + '" and Service__c = "' + inputParms.service_id[i] + '" and IsDeleted=0';
        var sql1 = 'SELECT IFNULL(Book_Every__c, 0) Book_Every__c, Service_Level__c FROM User__c WHERE Id = "' + inputParms.worker_id[i] + '"';
        var sql2 = 'SELECT Levels__c, Price__c FROM Service__c WHERE Id = "' + inputParms.service_id[i] + '" and IsDeleted=0';
        execute.query(dbName, sql, '', function (err, data) {
            if (err) {
                logger.error('Error in getApptPrf: ', err);
            } else {
                execute.query(dbName, sql1, '', function (err, userData) {
                    execute.query(dbName, sql2, '', function (err, durations) {
                        var temp = 0;
                        var priceTemp = 0;
                        var levels = JSON.parse(durations[0].Levels__c)
                        var sql3 = 'SELECT Guest_Charge__c, Taxable__c, Service_Group__c FROM Service__c WHERE Id = "' + inputParms.service_id[i] + '" and IsDeleted=0';
                        execute.query(dbName, sql3, '', function (err, serviceData) {
                            var sql4 = 'SELECT JSON__c FROM Preference__c WHERE IsDeleted = 0 AND Name = "Service Groups"';
                            execute.query(dbName, sql4, '', function (err1, serviceColor) {
                                var Service_Tax__c;
                                var sql5 = 'SELECT JSON__c, Name FROM Preference__c WHERE (Name = "Sales Tax") and IsDeleted=0';
                                execute.query(dbName, sql5, '', function (err, result) {
                                    const serviceTax = JSON.parse(result[0].JSON__c);
                                    if (serviceData[0].Taxable__c == 0) {
                                        Service_Tax__c = 0;
                                    } else {
                                        if (data[0].Price__c) {
                                            Service_Tax__c = data[0].Price__c * serviceTax.serviceTax / 100;
                                        } else {
                                            Service_Tax__c = durations[0].Price__c * serviceTax.serviceTax / 100;
                                        }
                                    }
                                    var prefeData = JSON.parse(serviceColor[0].JSON__c);
                                    var colorData = prefeData.filter(function (color) {
                                        if (color.serviceGroupName == serviceData[0].Service_Group__c) {
                                            return color;
                                        }
                                    });
                                    var sql6 = 'SELECT Package_Details__c FROM Client_Package__c WHERE Client__c ="' + inputParms.Client__c + '"';
                                    execute.query(dbName, sql6, function (error, packageData) {
                                        var isPackeged;
                                        var bookedPackagec;
                                        if (packageData.length > 0) {
                                            for (let i = 0; i < packageData.length; i++) {
                                                var packageDetails = JSON.parse(packageData[i].Package_Details__c);
                                                packageDetails.filter(function (service) {
                                                    if (service.serviceId == inputParms.service_id[i] && service.used < +service.reps) {
                                                        bookedPackagec = inputParms.Client__c;
                                                        isPackeged = 1;
                                                    } else {
                                                        bookedPackagec = "";
                                                        isPackeged = 0;
                                                    }
                                                });
                                            }
                                        } else {
                                            bookedPackagec = "";
                                            isPackeged = 0;
                                        }
                                        getExpressBookingServices(dbName, inputParms.worker_id[i], inputParms.service_id[i], function (resourceData) {
                                            var resource = resourceData;
                                            var workerPrice = data[0].Price__c;
                                            if (data[0].Price__c === null || !data[0].Price__c) {
                                                levels.filter(function (level) {
                                                    if (userData[0].Service_Level__c == level.levelNumber) {
                                                        priceTemp++;
                                                        workerPrice = level.price;
                                                    }
                                                });
                                                if (priceTemp === 0) {
                                                    workerPrice = durations[0].Price__c
                                                }
                                            }
                                            var sql7 = `select * from Ticket_Service__c
                                            where Client__c = "` + inputParms.Client__c + `" and Service__c = "` + inputParms.service_id[i] + `"
                                            and Worker__c = "` + inputParms.worker_id[i] + `" and Preferred_Duration__c = "1"`;
                                            execute.query(dbName, sql7, function (error7, prefDuration) {
                                                if (prefDuration.length > 0) {
                                                    inputParms.durations.push(prefDuration[0].Buffer_After__c + prefDuration[0].Duration_1__c +
                                                        prefDuration[0].Duration_2__c + prefDuration[0].Duration_3__c);
                                                    inputParms.serviceData.push({
                                                        'Duration_1__c': prefDuration[0].Duration_1__c,
                                                        'Duration_2__c': prefDuration[0].Duration_2__c,
                                                        'Duration_3__c': prefDuration[0].Duration_3__c,
                                                        'Buffer_After__c': prefDuration[0].Buffer_After__c,
                                                        'Duration_1_Available_for_Other_Work__c': prefDuration[0].Duration_1_Available_for_Other_Work__c,
                                                        'Duration_2_Available_for_Other_Work__c': prefDuration[0].Duration_2_Available_for_Other_Work__c,
                                                        'Duration_3_Available_for_Other_Work__c': prefDuration[0].Duration_3_Available_for_Other_Work__c,
                                                        'Price': workerPrice,
                                                        'Guest_Charge__c': serviceData[0].Guest_Charge__c,
                                                        'Taxable__c': serviceData[0].Taxable__c,
                                                        'serviceGroup': serviceData[0].Service_Group__c,
                                                        'color': colorData[0].serviceGroupColor,
                                                        'Service_Tax__c': Service_Tax__c,
                                                        'Resources__c': resource,
                                                        'Booked_Package__c': bookedPackagec,
                                                        'IsPackage': isPackeged
                                                    });
                                                } else if (data[0].Buffer_After__c != 0 || data[0].Duration_1__c != 0 ||
                                                    data[0].Duration_2__c != 0 || data[0].Duration_3__c != 0) {
                                                    inputParms.durations.push(data[0].Buffer_After__c + data[0].Duration_1__c +
                                                        data[0].Duration_2__c + data[0].Duration_3__c);
                                                    inputParms.serviceData.push({
                                                        'Duration_1__c': data[0].Duration_1__c,
                                                        'Duration_2__c': data[0].Duration_2__c,
                                                        'Duration_3__c': data[0].Duration_3__c,
                                                        'Buffer_After__c': data[0].Buffer_After__c,
                                                        'Duration_1_Available_for_Other_Work__c': data[0].Duration_1_Available_for_Other_Work__c,
                                                        'Duration_2_Available_for_Other_Work__c': data[0].Duration_2_Available_for_Other_Work__c,
                                                        'Duration_3_Available_for_Other_Work__c': data[0].Duration_3_Available_for_Other_Work__c,
                                                        'Price': workerPrice,
                                                        'Guest_Charge__c': serviceData[0].Guest_Charge__c,
                                                        'Taxable__c': serviceData[0].Taxable__c,
                                                        'serviceGroup': serviceData[0].Service_Group__c,
                                                        'color': colorData[0].serviceGroupColor,
                                                        'Service_Tax__c': Service_Tax__c,
                                                        'Resources__c': resource,
                                                        'Booked_Package__c': bookedPackagec,
                                                        'IsPackage': isPackeged
                                                    });
                                                } else {
                                                    levels.filter(function (level) {
                                                        if (userData[0].Service_Level__c == level.levelNumber) {
                                                            temp++;
                                                            inputParms.durations.push(level.totalDuration);
                                                            inputParms.serviceData.push({
                                                                'Duration_1__c': level.duration1,
                                                                'Duration_2__c': level.duration2,
                                                                'Duration_3__c': level.duration3,
                                                                'Buffer_After__c': level.bufferAfter,
                                                                'Duration_1_Available_for_Other_Work__c': level.duration1AvailableForOtherWork,
                                                                'Duration_2_Available_for_Other_Work__c': level.duration2AvailableForOtherWork,
                                                                'Duration_3_Available_for_Other_Work__c': level.duration3AvailableForOtherWork,
                                                                'Price': workerPrice,
                                                                'Guest_Charge__c': serviceData[0].Guest_Charge__c,
                                                                'Taxable__c': serviceData[0].Taxable__c,
                                                                'serviceGroup': serviceData[0].Service_Group__c,
                                                                'color': colorData[0].serviceGroupColor,
                                                                'Service_Tax__c': Service_Tax__c,
                                                                'Resources__c': resource,
                                                                'Booked_Package__c': bookedPackagec,
                                                                'IsPackage': isPackeged
                                                            });
                                                        }
                                                    });
                                                    if (temp === 0) {
                                                        inputParms.durations.push(levels[0].totalDuration);
                                                        inputParms.serviceData.push({
                                                            'Duration_1__c': levels[0].duration1,
                                                            'Duration_2__c': levels[0].duration2,
                                                            'Duration_3__c': levels[0].duration3,
                                                            'Buffer_After__c': levels[0].bufferAfter,
                                                            'Duration_1_Available_for_Other_Work__c': levels[0].duration1AvailableForOtherWork,
                                                            'Duration_2_Available_for_Other_Work__c': levels[0].duration2AvailableForOtherWork,
                                                            'Duration_3_Available_for_Other_Work__c': levels[0].duration3AvailableForOtherWork,
                                                            'Price': workerPrice,
                                                            'Guest_Charge__c': serviceData[0].Guest_Charge__c,
                                                            'Taxable__c': serviceData[0].Taxable__c,
                                                            'serviceGroup': serviceData[0].Service_Group__c,
                                                            'color': colorData[0].serviceGroupColor,
                                                            'Service_Tax__c': Service_Tax__c,
                                                            'Resources__c': resource,
                                                            'Booked_Package__c': bookedPackagec,
                                                            'IsPackage': isPackeged
                                                        });
                                                    }
                                                }
                                                getDursrvData(i + 1, dbName, inputParms, callback);
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            }
        });
    } else {
        inputParms.duration_c = 0;
        inputParms.Service_Sales__c = 0;
        inputParms.Service_Tax__c = 0;
        for (let i = 0; i < inputParms.durations.length; i++) {
            inputParms.Duration__c = inputParms.duration_c + inputParms.durations[i];
            inputParms.Service_Sales__c = inputParms.Service_Sales__c + inputParms.serviceData[i].Price;
            inputParms.serviceData[i].Duration__c = inputParms.durations[i];
            inputParms.serviceData[i].Rebooked__c = 0;
            inputParms.serviceData[i].serviceId = inputParms.service_id[i];
            inputParms.serviceData[i].workerName = inputParms.worker_id[i];
            inputParms.serviceData[i].serviceName = inputParms.service_id[i];
            inputParms.Service_Tax__c = inputParms.Service_Tax__c + inputParms.serviceData[i].Service_Tax__c;
            if (inputParms.serviceData[i].IsPackage > 0) {
                inputParms.IsPackage = 1;
            } else {
                inputParms.IsPackage = 0;
            }
        }
        callback(inputParms);
    }
}

function getExpressBookingServices(dbName, worker_id, service_id, callback) {
    var resources;
    var sql = ' SELECT GROUP_CONCAT(IFNULL(sr.Priority__c,0)) as priority,' +
        ' IFNULL(GROUP_CONCAT(r.Name," #1" ),"#1") as resourceName,' +
        ' IFNULL(ws.Price__c,0) as Price__c ,' +
        ' IFNULL(s.Buffer_After__c,0) as serviceBuffer, IFNULL(ws.Duration_1__c,0) as workerDuration1, ' +
        ' IFNULL(s.Duration_1__c,0) as serviceDuration1, IFNULL(ws.Duration_2__c,0) as workerDuration2, ' +
        ' IFNULL(s.Duration_2__c,0) as serviceDuration2, IFNULL(ws.Duration_3__c,0) as workerDuration3, ' +
        ' IFNULL(s.Duration_3__c,0) as serviceDuration3, ws.Duration_1_Available_for_Other_Work__c as workerAvail1,' +
        ' ws.Duration_2_Available_for_Other_Work__c as workerAvail2,' +
        ' ws.Duration_3_Available_for_Other_Work__c as workerAvail3,' +
        ' (IFNULL(ws.Duration_1__c,0)+IFNULL(ws.Duration_2__c,0)+IFNULL(ws.Duration_3__c,0)+IFNULL(ws.Buffer_After__c,0)) as sumDurationBuffer ' +
        ' FROM Worker_Service__c as ws' +
        ' left join Service__c as s on ws.Service__c = s.Id ' +
        ' left join Service_Resource__c as sr on sr.Service__c = s.Id ' +
        ' left join Resource__c as r on sr.Resource__c = r.Id ' +
        ' and r.IsDeleted =0 ' +
        ' and s.IsDeleted = 0 ' +
        ' and sr.IsDeleted = 0 ' +
        ' and ws.IsDeleted= 0 ' +
        ' where ws.Worker__c = "' + worker_id + '" ' +
        ' and ws.Service__c = "' + service_id + '" ' +
        ' and s.Is_Class__c=0 ' +
        ' GROUP by s.Id order by s.Service_Group__c, s.Name;'
    execute.query(dbName, sql, '', function (err, data) {
        if (err) {
            logger.error('Error in appoitments dao - getExpressBookingServices:', err);
        } else {
            if (data.length > 0) {
                resources = data[0].resourceName.replace(/#1/g, '');
                callback(resources);
            }
        }
    });
}

function apptbookingMethod(index, req, res, loginId, date, dbName, appointmentbookingObj) {
    if (index == 1) {
        var apptServicesData = appointmentbookingObj.serviceData;
        var id = uniqid();
        var serviceQuery = '';
        var apptName;
        var newClient = 0;
        var apptrebok;
        var records = [];
        var updaterecords = [];
        var queries = '';
        moment.suppressDeprecationWarnings = true;
        var rebookdata;
        var indexParm = 0;
        var Rebooked__c = 0;
        if (appointmentbookingObj.isDepositRequired) {
            var stastus = 'Pending Deposit';
        } else {
            var stastus = 'Booked';
        }
        var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
        var sqlnewClient = 'SELECT New_Client__c FROM `Appt_Ticket__c` WHERE Client__c="' + appointmentbookingObj.Client__c + '" ';
        if (appointmentbookingObj.bookingType === 'findappt') {
            isRebookedService(dbName, appointmentbookingObj.Client__c, appointmentbookingObj.apptCreatedDate, function (err, redata) {
                rebookdata = redata;
                var apptrebokstart
                var rebtime = dateFns.getDateTmFrmDBDateStr(appointmentbookingObj.apptCreatedDate);
                for (var i = 0; i < redata.length; i++) {
                    apptrebokstart = dateFns.addMinToDBStr(redata[i].Appt_Date_Time__c, parseInt(redata[i].Duration__c));
                    apptrebok = dateFns.addMinToDBStr(redata[i].Appt_Date_Time__c, parseInt(redata[i].Duration__c) + 1440);
                    apptrebok = dateFns.getDateFrmDBDateStr(apptrebok);
                    apptrebokstart = dateFns.getDateFrmDBDateStr(apptrebokstart);
                    if (rebtime.getTime() >= apptrebokstart.getTime() && rebtime.getTime() <= apptrebok.getTime()) {
                        Rebooked__c = 1;
                        break;
                    }
                }
            });
        }
        execute.query(dbName, selectSql, '', function (err, result) {
            if (err) {
                sendResponse(res, false, '000');
            } else {
                apptName = ('00000' + result[0].Name).slice(-6);
            }
            execute.query(dbName, sqlnewClient, '', function (err, result) {
                if (result.length === 0) {
                    newClient = 1;
                }
                if (appointmentbookingObj && (appointmentbookingObj.apptId === '' || !appointmentbookingObj.apptId)) {
                    var apptDate = appointmentbookingObj.Appt_Date_Time__c;
                    var apptObjData = {
                        Id: id,
                        OwnerId: loginId,
                        IsDeleted: 0,
                        Name: apptName,
                        CreatedDate: date,
                        CreatedById: loginId,
                        LastModifiedDate: date,
                        LastModifiedById: loginId,
                        SystemModstamp: date,
                        LastModifiedDate: date,
                        Appt_Date_Time__c: appointmentbookingObj.Appt_Date_Time__c,
                        Client_Type__c: appointmentbookingObj.Client_Type__c,
                        Client__c: appointmentbookingObj.Client__c,
                        Duration__c: appointmentbookingObj.Duration__c,
                        Status__c: stastus,
                        Service_Tax__c: appointmentbookingObj.Service_Tax__c,
                        Service_Sales__c: appointmentbookingObj.Service_Sales__c,
                        New_Client__c: newClient,
                        Is_Booked_Out__c: 0,
                        Is_Standing_Appointment__c: 0,
                        Reminder_Requested__c: 0,
                        isNoService__c: 0,
                        isRefund__c: 0,
                        isTicket__c: 0,
                        Payments__c: 0.00,
                        Notes__c: appointmentbookingObj.Service_notes,
                        Has_Booked_Package__c: appointmentbookingObj.IsPackage,
                        Booked_Online__c: appointmentbookingObj.Booked_Online__c ? 1 : 0,
                        Rebooked_Rollup_Max__c: Rebooked__c,
                        Business_Rebook__c: Rebooked__c
                    };
                    var insertQuery = 'INSERT INTO Appt_Ticket__c SET ?';
                    serviceQuery += `SELECT ts.Service_Date_Time__c 
                    FROM Ticket_Service__c ts
                    LEFT JOIN Appt_Ticket__c a on a.Id = ts.Appt_Ticket__c
                    WHERE a.Status__c NOT IN ('Canceled') AND ts.isDeleted =0 AND (`;
                    for (var i = 0; i < apptServicesData.length; i++) {
                        var serviceStart = apptDate;
                        var serviceEnd = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration_1__c, 10));
                        apptServicesData[i]['wrkRebooked'] = 0;
                        if (rebookdata && rebookdata.length > 0) {
                            for (var j = 0; j < rebookdata.length; j++) {
                                if (apptServicesData[i].workerName === rebookdata[j]['Worker__c']) {
                                    apptServicesData[i]['wrkRebooked'] = 1;
                                }
                            }
                        }
                        if (!apptServicesData[i].Duration_1__c) {
                            apptServicesData[i].Duration_1__c = 0;
                        }
                        if (!apptServicesData[i].Duration_2__c) {
                            apptServicesData[i].Duration_2__c = 0;
                        }
                        if (!apptServicesData[i].Duration_3__c) {
                            apptServicesData[i].Duration_3__c = 0;
                        }
                        if (!apptServicesData[i].Guest_Charge__c) {
                            apptServicesData[i].Guest_Charge__c = 0;
                        }
                        if (!apptServicesData[i].Buffer_After__c) {
                            apptServicesData[i].Buffer_After__c = 0;
                        }
                        if (!apptServicesData[i].Duration__c) {
                            apptServicesData[i].Duration__c = parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10);
                        }
                        records.push([uniqid(),
                            0,
                            date, loginId,
                            date, loginId,
                            date,
                        apptObjData.Id,
                        appointmentbookingObj.Client_Type__c,
                        appointmentbookingObj.Client__c,
                        apptServicesData[i].workerName,
                            apptDate,
                            'Booked',
                        apptServicesData[i].color,
                        apptServicesData[i].Duration_1__c,
                        apptServicesData[i].Duration_2__c,
                        apptServicesData[i].Duration_3__c,
                        apptServicesData[i].Duration__c,
                        apptServicesData[i].Buffer_After__c,
                        apptServicesData[i].Guest_Charge__c,
                        apptServicesData[i].Duration_1_Available_for_Other_Work__c,
                        apptServicesData[i].Duration_2_Available_for_Other_Work__c,
                        apptServicesData[i].Duration_3_Available_for_Other_Work__c,
                        apptServicesData[i].Preferred_Duration__c,
                        apptServicesData[i].Service_Tax__c,
                            0,
                        apptServicesData[i].Price,
                        apptServicesData[i].Price,
                        apptServicesData[i].Taxable__c,
                            0,
                        apptServicesData[i]['wrkRebooked'],
                        apptServicesData[i].serviceId,
                        apptServicesData[i].Booked_Package__c,
                            '',
                        apptServicesData[i].Resources__c,

                        ]);
                        if (parseInt(apptServicesData[i].Duration_1__c, 10) > 0 && !apptServicesData[i].Duration_1_Available_for_Other_Work__c) {
                            serviceQuery += `(ts.Worker__c = '` + apptServicesData[i].workerName + `' AND 
                            (
                                (
                                    ((ts.Service_Date_Time__c >= '` + serviceStart + `' && ts.Service_Date_Time__c < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                    OR (ts.Service_Date_Time__c < '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceEnd + `')
                                    )
                                    AND ts.Duration_1_Available_for_other_Work__c != 1
                                ) OR
                                (
                                    ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceEnd + `')
                                    )
                                    AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1)
                                ) OR
                                (
                                    ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceEnd + `')
                                    )
                                    AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1 && ts.Duration_3_Available_for_other_Work__c != 1)
                                )
                            )
                        ) OR `;
                        }
                        serviceStart = serviceEnd
                        serviceEnd = dateFns.addMinToDBStr(serviceStart, parseInt(apptServicesData[i].Duration_2__c, 10));
                        if (parseInt(apptServicesData[i].Duration_2__c, 10) > 0 && !apptServicesData[i].Duration_2_Available_for_Other_Work__c) {
                            serviceQuery += `(ts.Worker__c = '` + apptServicesData[i].workerName + `' AND 
                            (
                                (
                                    ((ts.Service_Date_Time__c >= '` + serviceStart + `' && ts.Service_Date_Time__c < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                    OR (ts.Service_Date_Time__c < '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceEnd + `')
                                    )
                                    AND ts.Duration_1_Available_for_other_Work__c != 1
                                ) OR
                                (
                                    ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceEnd + `')
                                    )
                                    AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1)
                                ) OR
                                (
                                    ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceEnd + `')
                                    )
                                    AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1 && ts.Duration_3_Available_for_other_Work__c != 1)
                                )
                            )
                        ) OR `;
                        }
                        serviceStart = serviceEnd
                        serviceEnd = dateFns.addMinToDBStr(serviceStart, parseInt(apptServicesData[i].Duration_3__c, 10));
                        if (parseInt(apptServicesData[i].Duration_3__c, 10) > 0 && !apptServicesData[i].Duration_3_Available_for_Other_Work__c) {
                            serviceQuery += `(ts.Worker__c = '` + apptServicesData[i].workerName + `' AND 
                            (
                                (
                                    ((ts.Service_Date_Time__c >= '` + serviceStart + `' && ts.Service_Date_Time__c < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                    OR (ts.Service_Date_Time__c < '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceEnd + `')
                                    )
                                    AND ts.Duration_1_Available_for_other_Work__c != 1
                                ) OR
                                (
                                    ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c MINUTE) < '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceEnd + `')
                                    )
                                    AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1)
                                ) OR
                                (
                                    ((DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) > '` + serviceStart + `' && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) < '` + serviceEnd + `')
                                    OR (DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c MINUTE) < '` + serviceStart + `' 
                                        && DATE_ADD(ts.Service_Date_Time__c, INTERVAL ts.Duration_1__c + ts.Duration_2__c + ts.Duration_3__c MINUTE) > '` + serviceEnd + `')
                                    )
                                    AND  (ts.Duration_1_Available_for_other_Work__c != 1 && ts.Duration_2_Available_for_other_Work__c != 1 && ts.Duration_3_Available_for_other_Work__c != 1)
                                )
                            )
                        ) OR `;
                        }
                        if (apptServicesData[i].Duration__c) {
                            apptDate = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration__c, 10));
                        } else {
                            apptDate = dateFns.addMinToDBStr(apptDate, parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10));
                        }

                    }
                    if (serviceQuery && serviceQuery.length > 236) {
                        serviceQuery = serviceQuery.slice(0, -4) + ')';
                    } else {
                        serviceQuery = serviceQuery.slice(0, -5);
                    }
                    var insertQuery1 = 'INSERT INTO Ticket_Service__c ' +
                        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                        ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                        ' Worker__c, Service_Date_Time__c, Status__c, Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c, Buffer_After__c, Guest_Charge__c, ' +
                        ' Duration_1_Available_for_Other_Work__c,Duration_2_Available_for_Other_Work__c,Duration_3_Available_for_Other_Work__c,Preferred_Duration__c,Service_Tax__c,' +
                        ' Is_Booked_Out__c, Net_Price__c,Price__c,Taxable__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Booked_Package__c, Notes__c, Resources__c) VALUES ?';
                    if (serviceQuery && serviceQuery.length > 235) {
                        execute.query(dbName, serviceQuery, '', function (err, srvcresult) {
                            if (srvcresult.length === 0 || appointmentbookingObj.bookingType === 'newappt') {
                                execute.query(dbName, insertQuery, apptObjData, function (err, result) {
                                    if (err) {
                                        logger.error('Error in WorkerServices dao - updateWorkerService:', err);
                                        sendResponse(res, false, '000');
                                    } else {
                                        var clientUpdate = "UPDATE Contact__c SET Active__c = 1 WHERE Id='" + appointmentbookingObj.Client__c + "';";
                                        if (appointmentbookingObj && appointmentbookingObj.waitingId) {
                                            clientUpdate += " DELETE FROM Waiting_List__c WHERE Id = '" + appointmentbookingObj.waitingId + "'"
                                        }
                                        execute.query(dbName, clientUpdate, '', function (err, result) {
                                            execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                                if (err1) {
                                                    logger.error('Error in WorkerServices dao - updateWorkerService:', err1);
                                                    sendResponse(res, false, '000');
                                                } else {
                                                    indexParm++;
                                                    if (indexParm === 1) {
                                                        sendResponseWithMsg(res, true, apptObjData.Id, "Appointment booked successfully");
                                                    }
                                                }
                                            });
                                        });
                                    }
                                });
                            } else {
                                sendResponse(res, false, '008');
                            }
                        });
                    } else {
                        execute.query(dbName, insertQuery, apptObjData, function (err, result) {
                            if (err) {
                                logger.error('Error in WorkerServices dao - updateWorkerService:', err);
                                sendResponse(res, false, '000');
                            } else {
                                var clientUpdate = "UPDATE Contact__c SET Active__c = 1 WHERE Id='" + appointmentbookingObj.Client__c + "';";
                                if (appointmentbookingObj && appointmentbookingObj.waitingId) {
                                    clientUpdate += " DELETE FROM Waiting_List__c WHERE Id = '" + appointmentbookingObj.waitingId + "'"
                                }
                                execute.query(dbName, clientUpdate, '', function (err, result) {
                                    execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                        if (err1) {
                                            logger.error('Error in WorkerServices dao - updateWorkerService:', err1);
                                            sendResponse(res, false, '000');
                                        } else {
                                            indexParm++;
                                            if (indexParm === 1) {
                                                sendResponseWithMsg(res, true, apptObjData.Id, "Appointment booked successfully");
                                            }
                                        }
                                    });
                                });
                            }
                        });
                    }
                } else {
                    var sql = 'SELECT Status__c FROM Appt_Ticket__c WHERE Id = "' + appointmentbookingObj.apptId + '"';
                    execute.query(dbName, sql, '', function (err, apptData) {
                        appointmentbookingObj.Status__c = apptData[0].Status__c;
                        var apptStatus;
                        if (appointmentbookingObj.Status__c === 'No Show') {
                            apptStatus = 'Booked';
                        } else {
                            apptStatus = appointmentbookingObj.Status__c;
                        }
                        var apptDate1 = appointmentbookingObj.Appt_Date_Time__c;
                        var updateQuery = "UPDATE Appt_Ticket__c" +
                            " SET Appt_Date_Time__c = '" + apptDate1 +
                            "', Client_Type__c = '" + appointmentbookingObj.Client_Type__c +
                            "', Client__c = '" + appointmentbookingObj.Client__c +
                            "', Duration__c = '" + appointmentbookingObj.Duration__c +
                            "', Status__c = '" + apptStatus +
                            "', Service_Tax__c= '" + appointmentbookingObj.Service_Tax__c +
                            "', Service_Sales__c= '" + appointmentbookingObj.Service_Sales__c +
                            "', LastModifiedDate = '" + date +
                            "', LastModifiedById = '" + loginId +
                            "', Has_Booked_Package__c = '" + appointmentbookingObj.IsPackage +
                            "' WHERE Id = '" + appointmentbookingObj.apptId + "'";
                        execute.query(dbName, updateQuery, '', function (err, result) {
                            if (err) {
                                logger.error('Error in WorkerServices dao - updateWorkerService:', err);
                                indexParm++;
                                if (indexParm === 2) {
                                    sendResponseWithMsg(res, true, { 'apptId': result }, "Appointment modified successfully");
                                    // done(err, { 'apptId': result });
                                }
                            } else {
                                if (req.body.deletedTsids.length > 0) {
                                    for (var i = 0; i < req.body.deletedTsids.length; i++) {
                                        queries += mysql.format('UPDATE Ticket_Service__c' +
                                            ' SET IsDeleted = 1' +
                                            ', LastModifiedDate = "' + date +
                                            '", LastModifiedById = "' + loginId +
                                            '" WHERE Id = "' + req.body.deletedTsids[i] + '";');
                                    }
                                }
                                if (apptServicesData.length > 0) {
                                    for (var i = 0; i < apptServicesData.length; i++) {
                                        // var sqlTicket = 'SELECT Id FROM Ticket_Service__c WHERE Service__c = "'+ appointmentbookingObj.service_id[i]+'" and Worker__c = "'+ appointmentbookingObj.worker_id[i]+'"';
                                        if (appointmentbookingObj.ts_id[i] && appointmentbookingObj.apptId) {
                                            var tsStatus;
                                            if (appointmentbookingObj.Status__c === 'No Show') {
                                                tsStatus = 'Booked';
                                            } else {
                                                tsStatus = appointmentbookingObj.Status__c;
                                            }
                                            var Rebooked = apptServicesData[i].Rebooked__c ? apptServicesData[i].Rebooked__c : 0;
                                            var avail1 = apptServicesData[i].Duration_1_Available_for_Other_Work__c ? 1 : 0;
                                            var avail2 = apptServicesData[i].Duration_2_Available_for_Other_Work__c ? 1 : 0;
                                            var avail3 = apptServicesData[i].Duration_3_Available_for_Other_Work__c ? 1 : 0;
                                            var avail4 = apptServicesData[i].Preferred_Duration__c ? 1 : 0;
                                            if (!apptServicesData[i].Duration_1__c) {
                                                apptServicesData[i].Duration_1__c = 0;
                                            }
                                            if (!apptServicesData[i].Duration_2__c) {
                                                apptServicesData[i].Duration_2__c = 0;
                                            }
                                            if (!apptServicesData[i].Duration_3__c) {
                                                apptServicesData[i].Duration_3__c = 0;
                                            }
                                            if (!apptServicesData[i].Guest_Charge__c) {
                                                apptServicesData[i].Guest_Charge__c = 0;
                                            }
                                            if (!apptServicesData[i].Buffer_After__c) {
                                                apptServicesData[i].Buffer_After__c = 0;
                                            }
                                            if (!apptServicesData[i].Duration__c) {
                                                apptServicesData[i].Duration__c = parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10);
                                            }
                                            if (appointmentbookingObj.Booked_Online__c) {
                                                apptServicesData[i].workerName = apptServicesData[i].workerName;
                                            }
                                            queries += mysql.format('UPDATE Ticket_Service__c' +
                                                ' SET Visit_Type__c = "' + appointmentbookingObj.Client_Type__c +
                                                '", Client__c = "' + appointmentbookingObj.Client__c +
                                                '", Worker__c = "' + apptServicesData[i].workerName +
                                                '", Service_Date_Time__c = "' + apptDate1 +
                                                '", Service_Group_Color__c = "' + apptServicesData[i].color +
                                                '", Duration_1__c = "' + apptServicesData[i].Duration_1__c +
                                                '", Duration_2__c = "' + apptServicesData[i].Duration_2__c +
                                                '", Duration_3__c = "' + apptServicesData[i].Duration_3__c +
                                                '", Duration__c = "' + apptServicesData[i].Duration__c +
                                                '", Buffer_After__c = "' + apptServicesData[i].Buffer_After__c +
                                                '", Guest_Charge__c = "' + apptServicesData[i].Guest_Charge__c +
                                                "', Status__c = '" + tsStatus +
                                                '", Duration_1_Available_for_Other_Work__c = "' + avail1 +
                                                '", Duration_2_Available_for_Other_Work__c = "' + avail2 +
                                                '", Duration_3_Available_for_Other_Work__c = "' + avail3 +
                                                '", Preferred_Duration__c = "' + avail4 +
                                                '", Service_Tax__c = "' + apptServicesData[i].Service_Tax__c +
                                                '", Is_Booked_Out__c = "' + 0 +
                                                '", Net_Price__c = "' + apptServicesData[i].Price +
                                                '", Taxable__c = "' + apptServicesData[i].Taxable__c +
                                                '", Non_Standard_Duration__c = "' + 0 +
                                                '", Rebooked__c = "' + Rebooked +
                                                '", Resources__c = "' + apptServicesData[i].Resources__c +
                                                '", Service__c = "' + apptServicesData[i].serviceId +
                                                '", Booked_Package__c = "' + apptServicesData[i].Booked_Package__c +
                                                '", LastModifiedDate = "' + date +
                                                '", LastModifiedById = "' + loginId +
                                                '", Appt_Ticket__c = "' + appointmentbookingObj.apptId +
                                                '" WHERE Id = "' + appointmentbookingObj.ts_id[i] + '";');
                                        } else {
                                            if (appointmentbookingObj.Booked_Online__c) {
                                                apptServicesData[i].workerName = apptServicesData[i].workerName;
                                            }
                                            if (!apptServicesData[i].Duration_1__c) {
                                                apptServicesData[i].Duration_1__c = 0;
                                            }
                                            if (!apptServicesData[i].Duration_2__c) {
                                                apptServicesData[i].Duration_2__c = 0;
                                            }
                                            if (!apptServicesData[i].Duration_3__c) {
                                                apptServicesData[i].Duration_3__c = 0;
                                            }
                                            if (!apptServicesData[i].Guest_Charge__c) {
                                                apptServicesData[i].Guest_Charge__c = 0;
                                            }
                                            if (!apptServicesData[i].Buffer_After__c) {
                                                apptServicesData[i].Buffer_After__c = 0;
                                            }
                                            if (!apptServicesData[i].Duration__c) {
                                                apptServicesData[i].Duration__c = parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10);
                                            }
                                            updaterecords.push([uniqid(),
                                                0,
                                                date, loginId,
                                                date, loginId,
                                                date,
                                            appointmentbookingObj.apptId,
                                            appointmentbookingObj.Client_Type__c,
                                            appointmentbookingObj.Client__c,
                                            apptServicesData[i].workerName,
                                                apptDate1,
                                                'Booked',
                                            apptServicesData[i].color,
                                            apptServicesData[i].Duration_1__c,
                                            apptServicesData[i].Duration_2__c,
                                            apptServicesData[i].Duration_3__c,
                                            apptServicesData[i].Duration__c,
                                            apptServicesData[i].Buffer_After__c,
                                            apptServicesData[i].Guest_Charge__c,
                                            apptServicesData[i].Duration_1_Available_for_Other_Work__c,
                                            apptServicesData[i].Duration_2_Available_for_Other_Work__c,
                                            apptServicesData[i].Duration_3_Available_for_Other_Work__c,
                                                0,
                                            apptServicesData[i].Service_Tax__c,
                                                0,
                                            apptServicesData[i].Price,
                                            apptServicesData[i].Price,
                                            apptServicesData[i].Taxable__c,
                                                0,
                                                0,
                                            apptServicesData[i].serviceId,
                                            apptServicesData[i].Booked_Package__c,
                                                '',
                                            apptServicesData[i].Resources__c,
                                            ]);
                                        }
                                        if (apptServicesData[i].Duration__c) {
                                            // apptDate1 = new Date(apptDate1.getTime() + parseInt(apptServicesData[i].Duration__c, 10) * 60000);
                                            apptDate1 = dateFns.addMinToDBStr(apptDate1, parseInt(apptServicesData[i].Duration__c, 10));
                                        } else {
                                            // apptDate1 = new Date(apptDate1.getTime() + parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10) * 60000);
                                            apptDate1 = dateFns.addMinToDBStr(apptDate1, parseInt(apptServicesData[i].Duration_1__c, 10) + parseInt(apptServicesData[i].Duration_2__c, 10) + parseInt(apptServicesData[i].Duration_3__c, 10));
                                        }
                                    }
                                    if (updaterecords.length > 0) {
                                        var insertQuery1 = 'INSERT INTO Ticket_Service__c' +
                                            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                            ' SystemModstamp, Appt_Ticket__c, Visit_Type__c, Client__c,' +
                                            ' Worker__c, Service_Date_Time__c, Status__c, Service_Group_Color__c,Duration_1__c,Duration_2__c,Duration_3__c, Duration__c, Buffer_After__c,Guest_Charge__c,Duration_1_Available_for_Other_Work__c,' +
                                            ' Duration_2_Available_for_Other_Work__c, Duration_3_Available_for_Other_Work__c,Preferred_Duration__c, Service_Tax__c,' +
                                            ' Is_Booked_Out__c, Net_Price__c, Price__c,Taxable__c, Non_Standard_Duration__c, Rebooked__c,Service__c, Booked_Package__c, Notes__c, Resources__c) VALUES ?';
                                        execute.query(dbName, insertQuery1, [updaterecords], function (err1, result1) {
                                            if (err1) {
                                                logger.error('Error in WorkerServices dao - updateWorkerService:', err1);
                                                indexParm++;
                                                sendResponseWithMsg(res, false, { statusCode: '9999' }, err1);
                                            } else {
                                                indexParm++;
                                                if (indexParm === 2) {
                                                    sendResponseWithMsg(res, true, { 'apptId': appointmentbookingObj.apptId }, "Appointment modified successfully");
                                                }
                                            }
                                        });
                                    } else {
                                        indexParm++;
                                        if (indexParm === 2) {
                                            sendResponseWithMsg(res, true, { 'apptId': appointmentbookingObj.apptId }, "Appointment modified successfully");
                                        }
                                    }
                                    if (queries.length > 0) {
                                        execute.query(dbName, queries, function (err, result) {
                                            indexParm++;
                                            if (indexParm === 2) {
                                                sendResponseWithMsg(res, true, { 'apptId': appointmentbookingObj.apptId }, "Appointment modified successfully");
                                            }
                                        });
                                    } else {
                                        indexParm++;
                                        if (indexParm === 2) {
                                            sendResponseWithMsg(res, true, { 'apptId': appointmentbookingObj.apptId }, "Appointment modified successfully");
                                        }
                                    }
                                } else {
                                    sendResponseWithMsg(res, true, { 'apptId': appointmentbookingObj.apptId }, "Appointment modified successfully");
                                    // done(null, { 'apptId': appointmentbookingObj.apptId });
                                }
                            }
                        });
                    });
                }
            });
        });
    }
}
// function sendResponse(indexParam, callback, error, result) {
//     if (indexParam == 4) {
//         callback(error, result);
//     }
// }
function srchAry(nameKey, index, myArray) {
    var rtnVal = null;
    for (var i = 0; i < myArray.length; i++) {
        if (myArray[i].Appt_Ticket__c === nameKey && index !== i) {
            rtnVal = i;
        }
    }
    return rtnVal;
}

function checkDefaultAppId(dbName, workerId, done) {
    var sqlquery = ' select us.Appointment_Hours__c,us.FirstName,ch.Id,ch.Name from Company_Hours__c as ch' +
        ' left join User__c as us on  us.Id = "' + workerId + '" ' +
        ' and us.IsActive = 1' +
        ' where ch.Id = us.Appointment_Hours__c ';
    execute.query(dbName, sqlquery, function (err, res) {
        if (err) {
            done(err, null);
        } else {
            if (res.length > 0) {
                if (res[0].Appointment_Hours__c === '' || res[0].Appointment_Hours__c === undefined || res[0].Appointment_Hours__c === null) {
                    var finResult;
                    var querysql = ' select ch.Id,ch.Name from Company_Hours__c as ch' +
                        ' where ch.isDefault__c = 1 ';
                    execute.query(dbName, querysql, function (error, rest) {
                        if (error) {
                            done(error, null);
                        } else {
                            finResult = rest[0].Id;
                            done(error, finResult);
                        }
                    });
                } else {
                    var finResults;
                    finResults = res[0].Appointment_Hours__c;
                    done(err, finResults);
                }
            } else {
                done(err, null);
            }
        }
    });
}

function sortJSONAry(JSONAry, JSONAttrb, order) {
    var leng = JSONAry.length;
    for (var i = 0; i < leng; i++) {
        for (var j = i + 1; j < leng; j++) {
            if ((order === 'asc' && JSONAry[i][JSONAttrb] > JSONAry[j][JSONAttrb]) ||
                (order === 'desc' && JSONAry[i][JSONAttrb] < JSONAry[j][JSONAttrb])) {
                var tempObj = JSONAry[i];
                JSONAry[i] = JSONAry[j];
                JSONAry[j] = tempObj;
            }
        }
    }
    return JSONAry;
}

function removeDuplicates(originalArray, prop) {
    const newArray = [];
    const lookupObject = {};
    for (let i = 0; i < originalArray.length; i++) {
        lookupObject[originalArray[i][prop]] = originalArray[i];
    }
    for (const field of Object.keys(lookupObject)) {
        newArray.push(lookupObject[field]);
    }
    return newArray;
}

function isRebookedService(dbName, Client__c, apptCreatedDate, callback) {
    var todayDate = dateFns.getDateTmFrmDBDateStr(apptCreatedDate);
    todayDate.setDate(todayDate.getDate() - 1);
    var prvsDate = dateFns.getDBDatStr(todayDate);
    var searchDate = apptCreatedDate.split(' ')[0] + ' 23:59:59';
    // prvsDate = dateFns.getDateTmFrmDBDateStr(prvsDate);
    var cstHrsQry = `select at.Id, at.Duration__c, at.Client__c, at.Appt_Date_Time__c,
                    ts.Id, s.Name, s.Service_Group__c, ts.Service_Date_Time__c,
                    ts.Duration__c, ts.Visit_Type__c,
                    ts.CreatedDate, ts.LastModifiedDate,
                    ts.Status__c, ts.Price__c, ts.Net_Price__c, ts.Worker__c, u.Id
                from Appt_Ticket__c as at
                LEFT JOIN Ticket_Service__c as ts on ts.Appt_Ticket__c = at.Id
                LEFT JOIN Service__c as s on s.Id = ts.Service__c
                LEFT JOIN User__c as u on u.Id = ts.Worker__c
                where at.LastModifiedDate >= '` + prvsDate + `' and at.LastModifiedDate <= '` + searchDate + `' and at.Client__c = '` + Client__c + `' and at.isNoService__c = 0
                and(at.Status__c = 'Complete' or at.Status__c = 'Checked In') ORDER BY at.Appt_Date_Time__c DESC`;
    execute.query(dbName, cstHrsQry, '', function (err, data) {
        if (err) {
            callback(err, null);
        } else {
            callback(null, data);
        }
    });
}

function reBookedChecked(dbName, clientId, serviceDate, tempService, callback) {
    const startTimes = moment().format('YYYY-MM-DD 00:00:00');
    const endTimes = moment().format('YYYY-MM-DD 23:59:59');
    var query = 'SELECT DISTINCT ap.Id,ap.Status__c,ap.Client__c,ap.LastModifiedDate,ap.Appt_Date_Time__c, time(ap.Appt_Date_Time__c) as timess' +
        ' FROM Appt_Ticket__c as ap' +
        ' left join Ticket_Service__c as ts on ts.Appt_Ticket__c = ap.Id' +
        ' where "' + startTimes + '" <= ap.LastModifiedDate' +
        ' and  ap.LastModifiedDate <= "' + endTimes + '"' +
        ' and ts.Client__c = "' + clientId + '"' +
        ' and ap.Status__c <> "Booked"' +
        ' and ts.Status__c <> "Booked"';
    execute.query(dbName, query, '', function (err, data) {
        if (err) {
            callback(err, null);
        } else {
            if (data.length >= 1) {
                callback(null, data);
            } else if (data.length === 0) {
                callback(null, data);
            } else {
                callback(null, data);
            }
        }
    });
}
//--- Start of function to get data for all days and all input workers working days (Company and Custom Hours) ---//
function getWrkHrs(dbName, id, date, callback) {
    var cmpHrsQry = `SELECT ` +
        `CONCAT(hrs.MondayStartTime__c,'|',hrs.MondayEndTime__c) as Monday, ` +
        `CONCAT(hrs.TuesdayStartTime__c,'|',hrs.TuesdayEndTime__c) as Tuesday, ` +
        `CONCAT(hrs.WednesdayStartTime__c,'|',hrs.WednesdayEndTime__c) as Wednesday, ` +
        `CONCAT(hrs.ThursdayStartTime__c,'|',hrs.ThursdayEndTime__c) as Thursday, ` +
        `CONCAT(hrs.FridayStartTime__c,'|',hrs.FridayEndTime__c) as Friday, ` +
        `CONCAT(hrs.SaturdayStartTime__c,'|',hrs.SaturdayEndTime__c) as Saturday, ` +
        `CONCAT(hrs.SundayStartTime__c,'|',hrs.SundayEndTime__c) as Sunday, ` +
        `hrs.Id as compHrsId, u.Id, u.Appointment_Hours__c ` +
        `FROM User__c as u ` +
        `LEFT JOIN Company_Hours__c as hrs on hrs.Id = u.Appointment_Hours__c ` +
        `WHERE hrs.IsDeleted = 0 AND u.Id = '` + id + `'`;
    execute.query(dbName, cmpHrsQry, '', function (err, cmpHrsData) {
        if (err) {
            logger.error('Error in getWrkHrs:', err);
            callback(null, null);
        } else {
            if (cmpHrsData && cmpHrsData.length > 0) {
                var compHrsIds = '(';
                for (var i = 0; i < cmpHrsData.length; i++) {
                    compHrsIds += '\'' + cmpHrsData[i].compHrsId + '\','
                }
                compHrsIds = compHrsIds.slice(0, -1);
                compHrsIds += ')';
                //--- Adding custom hours ---//
                getCstHrs(dbName, compHrsIds, date, function (cstHrsData) {
                    callback(cmpHrsData, cstHrsData);
                });
            } else {
                callback(null, null);
            }
        }
    });
}
//--- End of function to get data for all days and all input workers working days (Company and Custom Hours) ---//

//--- Start of function to get data for Custom Hours for the next 2 weeks from the selected date ---//
function getCstHrs(dbName, compHrsIds, date, callback) {
    compHrsIds = compHrsIds.replace(/-OLB/g, '');
    var cstHrsQry = "SELECT Date__c, All_Day_Off__c, StartTime__c, EndTime__c, Company_Hours__c FROM CustomHours__c WHERE " +
        "Date__c = '" + date + "' AND Company_Hours__c IN " + compHrsIds + " AND IsDeleted=0";
    execute.query(dbName, cstHrsQry, '', function (err, data) {
        if (err) {
            callback(null);
        } else {
            callback(data);
        }
    });
}

function formatAMPM(time, bookingDate) {
    let hours;
    let minutes = time.split(' ')[0].split(':')[1];
    if (time.split(' ')[1] === 'AM') {
        hours = time.split(' ')[0].split(':')[0];
        if (+hours === 12) {
            hours = 0;
        }
    } else if (time.split(' ')[1] === 'PM') {
        hours = time.split(' ')[0].split(':')[0];
        if (parseInt(hours, 10) !== 12) {
            hours = parseInt(hours, 10) + 12;
        }
    }
    minutes = parseInt(minutes, 10);
    if (minutes == 0) {
        minutes = '00';
    }
    return bookingDate.getFullYear() + '-' + (bookingDate.getMonth() + 1) + '-' + bookingDate.getDate() + ' ' + hours + ':' + minutes;
}

function avlDurColor(result) {
    var data = [];
    for (let i = 0; i < result.length; i++) {
        var tempData = [];
        var tempResObj = JSON.parse(JSON.stringify(result[i]));
        var addition = tempResObj.Duration_1_Available_for_Other_Work__c + tempResObj.Duration_2_Available_for_other_Work__c + tempResObj.Duration_3_Available_for_other_Work__c;
        if (addition > 0) {
            if (tempResObj.Duration_1__c > 0) {
                tempData.push([tempResObj.Duration_1__c, tempResObj.Duration_1_Available_for_Other_Work__c])
            }
            if (tempResObj.Duration_2__c > 0) {
                if (tempResObj.Duration_1_Available_for_Other_Work__c == tempResObj.Duration_2_Available_for_other_Work__c) {
                    tempData[0][0] = tempData[0][0] + tempResObj.Duration_2__c;
                } else {
                    tempData.push([tempResObj.Duration_2__c, tempResObj.Duration_2_Available_for_other_Work__c]);
                }
            }
            if (tempResObj.Duration_3__c > 0) {
                if (tempResObj.Duration_2_Available_for_other_Work__c == tempResObj.Duration_3_Available_for_other_Work__c) {
                    tempData[tempData.length - 1][0] = tempData[tempData.length - 1][0] + tempResObj.Duration_3__c;
                } else {
                    tempData.push([tempResObj.Duration_3__c, tempResObj.Duration_3_Available_for_other_Work__c]);
                }
            }
            if (tempResObj.Buffer_After__c > 0 && tempResObj.Duration_3_Available_for_other_Work__c == 0) {
                tempData[tempData.length - 1][0] = tempData[tempData.length - 1][0] + tempResObj.Buffer_After__c;
            } else if (tempResObj.Buffer_After__c > 0) {
                tempData.push([tempResObj.Buffer_After__c, 0]);
            }
            if (tempData.length === 0) {
                tempData = [
                    [0, 0]
                ];
            }
            tempData.forEach((obj, index) => {
                data.push(JSON.parse(JSON.stringify(tempResObj)));
                data[data.length - 1].Duration__c = obj[0];
                if (index > 0) {
                    data[data.length - 1].Service_Date_Time__c = addMinToDBStr(data[data.length - 2].Service_Date_Time__c, data[data.length - 2].Duration__c);
                    data[data.length - 1].showClientService = false;
                    data[data.length - 1].displayDuration = 0;
                } else {
                    data[data.length - 1].showClientService = true;
                    data[data.length - 1].displayDuration = tempResObj.Duration__c;
                }
                if (obj[1]) {
                    data[data.length - 1].available_service = 1;
                } else {
                    data[data.length - 1].available_service = 0;
                }
            });
        } else {
            tempResObj.available_service = 0;
            tempResObj.showClientService = true;
            tempResObj.displayDuration = tempResObj.Duration__c;
            data.push(tempResObj);
        }
    }
    return data;
}

function addMinToDBStr(dateStr, min) {
    const dtTmArry = dateStr.split(' ');
    const dtArry = dtTmArry[0].split('-');
    const tmArry = dtTmArry[1].split(':');
    const datObj = new Date(dtArry[0], (parseInt(dtArry[1], 10) - 1), dtArry[2], tmArry[0], tmArry[1], tmArry[2]);
    datObj.setTime(datObj.getTime() + min * 60000);
    if (!datObj) {
        datObj = new Date();
    }
    return datObj.getFullYear() +
        '-' + ('0' + (datObj.getMonth() + 1)).slice(-2) +
        '-' + ('0' + datObj.getDate()).slice(-2) +
        ' ' + ('0' + datObj.getHours()).slice(-2) +
        ':' + ('0' + datObj.getMinutes()).slice(-2) +
        ':' + ('0' + datObj.getSeconds()).slice(-2);
}
//--- End of function to get data for Custom Hours for the next 2 weeks from the selected date ---
var sendResponse = require('../common/response').sendResponse;
var config = require('../common/config');
var execute = require('../common/dbConnection');

// --- Start of Controller
module.exports.controller = function (app) {

    app.post('/api/setup/clientpreferences/visittypes', function (req, res) {
        visitTypesSRVC.saveVisitTypes(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/setup/clientpreferences/visittypes', function (req, res) {
        visitTypesSRVC.getVisitTypes(req, function (data) {
            utils.sendResponse(res, data.httpCode, data.statusCode, data.result);
        });
    });
    app.get('/api/setup/clientpreferences/visittype/active', function (req, res) {
        var dbName = req.headers['db'];
        var JSON__c_str = []
        var sqlQuery = 'SELECT * FROM Preference__c WHERE Name = "Client Visit Types"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (result && result.length > 0) {
                for (var i = 0; i < JSON.parse(result[0].JSON__c).length; i++) {
                    if (JSON.parse(result[0].JSON__c)[i].active === true) {
                        JSON__c_str.push({ 'visitType': JSON.parse(result[0].JSON__c)[i].visitType });
                    }
                }
                sendResponse(res, true, JSON__c_str);
            } else {
                logger.error('Error in VisitTypes dao - getVisitTypes:', err);
                sendResponse(res, false, '000');
            }
        });
    });
};
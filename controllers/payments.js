var uniqid = require('uniqid');
var md5 = require('md5');
var mysql = require('mysql');
var moment = require('moment');
var execute = require('../common/dbConnection');
var logger = require('../common/logger');
var sendResponse = require('../common/response').sendResponse;
var sendResponseWithMsg = require('../common/response').sendResponseWithMsg;

var config = require('../common/config');

module.exports.controller = function(app) {
    // Api for payments 
    app.post('/api/checkout/ticketpayments', function(req, res) {
        var dbName = req.headers['db'];
        var dateTime = req.headers['dt'];
        var loginId;
        if (req.headers['id']) {
            loginId = req.headers['id'];
        } else {
            loginId = uniqid();
        }
        var date = new Date();
        var startDatetime = new Date(new Date(date).getFullYear(), new Date(date).getMonth(), new Date(date).getDate());
        startDatetime = moment(startDatetime).format('YYYY-MM-DD');
        try {
            var ticketpaymentsData = req.body;
            ticketpaymentsData.cmpId = req.headers['cid'];
            ticketpaymentsData.merchantAccnt = '';
            ticketpaymentsData.paymentGateWay = 'AnywhereCommerce';
            var ticketlistcharge = ticketpaymentsData.listCharge.replace("$", "");
            if (parseFloat(ticketlistcharge).toFixed(2) >= ticketpaymentsData.amountToPay) {
                ticketpaymentsData.status = 'Complete';
            } else {
                ticketpaymentsData.status = 'Checked In';
            }
            ticketpaymentsData.Online__c = 0;
            sumAllPricesFunction(dbName, ticketpaymentsData, function(sumData) {
                ticketpaymentsData = sumData;
                ticketsDataFunction(dbName, ticketpaymentsData, function(ticketsData) {
                    ticketpaymentsData = ticketsData;
                    var indexParam = 0;
                    var paymentOtherObjData = {
                        Id: ticketpaymentsData.Id ? ticketpaymentsData.Id : uniqid(),
                        IsDeleted: 0,
                        CreatedDate: dateTime,
                        CreatedById: loginId,
                        LastModifiedDate: dateTime,
                        LastModifiedById: loginId,
                        SystemModstamp: dateTime,
                        Amount_Paid__c: ticketpaymentsData.amountToPay,
                        Appt_Ticket__c: ticketpaymentsData.apptId,
                        Merchant_Account_Name__c: ticketpaymentsData.merchantAccnt,
                        Payment_Gateway_Name__c: ticketpaymentsData.paymentGateWay,
                        Notes__c: ticketpaymentsData.notes,
                        Payment_Type__c: ticketpaymentsData.paymentType,
                        Approval_Code__c: ticketpaymentsData.approvalCode,
                        Reference_Number__c: ticketpaymentsData.refCode,
                        Drawer_Number__c: ticketpaymentsData.cashDrawer
                    };
                    var insertQuery = 'INSERT INTO Ticket_Payment__c SET ?';
                    if (ticketpaymentsData.paymentName === 'Gift Redeem') {
                        ticketpaymentsData.listCharge = parseFloat(ticketpaymentsData.listCharge).toFixed(2);
                        ticketpaymentsData.amountToPay = parseFloat(ticketpaymentsData.amountToPay).toFixed(2);
                        var sql = `SELECT * 
                                        FROM 
                                            Ticket_Other__c toc
                                            LEFT JOIN Appt_Ticket__c a on a.Id= toc.Ticket__c
                                        WHERE toc.Gift_Number__c = '` + ticketpaymentsData.giftNumber.replace("'", "\\'") + `'
                                        AND a.Status__c = 'Complete' AND toc.Ticket__c != '` + ticketpaymentsData.apptId + `'`;
                        var sql2 = 'SELECT IFNULL(SUM(tp.Amount_Paid__c), 0) as payedByGift from Ticket_Payment__c as tp  WHERE  tp.Gift_Number__c = "' + ticketpaymentsData.giftNumber + '" and tp.Isdeleted = 0'
                        execute.query(dbName, sql + ';' + sql2, '', function(err, result) {
                            if (err) {
                                sendResponseWithMsg(res, false, [], err);
                            } else if (result[0].length === 0) {
                                sendResponseWithMsg(res, false, [], result);
                            } else {
                                var amountPaid = 0;
                                paymentOtherObjData.Gift_Number__c = ticketpaymentsData.giftNumber;
                                if (result[0][0]['Expires__c'] && result[0][0]['Expires__c'] < startDatetime) {
                                    sendResponseWithMsg(res, false, [], result);
                                } else {
                                    if (result[0][0].Amount__c - result[1][0]['payedByGift'] > 0) {
                                        if (((result[0][0].Amount__c - result[1][0]['payedByGift']) >= ticketpaymentsData.amountToPay) &&
                                            (ticketpaymentsData.listCharge === ticketpaymentsData.amountToPay)) {
                                            amountPaid = ticketpaymentsData.amountToPay;
                                            ticketpaymentsData.status = 'Complete';
                                        }
                                        if (((result[0][0].Amount__c - result[1][0]['payedByGift']) < ticketpaymentsData.amountToPay) &&
                                            (ticketpaymentsData.listCharge !== ticketpaymentsData.amountToPay)) {
                                            amountPaid = result[0][0].Amount__c - result[1][0]['payedByGift'];
                                            ticketpaymentsData.status = 'Complete';
                                        }
                                        if (((result[0][0].Amount__c - result[1][0]['payedByGift']) < ticketpaymentsData.amountToPay) &&
                                            (ticketpaymentsData.listCharge === ticketpaymentsData.amountToPay)) {
                                            amountPaid = result[0][0].Amount__c - result[1][0]['payedByGift'];
                                            ticketpaymentsData.status = 'Checked In';
                                        }
                                        if (((result[0][0].Amount__c - result[1][0]['payedByGift']) > ticketpaymentsData.amountToPay) &&
                                            (ticketpaymentsData.listCharge !== ticketpaymentsData.amountToPay)) {
                                            amountPaid = ticketpaymentsData.amountToPay;
                                            ticketpaymentsData.status = 'Checked In';
                                        }
                                        paymentOtherObjData.Amount_Paid__c = amountPaid;
                                        var insertQuery = 'INSERT INTO Ticket_Payment__c SET ?';
                                        addToTicketPaymentsTbl(res, paymentOtherObjData, dbName, cmpId, insertQuery, ticketpaymentsData, indexParam, loginId, dateTime);
                                    } else {
                                        sendResponseWithMsg(res, false, [], '');
                                    }
                                }

                            }
                        });
                    } else if (ticketpaymentsData.paymentName === 'Credit card') {
                        crtCardVldChkFunction(res, paymentOtherObjData, dbName, insertQuery, ticketpaymentsData, indexParam, loginId, dateTime);
                    } else {
                        addToTicketPaymentsTbl(res, paymentOtherObjData, dbName, '', insertQuery, ticketpaymentsData, indexParam, loginId, dateTime);
                    }
                });
            });
        } catch (err) {
            logger.error('Unknown error in CheckOut dao - addToTicketpayments:', err);
            return (err, { statusCode: '9999' });
        }

    });
}

function crtCardVldChkFunction(res, paymentOtherObjData, dbName, insertQuery, ticketpaymentsData, indexParam, loginId, dateTime) {
    payments(ticketpaymentsData, function(reqObj) {
        ticketpaymentsData = reqObj.data;
        var request = require("request");
        request.post({
                rejectUnauthorized: false,
                url: reqObj.url,
                method: "POST",
                headers: {
                    'Content-Type': 'application/xml',
                },
                body: reqObj.xml
            },
            function(error, response, body) {
                if (error) {
                    sendResponseWithMsg(res, false, [], '');
                } else {
                    let cardTokenId = '';
                    const parseString = require('xml2js').parseString;
                    parseString(body, function(err, result) {
                        cardTokenId = result;
                        if (cardTokenId.ERROR && cardTokenId.ERROR.ERRORSTRING[0] === 'INVALID MERCHANTREF') {
                            // this.errorMsgAry[0] = 'INVALID MERCHANTREF';
                            sendResponseWithMsg(res, false, [], 'INVALID MERCHANTREF');
                        } else if ((cardTokenId.ERROR) && (!cardTokenId.SECURECARDUPDATERESPONSE || !cardTokenId.SECURECARDREGISTRATIONRESPONSE)) {
                            if ((cardTokenId.ERROR.ERRORSTRING[0].split(' ')[0] === cardTokenId.ERROR.ERRORSTRING[0].split(' ')[0] || ticketpaymentsData.cardNumber.toString() === '0') &&
                                (cardTokenId.ERROR.ERRORSTRING[0] !== 'INVALID CARDEXPIRY') &&
                                (cardTokenId.ERROR.ERRORSTRING[0] !== 'java.lang.StringIndexOutOfBoundsException: String index out of range: 12')) {
                                //   this.errorMsgAry[1] = 'Credit Card Processing Error: INVALID CARDNUMBER field';
                                sendResponseWithMsg(res, false, [], 'Credit Card Processing Error: INVALID CARDNUMBER field');
                            } else if (cardTokenId.ERROR.ERRORSTRING[0] === 'INVALID CARDEXPIRY') {
                                //   this.errorMsgAry[2] = 'Invalid card expiry';
                                sendResponseWithMsg(res, false, [], 'Invalid card expiry');
                            } else if (cardTokenId.ERROR.ERRORSTRING[0] === 'java.lang.StringIndexOutOfBoundsException: String index out of range: 12') {
                                //   this.errorMsgAry[3] = 'Card number must be 12 digits';
                                sendResponseWithMsg(res, false, [], 'Card number must be 12 digits');
                            }
                        } else {
                            ticketpaymentsData.approvalCode = cardTokenId.PAYMENTRESPONSE.APPROVALCODE[0];
                            ticketpaymentsData.refCode = cardTokenId.PAYMENTRESPONSE.UNIQUEREF[0];
                            addToTicketPaymentsTbl(res, paymentOtherObjData, dbName, '', insertQuery, ticketpaymentsData, indexParam, loginId, dateTime);
                        }
                    });
                }
            });
    });
}

function payments(data, callback) {
    data.cardType = getCardType(data.cardNumber);
    data.cardExp = ('0' + data.expMonth).slice(-2) + data.expYear.toString().slice(-2);
    data.terminalid = '3099001';
    data.currency = 'USD';
    data.terminalType = '1';
    data.transactionType = '4';
    data.Id = uniqid();
    const d = new Date();
    const dateTime = ('00' + (d.getMonth() + 1)).slice(-2) + '-' + ('00' + d.getDate()).slice(-2) + '-' +
        (d.getFullYear() + '').slice(-2) + ':' +
        ('00' + d.getHours()).slice(-2) + ':' +
        ('00' + d.getMinutes()).slice(-2) + ':' +
        ('00' + d.getSeconds()).slice(-2) + ':000';
    var hash = md5('3099001' + data.Id + data.amountToPay + dateTime + 'Secret2Pass');
    const returnXml = '<?xml version=\'1.0\' encoding=\'utf-8\'?>' +
        '<PAYMENT>' +
        '<ORDERID>' + data.Id + '</ORDERID>' +
        '<TERMINALID>' + data.terminalid + '</TERMINALID>' +
        '<AMOUNT>' + data.amountToPay + '</AMOUNT>' +
        '<DATETIME>' + dateTime + '</DATETIME>' +
        '<CARDNUMBER>' + data.cardNumber + '</CARDNUMBER>' +
        '<CARDTYPE>' + data.cardType + '</CARDTYPE>' +
        '<CARDEXPIRY>' + data.cardExp + '</CARDEXPIRY>' +
        '<HASH>' + hash + '</HASH>' +
        '<CURRENCY>' + data.currency + '</CURRENCY>' +
        '<TERMINALTYPE>' + data.terminalType + '</TERMINALTYPE>' +
        '<TRANSACTIONTYPE>' + data.transactionType + '</TRANSACTIONTYPE>' +
        '</PAYMENT>';
    var reqObj = {
        'url': 'https://testpayments.anywherecommerce.com/merchant/xmlpayment',
        'xml': returnXml,
        'data': data
    };
    callback(reqObj);
}

function getCardType(cardNum) {
    if (cardNum.match(/^4[0-9]{6,}$/) != null) {
        return 'VISA';
    } else if (cardNum.match(/^3[47][0-9]{5,}$/) != null) {
        return 'AMEX';
    } else if (cardNum.match(/^6(?:011|5[0-9]{2})[0-9]{3,}$/) != null) {
        return 'DISCOVER';
    } else if (cardNum.match(/^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$/) != null) {
        return 'MASTERCARD';
    } else {
        return 'VISA';
    }
}

function addToTicketPaymentsTbl(res, paymentOtherObjData, dbName, cmpId, insertQuery, ticketpaymentsData, indexParm, loginId, dateTime) {
    execute.query(dbName, insertQuery, paymentOtherObjData, function(ticketPaymentErr, ticketPaymentResult) {
        if (ticketPaymentErr) {
            logger.error('Error in CheckOut dao - addToTicketpayments:', ticketPaymentErr);
            sendResponseWithMsg(res, false, [], ticketPaymentErr);
        } else {
            if (ticketpaymentsData && ticketpaymentsData.Online__c === 0) {
                var rcvdAmount = 0;
                if (ticketpaymentsData.ticketOthersList) {
                    for (var i = 0; i < ticketpaymentsData.ticketOthersList.length; i++) {
                        if (ticketpaymentsData.ticketOthersList[i].Transaction_Type__c === 'Received on Account') {
                            rcvdAmount += ticketpaymentsData.ticketOthersList[i].Amount__c;
                        } else if (ticketpaymentsData.ticketOthersList[i].Transaction_Type__c === 'Deposit') {
                            rcvdAmount += ticketpaymentsData.ticketOthersList[i].Amount__c;
                        } else if (ticketpaymentsData.ticketOthersList[i].Transaction_Type__c === 'Pre Payment') {
                            rcvdAmount += ticketpaymentsData.ticketOthersList[i].Amount__c;
                        }
                    }
                }
                var sqlQuery = "UPDATE Appt_Ticket__c" +
                    " SET Status__c = '" + ticketpaymentsData.status +
                    "', Service_Sales__c = '" + ticketpaymentsData.serviceAmount +
                    "', Service_Tax__c = '" + ticketpaymentsData.servicesTax +
                    "', Product_Sales__c = '" + ticketpaymentsData.productAmount +
                    "', Product_Tax__c = '" + ticketpaymentsData.productsTax +
                    "', Other_Sales__c = '" + ticketpaymentsData.otherCharge +
                    "', Tips__c = '" + ticketpaymentsData.tipsCharge +
                    "', Payments__c = '" + ticketpaymentsData.listCharge +
                    "', isTicket__c = " + 1 +
                    ", LastModifiedDate = '" + dateTime +
                    "', LastModifiedById = '" + loginId +
                    "' WHERE Id = '" + ticketpaymentsData.apptId + "';";
                if (ticketpaymentsData.paymentName === 'Account Charge') {
                    sqlQuery += 'UPDATE Contact__c SET Current_Balance__c=Current_Balance__c+' + (ticketpaymentsData.amountToPay - rcvdAmount) + ' WHERE Id="' + ticketpaymentsData.clientId + '";';
                } else {
                    sqlQuery += 'UPDATE `Contact__c` SET `Active__c` = "1", Current_Balance__c=Current_Balance__c-' + rcvdAmount + ' WHERE Id = "' + ticketpaymentsData.clientId + '";'
                }
                sqlQuery += 'UPDATE `Ticket_Service__c` SET `Status__c` = "' + ticketpaymentsData.status + '" WHERE `Appt_Ticket__c` = "' + ticketpaymentsData.apptId + '";'
                if (ticketpaymentsData.ticketProductsList) {
                    for (var i = 0; i < ticketpaymentsData.ticketProductsList.length; i++) {
                        sqlQuery += 'UPDATE Product__c SET Quantity_On_Hand__c = Quantity_On_Hand__c -  ' + ticketpaymentsData.ticketProductsList[i]['quantity'] + ' WHERE Id = "' + ticketpaymentsData.ticketProductsList[i]['quantity'] + '";'
                    }
                }
                execute.query(dbName, sqlQuery, '', function(ticketErr, ticketResult) {
                    if (ticketErr) {
                        logger.error('Error in CheckOut dao - addToTicketpayments:', ticketErr);
                    } else {
                        if (ticketpaymentsData && ticketpaymentsData.clientPckgData && ticketpaymentsData.clientPckgData.length > 0 && ticketpaymentsData.status === 'Complete' && (ticketpaymentsData.clientId)) {
                            var clientData = {
                                pckInfo: ticketpaymentsData.clientPckgData,
                                serviceInfo: ticketpaymentsData.ticketServices
                            }
                            ifClientPackagesExist(res, clientData, dbName, ticketpaymentsData, indexParm, loginId, dateTime);
                        } else {
                            indexParm++;
                            if (indexParm === 3) {
                                sendResponse(res, true, ticketResult);
                            }
                        }
                        if ((ticketpaymentsData && ticketpaymentsData.clientRwdData && ticketpaymentsData.clientRwdData.length > 0) && (ticketpaymentsData.clientId)) {
                            var clientRwdData = ticketpaymentsData.clientRwdData;
                            ifClientRewardsExist(res, clientRwdData, dbName, ticketpaymentsData, indexParm, loginId, dateTime);
                        } else {
                            indexParm++;
                            if (indexParm === 3) {
                                sendResponse(res, true, ticketResult);
                            }
                        }
                        if ((ticketpaymentsData && ticketpaymentsData.ticketProductsList && ticketpaymentsData.ticketProductsList.length > 0)) {
                            var ticketProductsList = ticketpaymentsData.ticketProductsList;
                            ifTicketProductsExist(res, ticketProductsList, dbName, ticketpaymentsData, indexParm, loginId);
                        } else {
                            indexParm++;
                            if (indexParm === 3) {
                                sendResponse(res, true, ticketResult);
                            }
                        }
                    }
                });
            } else {
                var sqlQuery = '';
                if (ticketpaymentsData.isUpdateAppt1) {
                    var appt1Stastus = 'Booked';
                    sqlQuery = "UPDATE Appt_Ticket__c" +
                        " SET Status__c = '" + appt1Stastus +
                        "', LastModifiedDate = '" + dateTime +
                        "', LastModifiedById = '" + loginId +
                        "' WHERE Id = '" + ticketpaymentsData.apptId1 + "';";
                }
                if (ticketpaymentsData.Online__c || ticketpaymentsData.isDepositRequired) {
                    var stastus = 'Complete';
                }
                sqlQuery += 'UPDATE Contact__c SET Active__c = "1", Current_Balance__c=Current_Balance__c-' + ticketpaymentsData.amountToPay + ' WHERE Id = "' + ticketpaymentsData.clientId + '";'
                sqlQuery += "UPDATE Appt_Ticket__c" +
                    " SET Status__c = '" + stastus +
                    "', Payments__c = '" + ticketpaymentsData.amountToPay +
                    "', Other_Sales__c = '" + ticketpaymentsData.amountToPay +
                    "', isTicket__c = " + 1 +
                    ", LastModifiedDate = '" + dateTime +
                    "', LastModifiedById = '" + loginId +
                    "' WHERE Id = '" + ticketpaymentsData.apptId + "';";
                execute.query(dbName, sqlQuery, '', function(apptTicketErr, apptTicketResult) {
                    if (apptTicketErr) {
                        logger.error('Error in CheckOut dao - addToTicketpayments:', apptTicketErr);
                    } else {
                        if ((ticketpaymentsData && ticketpaymentsData.clientPckgData && ticketpaymentsData.clientPckgData.length > 0) && (ticketpaymentsData.clientId) && stastus === 'Complete') {
                            var clientData = {
                                pckInfo: ticketpaymentsData.clientPckgData,
                                serviceInfo: ticketpaymentsData.ticketServices
                            }
                            ifClientPackagesExist(res, clientData, dbName, ticketpaymentsData, 0, loginId, dateTime);
                        }
                        if (ticketpaymentsData.isGiftPurchase && ticketpaymentsData.isGiftPurchase === true) {
                            var companyid;
                            if (ticketpaymentsData.cmpId) {
                                companyid = ticketpaymentsData.cmpId;
                            } else {
                                companyid = cmpId;
                            }
                            var giftSql = 'SELECT Appt_Date_Time__c as aptDate, c.Id as clientId, c.Phone, c.FirstName, c.LastName, c.Email,  pref.JSON__c FROM `Preference__c` as pref ' +
                                ' JOIN Contact__c as c JOIN Appt_Ticket__c as a on a.Client__c = c.Id WHERE pref.Name = "Gifts Online" AND a.Id = "' + ticketpaymentsData.apptId + '"';
                            var cmpSql = "SELECT Name,Email__c,Phone__c FROM Company__c where Id = '" + companyid + "'";
                            execute.query(dbName, giftSql + ';' + cmpSql, '', function(giftErr, giftResult) {
                                if (giftErr) {
                                    logger.error('Error in CheckOut dao - addToTicketpayments:', giftErr);
                                    sendResponseWithMsg(res, false, [], giftErr);
                                } else {
                                    var clientData = giftResult[0][0];
                                    var companyData = giftResult[1][0];
                                    var emailTempalte = JSON.parse(giftResult[0][0].JSON__c)['emailTemplate'];
                                    emailTempalte = emailTempalte.replace(/{{GiftNumber}}/g, ticketpaymentsData.giftNumber);
                                    emailTempalte = emailTempalte.replace(/{{GiftIssued}}/g, moment(new Date()).format('MM/DD/YYYY'));
                                    emailTempalte = emailTempalte.replace(/{{GiftAmount}}/g, parseFloat(ticketpaymentsData.giftPurchaseObj.Amount).toFixed(2));
                                    emailTempalte = emailTempalte.replace(/{{ClientPrimaryPhone}}/g, clientData.Phone);
                                    emailTempalte = emailTempalte.replace(/{{ClientPrimaryEmail}}/g, clientData.Email);
                                    emailTempalte = emailTempalte.replace(/{{ClientFirstName}}/g, clientData.FirstName);
                                    emailTempalte = emailTempalte.replace(/{{ClientLastName}}/g, clientData.LastName);
                                    emailTempalte = emailTempalte.replace(/{{CompanyName}}/g, companyData.Name);
                                    emailTempalte = emailTempalte.replace(/{{CompanyEmail}}/g, companyData.Email__c);
                                    emailTempalte = emailTempalte.replace(/{{CompanyPrimaryPhone}}/g, companyData.Phone__c);
                                    emailTempalte = emailTempalte.replace(/{{AppointmentDate\/Time}}/g, clientData.aptDate);
                                    emailTempalte = emailTempalte.replace(/{{GiftMessage}}/g, ticketpaymentsData.giftPurchaseObj.PersonalMessage);
                                    emailTempalte = emailTempalte.replace(/{{GiftRecipient}}/g, ticketpaymentsData.giftPurchaseObj.RecipientName);
                                    emailTempalte = emailTempalte.replace(/{{BookedServices}}/g, 'xyz');
                                    var ToMail = ticketpaymentsData.giftPurchaseObj.RecipientEmail;
                                    var subject = 'eGift Card from' + ' ' + clientData.FirstName + ' ' + clientData.LastName;
                                    getCompanyEmail(dbName, function(email) {
                                        mail.sendemail(ToMail, email, subject, emailTempalte, '', function(err, result) {
                                            if (err) {
                                                sendResponseWithMsg(res, false, [], err);
                                            } else {
                                                var insertData = {
                                                    Appt_Ticket__c: ticketpaymentsData.apptId,
                                                    Client__c: clientData.clientId,
                                                    Sent__c: dateTime,
                                                    Type__c: 'Email to Recipient',
                                                    Name: 'Email to Recipient',
                                                    Id: uniqid(),
                                                    OwnerId: loginId,
                                                    IsDeleted: 0,
                                                    CreatedDate: dateTime,
                                                    CreatedById: loginId,
                                                    LastModifiedDate: dateTime,
                                                    LastModifiedById: loginId,
                                                    SystemModstamp: dateTime,
                                                    LastModifiedDate: dateTime,
                                                }
                                                var sqlQuery = 'INSERT INTO Email__c SET ?';
                                                execute.query(dbName, sqlQuery, insertData, function(err1, result1) {
                                                    if (err1) {
                                                        logger.error('Error in send reminder email - insert into Email_c table:', err1);
                                                        sendResponseWithMsg(res, false, [], err1);
                                                    } else {
                                                        sendResponse(res, true, result1);
                                                    }
                                                });

                                            }
                                        });
                                    });
                                }
                            });
                        } else {
                            sendResponse(res, true, apptTicketResult);
                        }
                    }
                });
            }
        }
    });

}

function ifClientPackagesExist(res, clientData, dbName, ticketpaymentsData, indexParam, loginId, dateTime) {
    var date = new Date();
    var records = [];
    var cltPkgQuery = '';
    var pkgIds = "'";
    var insAry = clientData.pckInfo;
    var ticketServices = clientData.serviceInfo;
    var sqlQuery = '';
    for (let i = 0; i < insAry.length; i++) {
        records.push([uniqid(),
            0,
            dateTime,
            loginId,
            dateTime,
            loginId,
            dateTime,
            insAry[i].apptId,
            insAry[i].clientId,
            insAry[i].pckgId,
            JSON.stringify(insAry[i].pckgDetails)
        ])
    }
    // For adding client PackageId to ticket service
    if (ticketServices) {
        Loop1: for (let j = 0; j < ticketServices.length; j++) {
            if (ticketServices[j]['Booked_Package__c'] && !ticketServices[j]['Client_Package__c']) {
                for (let i = 0; i < insAry.length; i++) {
                    if (ticketServices[j]['Booked_Package__c'] === insAry[i]['pckgId']) {
                        const servicePackages = insAry[i]['pckgDetails'];
                        for (let k = 0; k < servicePackages.length; k++) {
                            const servicePackage = servicePackages[k];
                            if (servicePackage.serviceId === ticketServices[j]['ServiceId']) {
                                if (+servicePackage.used < +servicePackage.reps) {
                                    servicePackage.used = +servicePackage.used;
                                    servicePackage.used += 1;
                                    insAry[i]['pckgDetails'][k] = servicePackage;
                                    ticketServices[j]['Client_Package__c'] = records[i][0];
                                    sqlQuery += "UPDATE Ticket_Service__c" +
                                        " SET Client_Package__c = '" + ticketServices[j]['Client_Package__c'] +
                                        "', LastModifiedDate = '" + dateTime +
                                        "', LastModifiedById = '" + loginId +
                                        "' WHERE Id = '" + ticketServices[j]['TicketServiceId'] + "';";
                                    continue Loop1;
                                }
                            }
                        }
                    }
                }
            }

        }
        for (var i = 0; i < records.length; i++) {
            records[i][10] = JSON.stringify(insAry[i].pckgDetails);
        }
    }

    var clientInsertQuery = 'INSERT INTO Client_Package__c' +
        ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
        ' SystemModstamp, Ticket__c, Client__c, Package__c, Package_Details__c) VALUES ?';
    if (records && records.length > 0) {
        execute.query(dbName, clientInsertQuery, [records], function(ticketErr, ticketResult) {
            if (ticketErr) {
                logger.error('Error in CheckOut dao - addToTicketpayments:', ticketErr);
                sendResponseWithMsg(res, false, [], ticketErr);
            } else {
                if (sqlQuery !== '') {
                    execute.query(dbName, sqlQuery, '', function(ticketsrErr, ticketsrResult) {
                        if (ticketsrErr) {
                            logger.error('Error in CheckOut dao - addToTicketpayments:', ticketsrErr);
                            sendResponseWithMsg(res, false, [], ticketsrErr);
                        } else {
                            indexParam += 3;
                            if (indexParam === 3) {
                                sendResponse(res, true, ticketResult);
                            }
                        }
                    });
                } else {
                    indexParam += 3;
                    if (indexParam === 3) {
                        sendResponseWithMsg(res, false, [], err);
                    }
                }

            }
        });
    }

}

function ifClientRewardsExist(res, clientRwdData, dbName, ticketpaymentsData, indexParam, loginId, dateTime) {
    var insData = clientRwdData.filter((obj) => obj.isNew === true);
    var updateData = clientRwdData.filter((obj) => obj.isNew === false);
    var usedRecords = clientRwdData.filter((obj) => obj.used > 0);
    var reducePointsData = clientRwdData.filter((obj) => (obj.used > 0) && (obj.points <= 0));
    var crQueries = '';
    if (insData && insData.length > 0) {
        var insRecord1 = [];
        var insRecord2 = [];
        for (var k = 0; k < insData.length; k++) {
            insRecord1.push([
                uniqid(),
                0,
                dateTime,
                loginId,
                dateTime,
                loginId,
                dateTime,
                ticketpaymentsData.clientId,
                insData[k].rwdId,
                insData[k].points,
            ])
            insRecord2.push([
                uniqid(),
                0,
                dateTime,
                loginId,
                dateTime,
                loginId,
                dateTime,
                insRecord1[k][0],
                insData[k].points,
                ticketpaymentsData.apptId,
            ])
        }
        var insertQuery1 = 'INSERT INTO Client_Reward__c' +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Client__c, Reward__c, Points_Balance__c) VALUES ?';
        var insertQuery2 = 'INSERT INTO Client_Reward_Detail__c' +
            ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
            ' SystemModstamp, Client_Reward__c, Points_c, Ticket_c) VALUES ?';
        execute.query(dbName, insertQuery1, [insRecord1], function(err1, result1) {
            if (err1) {
                logger.error('Error in WorkerServices dao - updateWorkerService:', err1);
                sendResponseWithMsg(res, false, [], err1);
            } else {
                execute.query(dbName, insertQuery2, [insRecord2], function(err2, result2) {
                    if (err2) {
                        logger.error('Error in WorkerServices dao - updateWorkerService:', err2);
                        sendResponseWithMsg(res, false, [], err2);
                    } else {
                        indexParam++;
                        if (indexParam === 3) {
                            sendResponse(res, true, result2);
                        }
                    }
                });
            }
        });
    } else {
        indexParam++;
        if (indexParam === 3) {
            sendResponseWithMsg(res, false, [], err);
        }
    }
    if (reducePointsData && reducePointsData.length > 0) {
        for (var k = 0; k < reducePointsData.length; k++) {
            crQueries += mysql.format('UPDATE Client_Reward__c' +
                ' SET Points_Balance__c = Points_Balance__c- ' + reducePointsData[k].used +
                ' WHERE Id = "' + reducePointsData[k].crId + '";');
        }
    }
    if (updateData && updateData.length > 0) {
        var updateRecord1 = [];
        for (var k = 0; k < updateData.length; k++) {
            crQueries += mysql.format('UPDATE Client_Reward__c' +
                ' SET Points_Balance__c = Points_Balance__c+ "' + (updateData[k].points - updateData[k].used) +
                '" WHERE Id = "' + updateData[k].crId + '";');
            updateRecord1.push([
                uniqid(),
                0,
                dateTime,
                loginId,
                dateTime,
                loginId,
                dateTime,
                updateData[k].crId,
                updateData[k].points,
                ticketpaymentsData.apptId,
            ])
        }
        if (usedRecords && usedRecords.length > 0) {
            for (var k = 0; k < usedRecords.length; k++) {
                updateRecord1.push([
                    uniqid(),
                    0,
                    dateTime,
                    loginId,
                    dateTime,
                    loginId,
                    dateTime,
                    usedRecords[k].crId, -usedRecords[k].used,
                    ticketpaymentsData.apptId,
                ])
            }
        }
        if (crQueries && crQueries.length > 0) {
            execute.query(dbName, crQueries, '', function(editErr, editResult) {
                if (editErr) {
                    logger.error('Error in WorkerServices dao - updateWorkerService:', editErr);
                    sendResponseWithMsg(res, false, [], editErr);
                } else {
                    indexParam++;
                    if (indexParam === 3) {
                        sendResponse(res, true, editResult);
                    }
                }
            });
        }
        if (updateRecord1 && updateRecord1.length > 0) {
            var insertQuery2 = 'INSERT INTO Client_Reward_Detail__c' +
                ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                ' SystemModstamp, Client_Reward__c, Points_c, Ticket_c) VALUES ?';
            execute.query(dbName, insertQuery2, [updateRecord1], function(err2, result2) {
                if (err2) {
                    logger.error('Error in WorkerServices dao - updateWorkerService:', err2);
                    sendResponseWithMsg(res, false, [], err2);
                } else {
                    indexParam++;
                    if (indexParam === 3) {
                        sendResponse(res, true, result2);
                    }
                }
            });
        }
    } else {
        indexParam++;
        if (indexParam === 3) {
            sendResponseWithMsg(res, false, [], err2);
        }

    }
}

function ifTicketProductsExist(res, ticketProductsList, dbName, ticketpaymentsData, indexParam, loginId) {
    var updateQuery = '';
    for (var i = 0; i < ticketProductsList.length; i++) {
        updateQuery += 'UPDATE Product__c SET Quantity_On_Hand__c = Quantity_On_Hand__c - ' + parseInt(ticketProductsList[i].quantity) + ' WHERE Id = "' + ticketProductsList[i].Product__c + '";';
    }
    if (updateQuery && updateQuery.length > 0) {
        execute.query(dbName, updateQuery, function(prdErr, prdResult) {
            if (prdErr) {
                logger.error('Error in CheckOut dao - addToProduct:', prdErr);
                sendResponseWithMsg(res, false, [], prdErr);
            } else {
                indexParam++;
                if (indexParam === 3) {
                    sendResponse(res, true, prdResult);
                }
            }
        });
    } else {
        indexParam += 3;
        if (indexParam === 3) {
            sendResponse(res, true, prdResult);
        }

    }

}

function getCompanyEmail(dbName, callback) {
    execute.query(dbName, 'SELECT Email__c, Name from Company__c where isDeleted = 0', function(err, result) {
        if (err) {
            logger.error('Error in getCompanyEmail CommonSRVC:', err);
            callback('');
        } else {
            callback({
                'email': result[0]['Email__c'],
                'name': result[0]['Name']
            });
        }
    });
}

function sumAllPricesFunction(dbName, ticketpaymentsData, callback) {
    var prdPriceSum = 'SELECT IFNULL(SUM(Net_Price__c), 0) as Net_Price__c, IFNULL(SUM(Product_Tax__c), 0) as Product_Tax__c FROM Ticket_Product__c WHERE Appt_Ticket__c = "' + ticketpaymentsData.apptId + '"';
    execute.query(dbName, prdPriceSum, '', function(prdErr, prdData) {
        if (prdData.length > 0) {
            ticketpaymentsData.productAmount = prdData[0].Net_Price__c;
            ticketpaymentsData.productsTax = prdData[0].Product_Tax__c;
        } else {
            ticketpaymentsData.productAmount = '';
            ticketpaymentsData.productsTax = '';
        }
        var srvPriceSum = 'SELECT IFNULL(SUM(Net_Price__c), 0) as Net_Price__c, IFNULL(SUM(Service_Tax__c), 0) as Service_Tax__c FROM Ticket_Service__c WHERE Appt_Ticket__c = "' + ticketpaymentsData.apptId + '"';
        execute.query(dbName, srvPriceSum, '', function(srvErr, srvData) {
            if (srvData.length > 0) {
                ticketpaymentsData.serviceAmount = srvData[0].Net_Price__c;
                ticketpaymentsData.servicesTax = srvData[0].Service_Tax__c;
            } else {
                ticketpaymentsData.serviceAmount = '';
                ticketpaymentsData.servicesTax = '';
            }
            var otherPriceSum = 'SELECT IFNULL(SUM(Amount__c), 0) as Amount__c FROM Ticket_Other__c WHERE Ticket__c = "' + ticketpaymentsData.apptId + '"';
            execute.query(dbName, otherPriceSum, '', function(othErr, othData) {
                if (othData.length > 0) {
                    ticketpaymentsData.otherCharge = othData[0].Amount__c;
                } else {
                    ticketpaymentsData.otherCharge = '';
                }
                var tipPriceSum = 'SELECT IFNULL(SUM(Tip_Amount__c), 0) as Tip_Amount__c FROM Ticket_Tip__c WHERE Appt_Ticket__c = "' + ticketpaymentsData.apptId + '"';
                execute.query(dbName, tipPriceSum, '', function(tipErr, tipData) {
                    ticketpaymentsData.tipsCharge = tipData[0].Tip_Amount__c;
                    var payPriceSum = 'SELECT IFNULL(SUM(Amount_Paid__c), 0) as Amount_Paid__c FROM Ticket_Payment__c WHERE Appt_Ticket__c = "' + ticketpaymentsData.apptId + '"';
                    execute.query(dbName, payPriceSum, '', function(tipErr, paymentData) {
                        if (othData.length > 0) {
                            ticketpaymentsData.Amount_Paid__c = paymentData[0].Amount_Paid__c;
                        } else {
                            ticketpaymentsData.Amount_Paid__c = '';
                        }
                        callback(ticketpaymentsData);
                    });
                });
            });
        });
    });
}

function ticketsDataFunction(dbName, ticketpaymentsData, callback) {
    var sql = 'SELECT p.Id as Product__c, p.Standard_Cost__c, p.Size__c, p.Name,tp.Taxable__c, tp.Product_Tax__c, tp.Promotion__c, tp.Reward__c, tp.Id, ' +
        ' tp.Redeem_Rule_Name__c, IFNULL(tp.Net_Price__c,0) as Net_Price__c, tp.Price__c, tp.Qty_Sold__c as quantity,tp.Worker__c as workerId, ' +
        ' CONCAT(u.FirstName, " ", u.LastName) as workerName FROM Ticket_Product__c as tp LEFT JOIN Product__c as p on p.Id = tp.Product__c ' +
        ' LEFT JOIN User__c as u on u.Id =tp.Worker__c where tp.Appt_Ticket__c ="' + ticketpaymentsData.apptId + '" and tp.isDeleted =0 GROUP BY tp.Id';
    var sql1 = 'SELECT c.Id Client__c, tco.Online__c, CONCAT(u.FirstName, " ", u.LastName) workerName, p.Name as packageName, tco.* FROM Ticket_Other__c as tco ' +
        ' left JOIN Package__c as p on p.Id = tco.Package__c ' +
        ' left JOIN Appt_Ticket__c as a on a.Id = tco.Ticket__c ' +
        ' LEFT JOIN Contact__c c on c.Id = a.Client__c ' +
        ' left JOIN User__c as u on u.Id = tco.Worker__c ' +
        ' where tco.isDeleted = 0 and tco.Ticket__c = "' + ticketpaymentsData.apptId + '"';
    var sql2 = 'SELECT ts.Booked_Package__c,ts.Client_Package__c,at.isNoService__c, ts.Service_Tax__c, ts.Price__c, ts.Net_Price__c, ts.Id as TicketServiceId, ' +
        ' CONCAT(u.FirstName," ", u.LastName) as workerName, u.Id as workerId,ts.Notes__c,ts.reward__c, ts.Promotion__c, ' +
        ' s.Id as ServiceId, s.Name as ServiceName, ts.Net_Price__c as netPrice ' +
        ', ts.Taxable__c, ts.Redeem_Rule_Name__c FROM Ticket_Service__c as ts JOIN Service__c as s on ts.Service__c = s.Id ' +
        ' LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c LEFT JOIN User__c as u on u.Id = ts.Worker__c WHERE ts.Appt_Ticket__c = "' + ticketpaymentsData.apptId + '" and ts.Booked_Package__c != "" and ts.Booked_Package__c != "0" and ts.Client_Package__c = "" and ts.isDeleted = 0';
    execute.query(dbName, sql, '', function(err, prdData) {
        execute.query(dbName, sql1, '', function(err1, othData) {
            execute.query(dbName, sql2, '', function(err2, srvData) {
                ticketpaymentsData.ticketProductsList = prdData;
                ticketpaymentsData.ticketOthersList = othData;
                ticketpaymentsData.ticketServices = srvData;
                ticketpaymentsData.Amount_Paid__c = parseFloat(ticketpaymentsData.amountToPay) + parseFloat(ticketpaymentsData.Amount_Paid__c);
                if (ticketpaymentsData.clientId && ticketpaymentsData.Amount_Paid__c >= ticketpaymentsData.listCharge) {
                    var query = "SELECT * from Package__c where Active__c=" + 1 + ' and isDeleted=0';
                    execute.query(dbName, query, '', function(error, packageData) {
                        var clientPckgArray = [];
                        if ((othData && othData.length > 0) && (ticketpaymentsData.clientId)) {
                            for (let i = 0; i < othData.length; i++) {
                                for (let j = 0; j < packageData.length; j++) {
                                    if (othData[i]['Package__c'] === packageData[j]['Id']) {
                                        const tempPkgListJSON = JSON.parse(packageData[j].JSON__c);
                                        clientPckgArray.push({
                                            'pckgId': packageData[j]['Id'],
                                            'clientId': this.ticketOthersList[i]['Client__c'],
                                            'apptId': this.apptId,
                                            'pckgDetails': tempPkgListJSON,
                                        });
                                    }
                                }
                            }
                        } else {
                            ticketpaymentsData.clientPckgData = clientPckgArray;
                        }
                        var query1 = 'SELECT * from Contact__c where Id = "' + ticketpaymentsData.clientId + '" and Active_Rewards__c = "1" and IsDeleted = 0';
                        execute.query(dbName, query1, '', function(error1, cltData) {
                            if (cltData.length > 0) {
                                var query2 = 'SELECT * from Reward__c where Active__c = "1" and IsDeleted = 0';
                                execute.query(dbName, query2, '', function(error2, rwtData) {
                                    if (rwtData.length > 0) {
                                        var usedPoints = 0;
                                        var gainPoints = 0;
                                        var cltRwdId;
                                        var isNew;
                                        var temp = JSON.parse(rwtData[0].Award_Rules__c);
                                        var temp2 = JSON.parse(rwtData[0].Redeem_Rules__c);
                                        var query3 = 'SELECT Appt_Date_Time__c from Appt_Ticket__c where Id = "' + ticketpaymentsData.apptId + '"';
                                        execute.query(dbName, query3, '', function(error3, apptData) {
                                            var query4 = 'SELECT Redeem_Rule_Name__c from Ticket_Service__c where Appt_Ticket__c = "' + ticketpaymentsData.apptId + '"';
                                            execute.query(dbName, query4, '', function(error4, tktsrvData) {
                                                var query5 = 'SELECT Redeem_Rule_Name__c from Ticket_Product__c where Appt_Ticket__c = "' + ticketpaymentsData.apptId + '"';
                                                execute.query(dbName, query5, '', function(error5, tktprdData) {
                                                    var query6 = 'SELECT Id, Client__c from Client_Reward__c where Client__c = "' + ticketpaymentsData.clientId + '"';
                                                    execute.query(dbName, query6, '', function(error6, cltrwdData) {
                                                        if (cltrwdData.length > 0) {
                                                            isNew = false;
                                                            cltRwdId = cltrwdData[0].Id;
                                                        } else {
                                                            isNew = true;
                                                            cltRwdId = '';
                                                        }
                                                        for (let j = 0; j < temp.length; j++) {
                                                            if (new Date(temp[j].startDate).getTime() < new Date(apptData[0].Appt_Date_Time__c).getTime() && new Date(apptData[0].Appt_Date_Time__c).getTime() < new Date(temp[j].endDate).getTime()) {
                                                                if (temp[j]['item'] === 'Services') {
                                                                    if (temp[j]['forEvery'] === 'Amount Spent On') {
                                                                        gainPoints = gainPoints + (parseFloat(temp[j]['awardPoints']) * parseFloat(ticketpaymentsData.serviceAmount));
                                                                    } else if (temp[j]['forEvery'] === 'Individual') {
                                                                        gainPoints = gainPoints + (tktsrvData.length * parseFloat(temp[j]['awardPoints']));
                                                                    }
                                                                }
                                                                if (temp[j]['item'] === 'Products') {
                                                                    if (temp[j]['forEvery'] === 'Amount Spent On') {
                                                                        gainPoints = gainPoints + (parseFloat(temp[j]['awardPoints']) * parseFloat(ticketpaymentsData.productAmount));
                                                                    } else if (temp[j]['forEvery'] === 'Individual') {
                                                                        gainPoints = gainPoints + (tktprdData.length * parseFloat(temp[j]['awardPoints']));
                                                                    }
                                                                }
                                                                if (temp[j]['item'] === 'Gifts') {
                                                                    gainPoints = gainPoints + (parseFloat(temp[j]['awardPoints']) * (ticketpaymentsData.giftCharge));
                                                                }
                                                            }
                                                        }
                                                        if (tktsrvData && tktsrvData.length > 0) {
                                                            for (let i = 0; i < tktsrvData.length; i++) {
                                                                for (let j = 0; j < temp2.length; j++) {
                                                                    if (temp2[j]['redeemName'] === tktsrvData[i]['Redeem_Rule_Name__c']) {
                                                                        usedPoints = usedPoints + temp2[j]['redeemPoints'];
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (tktprdData && tktprdData.length > 0) {
                                                            for (let i = 0; i < tktprdData.length; i++) {
                                                                for (let j = 0; j < temp2.length; j++) {
                                                                    if (temp2[j]['redeemName'] === tktprdData[i]['Redeem_Rule_Name__c']) {
                                                                        usedPoints = usedPoints + temp2[j]['redeemPoints'];
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ticketpaymentsData.clientRwdData = [];
                                                        ticketpaymentsData.clientRwdData.push({ 'rwId': rwtData[0].Id, 'points': gainPoints, 'used': usedPoints, 'crId': cltRwdId, 'isNew': isNew });
                                                        callback(ticketpaymentsData);
                                                    });
                                                })
                                            });
                                        });
                                    } else {
                                        ticketpaymentsData.clientRwdData = rwtData;
                                        callback(ticketpaymentsData);
                                    }
                                });
                            } else {
                                ticketpaymentsData.clientRwdData = cltData;
                                callback(ticketpaymentsData);
                            }
                        });
                    });
                } else {
                    ticketpaymentsData.clientPckgData = [];
                    ticketpaymentsData.clientRwdData = [];
                    callback(ticketpaymentsData);
                }
            });
        });
    });
}
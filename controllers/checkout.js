var cfg = require('../common/config');
var mysql = require('mysql');
var async = require('async');
var pdf = require('html-pdf');
var execute = require('../common/dbConnection');
var moment = require('moment');
var uniqid = require('uniqid');
var fs = require('fs');
var logger = require('../common/logger');
var dateFns = require('./../common/dateFunctions');
var sendResponse = require('../common/response').sendResponse;
var sendResponseWithMsg = require('../common/response').sendResponseWithMsg;
var htmlemailtemp = require('../common/response').Mailtemplate;
var path = require('path');
var sendGrid = require('@sendgrid/mail');
var FCM = require('fcm-push');
sendGrid.setApiKey("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");


// --- Start of Controller
module.exports.controller = function (app) {
    app.get('/api/appointments/date/worker/:type/:appdate/:worker/:viewBy', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var weekStart = moment(req.params.appdate).startOf('week').format('YYYY-MM-DD 00:00:00'); // for week start
        var weekEnd = moment(req.params.appdate).endOf('week').format('YYYY-MM-DD 23:59:59'); // for week end
        moment.suppressDeprecationWarnings = true;
        var userDate = req.params.appdate.split(' ')[0];
        var usrSql = 'select View_Only_My_Appointments__c from User__c where Id = "' + loginId + '" ';
        var sqlQuery = "SELECT c.Id as clientId,a.Name as apptName, GROUP_CONCAT(DISTINCT ts.Service__c) as serviceIds,GROUP_CONCAT(DISTINCT ts.Id) as ticketServiceIds, IFNULL((a.Service_Sales__c+ a.Service_Tax__c),0) Service_Sales__c, u.View_Only_My_Appointments__c, CONCAT(u.FirstName,' ', LEFT(u.LastName,1)) as bookoutName, ts.Worker__c as workerId,a.Id as apptid, a.Appt_Date_Time__c as apdate,a.Status__c as apstatus, a.Is_Booked_Out__c, CONCAT(c.FirstName, ' ', c.LastName) as clientName, " +
            " ts.Visit_Type__c as visttype, c.MobilePhone as mbphone, ts.Rebooked__c as rebook, c.Email as cltemail, a.New_Client__c as newclient, " +
            " c.Phone as cltphone, a.Is_Standing_Appointment__c as standingappt, a.Has_Booked_Package__c as pkgbooking, a.Booked_Online__c as bookonline, " +
            " a.Notes__c as notes, c.Current_Balance__c, c.Client_Pic__c as clientpic,  GROUP_CONCAT(IF (ts.Booked_Package__c = '', NULL, ts.Booked_Package__c)) as Booked_Package__c, ts.Service_Group_Color__c, ts.Service_Date_Time__c as srvcDate, s.Name as srvcname, SUM(ts.Net_Price__c) as netprice, " +
            " ts.Duration__c as duration, GROUP_CONCAT(s.Name,' ', '(', IFNULL(CONCAT(u.FirstName,' ', LEFT(u.LastName,1)), 'Inactive Worker'), ')') as workerName, s.Service_Group__c, ts.Resources__c as resource, a.CreatedDate as creadate, " +
            " a.LastModifiedDate as lastmofdate FROM Appt_Ticket__c as a left join Contact__c as c on c.Id = a.Client__c and c.IsDeleted = 0 left join Ticket_Service__c as " +
            " ts on ts.isDeleted=0 and ts.Appt_Ticket__c = a.Id left join Service__c as s on s.Id = ts.Service__c left join User__c as u on u.Id = ts.Worker__c AND u.IsActive = 1 ";
        if (req.params.worker === 'all' && req.params.viewBy === 'One Day') {
            var type = req.params.type;
            var apptDate1 = req.params.appdate;
            var apptDate2 = dateFns.getDBNxtDay(req.params.appdate);
            sqlQuery = sqlQuery + " WHERE " +
                " a.Appt_Date_Time__c >='" + apptDate1 + "' and a.Appt_Date_Time__c <='" + apptDate2 + "' ";
            if (req.params.type === 'complete') {
                sqlQuery = sqlQuery + "and a.Status__c = 'Complete' ";
            } else if (req.params.type === 'check_out') {

                sqlQuery = sqlQuery + "and a.Status__c = 'Checked In' ";
            } else {
                sqlQuery = sqlQuery + "and a.Status__c != 'Complete' and a.Status__c != 'Checked In' and a.Status__c != '' and a.Status__c != 'Canceled' ";
            }
        } else if (req.params.viewBy === 'One Day' && req.params.worker !== 'all') {
            var type = req.params.type;
            var apptDate1 = req.params.appdate;
            var apptDate2 = dateFns.getDBNxtDay(req.params.appdate);
            sqlQuery = sqlQuery + " WHERE " +
                " ts.Worker__c ='" + req.params.worker + "' and a.Appt_Date_Time__c >='" + apptDate1 + "' " +
                " and a.Appt_Date_Time__c <='" + apptDate2 + "' ";
        } else if (req.params.viewBy === 'One Week' && req.params.worker === 'all') {
            sendResponse(res, false, 'One Week or One Weekday view requires selection of a Worker');
        } else if (req.params.viewBy === 'One Weekday' && req.params.worker === 'all') {
            sendResponse(res, false, 'One Week or One Weekday view requires selection of a Worker');
        } else if (req.params.viewBy === 'One Week' && req.params.worker !== 'all') {
            var startOfWeek1 = weekStart;
            var endOfWeek1 = weekEnd;
            sqlQuery = sqlQuery + " WHERE " +
                " ts.Worker__c ='" + req.params.worker + "' and a.Appt_Date_Time__c >='" + startOfWeek1 + "' " +
                " and a.Appt_Date_Time__c <='" + endOfWeek1 + "' ";
        } else if (req.params.viewBy === 'One Weekday' && req.params.worker !== 'all') {
            var monthDates = dateFns.getDBWkDays(req.params.appdate);
            montStr = '(';
            for (var i = 0; i < monthDates.length; i++) {
                montStr += '\'' + monthDates[i] + '\',';
            }
            montStr = montStr.slice(0, -1) + ')';
            sqlQuery = sqlQuery + " WHERE " +
                " ts.Worker__c ='" + req.params.worker + "' and DATE(a.Appt_Date_Time__c) in " + montStr + " " +
                " ";
        } else {
            var apptDate1 = req.params.appdate;
            var apptDate2 = dateFns.getDBNxtDay(req.params.appdate);
            var type = req.params.type;

            sqlQuery = sqlQuery + " WHERE " +
                " a.Appt_Date_Time__c >='" + apptDate1 + "' and a.Appt_Date_Time__c <='" + apptDate2 + "' ";
            if (req.params.type === 'complete') {

                sqlQuery = sqlQuery + "a.Status__c = Complete ";
            }
        }
        execute.query(dbName, usrSql, '', function (err, result) {
            if (err) {
                logger.error('Error in appoitments dao - getApptBookingData:', err);
                sendResponse(res, false, '');
            } else if (result.length > 0 && result[0]['View_Only_My_Appointments__c'] === 1) {
                sqlQuery = sqlQuery + " and ts.Worker__c ='" + loginId + "' and a.isNoService__c =0 and ts.Id is not null group by a.Id order by a.Appt_Date_Time__c asc";
            } else {
                sqlQuery = sqlQuery + "and a.isNoService__c =0 and ts.Id is not null group by a.Id order by a.Appt_Date_Time__c asc";
            }
            var taxQuery = `SELECT JSON__c as SalesTax FROM Preference__c as pref WHERE pref.Name='Sales Tax'`
            execute.query(dbName, sqlQuery + ';' + taxQuery, '', function (err, result) {
                if (err) {
                    logger.error('Error in appoitments dao - getApptBookingData:', err);
                    sendResponse(res, false, '');
                } else {
                    var netprice = 0;
                    var query = '';
                    if (result[0].length > 0) {
                        for (var i = 0; i < result[0].length; i++) {
                            result[0][i]['SalesTax'] = result[1][0]['SalesTax'];
                            netprice += result[0][i]['netprice'];
                            query += `SELECT DISTINCT Id, FirstName, LastName, Sms_Consent__c,
                            IFNULL(Client_Pic__c,"") AS image, IFNULL(MobilePhone, "") AS MobilePhone,
                            IFNULL(Phone, "") AS Phone, Email AS Email, IFNULL(Mobile_Carrier__c, "") AS Mobile_Carrier__c,
                            IFNULL(REPLACE(REPLACE(REPLACE(Phone, "(", ""), ")", ""), "-", ""),'') AS modiPhone,
                            REPLACE(REPLACE(REPLACE(MobilePhone, "(", ""), ")", ""), "-","") AS modfMobilePhone,
                            IsDeleted, IFNULL(Booking_Restriction_Type__c,'') as Booking_Restriction_Type__c,
                            BR_Reason_No_Email__c as noEmail, BR_Reason_Account_Charge_Balance__c as acctCharge,
                            Current_Balance__c, BR_Reason_Deposit_Required__c as depReq, BR_Reason_No_Show__c as noShow,
                            IFNULL(BR_Reason_Other_Note__c ,'') as note, BR_Reason_Other__c as other 
                            FROM Contact__c WHERE Id = "` + result[0][i].clientId + `" AND IsDeleted = 0 AND CONCAT(FirstName, ' ', LastName) != 'NO CLIENT';`;
                        }
                        execute.query(dbName, query, '', function (error, clientData) {
                            if (clientData.length > 0) {
                                for (let j = 0; j < result[0].length; j++) {
                                    result[0][j].client_data = clientData[j][0];
                                }
                                var results = {};
                                results.result = result[0];
                                results.TotalNetPrice = netprice;
                                sendResponse(res, true, results);
                            } else {
                                var results = {};
                                results.result = result[0];
                                results.TotalNetPrice = netprice;
                                sendResponse(res, true, results);
                            }

                        });
                    } else {
                        sendResponse(res, false, '');
                    }
                }
            });
        });
    });
    //name id color
    app.post('/api/checkout/products', function (req, res) {
        var dbName = req.headers['db'];
        //   var sqlQuery = 'SELECT p.*, pl.Color__c, IFNULL(p.Price__c, 0) as price, pl.Color__c FROM Product__c as p '
        //       + ' JOIN Product_Line__c as pl on pl.Id = p.Product_Line__c  WHERE p.Active__c = 1 AND pl.Active__c = 1 AND p.IsDeleted = 0 GROUP BY p.Id';
        var sqlQuery = 'SELECT Name as name, Id as id, Color__c as color FROM Product_Line__c WHERE Active__c = 1 AND IsDeleted = 0';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                sendResponseWithMsg(res, false, [], err)
            } else {
                sendResponse(res, true, result)
            }
        });
    });

    app.post('/api/checkout/products/:productid', function (req, res) {
        var dbName = req.headers['db'];
        var prodcut_lineid = req.params.productid;
        var sqlQuery = 'SELECT p.*, pl.Color__c, IFNULL(p.Price__c, 0) as price, pl.Color__c FROM Product__c as p ' +
            ' JOIN Product_Line__c as pl on pl.Id = p.Product_Line__c  WHERE p.Product_Line__c="' + prodcut_lineid + '" AND p.Active__c = 1 AND pl.Active__c = 1 AND p.IsDeleted = 0 GROUP BY p.Id';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - getCheckOutProducts:', err);
                sendResponseWithMsg(res, false, [], err)
            } else {
                sendResponse(res, true, result)
            }
        });
    });
    app.post('/api/checkout/services', function (req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT * FROM Preference__c WHERE Name = "Service Groups"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (result && result.length > 0) {
                var JSON__c_str = JSON.parse(result[0].JSON__c);
                result[0].JSON__c = JSON__c_str.sort(function (a, b) {
                    return a.sortOrder - b.sortOrder
                });
                var results = [];
                for (var i = 0; i < result[0].JSON__c.length; i++) {
                    var data = {};
                    data.name = result[0].JSON__c[i].serviceGroupName;
                    data.color = result[0].JSON__c[i].serviceGroupColor;
                    data.id = result[0].JSON__c[i].serviceGroupName;
                    results.push(data);
                }
                sendResponse(res, true, results)

            } else {
                if (err) {
                    sendResponseWithMsg(res, false, [], err)
                } else {
                    sendResponseWithMsg(res, false, [], "An unexpected error occurred on the server side")
                }

            }
        });
    });
    app.post('/api/checkout/services/:group', function (req, res) {
        var dbName = req.headers['db'];
        var group = req.params.group;
        var sqlQuery = '';
        sqlQuery += `SELECT * FROM Preference__c WHERE Name = 'Service Groups';`;
        sqlQuery += `SELECT S.*, IFNULL(ws.Duration_1__c , 0) as Duration_1,u.Service_Level__c,
        IFNULL(ws.Duration_2__c, 0) as Duration_2,
        IFNULL(ws.Duration_3__c,0) as Duration_3,
        IFNULL(ws.Buffer_After__c,0) as Buffer_After,
        IFNULL(S.Guest_Charge__c, 0) Guest_Charge,
        S.Service_Group__c, S.Id as id, S.Name as name,
        S.Levels__c,"Service" as type 
        FROM Worker_Service__c as ws 
        LEFT JOIN Service__c as S on  S.Id= ws.Service__c 
        LEFT JOIN User__c as u on u.Id = ws.Worker__c 
        WHERE S.Service_Group__c = '` + group + `' AND 
        S.isDeleted =0 AND S.Active__c = 1 AND u.IsActive = 1
        AND S.Is_Class__c = 0 GROUP BY S.Id`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - getCheckOut:', err);
                sendResponseWithMsg(res, false, [], err)
            } else {
                var statusColor;
                if (result[0] && result[0].length > 0) {
                    var JSON__c_str = JSON.parse(result[0][0].JSON__c);
                    for (let i = 0; i < JSON__c_str.length; i++) {
                        if (JSON__c_str[i].serviceGroupName == group) {
                            statusColor = JSON__c_str[i].serviceGroupColor;
                            break;
                        }
                    }
                }
                result[1].forEach(element => {
                    element.Service_Group__c = statusColor;
                });
                sendResponse(res, true, result[1])
            }
        });
    });

    app.post('/api/appointments/changestatus/:id', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var cmpId = req.headers['cid'];
        var dateTime = req.headers['dt'];
        var sqlQuery = '';
        var records = [];
        var records1 = [];
        var queries = '';
        var statusUpdatedDate;
        var clientCurBal;
        if (!req.body.apstatus) {
            req.body = req.body.apptrst[0];
            statusUpdatedDate = req.body.Appt_Date_Time__c;
            clientCurBal = parseInt(req.body.Current_Balance__c);
        } else {
            statusUpdatedDate = req.body.Status_Date_Time_c;
            clientCurBal = parseInt(req.body.Current_Balance__c);
        }
        var changeStatusTo = req.body.apstatus;
        var apptId = req.params.id;
        if (req.body.apstatus) {
            var changeStatusTo = "";
            if (req.body.apstatus == "Checked In") {
                changeStatusTo = "Checked In";
            } else if (req.body.apstatus != 'Complete' || req.body.apstatus != 'Checked In' || req.body.apstatus != '') {
                changeStatusTo = "Checked In";
            }
            var indexParams = 0;
            sqlQuery += 'UPDATE Appt_Ticket__c SET Status__c = "' + changeStatusTo + '", isTicket__c = 1, ' +
                ' Check_In_Time__c = "' + statusUpdatedDate + '",LastModifiedDate = "' + dateTime + '",' +
                ' LastModifiedById = "' + loginId + '" WHERE Id = "' + apptId + '";'
            sqlQuery += 'UPDATE `Ticket_Service__c` SET `Status__c` = "' + changeStatusTo + '" WHERE `Appt_Ticket__c` = "' + apptId + '";'
            if (req.body.ticketService && req.body.ticketService.length > 0) {

                var paymentsql = 'SELECT * FROM `Payment_Types__c` WHERE Name = "Prepaid Package"';
                for (var i = 0; i < req.body.ticketService.length; i++) {
                    sqlQuery += ` UPDATE Ticket_Service__c SET Net_Price__c = ` + req.body.ticketService[i]['Net_Price__c'] + `,Price__c = ` + req.body.ticketService[i]['Price__c'] + `,
                                Service_Tax__c = ` + req.body.ticketService[i]['Service_Tax__c'] + `,Client_Package__c ='` + req.body.ticketService[i]['Client_Package__c'] + `',Booked_Package__c ='` + req.body.ticketService[i]['Booked_Package__c'] + `' WHERE Id = '` + req.body.ticketService[i]['tsId'] + `';`
                }

                if (req.body.appointment) {
                    sqlQuery += ` UPDATE Appt_Ticket__c SET Has_Booked_Package__c = 1, Service_Sales__c = ` + req.body.appointment['Service_Sales__c'] + `,
                                Service_Tax__c = ` + req.body.appointment['Service_Tax__c'] + ` WHERE Id = '` + req.body.appointment['Appt_Ticket__c'] + `';`
                }
                if (req.body.updatingClientPackages.length > 0) {

                    for (var i = 0; i < req.body.updatingClientPackages.length; i++) {
                        sqlQuery += ` UPDATE Client_Package__c SET Package_Details__c = '` + JSON.stringify(req.body.updatingClientPackages[i]['Package_Details__c']) + `'
                                    WHERE Id = '` + req.body.updatingClientPackages[i]['Id'] + `';`
                    }
                }
                if (req.body.ticketOther && req.body.ticketOther.length > 0) {
                    for (i = 0; i < req.body.ticketOther.length; i++) {
                        records.push([
                            uniqid(),
                            0,
                            dateTime,
                            loginId,
                            dateTime,
                            loginId,
                            dateTime,
                            req.body.ticketOther[i].Appt_Ticket__c,
                            req.body.ticketOther[i].Amount__c,
                            'Package',
                            req.body.ticketOther[i].pckId,
                            req.body.ticketOther[i].Package_Price__c,
                            req.body.ticketOther[i].Service_Tax__c
                        ])
                    }
                }
                execute.query(dbName, paymentsql, function (getPamntErr, getPamntResult) {
                    if (getPamntResult && getPamntResult.length > 0) {
                        var paymentSql2 = 'SELECT Id, Appt_Ticket__c, Payment_Type__c FROM Ticket_Payment__c WHERE Payment_Type__c = "' + getPamntResult[0].Id + '" && Appt_Ticket__c ="' + apptId + '" AND isDeleted=0';
                        execute.query(dbName, paymentSql2, '', function (paymntErr1, paymntResult1) {
                            if (paymntErr1) {
                                logger.error('Error in appoitments dao - changeStatus:', paymntErr1);

                            } else {
                                if (paymntResult1 && paymntResult1.length > 0) {
                                    queries += 'UPDATE Ticket_Payment__c SET Amount_Paid__c = Amount_Paid__c+"' + req.body.payment.Amount_Paid__c
                                        // + '", Drawer_Number__c = "' + req.body.payment.Drawer_Number__c
                                        +
                                        '", LastModifiedDate = "' + dateTime +
                                        '",LastModifiedById = "' + loginId +
                                        '" WHERE Appt_Ticket__c = "' + req.body.apptId + '" && Payment_Type__c = "' + getPamntResult[0].Id + '" && IsDeleted=0';
                                } else if (req.body.ticketService && req.body.ticketService.length > 0) {
                                    if (!req.body.payment.Drawer_Number__c) {
                                        req.body.payment.Drawer_Number__c = null;
                                    }
                                    var ticketPaymentObj = {
                                        Id: uniqid(),
                                        IsDeleted: 0,
                                        CreatedDate: dateTime,
                                        CreatedById: loginId,
                                        LastModifiedDate: dateTime,
                                        LastModifiedById: loginId,
                                        SystemModstamp: dateTime,
                                        LastModifiedDate: dateTime,
                                        Amount_Paid__c: req.body.payment.Amount_Paid__c,
                                        Appt_Ticket__c: req.body.payment.Appt_Ticket__c,
                                        Approval_Code__c: '',
                                        Drawer_Number__c: req.body.payment.Drawer_Number__c,
                                        Notes__c: '',
                                        Payment_Type__c: getPamntResult[0].Id,
                                    }
                                    var insertQuery2 = 'INSERT INTO Ticket_Payment__c SET ?';
                                    execute.query(dbName, insertQuery2, ticketPaymentObj, function (err2, result2) {
                                        if (err2) { } else {
                                            indexParams++;
                                            if (indexParams == 3) {
                                                var resp = {};
                                                resp.status = true;
                                                resp.data = result2;
                                                resp.res = "";
                                                res.json(resp);
                                            }
                                        }
                                    });
                                }
                                if (queries && queries.length > 0) {
                                    execute.query(dbName, queries, function (cltPkgEditErr, cltPkgEditResult) {
                                        if (cltPkgEditErr) {
                                            logger.error('Error in CheckOut dao - addToTicketpayments:', cltPkgEditErr);
                                            callback(cltPkgEditErr, { statusCode: '9999' });
                                        } else {
                                            //1
                                            indexParams++;
                                            if (indexParams == 3) {
                                                var resp = {};
                                                resp.status = true;
                                                resp.data = cltPkgEditResult;
                                                resp.res = "";
                                                res.json(resp);
                                            }
                                        }
                                    });
                                }
                            }
                        });
                    }
                    execute.query(dbName, sqlQuery, '', function (err, result) {
                        if (err) {
                            logger.error('Error in appoitments dao - changeStatus:', err);
                        } else {
                            var insertQuery1 = 'INSERT INTO Ticket_Other__c (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                ' SystemModstamp, Ticket__c, Amount__c, Transaction_Type__c,Package__c, Package_Price__c, Service_Tax__c)VALUES ?';

                            if (records && records.length > 0) {
                                execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                    if (err1) {
                                        logger.error('Error in appoitments dao - changeStatus:', err1);
                                    } else {
                                        // 2
                                        indexParams++;
                                        if (indexParams == 3) {
                                            var resp = {};
                                            resp.status = true;
                                            resp.data = result1;
                                            resp.res = "";
                                            res.json(resp);
                                        }

                                    }
                                });
                            } else {
                                // 2
                                indexParams++;
                                if (indexParams == 3) {
                                    var resp = {};
                                    resp.status = true;
                                    resp.data = null;
                                    resp.res = "";
                                    res.json(resp);
                                }
                            }

                        }
                        if (clientCurBal > 0) {

                            data = {
                                'Ticket__c': req.body.apptId,
                                'Amount__c': req.body.client_data.Current_Balance__c,
                                'Transaction_Type__c': 'Received on Account'
                            }
                            req.body = data;
                            addToTicketOther(req, function (err, data) {
                                indexParams++;
                                if (indexParams == 3) {
                                    var resp = {};
                                    resp.status = true;
                                    resp.data = result;
                                    resp.res = "";
                                    res.json(resp);
                                }
                            })
                        } else if (clientCurBal < 0 && req.body.serviceSales > 0) {
                            var paymentSql = `SELECT Id, Name FROM Payment_Types__c WHERE Name  ='Account Charge'`;
                            execute.query(dbName, paymentSql, '', function (err, ptyresult) {
                                if (ptyresult.length > 0) {
                                    var ticketpaymentSql = `SELECT Id, Payment_Type__c
                                        FROM Ticket_Payment__c WHERE Payment_Type__c  ='` + ptyresult[0].Id + `'
                                        AND Appt_Ticket__c = '` + req.body.apptId + `'`;
                                    execute.query(dbName, ticketpaymentSql, '', function (err, tktptyresult) {
                                        if (tktptyresult.length === 0) {
                                            data = {
                                                'Id': uniqid(),
                                                'apptId': req.body.apptId,
                                                'paymentType': ptyresult[0].Id,
                                                'amountToPay': (req.body.serviceSales < Math.abs(req.body.clientCurBal) ?
                                                    req.body.serviceSales : Math.abs(req.body.clientCurBal))
                                            }
                                            var paymentOtherObjData = {
                                                Id: uniqid(),
                                                IsDeleted: 0,
                                                CreatedDate: dateTime,
                                                CreatedById: loginId,
                                                LastModifiedDate: dateTime,
                                                LastModifiedById: loginId,
                                                SystemModstamp: dateTime,
                                                Amount_Paid__c: (req.body.serviceSales < Math.abs(req.body.clientCurBal) ?
                                                    req.body.serviceSales : Math.abs(req.body.clientCurBal)),
                                                Appt_Ticket__c: req.body.apptId,
                                                Payment_Type__c: ptyresult[0].Id,
                                            };
                                            var insertQuery = 'INSERT INTO Ticket_Payment__c SET ?';
                                            if (req.body.clientId && req.body.clientId !== '') {
                                                var updateQuery = ' UPDATE Contact__c SET Current_Balance__c=Current_Balance__c+' + (req.body.serviceSales + req.body.serviceTaxAmt) + ' WHERE Id="' + req.body.clientId + '";';
                                            }
                                            execute.query(dbName, insertQuery + ';' + updateQuery, paymentOtherObjData, function (ticketPaymentErr, ticketPaymentResult) {
                                                indexParams++;
                                                if (indexParams == 3) {
                                                    var resp = {};
                                                    resp.status = true;
                                                    resp.data = ticketPaymentResult;
                                                    resp.Error = ticketPaymentErr;
                                                    resp.res = "";
                                                    res.json(resp);
                                                }

                                            });
                                        } else {
                                            indexParams++;
                                            if (indexParams == 3) {
                                                var resp = {};
                                                resp.status = true;
                                                resp.data = result;
                                                resp.Error = err;
                                                resp.res = "";
                                                res.json(resp);
                                            }

                                        }
                                    });
                                } else {
                                    indexParams++;
                                    if (indexParams == 3) {
                                        var resp = {};
                                        resp.status = true;
                                        resp.data = result;
                                        resp.Error = err;
                                        resp.res = "";
                                        res.json(resp);
                                    }
                                }
                            });
                        } else {
                            indexParams++;
                            if (indexParams == 3) {
                                var resp = {};
                                resp.status = true;
                                resp.data = result;
                                resp.Error = err;
                                resp.res = "";
                                res.json(resp);
                            }
                        }
                    });
                });
            } else {
                execute.query(dbName, sqlQuery, '', function (err, ptyresult) {
                    if (clientCurBal > 0) {

                        data = {
                            'Ticket__c': req.body.apptId,
                            'Amount__c': clientCurBal,
                            'Transaction_Type__c': 'Received on Account'
                        }
                        req.body = data;
                        addToTicketOther(req, function (err, data) {
                            indexParams = 3;
                            if (indexParams == 3) {
                                if (err) {
                                    sendResponseWithMsg(res, false, [], err)
                                } else {
                                    sendResponse(res, true, data)
                                }
                            }
                        })
                    } else if (clientCurBal < 0 && req.body.serviceSales >= 0) {

                        var paymentSql = `SELECT Id, Name FROM Payment_Types__c WHERE Name  ='Account Charge'`;
                        execute.query(dbName, paymentSql, '', function (err, ptyresult) {
                            if (ptyresult.length > 0) {

                                var ticketpaymentSql = `SELECT Id, Payment_Type__c
                                FROM Ticket_Payment__c WHERE Payment_Type__c  ='` + ptyresult[0].Id + `'
                                AND Appt_Ticket__c = '` + req.body.apptId + `'`;
                                execute.query(dbName, ticketpaymentSql, '', function (err, tktptyresult) {
                                    if (tktptyresult.length === 0) {

                                        data = {
                                            'Id': uniqid(),
                                            'apptId': req.body.apptId,
                                            'paymentType': ptyresult[0].Id,
                                            'amountToPay': (req.body.serviceSales < Math.abs(req.body.Current_Balance__c) ?
                                                req.body.serviceSales : Math.abs(req.body.Current_Balance__c))
                                        }

                                        var paymentOtherObjData = {
                                            Id: uniqid(),
                                            IsDeleted: 0,
                                            CreatedDate: dateTime,
                                            CreatedById: loginId,
                                            LastModifiedDate: dateTime,
                                            LastModifiedById: loginId,
                                            SystemModstamp: dateTime,
                                            Amount_Paid__c: (req.body.serviceSales < Math.abs(req.body.Current_Balance__c) ?
                                                req.body.serviceSales : Math.abs(req.body.Current_Balance__c)),
                                            Appt_Ticket__c: req.body.apptId,
                                            Payment_Type__c: ptyresult[0].Id,
                                        };

                                        var insertQuery = 'INSERT INTO Ticket_Payment__c SET ?';
                                        if (req.body.clientId && req.body.clientId !== '') {

                                            var updateQuery = ' UPDATE Contact__c SET Current_Balance__c=Current_Balance__c+' + (req.body.serviceSales + req.body.serviceTaxAmt) + ' WHERE Id="' + req.body.clientId + '";';
                                        }
                                        execute.query(dbName, insertQuery + ';' + updateQuery, paymentOtherObjData, function (ticketPaymentErr, ticketPaymentResult) {
                                            indexParams = 3;
                                            if (indexParams == 3) {
                                                if (ticketPaymentErr) {
                                                    sendResponseWithMsg(res, false, [], ticketPaymentErr)
                                                } else {
                                                    sendResponse(res, true, ticketPaymentResult)
                                                }
                                            }
                                        });
                                    } else {
                                        indexParams = 3;
                                        if (indexParams == 3) {
                                            if (err) {
                                                sendResponseWithMsg(res, false, [], err)
                                            } else {
                                                sendResponse(res, true, [])
                                            }
                                        }
                                    }
                                });
                            } else {
                                indexParams = 3;
                                // 3
                                if (indexParams == 3) {
                                    if (err) {
                                        sendResponseWithMsg(res, false, [], err)
                                    } else {
                                        sendResponse(res, true, [])
                                    }
                                }
                            }
                        });
                    } else {
                        indexParams = 3;
                        // 3
                        if (indexParams == 3) {
                            //    var resp = {};
                            //    resp.status = true;
                            //    resp.data = [];
                            //    resp.Error = err;
                            //    resp.res = "";
                            //    res.json(resp);
                            if (err) {
                                sendResponseWithMsg(res, false, [], err)
                            } else {
                                sendResponse(res, true, [])
                            }
                        }
                        //  sendChangeStatusResponse(indexParams, done, err, []);
                    }
                })
            }
            var sqlQuery1 = `SELECT ts.Worker__c, ts.Client__c, ts.Service_Date_Time__c, ts.Service__c, s.Name,
                            c.FirstName as cFirstName, c.LastName as cLastName, c.Id as cId, u.Device_Token__c DeviceToken, u.FirstName, u.LastName
                            FROM Ticket_Service__c ts LEFT JOIN User__c u on u.Id=ts.Worker__c
                            LEFT JOIN Contact__c c on c.Id = ts.Client__c
                            LEFT JOIN Service__c s on s.Id = ts.Service__c
                            WHERE ts.Appt_Ticket__c= '` + apptId + `' ORDER BY ts.Service_Date_Time__c ASC`;
            execute.query(dbName, sqlQuery1, '', function (wrkErr, wrkObj) {
                var msg = '';
                var workerId = [];
                var values = [];
                var clientId = '';
                if (wrkObj && wrkObj[0].cId) {
                    clientId = wrkObj[0].cId;
                }
                for (let i = 0; i < wrkObj.length; i++) {
                    var date = getUsrDtStrFrmDBStr(wrkObj[i].Service_Date_Time__c);
                    var clientName = '';
                    if (!wrkObj[i].cFirstName) {
                        clientName = 'No Client'
                    } else {
                        clientName = wrkObj[i].cFirstName + ' ' + wrkObj[i].cLastName;
                    }
                    if (msg === '') {
                        msg = 'CHECKED IN:' + '\n' + clientName + '\n' + date[1] + ' ' +
                            wrkObj[i].Name + ' ' + wrkObj[i].FirstName + ' ' + wrkObj[i].LastName;
                    } else {
                        msg = msg + '.\n' + date[1] + ' ' + wrkObj[i].Name + ' ' + wrkObj[i].FirstName + ' ' + wrkObj[i].LastName;
                    }
                    if (values.indexOf(wrkObj[i].Worker__c) === -1) {
                        workerId.push(wrkObj[i]);
                        values.push(wrkObj[i].Worker__c);
                    }
                }
                for (let k = 0; k < workerId.length; k++) {
                    var serverKey = cfg.serviceKey;
                    var fcm = new FCM(serverKey);
                    var message = {
                        to: workerId[k].DeviceToken,
                        collapse_key: 'your_collapse_key',
                        data: {
                            // apptId: apptId
                            your_custom_data_key: 'your_custom_data_value'
                        },
                        notification: {
                            title: '',
                            body: msg,
                            // aps: {
                            //     sound: 'default',
                            ApptId: apptId,
                            ClientID: clientId
                            // }
                        }
                    };
                    fcm.send(message, function (err, response) {
                        if (err) {
                            logger.info("Something has gone wrong!", err);
                        } else {
                            logger.info("Successfully sent with response: ", response);
                        }
                    });
                }
            });
        } else {

            var resp = {};
            resp.status = false;
            resp.data = "apstatus should not be empty or false";
            resp.Error = 'err';
            resp.res = "";
            res.json(resp);
        }
    });

    app.post('/api/checkout/promotion', function (req, res) {
        //  Product_Discount__c is 0
        var dbName = req.headers['db'];
        var sqlQuery = `SELECT *, IFNULL(Discount_Amount__c,0) as discountAmount,
                        IFNULL(Discount_Percentage__c,0) as discountPers FROM Promotion__c
                        WHERE isDeleted = 0 and Active__c = 1
                        and (Start_Date__c <= '` + req.headers.dt + `' or Start_Date__c IS NULL)
                        and (End_Date__c >= '` + req.headers.dt + `' or End_Date__c IS NULL)`;
        if (req.body.type_of_service === 'services') {
            sqlQuery += ` and Service_Discount__c = 1`;
        } else if (req.body.type_of_service === 'products') {
            sqlQuery += ` and Product_Discount__c = 1`;
        }
        sqlQuery += ` order by Sort_Order__c ASC`;
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                sendResponseWithMsg(res, false, [], err)
            } else {
                sendResponse(res, true, result)
            }
        });
    })

    app.post('/api/Checkout/worker/services/:serviceid/:type?', function (req, res) {
        var dbName = req.headers['db'];
        var Id = req.params.serviceid;
        var type = req.params.type;
        var sqlQuery = 'SELECT CONCAT(u.FirstName," " , LEFT(u.LastName,1)) as FullName,IFNULL(ws.Price__c,0) Price__c,u.Service_Level__c,s.Guest_Charge__c, s.Id as serviceId, s.Levels__c, s.Name as serviceName, u.Id,u.Id workername, ' +
            ' IF(ws.Price__c = null, IFNULL(s.Price__c,0), IF(ws.Price__c>0,ws.Price__c, IFNULL(s.Price__c,0))) as show__Price, ' +
            ' CONCAT(u.FirstName," " , u.LastName) as workerName, u.StartDay,u.Book_Every__c, s.Taxable__c,' +
            'IFNULL(ws.Duration_1__c, 0) as Duration_1,' +
            'IFNULL(ws.Duration_2__c, 0) as Duration_2,' +
            'IFNULL(ws.Duration_3__c,0) as Duration_3,' +
            'IFNULL(ws.Buffer_After__c,0) as Buffer_After,' +
            'IFNULL(ws.Duration_1_Available_For_Other_Work__c, 0) as Duration_1_Available_For_Other_Work__c,' +
            'IFNULL(ws.Duration_2_Available_For_Other_Work__c,0) as Duration_2_Available_For_Other_Work__c,' +
            'IFNULL(ws.Duration_3_Available_For_Other_Work__c,0) as Duration_3_Available_For_Other_Work__c,' +
            ' IFNULL(s.Duration_1__c,0) as Duration_1__c,' +
            ' IFNULL(s.Duration_2__c,0) as Duration_2__c,' +
            ' IFNULL(s.Duration_3__c,0) as Duration_3__c,' +
            ' IFNULL(s.Buffer_After__c,0) as Buffer_After__c,' +
            ' s.Duration_1_Available_For_Other_Work__c as SDuration_1_Available_For_Other_Work__c,' +
            ' s.Duration_2_Available_For_Other_Work__c as SDuration_2_Available_For_Other_Work__c,' +
            ' s.Duration_3_Available_For_Other_Work__c as SDuration_3_Available_For_Other_Work__c,' +
            ' GROUP_CONCAT(DISTINCT(r.Name)) Resources__c, s.Resource_Filter__c' +
            ' FROM Worker_Service__c as ws' +
            ' RIGHT JOIN Service__c as s on s.Id = ws.Service__c ' +
            ' LEFT JOIN Service_Resource__c as sr on sr.Service__c = s.Id and sr.IsDeleted=0' +
            ' LEFT JOIN Resource__c as r on r.Id = sr.Resource__c' +
            ' JOIN User__c as u on u.Id =ws.Worker__c WHERE ws.Service__c = "' + Id + '" '
        if (type) {
            sqlQuery += 'AND ws.Self_Book__c = 1'
        }
        sqlQuery += ' and ws.isDeleted =0 and u.IsActive =1 GROUP BY u.Id ORDER BY u.Display_Order__c IS NULL, u.Display_Order__c ASC';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                sendResponseWithMsg(res, false, [], err);
            } else {
                for (var i = 0; i < result.length; i++) {
                    if (result[i]['Price__c'] === null || result[i]['Price__c'] === 'null' ||
                        result[i]['Price__c'] === 0) {
                        for (var j = 0; j < JSON.parse(result[i].Levels__c).length; j++) {
                            if (result[i]['Service_Level__c'] === JSON.parse(result[i].Levels__c)[j]['levelNumber']) {
                                result[i]['Price'] = JSON.parse(result[i].Levels__c)[j]['price'];
                            }
                        }
                    }
                    if ((result[i]['Duration_1'] === null || result[i]['Duration_1'] === 'null' ||
                        result[i]['Duration_1'] === 0)) {
                        for (var j = 0; j < JSON.parse(result[i].Levels__c).length; j++) {
                            if (result[i]['Service_Level__c'] === JSON.parse(result[i].Levels__c)[j]['levelNumber']) {
                                result[i]['Duration_1'] = JSON.parse(result[i].Levels__c)[j]['duration1'] ? JSON.parse(result[i].Levels__c)[j]['duration1'] : 0;
                                result[i]['Duration_2'] = JSON.parse(result[i].Levels__c)[j]['duration2'] ? JSON.parse(result[i].Levels__c)[j]['duration2'] : 0;
                                result[i]['Duration_3'] = JSON.parse(result[i].Levels__c)[j]['duration3'] ? JSON.parse(result[i].Levels__c)[j]['duration3'] : 0;
                                result[i]['Buffer_After'] = JSON.parse(result[i].Levels__c)[j]['bufferAfter'] ? JSON.parse(result[i].Levels__c)[j]['bufferAfter'] : 0;
                                result[i]['Duration_1__c'] = JSON.parse(result[i].Levels__c)[j]['duration1'] ? JSON.parse(result[i].Levels__c)[j]['duration1'] : 0;
                                result[i]['Duration_2__c'] = JSON.parse(result[i].Levels__c)[j]['duration2'] ? JSON.parse(result[i].Levels__c)[j]['duration2'] : 0;
                                result[i]['Duration_3__c'] = JSON.parse(result[i].Levels__c)[j]['duration3'] ? JSON.parse(result[i].Levels__c)[j]['duration3'] : 0;
                                result[i]['Buffer_After__c'] = JSON.parse(result[i].Levels__c)[j]['bufferAfter'] ? JSON.parse(result[i].Levels__c)[j]['bufferAfter'] : 0;
                                result[i]['Duration_1_Available_For_Other_Work__c'] = JSON.parse(result[i].Levels__c)[j]['duration1AvailableForOtherWork'] ? JSON.parse(result[i].Levels__c)[j]['duration1AvailableForOtherWork'] : 0;
                                result[i]['Duration_2_Available_For_Other_Work__c'] = JSON.parse(result[i].Levels__c)[j]['duration2AvailableForOtherWork'] ? JSON.parse(result[i].Levels__c)[j]['duratduration2AvailableForOtherWork'] : 0;
                                result[i]['Duration_3_Available_For_Other_Work__c'] = JSON.parse(result[i].Levels__c)[j]['duration3AvailableForOtherWork'] ? JSON.parse(result[i].Levels__c)[j]['duration3AvailableForOtherWork'] : 0;
                            } else {
                                result[i]['Duration_1'] = result[i]['Duration_1__c'];
                                result[i]['Duration_2'] = result[i]['Duration_2__c'];
                                result[i]['Duration_3'] = result[i]['Duration_3__c'];
                                result[i]['Buffer_After'] = result[i]['Buffer_After__c'];
                                result[i]['Duration_1__c'] = result[i]['Duration_1__c'];
                                result[i]['Duration_2__c'] = result[i]['Duration_2__c'];
                                result[i]['Duration_3__c'] = result[i]['Duration_3__c'];
                                result[i]['Buffer_After__c'] = result[i]['Buffer_After__c'];
                                result[i]['Duration_1_Available_For_Other_Work__c'] = result[i]['SDuration_1_Available_For_Other_Work__c'];
                                result[i]['Duration_2_Available_For_Other_Work__c'] = result[i]['SDuration_2_Available_For_Other_Work__c'];
                                result[i]['Duration_3_Available_For_Other_Work__c'] = result[i]['SDuration_3_Available_For_Other_Work__c'];
                            }
                        }
                    } else {
                        result[i]['Duration_1__c'] = result[i]['Duration_1'];
                        result[i]['Duration_2__c'] = result[i]['Duration_2'];
                        result[i]['Duration_3__c'] = result[i]['Duration_3'];
                        result[i]['Buffer_After__c'] = result[i]['Buffer_After'];
                        result[i]['Duration_1_Available_For_Other_Work__c'] = result[i]['Duration_1_Available_For_Other_Work__c'];
                        result[i]['Duration_2_Available_For_Other_Work__c'] = result[i]['Duration_2_Available_For_Other_Work__c'];
                        result[i]['Duration_3_Available_For_Other_Work__c'] = result[i]['Duration_3_Available_For_Other_Work__c'];
                    }
                }
                if (result.length != 0) {
                    sendResponse(res, true, result)
                } else {
                    sendResponseWithMsg(res, false, result, "No workers")
                }

            }
        });
    })

    app.post('/api/checkout/client/rewards/:id', function (req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT cr.Points_Balance__c as points, r.*, cr.Id as crId, crd.Id as crdId FROM Reward__c as r LEFT JOIN Client_Reward__c as cr on r.Id = cr.Reward__c ' +
            ' LEFT JOIN Client_Reward_Detail__c as crd on crd.Client_Reward__c = cr.Id  WHERE cr.Client__c = "' + req.params.id + '" and r.isDeleted=0';
        execute.query(dbName, sqlQuery, '', function (rwdErr, rwdResult) {
            if (rwdErr) {

            } else {

                var resultfinal = []
                for (var i = 0; i < rwdResult.length; i++) {
                    var pares = JSON.parse(rwdResult[i].Redeem_Rules__c);
                    for (var j = 0; j < pares.length; j++) {
                        var data = {}
                        data = pares[j];
                        data['parent_title'] = rwdResult[i].Name;
                        data['parent_id'] = rwdResult[i].Id;
                        resultfinal.push(data);
                    }
                }
                sendResponse(res, true, resultfinal)
            }
        });

    })

    app.post('/api/Checkout/worker/products/:id/:type?', function (req, res) {
        var dbName = req.headers['db'];
        var productid = req.params.id;
        var sqlQuery = 'SELECT * from User__c where  Retail_Only__c = 1 ';
        if (req.params.type) {
            sqlQuery += ' or isActive =1 order by case when Display_Order__c is null then 1 else 0 end, Display_Order__c,CONCAT(FirstName, " ", LastName), CreatedDate'
        } else {
            sqlQuery += ' order by case when Display_Order__c is null then 1 else 0 end, Display_Order__c,CONCAT(FirstName, " ", LastName), CreatedDate'
        }
        var sqlQuery1 = 'SELECT Price__c as show__Price FROM Product__c WHERE Id="' + productid + '"';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                sendResponseWithMsg(res, false, "", err)
            } else {
                execute.query(dbName, sqlQuery1, '', function (err1, result1) {
                    if (err1) {
                        sendResponseWithMsg(res, false, "", err)
                    } else {
                        result_final = [];
                        for (var i = 0; i < result.length; i++) {
                            var data = result[i];
                            data['show__Price'] = result1[0].show__Price;
                            data['name'] = result[i].FirstName + " " + result[i].LastName;
                            result_final.push(data);
                        }

                        sendResponse(res, true, result_final)
                    }
                });
            }
        });
    })
    app.post('/api/checkout/favorites', function (req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT JSON__c FROM Preference__c WHERE Name = "Favorite Items" ';
        execute.query(dbName, sqlQuery, '', function (err, JSON_Cresult) {
            if (JSON_Cresult && JSON_Cresult.length > 0) {
                var JSON__c_str = JSON.parse(JSON_Cresult[0].JSON__c);
                var serviceIds = '';
                var productIds = '';
                var promotionIds = '';
                for (var i = 0; i < JSON__c_str.length; i++) {
                    JSON__c_str[i]['name'] = '';
                    if (JSON__c_str[i].id && JSON__c_str[i].id != '') {
                        if (JSON__c_str[i].type === 'Service') {
                            serviceIds += '\'' + JSON__c_str[i].id + '\',';
                        } else if (JSON__c_str[i].type === 'Product') {
                            productIds += '\'' + JSON__c_str[i].id + '\',';
                        } else if (JSON__c_str[i].type === 'Promotion') {
                            promotionIds += '\'' + JSON__c_str[i].id + '\',';

                        }
                    }
                }
                var Json_c_str_cc = []
                for (var i = 0; i < JSON__c_str.length; i++) {
                    if (JSON__c_str[i].type != "Promotion") {
                        Json_c_str_cc.push(JSON__c_str[i])
                    }
                }
                var temp = 0;
                resultJson = [];
                if (serviceIds.length > 0) {
                    serviceIds = serviceIds.slice(0, -1);
                    serviceIds = '(' + serviceIds + ')';
                    var serviceSql = `
              SELECT
                 s.Id,
                 s.Service_Group__c,
                 s.Price__c,
                     s.Name,
                     ws.Worker__c,
                 IFNULL(
                     s.Duration_1__c,
                     ws.Duration_1__c
                 ) AS Duration_1__c,
                 IFNULL(
                     s.Duration_2__c,
                     ws.Duration_2__c
                 ) AS Duration_2__c,
                 IFNULL(
                     s.Duration_3__c,
                     ws.Duration_3__c
                 ) AS Duration_3__c,
                 IFNULL(
                     s.Buffer_After__c,
                     ws.Buffer_After__c
                 ) AS Buffer_After__c,
                 s.Guest_Charge__c,
                 s.Taxable__c
             FROM
                 Service__c AS s
             LEFT JOIN Worker_Service__c AS ws on ws.Service__c = s.Id
             WHERE
                 s.IsDeleted = 0 AND s.Id IN
                     ` + serviceIds + `
                  GROUP BY s.Id`
                    execute.query(dbName, serviceSql, '', function (err, result) {
                        if (!err) {

                            if (result && result.length > 0) {
                                for (var i = 0; i < result.length; i++) {
                                    resultJson.push(result[i]);
                                }
                            }
                        }
                        temp++;
                        if (temp == 3) {
                            if (err) {
                                sendResponseWithMsg(res, false, "", err)
                            } else {
                                sendResponse(res, true, updateJsonc(Json_c_str_cc, resultJson))
                            }
                        }
                    });
                } else {
                    temp++;
                    if (temp == 3) {
                        if (err) {
                            sendResponseWithMsg(res, false, "", err)
                        } else {
                            sendResponse(res, true, updateJsonc(Json_c_str_cc, resultJson))
                        }
                    }
                }
                if (productIds.length > 0) {
                    productIds = productIds.slice(0, -1);
                    productIds = '(' + productIds + ')';
                    var productSql = 'SELECT Id, CONCAT(Name, " - ", Size__c, " ", Unit_of_Measure__c) as Name, Product_Line__c, Price__c, ' +
                        ' Product_Pic__c, Size__c, Taxable__c, Unit_of_Measure__c FROM Product__c WHERE IsDeleted  = 0 And Id IN ' + productIds;
                    execute.query(dbName, productSql, '', function (err, result) {
                        if (!err) {
                            if (result && result.length > 0) {
                                for (var i = 0; i < result.length; i++) {
                                    resultJson.push(result[i]);
                                }
                            }
                        }
                        temp++;
                        if (temp == 3) {
                            if (err) {
                                sendResponseWithMsg(res, false, "", err)
                            } else {
                                sendResponse(res, true, updateJsonc(Json_c_str_cc, resultJson))
                            }
                        }
                    });

                } else {
                    temp++;
                    if (temp == 3) {
                        if (err) {
                            sendResponseWithMsg(res, false, "", err)
                        } else {
                            sendResponse(res, true, updateJsonc(Json_c_str_cc, resultJson))
                        }
                    }
                }
                if (promotionIds.length > 0) {
                    promotionIds = promotionIds.slice(0, -1);
                    promotionIds = '(' + promotionIds + ')';
                    var promotionSql = 'SELECT Id, Name, Start_Date__c, End_Date__c, Discount_Amount__c, Discount_Percentage__c, Product_Discount__c, Active__c, Service_Discount__c FROM Promotion__c WHERE IsDeleted  = 0 And Id IN ' + promotionIds;
                    execute.query(dbName, promotionSql, '', function (err, result) {
                        if (!err) {
                            if (result && result.length > 0) {
                                for (var i = 0; i < result.length; i++) {
                                    resultJson.push(result[i]);
                                }
                            }
                        }
                        temp++;

                        if (temp == 3) {
                            if (err) {
                                sendResponseWithMsg(res, false, "", err)
                            } else {
                                sendResponse(res, true, updateJsonc(Json_c_str_cc, resultJson))
                            }
                        }
                    });

                } else {

                    temp++;

                    if (temp == 3) {
                        if (err) {
                            sendResponseWithMsg(res, false, "", err)
                        } else {
                            sendResponse(res, true, updateJsonc(Json_c_str_cc, resultJson))
                        }
                    }
                }
            } else {
                sendResponseWithMsg(res, false, "", err)


            }
        });

    });
    app.post('/api/checkout/favs', function (req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT JSON__c FROM Preference__c WHERE Name = "Favorite Items" ';
        execute.query(dbName, sqlQuery, '', function (err, JSON_Cresult) {
            if (JSON_Cresult && JSON_Cresult.length > 0) {
                var JSON__c_str = JSON.parse(JSON_Cresult[0].JSON__c);
                var serviceIds = '';
                var productIds = '';
                var promotionIds = '';
                for (var i = 0; i < JSON__c_str.length; i++) {
                    JSON__c_str[i]['name'] = '';
                    if (JSON__c_str[i].id && JSON__c_str[i].id != '') {
                        if (JSON__c_str[i].type === 'Service') {
                            serviceIds += '\'' + JSON__c_str[i].id + '\',';
                        } else if (JSON__c_str[i].type === 'Product') {
                            productIds += '\'' + JSON__c_str[i].id + '\',';
                        } else if (JSON__c_str[i].type === 'Promotion') {
                            promotionIds += '\'' + JSON__c_str[i].id + '\',';
                        }
                    }
                }
                var Json_c_str_cc = []
                for (var i = 0; i < JSON__c_str.length; i++) {
                    if (JSON__c_str[i].type != "Promotion") {
                        Json_c_str_cc.push(JSON__c_str[i])
                    }
                }
                resultJson = [];
                if (serviceIds.length > 0) {
                    serviceIds = serviceIds.slice(0, -1);
                    serviceIds = '(' + serviceIds + ')';
                } else {
                    console.log("Services Length is Zero")
                }
                if (productIds.length > 0) {
                    productIds = productIds.slice(0, -1);
                    productIds = '(' + productIds + ')';
                    var sqlQuerypr = 'SELECT p.*, pl.Color__c, IFNULL(p.Price__c, 0) as price, pl.Color__c FROM Product__c as p ' +
                        ' JOIN Product_Line__c as pl on pl.Id = p.Product_Line__c  WHERE p.Active__c = 1 AND pl.Active__c = 1 AND p.IsDeleted = 0 AND p.Id IN ' + productIds + ' GROUP BY p.Id';
                    execute.query(dbName, sqlQuerypr, '', function (errp, prdata) {
                        if (!errp) {
                            var sqlQueryserv = 'SELECT S.*, IFNULL(ws.Duration_1__c , 0) as Duration_1,u.Service_Level__c,IFNULL(ws.Duration_2__c, 0) as Duration_2,IFNULL(ws.Duration_3__c,0) as Duration_3,IFNULL(ws.Buffer_After__c,0) as Buffer_After,IFNULL(S.Guest_Charge__c, 0) Guest_Charge,S.Service_Group__c, S.Id as id, S.Name as name,S.Levels__c,"Service" as type FROM Worker_Service__c as ws LEFT JOIN Service__c as S on  S.Id= ws.Service__c LEFT JOIN User__c as u on u.Id = ws.Worker__c WHERE S.Id IN ' + serviceIds + ' AND S.isDeleted =0 AND S.Active__c = 1 AND u.IsActive = 1 AND S.Is_Class__c = 0 GROUP BY S.Id';
                            execute.query(dbName, sqlQueryserv, '', function (errs, serdata) {
                                if (!errs) {
                                    resultJson.push({ "Products": prdata }, { "Services": serdata });
                                    sendResponseWithMsg(res, true, resultJson, "Favourites Data")
                                } else {
                                    sendResponseWithMsg(res, false, "", errs)
                                }
                            })
                        } else {
                            sendResponseWithMsg(res, false, "", errp)
                        }
                    })
                } else {
                    sendResponseWithMsg(res, false, "", errp)
                }
            } else {
                sendResponseWithMsg(res, false, "", err)
            }
        });
    });
    app.post('/api/setup/company/paymenttypes', function (req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT *, \'\' as Icon_Name FROM Payment_Types__c WHERE isDeleted = 0 order by Sort_Order__c ASC;';
        sqlQuery += 'SELECT * FROM Company__c WHERE isDeleted = 0; ';
        sqlQuery += 'SELECT JSON__c FROM Preference__c  WHERE Name = "Receipt Memo" ORDER BY Name';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in PaymentTypes dao - getPaymentTypes:', err);
            } else {
                sendResponse(res, true, { 'paymentResult': result[0], 'Id': uniqid(), 'cmpInfo': result[1], 'recieptMemo': result[2] })
            }
        });
    });

    app.post('/api/Checkout/worker/tips', function (req, res) {
        var dbName = req.headers['db'];
        var sqlQuery = 'SELECT * FROM `User__c` WHERE IsActive = 1 OR Retail_Only__c = 1';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in Tips dao:', err);
            } else {
                sendResponse(res, true, result)
            }
        });
    })

    app.post('/api/checkout/emailreciept', function (req, res) {

        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var recieptMailData = req.body;
        var cname = req.headers.cname ? req.headers.cname : '';
        var fileName = recieptMailData.apptName;
        var htmlFile = htmlemailtemp();
        fs.writeFile("templates/reciept.html", htmlFile, function (err) {

            if (err) {
                return err;
            } else {
                var html = fs.readFileSync('templates/reciept.html', 'utf8');
                var options = { format: 'Letter' };
                pdf.create(html, options).toFile('uploads/receipts/' + fileName + '.pdf', function (err, res1) {
                    if (err) {
                        logger.error('Error in creating pdf file:', err);
                        sendResponse(err, false, '')
                    } else {
                        fs.readFile('uploads/receipts/' + fileName + '.pdf', function (err, data) {
                            if (err) {
                                logger.error('Error in reading pdf file :', err);
                                sendResponse(err, false, '')
                            } else {
                                var subjectText = cname + ' Green Receipt: ' + fileName;
                                data = new Buffer(data).toString('base64');
                                var attachment = [{ filename: 'Report.pdf', content: data }]
                                var htmlMessage = "Attached is your green receipt from" + cname;
                                execute.query(dbName, 'SELECT Email__c, Name from Company__c where isDeleted = 0', function (err, result) {
                                    if (err) {
                                        logger.error('Error in getCompanyEmail CommonSRVC:', err);
                                    } else {
                                        var fromAddress = result[0]['Email__c'];
                                        var name = result[0]['Name'];
                                        var toAddress = recieptMailData.clientEmail;
                                        if (!fromAddress) {
                                            fromAddress = {
                                                email: 'info@stxsoftware.com',
                                                name: 'STX Team'
                                            };
                                        }
                                        if (!subjectText) {
                                            subjectText = 'STX';
                                        }
                                        if (!htmlMessage) {
                                            htmlMessage = '<div></div>';
                                        }
                                        var mailOptions = {
                                            to: toAddress,
                                            from: fromAddress,
                                            subject: subjectText,
                                            html: htmlMessage
                                        };
                                        if (attachment) {
                                            mailOptions.attachments = attachment;
                                        }
                                        sendGrid.send(mailOptions, function (error, info) {
                                            if (error) {
                                                logger.error('Error in Email Receipt - EmailReciept:', error);
                                                sendResponse(error, true, error)
                                            } else {
                                                fs.readdir('uploads/receipts/', (err, files) => {
                                                    if (err) throw err;
                                                    for (const file of files) {
                                                        fs.unlink(path.join('uploads/receipts/', file), err => {
                                                            if (err) throw err;
                                                        });
                                                    }
                                                });
                                                if (recieptMailData.clientId) {
                                                    var insertData = {
                                                        Appt_Ticket__c: recieptMailData.apptId,
                                                        Client__c: recieptMailData.clientId,
                                                        Sent__c: dateTime,
                                                        Type__c: 'Receipt Email',
                                                        Name: 'Appt Receipt Email',
                                                        Id: uniqid(),
                                                        OwnerId: loginId,
                                                        IsDeleted: 0,
                                                        CreatedDate: dateTime,
                                                        CreatedById: loginId,
                                                        LastModifiedDate: dateTime,
                                                        LastModifiedById: loginId,
                                                        SystemModstamp: dateTime,
                                                        LastModifiedDate: dateTime,
                                                    }
                                                    var sqlQuery = 'INSERT INTO Email__c SET ?';
                                                    execute.query(dbName, sqlQuery, insertData, function (err3, result1) {
                                                        if (err3) {
                                                            logger.error('Error in send  email - insert into Email_c table:', err1);
                                                        } else {
                                                            sendResponseWithMsg(res, true, "Success", 'Email receipt sent successfully')
                                                            return false;
                                                        }
                                                    });
                                                } else {
                                                    sendResponseWithMsg(res, true, "Success", 'Email receipt sent successfully')
                                                    return false;
                                                }

                                            }
                                        });

                                    }
                                });
                            }
                        });
                    }
                });
                fs.unlink('templates/reciept.html', (err) => {
                    if (err) throw err;
                });
            }
        });
    });

    app.post('/api/checkout/tips', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var tipsData = req.body;
        tipRecurse(0, res, tipsData, dbName, loginId, dateTime);
    });
    app.get('/api/checkout/tips/:id', function (req, res) {
        var dbName = req.headers['db'];
        var Id = req.params.id;
        var sqlQuery = 'SELECT u.Id as Worker__c, CONCAT(u.FirstName," ",u.LastName) as workerName ,SUM(tp.Tip_Amount__c) as Tip_Amount__c, tp.Appt_Ticket__c, tp.Id as tipId FROM Ticket_Tip__c as tp ' +
            ' left join User__c as u on u.Id = tp.Worker__c ' +
            ' where tp.Appt_Ticket__c = "' + Id + '" and tp.isDeleted = 0 GROUP BY u.Id';
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - getTips:', err);
                sendResponse(res, true, err)
            } else {
                sendResponse(res, true, result)
            }
        });
    });
    app.put('/api/checkout/tips/deletetip', function (req, res) {

        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var ticketid = req.body.tipId;
        var apptid = req.body.Appt_Ticket__c;
        var tipamount = req.body.Tip_Amount__c;
        var sqlquery = "DELETE FROM Ticket_Tip__c WHERE Id = '" + ticketid + "'";
        execute.query(dbName, sqlquery, '', function (err, result) {
            if (!err) {
                var sq1 = "SELECT Tips__c FROM `Appt_Ticket__c` WHERE Id = '" + apptid + "' ";
                execute.query(dbName, sq1, '', function (err1, result1) {
                    if (!err1) {
                        var tip = result1[0].Tips__c;
                        var tipamountded = tip - tipamount;
                        var sql2 = 'UPDATE Appt_Ticket__c SET Tips__c = "' + tipamountded +
                            '",LastModifiedById = "' + loginId + '" WHERE Id = "' + apptid + '"';
                        execute.query(dbName, sql2, '', function (err2, result2) {
                            if (!err2) {
                                sendResponseWithMsg(res, true, result2, 'Tip Deleted successfully')
                            } else {
                                logger.error('Error in CheckOut dao - Delete Tp:', err2);
                                sendResponseWithMsg(res, false, result2, '')
                            }
                        })
                    } else {
                        logger.error('Error in CheckOut dao - Delete Tp:', err1);
                        sendResponseWithMsg(res, false, result1, '')
                    }
                })
            } else {
                logger.error('Error in CheckOut dao - Delete Tp:', err);
                sendResponseWithMsg(res, false, result, '')
            }
        })
    });
    app.put('/api/checkout/tips/:id', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        var Id = req.params.id;
        var tipObj = req.body.tips_data;
        tipUpdateRecursive(0, res, tipObj, dbName, loginId, dateTime, Id);
    });
    app.post('/api/checkout/addPaymentTipAmt', function (req, res) {
        var dbName = req.headers['db'];
        var dateTime = req.headers['dt'];
        var records = [];
        var record = [];
        var insertRecod = [];
        var loginId;
        var mainSql = '';
        if (req.headers['id']) {
            loginId = req.headers['id'];
        } else {
            loginId = uniqid();
        }
        var TipPersonObj = req.body;
        if (req.body.insert && req.body.insert.length > 0) {
            insertRecod = req.body.insert;
        } else {
            insertRecod = req.body.incluTicket;
        }
        if (insertRecod.length > 0) {
            records.push([
                uniqid(),
                0,
                dateTime,
                loginId,
                dateTime,
                loginId,
                dateTime,
                TipPersonObj.total,
                TipPersonObj.apptId,
                'AnywhereCommerce',
                TipPersonObj.Payment_Type__c,
            ]);
            var sqlQuery = 'INSERT INTO  Ticket_Payment__c' +
                ' (Id,IsDeleted,CreatedDate,CreatedById,LastModifiedDate,LastModifiedById,SystemModstamp,' +
                'Amount_Paid__c,Appt_Ticket__c,Payment_Gateway_Name__c,Payment_Type__c) VALUES ?';
        }
        execute.query(dbName, sqlQuery, [records], function (err, result) {

            if (err) {
                console.log(err);
            } else {

                if (result && result.affectedRows > 0) {
                    if (req.body.tipsArray.length > 0) {
                        var tipArry = req.body.tipsArray;
                        for (var t = 0; t < insertRecod.length; t++) {
                            var sql = " update Ticket_Tip__c set " +
                                " Tip_Option__c='" + req.body.tipName + "', " +
                                " Drawer_Number__c='" + tipArry[t].Drawer_Number__c + "' ," +
                                " Tip_Amount__c ='" + (+(insertRecod[t].split(' ')[1]) + (+tipArry[t].tipAmount)).toFixed(2) + "' " +
                                " where Id='" + tipArry[t].id + "' ";
                            execute.query(dbName, sql, '', function (err, result) {
                                if (err) {
                                    sendResponse(err, false, result)
                                    //  done(err, result);
                                } else {
                                    sendResponse(res, true, result)
                                    // done(err, result);
                                }
                            });
                        }
                    } else {
                        for (var i = 0; i < insertRecod.length; i++) {
                            record.push([
                                uniqid(),
                                TipPersonObj.Drawer_Number__c,
                                insertRecod[i].split(' ')[1],
                                insertRecod[i].split(' ')[0],
                                TipPersonObj.tipName,
                                TipPersonObj.apptId,
                                loginId,
                                loginId,
                                0
                            ]);
                            mainSql += `SELECT * FROM Ticket_Tip__c WHERE
                                          Appt_Ticket__c = '` + TipPersonObj.apptId + `' AND Worker__c= '` + insertRecod[i].split(' ')[0] + `';`
                            var sqlQuerys = 'INSERT INTO  Ticket_Tip__c' +
                                ' (Id,Drawer_Number__c,Tip_Amount__c,Worker__c,Tip_Option__c,Appt_Ticket__c,CreatedById,LastModifiedById,IsDeleted) VALUES ?';
                        }
                        execute.query(dbName, mainSql, '', function (mainerrs, mainresult) {
                            if (mainerrs) {

                            } else {
                                if (mainresult && mainresult.length > 0) {
                                    var sql = " update Ticket_Tip__c set " +
                                        " Tip_Option__c='" + req.body.tipName + "', " +
                                        " Drawer_Number__c='" + req.body.Drawer_Number__c + "' ," +
                                        " Tip_Amount__c =Tip_Amount__c+'" + req.body.total.toFixed(2) + "' " +
                                        " where Id='" + mainresult[0]['Id'] + "'; ";
                                    execute.query(dbName, sql, '', function (err, result) {
                                        if (err) {
                                            done(err, result);
                                        } else {
                                            sendResponse(res, true, result)
                                            //done(err, result);
                                        }
                                    });
                                } else {
                                    execute.query(dbName, sqlQuerys, [record], function (errs, result1) {
                                        if (errs) {
                                            logger.error('Error in CheckOut dao - extraTipAmt:', errs);
                                            done(errs, { statusCode: '9999' });
                                        } else {
                                            sendResponse(res, true, result)
                                            //  done(errs, result);
                                        }
                                    });
                                }
                            }
                        });

                    }

                } else {
                    sendResponse(res, true, result)
                    //    done(err, result);
                }
            }
        });

    });

    app.post('/api/checkout/pre_ticket_summery/:id', function (req, res) {
        var dbName = req.headers['db'];
        var ticketId = req.params.id;
        if (ticketId == 'new') {
            res.json({ "status": false, "result": {}, "message": "No Message" });
        } else {
            var sqlQuery = 'SELECT ts.Booked_Package__c,ts.Client_Package__c,at.isNoService__c, ts.Service_Tax__c, ts.Price__c, ts.Net_Price__c, ts.Id as TicketServiceId, ' +
                ' CONCAT(u.FirstName," ", u.LastName) as workerName, u.Id as workerId,ts.Notes__c,ts.reward__c, ts.Promotion__c, ' +
                ' s.Id as ServiceId, s.Name as ServiceName, ts.Net_Price__c as netPrice ' +
                ', ts.Taxable__c, ts.Redeem_Rule_Name__c FROM Ticket_Service__c as ts JOIN Service__c as s on ts.Service__c = s.Id ' +
                ' LEFT JOIN Appt_Ticket__c as at on at.Id = ts.Appt_Ticket__c LEFT JOIN User__c as u on u.Id = ts.Worker__c WHERE ts.Appt_Ticket__c = "' + ticketId + '" and ts.isDeleted = 0';
            var sqlQueryProducts = 'SELECT p.Id as Product__c, p.Standard_Cost__c, p.Size__c, p.Name,tp.Taxable__c, tp.Product_Tax__c, tp.Promotion__c, tp.Reward__c, tp.Id, ' +
                ' tp.Redeem_Rule_Name__c, IFNULL(tp.Net_Price__c,0) as Net_Price__c, tp.Price__c, tp.Qty_Sold__c as quantity,tp.Worker__c as workerId, ' +
                ' CONCAT(u.FirstName, " ", u.LastName) as workerName FROM Ticket_Product__c as tp LEFT JOIN Product__c as p on p.Id = tp.Product__c ' +
                ' LEFT JOIN User__c as u on u.Id =tp.Worker__c where tp.Appt_Ticket__c ="' + ticketId + '" and tp.isDeleted =0 GROUP BY tp.Id';
            execute.query(dbName, sqlQuery, '', function (err, result) {
                execute.query(dbName, sqlQueryProducts, '', function (err1, result1) {
                    if (err) {
                        logger.error('Unknown error in Pre_Ticket Summery:', err);
                        sendResponse(res, false, err);
                    } else {
                        if (result.length > 0 || result1.length > 0) {
                            var packIds = '(';
                            if (result && result.length > 0) {
                                for (i = 0; i < result.length; i++) {
                                    if (result[i]['Booked_Package__c'] !== '' && result[i]['Booked_Package__c'] !== null) {
                                        packIds = packIds + '"' + result[i]['Booked_Package__c'] + '",'
                                    }
                                }
                            }
                            packIds = packIds.substr(0).slice(0, -2);
                            packIds = packIds + '")';
                            var pckgQuery = 'select Id, Name as packageName, Discounted_Package__c as Package_Price__c, "Package" as Transaction_Type__c from Package__c where Id in ' + packIds + '';
                            execute.query(dbName, pckgQuery, '', function (pckgErr, pckgResult) {
                                if (err) {
                                    logger.error('Unknown error in Pre_Ticket Summery:', err);
                                    sendResponse(res, false, err);
                                } else {
                                    var sqlQueryTips = ' SELECT u.Id as Worker__c,tp.Drawer_Number__c, CONCAT(u.FirstName," ",u.LastName) as workerName ,SUM(tp.Tip_Amount__c) Tip_Amount__c, tp.Id as tipId,tp.Tip_Option__c FROM Ticket_Tip__c as tp ' +
                                        ' left join User__c as u on u.Id = tp.Worker__c ' +
                                        ' where tp.Appt_Ticket__c = "' + ticketId + '" and tp.isDeleted = 0 GROUP BY u.Id';
                                    execute.query(dbName, sqlQueryTips, '', function (err, result2) {
                                        if (err) {
                                            logger.error('Unknown error in Pre_Ticket Summery:', err);
                                            sendResponse(res, false, err);
                                        } else {
                                            var appointmentdet = "SELECT * FROM `Appt_Ticket__c` WHERE Id='" + ticketId + "'"
                                            execute.query(dbName, appointmentdet, '', function (err1, appintmedt) {
                                                if (!err1) {
                                                    var ticketpayments = "SELECT * FROM `Ticket_Payment__c` WHERE Appt_Ticket__c = '" + ticketId + "'";
                                                    execute.query(dbName, ticketpayments, '', function (err2, paymentsres) {
                                                        var serviceTotal = 0;
                                                        var ProductTotal = 0;
                                                        var TipTotal = 0;
                                                        var subtotal = 0;
                                                        var Tax = 0;
                                                        var TotalAmount = 0;
                                                        var TotalPaid = 0;
                                                        var alldata = [];
                                                        for (var i = 0; i < result.length; i++) {
                                                            var servicedata = {};
                                                            servicedata.type = "Services";
                                                            servicedata.name = result[i].ServiceName;
                                                            servicedata.price = result[i].Net_Price__c;
                                                            alldata.push(servicedata);
                                                            serviceTotal += result[i].Net_Price__c;
                                                            Tax += result[i].Service_Tax__c;
                                                            subtotal += result[i].Net_Price__c + result[i].Service_Tax__c;
                                                            TotalAmount += result[i].Net_Price__c + result[i].Service_Tax__c;

                                                        }
                                                        for (var j = 0; j < result1.length; j++) {
                                                            var productsdata = {};
                                                            productsdata.type = "Products";
                                                            productsdata.name = result1[j].Name + '(' + result1[j].quantity + ')';
                                                            productsdata.price = result1[j].Net_Price__c;
                                                            alldata.push(productsdata);
                                                            ProductTotal += result1[j].Net_Price__c * result1[j].quantity;
                                                            Tax += result1[j].Product_Tax__c;
                                                            subtotal += result1[j].Net_Price__c * result1[j].quantity + result1[j].Product_Tax__c;
                                                            TotalAmount += result1[j].Net_Price__c * result1[j].quantity + result1[j].Product_Tax__c;
                                                        }
                                                        for (var k = 0; k < result2.length; k++) {
                                                            TipTotal += result2[k].Tip_Amount__c;
                                                            // subtotal += result2[k].Tip_Amount__c;
                                                            TotalAmount += result2[k].Tip_Amount__c;
                                                        }
                                                        for (let m = 0; m < paymentsres.length; m++) {
                                                            TotalPaid += paymentsres[m].Amount_Paid__c;
                                                        }
                                                        var toamount = TotalAmount - TotalPaid;
                                                        var resp = {};
                                                        resp.alldata = alldata;
                                                        resp.ticetServices = result;
                                                        resp.packages = pckgResult;
                                                        resp.tickerProducts = result1;
                                                        resp.ticketTips = result2;
                                                        resp.serviceTotal = serviceTotal;
                                                        resp.ProductTotal = ProductTotal;
                                                        resp.TipTotal = TipTotal;
                                                        resp.SubTotal = subtotal;
                                                        resp.TaxTotal = Tax;
                                                        resp.CheckTotal = TotalAmount
                                                        resp.TotalAmount = TotalAmount - TotalPaid;
                                                        resp.payment = paymentsres;
                                                        resp.TicketNumber = appintmedt[0].Name;
                                                        resp.AutherizationCode = "";
                                                        if (toamount == 0) {
                                                            resp.TicketStatus = "Complete";
                                                            resp.TotalAmount = TotalAmount;
                                                        } else {
                                                            resp.TicketStatus = "";
                                                        }
                                                        sendResponse(res, true, resp);
                                                    })
                                                } else {
                                                    logger.error('Unknown error in Pre_Ticket Summery:', err1);
                                                    sendResponse(res, false, err1);
                                                }
                                            })
                                        }

                                    });


                                }
                            });
                        } else {
                            var respon = {};
                            respon.alldata = [];
                            sendResponse(res, false, respon);
                        }
                    }
                });
            });
        }
    })
    //This is using
    app.post('/api/checkout/createticket/:type', function (req, res) {
        var dbName = req.headers['db'];
        var loginId = req.headers['id'];
        var dateTime = req.headers['dt'];
        if (req.params.type == "new") {
            if (req.body.product) {
                var ticketProductObj = req.body;
                ticketProductObj.isNoService__c = req.body.product.isNoService__c;
                ticketProductObj.price = req.body.product.price;
                ticketProductObj.Qty_Sold__c = 1;
                ticketProductObj.Product__c = req.body.product.Id;
                ticketProductObj.netPrice = req.body.product.price;
                ticketProductObj.Taxable__c = req.body.product.Taxable__c;
                ticketProductObj.Worker__c = req.body.worker.Id;

                if (req.body.promo) {
                    ticketProductObj.Promotion__c = req.body.promo.Id;
                    var promoquery = "SELECT * FROM `Promotion__c` WHERE Id ='" + req.body.promo.Id + "' AND IsDeleted= 0";
                    execute.query(dbName, promoquery, '', function (err, result) {
                        if (result[0].Product_Discount__c == 1) {
                            if (result[0].Discount_Amount__c != 0) {
                                ticketProductObj.netPrice = parseFloat(req.body.product.price) - parseFloat(result[0].Discount_Amount__c);
                            } else {
                                var probal = parseFloat(req.body.product.price) - parseFloat(req.body.product.price) * parseFloat(result[0].Discount_Percentage__c) / 100;
                                ticketProductObj.netPrice = probal;
                            }
                        } else {
                            ticketProductObj.netPrice = req.body.product.price;
                        }
                    })
                } else {
                    ticketProductObj.Promotion__c = "";
                }

                if (req.body.rewards) {
                    if (ticketProductObj.Taxable__c == 0) {
                        ticketProductObj.Product_Tax__c = 0;
                    } else {
                        var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE (Name = "Sales Tax") and IsDeleted=0 Order By Name';
                        execute.query(dbName, sql2, '', function (err, result) {
                            const retailTax = JSON.parse(result[0].JSON__c);
                            if (err) {
                                sendResponseWithMsg(res, false, [], err)
                            } else {
                                ticketProductObj.Product_Tax__c = ticketProductObj.netPrice * retailTax.retailTax / 100;
                            }
                        });
                    }
                    var clientactiverew = "SELECT * FROM `Contact__c` WHERE Id = '" + req.body.client.Id + "' ";
                    execute.query(dbName, clientactiverew, '', function (err, result) {
                        if (result[0].Active_Rewards__c == 1) {
                            var queryrwrdc = 'SELECT * from Reward__c where Active__c = "1" and IsDeleted = 0 and Id="' + req.body.rewards.parent_id + '" ';
                            execute.query(dbName, queryrwrdc, '', function (err, resultss) {
                                if (result.length > 0) {
                                    var temp2 = JSON.parse(resultss[0].Redeem_Rules__c);
                                    if (temp2.length > 0) {
                                        var today = new Date();
                                        for (var i = 0; i < temp2.length; i++) {
                                            if (temp2[i].onOneItem == "Products" && temp2[i].redeemName == req.body.rewards.redeemName) {
                                                var startdatee = temp2[i].startDate;
                                                var enddatee = temp2[i].endDate;
                                                var now = moment().format("YYYY-DD-MM");
                                                var startdate = moment(startdatee).format("YYYY-DD-MM");
                                                var enddate = moment(enddatee).format("YYYY-DD-MM");
                                                if (now > startdate && now < enddate) {
                                                    ticketProductObj.Reward__c = req.body.rewards.parent_id;
                                                    ticketProductObj.redeemName = req.body.rewards.redeemName;
                                                    if (temp2[i].discountType != "Percent") {
                                                        ticketProductObj.netPrice = parseFloat(req.body.product.price) - parseFloat(temp2[i].discount);
                                                        if (req.params.type === 'new') {
                                                            ticketProductObj.isRefund__c = 0;
                                                            ticketProductObj.Status__c = 'Checked In';
                                                            createAppt(ticketProductObj, dbName, loginId, dateTime, function (err, done) {
                                                                if (err) {
                                                                    sendResponseWithMsg(res, false, [], err)
                                                                } else {
                                                                    ticketProductObj.Appt_Ticket__c = done.apptId;
                                                                    createTicketProduct(ticketProductObj, dbName, loginId, dateTime, function (err, result) {
                                                                        if (err) {
                                                                            sendResponseWithMsg(res, false, [], err)
                                                                        } else {
                                                                            sendResponse(res, true, result)
                                                                        }
                                                                    });
                                                                }

                                                            });
                                                        } else {
                                                            createTicketProduct(ticketProductObj, dbName, loginId, dateTime, function (err, done) {
                                                                if (err) {
                                                                    sendResponseWithMsg(res, false, [], err)
                                                                } else {
                                                                    sendResponse(res, true, done)
                                                                }
                                                            });
                                                        }
                                                    } else {
                                                        var calc = parseFloat(req.body.product.price) - parseFloat(req.body.product.price) * parseFloat(temp2[0].discount) / 100;
                                                        ticketProductObj.netPrice = calc;
                                                        if (req.params.type === 'new') {
                                                            ticketProductObj.isRefund__c = 0;
                                                            ticketProductObj.Status__c = 'Checked In';
                                                            createAppt(ticketProductObj, dbName, loginId, dateTime, function (err, done) {
                                                                if (err) {
                                                                    sendResponseWithMsg(res, false, [], err)
                                                                } else {
                                                                    ticketProductObj.Appt_Ticket__c = done.apptId;
                                                                    createTicketProduct(ticketProductObj, dbName, loginId, dateTime, function (err, result) {
                                                                        if (err) {
                                                                            sendResponseWithMsg(res, false, [], err)
                                                                        } else {
                                                                            sendResponse(res, true, result)
                                                                        }
                                                                    });
                                                                }

                                                            });
                                                        } else {
                                                            createTicketProduct(ticketProductObj, dbName, loginId, dateTime, function (err, done) {
                                                                if (err) {
                                                                    sendResponseWithMsg(res, false, [], err)
                                                                } else {
                                                                    sendResponse(res, true, done)
                                                                }
                                                            });
                                                        }
                                                        return false;
                                                    }
                                                    return false;
                                                } else {
                                                    ticketProductObj.Reward__c = "";
                                                    ticketProductObj.redeemName = "";
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    ticketProductObj.Reward__c = "";
                                    ticketProductObj.redeemName = "";
                                }
                            })
                        } else {
                            ticketProductObj.Reward__c = ""; //req.body.rewards.parent_id
                            ticketProductObj.redeemName = ""; //req.body.rewards.redeemName 
                        }
                    })
                }
                ticketProductObj.Client__c = req.body.client.Id;
                productTaxCal(ticketProductObj, dbName, function (data) {
                    ticketProductObj.Product_Tax__c = data;
                    ticketProductObj.isRefund__c = 0;
                    ticketProductObj.Status__c = 'Checked In';
                    createAppt(ticketProductObj, dbName, loginId, dateTime, function (err, done) {
                        if (err) {
                            sendResponseWithMsg(res, false, [], err)
                        } else {
                            ticketProductObj.Appt_Ticket__c = done.apptId;
                            createTicketProduct(ticketProductObj, dbName, loginId, dateTime, function (err, result) {
                                if (err) {
                                    sendResponseWithMsg(res, false, [], err)
                                } else {
                                    sendResponse(res, true, result)
                                }
                            });
                        }

                    });
                });
            } else if (req.body.service) {

                var details = req.body;
                var Guest_Charge = 0
                var service_details = req.body.service;
                var worker_details = req.body.worker;


                details.isNoService__c = req.body.service.isNoService__c;
                details.price = req.body.service.Price__c;
                details.redeemName = "";
                details.Qty_Sold__c = 1;
                if (req.body.promo) {
                    details.Promotion__c = req.body.promo.Id;
                    var promoquery = "SELECT * FROM `Promotion__c` WHERE Id ='" + req.body.promo.Id + "' AND IsDeleted= 0";
                    execute.query(dbName, promoquery, '', function (err, result) {
                        if (result[0].Service_Discount__c == 1) {
                            if (result[0].Discount_Amount__c != 0) {
                                details.netPrice = parseFloat(req.body.service.Price__c) - parseFloat(result[0].Discount_Amount__c);
                            } else {
                                details.netPrice = parseFloat(req.body.service.Price__c) - req.body.service.Price__c * result[0].Discount_Percentage__c / 100;
                            }
                        } else {
                            details.netPrice = req.body.service.Price__c
                        }
                    });
                } else {
                    details.Promotion__c = "";
                    details.netPrice = req.body.service.Price__c
                }


                if (details.service.Taxable__c == 0) {
                    details.Service_Tax__c = 0;
                } else {
                    var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE (Name = "Sales Tax") and IsDeleted=0 Order By Name';
                    execute.query(dbName, sql2, '', function (err, result) {
                        const serviceTax = JSON.parse(result[0].JSON__c);
                        if (err) {
                            sendResponseWithMsg(res, false, [], err)
                        } else {
                            details.Service_Tax__c = details.netPrice * serviceTax.serviceTax / 100;
                        }
                    });
                }
                details.Client__c = req.body.client.Id;
                if (worker_details.Duration_1 != 0)
                    details.Duration_1 = parseInt(worker_details.Duration_1);
                else if (service_details.Duration_1 != 0) {
                    details.Duration_1 = parseInt(service_details.Duration_1);
                } else {
                    details.Duration_1 = 0;
                }

                if (worker_details.Duration_2 != 0)
                    details.Duration_2 = parseInt(worker_details.Duration_2);
                else if (service_details.Duration_2 != 0) {
                    details.Duration_2 = parseInt(service_details.Duration_2);
                } else {
                    details.Duration_2 = 0;
                }

                if (worker_details.Duration_3 != 0)
                    details.Duration_3 = parseInt(worker_details.Duration_3);
                else if (service_details.Duration_3 != 0) {
                    details.Duration_3 = parseInt(service_details.Duration_3);
                } else {
                    details.Duration_3 = 0;
                }
                if (worker_details.Buffer_After != 0)
                    details.Buffer_After = parseInt(worker_details.Buffer_After);
                else if (service_details.Buffer_After != 0) {
                    details.Buffer_After = parseInt(service_details.Buffer_After);
                } else {
                    details.Buffer_After = 0;
                }
                details.Guest_Charge = Guest_Charge;

                details.serviceDur = parseInt(details.Duration_1) + parseInt(details.Duration_2) +
                    parseInt(details.Duration_3) + parseInt(details.Buffer_After);
                if (details.serviceDur === null) {
                    details.serviceDur = 0;
                }
                details.isRefund__c = 0;
                details.Status__c = 'Checked In';
                details.type = "Online";
                if (req.body.rewards) {
                    var clientactiverew = "SELECT * FROM `Contact__c` WHERE Id = '" + req.body.client.Id + "' ";
                    execute.query(dbName, clientactiverew, '', function (err, result) {

                        if (result[0].Active_Rewards__c == 1) {
                            var queryrwrdc = 'SELECT * from Reward__c where Active__c = "1" and IsDeleted = 0 and Id="' + req.body.rewards.parent_id + '" ';
                            execute.query(dbName, queryrwrdc, '', function (err, resultss) {
                                if (result.length > 0) {
                                    var temp2 = JSON.parse(resultss[0].Redeem_Rules__c);
                                    if (temp2.length > 0) {
                                        var today = new Date();
                                        for (var i = 0; i < temp2.length; i++) {
                                            if (temp2[i].onOneItem == "Services" && temp2[i].redeemName == req.body.rewards.redeemName) {
                                                var startdatee = temp2[i].startDate;
                                                var enddatee = temp2[i].endDate;
                                                var now = moment().format("YYYY-DD-MM");
                                                var startdate = moment(startdatee).format("YYYY-DD-MM");
                                                var enddate = moment(enddatee).format("YYYY-DD-MM");
                                                if (now > startdate && now < enddate) {
                                                    details.Reward__c = req.body.rewards.parent_id;
                                                    details.redeemName = req.body.rewards.redeemName;
                                                    if (temp2[i].discountType != "Percent") {
                                                        details.netPrice = parseFloat(req.body.service.Price__c) - parseFloat(temp2[i].discount);
                                                        createAppt(details, dbName, loginId, dateTime, function (err, done) {
                                                            if (err) {
                                                                res.json(err, done);
                                                            } else {
                                                                details.Appt_Ticket__c = done.apptId;
                                                                details.Appt_Date_Time__c = done.apptDate;
                                                                createTicketService(details, dbName, loginId, dateTime, function (err, result) {
                                                                    sendResponse(res, true, done)
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        var calc = parseFloat(req.body.service.Price__c) - parseFloat(req.body.service.Price__c) * parseFloat(temp2[0].discount) / 100;
                                                        details.netPrice = calc;
                                                        createAppt(details, dbName, loginId, dateTime, function (err, done) {
                                                            if (err) {
                                                                res.json(err, done);
                                                            } else {
                                                                details.Appt_Ticket__c = done.apptId;
                                                                details.Appt_Date_Time__c = done.apptDate;
                                                                createTicketService(details, dbName, loginId, dateTime, function (err, result) {
                                                                    sendResponse(res, true, done)
                                                                });
                                                            }
                                                        });
                                                        return false;
                                                    }

                                                    return false;
                                                } else {
                                                    details.Reward__c = "";
                                                    details.redeemName = "";
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    details.Reward__c = "";
                                    details.redeemName = "";
                                }
                            })
                        } else {
                            details.Reward__c = ""; //req.body.rewards.parent_id
                            details.redeemName = ""; //req.body.rewards.redeemName 
                        }
                    })
                } else {
                    details.Reward__c = "";
                    details.redeemName = "";
                }
                createAppt(details, dbName, loginId, dateTime, function (err, done) {
                    if (err) {
                        sendResponseWithMsg(res, false, [], err)
                    } else {
                        details.Appt_Ticket__c = done.apptId;
                        details.Appt_Date_Time__c = done.apptDate;
                        createTicketService(details, dbName, loginId, dateTime, function (err, result) {
                            sendResponse(res, true, done)
                        });
                    }
                });

            }
        } else {
            if (req.body.product) {
                var ticketProductObj = req.body;
                ticketProductObj.isNoService__c = req.body.product.isNoService__c;
                ticketProductObj.price = req.body.product.price;
                ticketProductObj.redeemName = ""; //req.body.rewards.redeemName
                ticketProductObj.Qty_Sold__c = 1;
                ticketProductObj.Product__c = req.body.product.Id;
                ticketProductObj.netPrice = req.body.product.price;
                ticketProductObj.Taxable__c = req.body.product.Taxable__c;
                ticketProductObj.Worker__c = req.body.worker.Id;
                ticketProductObj.Reward__c = ""; //req.body.rewards.parent_id

                if (req.body.promo) {
                    ticketProductObj.Promotion__c = req.body.promo.Id;
                    var promoquery = "SELECT * FROM `Promotion__c` WHERE Id ='" + req.body.promo.Id + "' AND IsDeleted= 0";
                    execute.query(dbName, promoquery, '', function (err, result) {
                        if (result[0].Product_Discount__c == 1) {
                            if (result[0].Discount_Amount__c != 0) {
                                ticketProductObj.netPrice = parseFloat(req.body.product.price) - parseFloat(result[0].Discount_Amount__c);
                            } else {
                                ticketProductObj.netPrice = parseFloat(req.body.product.price) - parseFloat(req.body.product.price) * parseFloat(result[0].Discount_Percentage__c) / 100;
                            }
                        } else {
                            ticketProductObj.netPrice = req.body.product.price;
                        }
                    })
                } else {
                    ticketProductObj.Promotion__c = "";
                }
                ticketProductObj.Client__c = req.body.client.Id;
                if (req.body.rewards) {

                    if (ticketProductObj.Taxable__c == 0) {
                        ticketProductObj.Product_Tax__c = 0;
                    } else {
                        var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE (Name = "Sales Tax") and IsDeleted=0 Order By Name';
                        execute.query(dbName, sql2, '', function (err, result) {
                            const retailTax = JSON.parse(result[0].JSON__c);
                            if (err) {
                                sendResponseWithMsg(res, false, [], err)
                            } else {
                                ticketProductObj.Product_Tax__c = ticketProductObj.netPrice * retailTax.retailTax / 100;
                            }
                        });
                    }
                    var clientactiverew = "SELECT * FROM `Contact__c` WHERE Id = '" + req.body.client.Id + "' ";
                    execute.query(dbName, clientactiverew, '', function (err, result) {
                        if (result[0].Active_Rewards__c == 1) {
                            var queryrwrdc = 'SELECT * from Reward__c where Active__c = "1" and IsDeleted = 0 and Id="' + req.body.rewards.parent_id + '" ';
                            execute.query(dbName, queryrwrdc, '', function (err, resultss) {
                                if (result.length > 0) {
                                    var temp2 = JSON.parse(resultss[0].Redeem_Rules__c);
                                    if (temp2.length > 0) {
                                        var today = new Date();
                                        for (var i = 0; i < temp2.length; i++) {
                                            if (temp2[i].onOneItem == "Products" && temp2[i].redeemName == req.body.rewards.redeemName) {
                                                var startdatee = temp2[i].startDate;
                                                var enddatee = temp2[i].endDate;
                                                var now = moment().format("YYYY-DD-MM");
                                                var startdate = moment(startdatee).format("YYYY-DD-MM");
                                                var enddate = moment(enddatee).format("YYYY-DD-MM");
                                                if (now > startdate && now < enddate) {
                                                    ticketProductObj.Reward__c = req.body.rewards.parent_id;
                                                    ticketProductObj.redeemName = req.body.rewards.redeemName;
                                                    if (temp2[i].discountType != "Percent") {
                                                        ticketProductObj.netPrice = parseFloat(req.body.product.price) - parseFloat(temp2[i].discount);
                                                        ticketProductObj.Appt_Ticket__c = req.params.type;
                                                        createTicketProduct(ticketProductObj, dbName, loginId, dateTime, function (err, result) {
                                                            if (err) {
                                                                sendResponseWithMsg(res, false, [], err)
                                                            } else {
                                                                sendResponse(res, true, result)
                                                            }
                                                        });
                                                    } else {
                                                        var calc = parseFloat(req.body.product.price) - parseFloat(req.body.product.price) * parseFloat(temp2[0].discount) / 100;
                                                        ticketProductObj.netPrice = calc;
                                                        ticketProductObj.Appt_Ticket__c = req.params.type;
                                                        createTicketProduct(ticketProductObj, dbName, loginId, dateTime, function (err, result) {
                                                            if (err) {
                                                                sendResponseWithMsg(res, false, [], err)
                                                            } else {
                                                                sendResponse(res, true, result)
                                                            }
                                                        });
                                                        return false;
                                                    }
                                                    return false;
                                                } else {
                                                    ticketProductObj.Reward__c = "";
                                                    ticketProductObj.redeemName = "";
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    ticketProductObj.Reward__c = "";
                                    ticketProductObj.redeemName = "";
                                }
                            })
                        } else {
                            ticketProductObj.Reward__c = ""; //req.body.rewards.parent_id
                            ticketProductObj.redeemName = ""; //req.body.rewards.redeemName 
                        }
                    })

                }
                ticketProductObj.Appt_Ticket__c = req.params.type;
                productTaxCal(ticketProductObj, dbName, function (data) {
                    ticketProductObj.Product_Tax__c = data;
                    createTicketProduct(ticketProductObj, dbName, loginId, dateTime, function (err, result) {
                        if (err) {
                            sendResponse(res, false, err)
                            logger.error('Error in create Product err ', err);
                        } else {
                            sendResponse(res, true, result)
                        }
                    });
                });
            }
            if (req.body.service) {
                var details = req.body;
                var Guest_Charge = 0
                var service_details = req.body.service;
                var worker_details = req.body.worker;
                details.isNoService__c = req.body.service.isNoService__c;
                details.Price__c = req.body.service.Price__c;
                details.redeemName = "";
                details.Qty_Sold__c = 1;
                details.Client__c = req.body.client.Id;
                if (worker_details.Duration_1 != 0)
                    details.Duration_1 = parseInt(worker_details.Duration_1);
                else if (service_details.Duration_1 != 0) {
                    details.Duration_1 = parseInt(service_details.Duration_1);
                } else {
                    details.Duration_1 = 0;
                }

                if (worker_details.Duration_2 != 0)
                    details.Duration_2 = parseInt(worker_details.Duration_2);
                else if (service_details.Duration_2 != 0) {
                    details.Duration_2 = parseInt(service_details.Duration_2);
                } else {
                    details.Duration_2 = 0;
                }

                if (worker_details.Duration_3 != 0)
                    details.Duration_3 = parseInt(worker_details.Duration_3);
                else if (service_details.Duration_3 != 0) {
                    details.Duration_3 = parseInt(service_details.Duration_3);
                } else {
                    details.Duration_3 = 0;
                }
                if (worker_details.Buffer_After != 0)
                    details.Buffer_After = parseInt(worker_details.Buffer_After);
                else if (service_details.Buffer_After != 0) {
                    details.Buffer_After = parseInt(service_details.Buffer_After);
                } else {
                    details.Buffer_After = 0;
                }
                details.Guest_Charge = Guest_Charge;

                details.serviceDur = parseInt(details.Duration_1) + parseInt(details.Duration_2) +
                    parseInt(details.Duration_3) + parseInt(details.Buffer_After);
                if (details.serviceDur === null) {
                    details.serviceDur = 0;
                }
                if (details.service.Taxable__c == 0) {
                    details.Service_Tax__c = 0;
                } else {
                    var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE (Name = "Sales Tax") and IsDeleted=0 Order By Name';
                    execute.query(dbName, sql2, '', function (err, result) {
                        const serviceTax = JSON.parse(result[0].JSON__c);
                        if (err) {
                            sendResponseWithMsg(res, false, [], err)
                        } else {
                            details.Service_Tax__c = details.netPrice * serviceTax.serviceTax / 100;
                        }
                    });
                }
                details.isRefund__c = 0;
                details.Status__c = 'Checked In';
                details.type = "Online";
                details.Appt_Ticket__c = req.params.type;
                details.Appt_Date_Time__c = req.body.Appt_Date_Time__c;
                details.netPrice = req.body.service.Price__c;

                if (req.body.promo) {
                    details.Promotion__c = req.body.promo.Id;
                    var promoquery = "SELECT * FROM `Promotion__c` WHERE Id ='" + req.body.promo.Id + "' AND IsDeleted= 0";
                    execute.query(dbName, promoquery, '', function (err, result) {
                        if (result[0].Service_Discount__c == 1) {
                            if (result[0].Discount_Amount__c != 0) {
                                details.netPrice = parseFloat(req.body.service.Price__c) - parseFloat(result[0].Discount_Amount__c);
                            } else {
                                details.netPrice = parseFloat(req.body.service.Price__c) - (req.body.service.Price__c * result[0].Discount_Percentage__c / 100);
                            }
                        } else {
                            details.netPrice = req.body.service.Price__c
                        }
                    })
                } else {
                    details.Promotion__c = "";
                }

                if (req.body.rewards) {
                    if (details.service.Taxable__c == 0) {
                        details.Service_Tax__c = 0;
                    } else {
                        var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE (Name = "Sales Tax") and IsDeleted=0 Order By Name';
                        execute.query(dbName, sql2, '', function (err, result) {
                            const serviceTax = JSON.parse(result[0].JSON__c);
                            if (err) {
                                sendResponseWithMsg(res, false, [], err)
                            } else {
                                details.Service_Tax__c = details.netPrice * serviceTax.serviceTax / 100;
                            }
                        });
                    }
                    var clientactiverew = "SELECT * FROM `Contact__c` WHERE Id = '" + req.body.client.Id + "' ";
                    execute.query(dbName, clientactiverew, '', function (err, result) {
                        if (result[0].Active_Rewards__c == 1) {
                            var queryrwrdc = 'SELECT * from Reward__c where Active__c = "1" and IsDeleted = 0 and Id="' + req.body.rewards.parent_id + '" ';
                            execute.query(dbName, queryrwrdc, '', function (err, resultss) {
                                if (result.length > 0) {
                                    var temp2 = JSON.parse(resultss[0].Redeem_Rules__c);
                                    if (temp2.length > 0) {
                                        var today = new Date();
                                        for (var i = 0; i < temp2.length; i++) {

                                            if (temp2[i].onOneItem == "Services" && temp2[i].redeemName == req.body.rewards.redeemName) {
                                                var startdatee = temp2[i].startDate;
                                                var enddatee = temp2[i].endDate;
                                                var now = moment().format("YYYY-DD-MM");
                                                var startdate = moment(startdatee).format("YYYY-DD-MM");
                                                var enddate = moment(enddatee).format("YYYY-DD-MM");
                                                if (now > startdate && now < enddate) {
                                                    details.Reward__c = req.body.rewards.parent_id;
                                                    details.redeemName = req.body.rewards.redeemName;
                                                    if (temp2[i].discountType != "Percent") {

                                                        details.netPrice = parseFloat(req.body.service.Price__c) - parseFloat(temp2[i].discount);
                                                        createTicketService(details, dbName, loginId, dateTime, function (err, result) {
                                                            if (err) {
                                                                console.log(err);
                                                            } else {
                                                                sendResponse(res, true, result)
                                                            }
                                                        });
                                                    } else {

                                                        var calc = parseFloat(req.body.service.Price__c) - parseFloat(req.body.service.Price__c) * parseFloat(temp2[0].discount) / 100;
                                                        details.netPrice = calc;
                                                        createTicketService(details, dbName, loginId, dateTime, function (err, result) {
                                                            if (err) {
                                                                console.log(err);
                                                            } else {
                                                                sendResponse(res, true, result)
                                                            }
                                                        });

                                                        return false;
                                                    }

                                                    return false;
                                                } else {
                                                    details.Reward__c = "";
                                                    details.redeemName = "";
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    details.Reward__c = "";
                                    details.redeemName = "";
                                }
                            })
                        } else {
                            details.Reward__c = ""; //req.body.rewards.parent_id
                            details.redeemName = ""; //req.body.rewards.redeemName 
                        }
                    })
                }
                details.service_notes = req.body.service_notes;
                createTicketService(details, dbName, loginId, dateTime, function (err, result) {
                    if (err) {
                        logger.error('Error in createTicket Service ', err);
                        sendResponse(res, false, err)
                    } else {
                        sendResponse(res, true, result)
                    }
                });

            }
            if (req.body.tip_left_amount) {
                var apptid = req.params.type
                var tipsData = {};
                tipsData.status = 'save';
                tipsData.Tip_Option__c = 'Tip Left in Drawer';
                tipsData.Tip_Amount__c = parseFloat(req.body.tip_left_amount);
                tipsData.Worker__c = req.body.tips_left_worker.Id;
                tipsData.Appt_Ticket__c = apptid;
                tipsData.Appt_Date_Time__c = '2019-07-06 16:43:41';
                tipsData.ProductAmount = 0;
                tipsData.ServiceAmount = 0;
                tipsData.ServiceTaxAmount = 0;
                tipsData.ProductTaxAmount = 0;
                tipsData.TipsAmount = parseFloat(req.body.tip_left_amount);
                tipsData.OthersAmount = 0;
                tipsData.deleteProductAmount = 0;
                tipsData.deleteServiceAmount = 0;
                tipsData.deleteServiceTaxAmount = 0;
                tipsData.deleteProductTaxAmount = 0;
                tipsData.deleteTipsAmount = 0;
                tipsData.deleteOthersAmount = 0;
                tipsData.apptId = apptid;
                tipsData.isNoService__c = 1;
                createTips(tipsData, dbName, loginId, dateTime, function (err, result) {
                    if (err) {
                        sendResponse(res, false, err)
                        logger.error('Error in create Ticket err ', err);
                    } else {
                        sendResponse(res, true, result)
                    }

                });
            }
            if (req.body.over_all_promo) {

                var ticketsquery =
                    'SELECT * FROM Ticket_Service__c WHERE Appt_Ticket__c = "14jx8h12cnjx5iqsid" '

                execute.query(dbName, ticketsquery, function (err, servicetickets) {
                    if (err) {
                        logger.error('Unknown error for getting ticket services for adding overall promotions:', err);
                    } else {
                        var productsquery =
                            'SELECT * FROM Ticket_Product__c WHERE Appt_Ticket__c = "14jx8h12cnjx5iqsid"'
                        execute.query(dbName, productsquery, function (err, producttickets) {
                            if (err) {
                                logger.error('Unknown error for getting ticket products for adding overall promotions:', err);
                            } else {
                                var TicketServiceData = servicetickets;
                                var ticketProductsList = producttickets;
                                var promoquery = "SELECT * FROM `Promotion__c` WHERE Id ='" + req.body.over_all_promo.Id + "' AND IsDeleted= 0";

                                execute.query(dbName, promoquery, '', function (err, result) {
                                    if (result[0].Service_Discount__c == 1) {
                                        for (var i = 0; i < TicketServiceData.length; i++) {
                                            if (TicketServiceData[i]['Promotion__c'] && !TicketServiceData[i]['Booked_Package__c']) {
                                                if (result[0].Discount_Amount__c != 0) {
                                                    var promovalue = TicketServiceData[i].Price__c - parseFloat(result[0].Discount_Amount__c);
                                                } else {
                                                    var promovalue = TicketServiceData[i].Price__c - TicketServiceData[i].Price__c * result[0].Discount_Percentage__c / 100;
                                                }

                                                var queries1 = mysql.format('UPDATE Ticket_Service__c SET Promotion__c = "' + req.body.over_all_promo.Id +
                                                    '", Net_Price__c = "' + promovalue + TicketServiceData[i].Service_Tax__c +
                                                    '", Service_Tax__c = "' + TicketServiceData[i].Service_Tax__c +
                                                    '", LastModifiedDate = "' + dateTime +
                                                    '", LastModifiedById = "' + loginId +
                                                    '" WHERE Id = "' + TicketServiceData[i].Id + '" AND (Promotion__c IS NULL OR Promotion__c = "");');
                                                execute.query(dbName, queries1, function (err, result) {
                                                    if (err) {
                                                        logger.error('Unknown error for inserting promotion  s:', err);
                                                        console.log(err)
                                                    } else {
                                                        console.log("Overall Promodd Added for services")
                                                    }
                                                });
                                            }
                                        }

                                    } else {

                                    }

                                    if (ticketProductsList && ticketProductsList.length == 0) {
                                        for (var i = 0; i < ticketProductsList.length; i++) {
                                            if (result[0].Product_Discount__c == 1) {
                                                if (result[0].Discount_Amount__c != 0) {
                                                    //ticketProductObj.netPrice = parseFloat(req.body.product.price)-parseFloat(result[0].Discount_Amount__c);
                                                    var promoprodvalue = ticketProductsList[i].Price__c - resparseFloat(result[0].Discount_Amount__c);
                                                } else {
                                                    var promoprodvalue = parseFloat(ticketProductsList[i].Price__c) - parseFloat(ticketProductsList[i].Price__c) * parseFloat(result[0].Discount_Percentage__c) / 100;
                                                }
                                            } else {

                                            }
                                            var queries2 = mysql.format('UPDATE Ticket_Product__c SET Promotion__c = "' + req.body.over_all_promo.Id +
                                                '", Net_Price__c = "' + promoprodvalue + ticketProductsList[i].Product_Tax__c +
                                                '", Product_Tax__c = "' + ticketProductsList[i].Product_Tax__c +
                                                '", LastModifiedDate = "' + dateTime +
                                                '", LastModifiedById = "' + loginId +
                                                '" WHERE Id = "' + ticketProductsList[i].Id + '" AND (Promotion__c IS NULL OR Promotion__c = "");');
                                            execute.query(dbName, queries2, function (err, result) {
                                                if (err) {
                                                    logger.error('Unknown error for inserting promotion  s:', err);
                                                    console.log(err)
                                                } else {
                                                    console.log("Overall Promo Updated for Products")
                                                }
                                            });
                                        }

                                    } else {

                                    }
                                })
                                res.json("ok");
                            }
                        })
                    }
                })
            }
        }
    });
    app.get('/api/checkout/worker_details/tips/:id', function (req, res) {
        var dbName = req.headers['db'];
        var sql = `select ts.Net_Price__c as srvPrice,
                   ts.Service__c as srvId, s.Name as srvName, u.*
                   from Ticket_Service__c ts
                   left join Service__c s on s.Id = ts.Service__c
                   left join User__c u on u.Id = ts.Worker__c
                   where ts.Appt_Ticket__c = '` + req.params.id + `'`;
        execute.query(dbName, sql, '', function (err, result) {
            if (err) {
                logger.error('Error appointment details checkout dao:', err);
                sendResponse(res, false, [])
            } else {
                sendResponse(res, true, result)
            }
        });
    });
}
function productTaxCal(ticketProductObj, dbName, callback) {
    if (ticketProductObj.Taxable__c == 0) {
        ticketProductObj.Product_Tax__c = 0;
        callback(ticketProductObj.Product_Tax__c);
    } else {
        var sql2 = 'SELECT JSON__c, Name FROM Preference__c WHERE (Name = "Sales Tax") and IsDeleted=0 Order By Name';
        execute.query(dbName, sql2, '', function (err, result) {
            const retailTax = JSON.parse(result[0].JSON__c);
            if (err) {
                sendResponseWithMsg(res, false, [], err)
            } else {
                ticketProductObj.Product_Tax__c = ticketProductObj.netPrice * retailTax.retailTax / 100;
                callback(ticketProductObj.Product_Tax__c);
            }
        });
    }
}
function updateJsonc(JSON_Cresult, resultJson) {
    for (var i = 0; i < JSON_Cresult.length; i++) {
        for (var j = 0; j < resultJson.length; j++) {
            if (JSON_Cresult[i].type === 'Service' && JSON_Cresult[i].id === resultJson[j].Id) {
                JSON_Cresult[i]['name'] = resultJson[j].Name;
                JSON_Cresult[i]['size'] = '';
                JSON_Cresult[i]['units'] = '';
                JSON_Cresult[i]['price'] = resultJson[j].Price__c;
                JSON_Cresult[i]['Duration_1__c'] = resultJson[j].Duration_1__c;
                JSON_Cresult[i]['Duration_2__c'] = resultJson[j].Duration_2__c;
                JSON_Cresult[i]['Duration_3__c'] = resultJson[j].Duration_3__c;
                JSON_Cresult[i]['Buffer_After__c'] = resultJson[j].Buffer_After__c;
                JSON_Cresult[i]['Service_Group_Color__c'] = resultJson[j].Service_Group_Color__c;
                JSON_Cresult[i]['Guest_Charge__c'] = resultJson[j].Guest_Charge__c;
                JSON_Cresult[i]['Taxable__c'] = resultJson[j].Taxable__c;
                JSON_Cresult[i]['Start_Date__c'] = '';
                JSON_Cresult[i]['End_Date__c'] = '';
                JSON_Cresult[i]['Discount_Amount__c'] = 0;
                JSON_Cresult[i]['Discount_Percentage__c'] = 0;
                JSON_Cresult[i]['Product_Discount__c'] = 0;
                JSON_Cresult[i]['Service_Discount__c'] = 0;
                JSON_Cresult[i]['Active__c'] = '';
                JSON_Cresult[i]['Product_Pic__c'] = '';
                JSON_Cresult[i]['Worker__c'] = resultJson[j].Worker__c;
                JSON_Cresult[i]['category'] = resultJson[j].Service_Group__c;
            } else if (JSON_Cresult[i].type === 'Product' && JSON_Cresult[i].id === resultJson[j].Id) {
                JSON_Cresult[i]['name'] = resultJson[j].Name;
                JSON_Cresult[i]['size'] = resultJson[j].Size__c;
                JSON_Cresult[i]['units'] = resultJson[j].Unit_of_Measure__c;
                JSON_Cresult[i]['price'] = resultJson[j].Price__c;
                JSON_Cresult[i]['Taxable__c'] = resultJson[j].Taxable__c;
                JSON_Cresult[i]['Duration_1__c'] = null;
                JSON_Cresult[i]['Duration_2__c'] = null;
                JSON_Cresult[i]['Duration_3__c'] = null;
                JSON_Cresult[i]['Buffer_After__c'] = null;
                JSON_Cresult[i]['Service_Group_Color__c'] = '';
                JSON_Cresult[i]['Start_Date__c'] = '';
                JSON_Cresult[i]['End_Date__c'] = '';
                JSON_Cresult[i]['Discount_Amount__c'] = 0;
                JSON_Cresult[i]['Discount_Percentage__c'] = 0;
                JSON_Cresult[i]['Product_Discount__c'] = 0;
                JSON_Cresult[i]['Service_Discount__c'] = 0;
                JSON_Cresult[i]['Active__c'] = '';
                JSON_Cresult[i]['Guest_Charge__c'] = '';
                JSON_Cresult[i]['Product_Pic__c'] = resultJson[j].Product_Pic__c;
                JSON_Cresult[i]['category'] = resultJson[j].Product_Line__c;
                JSON_Cresult[i]['Worker__c'] = '';
            } else if (JSON_Cresult[i].type === 'Promotion' && JSON_Cresult[i].id === resultJson[j].Id) {
                JSON_Cresult[i]['name'] = resultJson[j].Name;
                JSON_Cresult[i]['size'] = '';
                JSON_Cresult[i]['units'] = '';
                JSON_Cresult[i]['price'] = '';
                JSON_Cresult[i]['Taxable__c'] = '';
                JSON_Cresult[i]['Duration_1__c'] = null;
                JSON_Cresult[i]['Duration_2__c'] = null;
                JSON_Cresult[i]['Duration_3__c'] = null;
                JSON_Cresult[i]['Buffer_After__c'] = null;
                JSON_Cresult[i]['Service_Group_Color__c'] = '';
                JSON_Cresult[i]['Start_Date__c'] = resultJson[j].Start_Date__c;
                JSON_Cresult[i]['End_Date__c'] = resultJson[j].End_Date__c;
                JSON_Cresult[i]['Discount_Amount__c'] = resultJson[j].Discount_Amount__c;
                JSON_Cresult[i]['Discount_Percentage__c'] = resultJson[j].Discount_Percentage__c;
                JSON_Cresult[i]['Product_Discount__c'] = resultJson[j].Product_Discount__c;
                JSON_Cresult[i]['Service_Discount__c'] = resultJson[j].Service_Discount__c;
                JSON_Cresult[i]['Active__c'] = resultJson[j].Active__c;
                JSON_Cresult[i]['Guest_Charge__c'] = '';
                JSON_Cresult[i]['Product_Pic__c'] = '';
                JSON_Cresult[i]['Worker__c'] = '';
            }

        }
    }
    return JSON_Cresult;
}
function tipUpdateRecursive(i, res, tipObj, dbName, loginId, dateTime, Id) {
    if (i < tipObj.length) {
        var sqlQuery = "UPDATE Ticket_Tip__c" +
            " SET Tip_Amount__c = '" + tipObj[i].Tip_Amount__c +
            "', LastModifiedDate = '" + dateTime +
            "', LastModifiedById = '" + loginId +
            "' WHERE Id = '" + tipObj[i].tipId + "'";
        execute.query(dbName, sqlQuery, '', function (err, result) {
            if (err) {
                logger.error('Error in CheckOut dao - updateTips:', err);
                sendResponse(res, true, err)
            } else {
                tipUpdateRecursive(i + 1, res, tipObj, dbName, loginId, dateTime, Id)
            }
        });
    } else {
        var sql = 'SELECT SUM(Tip_Amount__c) as Tip_Amount__c FROM Ticket_Tip__c WHERE Appt_Ticket__c = "' + Id + '"'
        execute.query(dbName, sql, '', function (err, sumOfTips) {
            var sql1 = 'UPDATE Appt_Ticket__c SET Tips__c = "' + sumOfTips[0].Tip_Amount__c +
                '", LastModifiedDate = "' + dateTime +
                '", LastModifiedById = "' + loginId + '" WHERE Id = "' + Id + '"';
            execute.query(dbName, sql1, '', function (err, data) {
                var result = { 'apptId': Id }
                sendResponseWithMsg(res, true, result, 'Tip Modified successfully')
            });
        });
    }
}

function tipRecurse(i, res, tipsData, dbName, loginId, dateTime) {
    if (i < tipsData.cashTipsModel.length) {
        var tipsObjData = {};
        tipsObjData.Appt_Ticket__c = tipsData.appt_id;
        tipsObjData.Tip_Amount__c = tipsData.cashTipsModel[i].price;
        tipsObjData.Worker__c = tipsData.cashTipsModel[i].worker_data.Id;
        tipsObjData.Drawer_Number__c = '';
        tipsObjData.Tip_Option__c = '';
        createTips(tipsObjData, dbName, loginId, dateTime, function (err, result) {
            if (err) {
                sendResponse(res, true, err)
            } else {
                tipRecurse(i + 1, res, tipsData, dbName, loginId, dateTime)
            }
        });
    } else {
        var sql = 'SELECT SUM(Tip_Amount__c) as Tip_Amount__c FROM Ticket_Tip__c WHERE Appt_Ticket__c = "' + tipsData.appt_id + '"'
        execute.query(dbName, sql, '', function (err, sumOfTips) {
            var sql1 = 'UPDATE Appt_Ticket__c SET Tips__c = "' + sumOfTips[0].Tip_Amount__c +
                '", LastModifiedDate = "' + dateTime +
                '", LastModifiedById = "' + loginId + '" WHERE Id = "' + tipsData.appt_id + '"';
            execute.query(dbName, sql1, '', function (err, data) {
                var result = { 'apptId': tipsData.appt_id }
                sendResponseWithMsg(res, true, result, 'Tip Added successfully')
            });
        });
    }
}

function sendemail(toAddress, fromAddress, subjectText, htmlMessage, attachment, callback) {

    if (!fromAddress) {
        fromAddress = {
            email: 'info@stxsoftware.com',
            name: 'STX Team'
        };
    }
    if (!subjectText) {
        subjectText = 'STX';
    }
    if (!htmlMessage) {
        htmlMessage = '<div></div>';
    }
    var mailOptions = {
        to: toAddress,
        from: fromAddress,
        subject: subjectText,
        html: htmlMessage
    };
    if (attachment) {
        mailOptions.attachments = attachment;
    }
    sendGrid.send(mailOptions, function (error, info) {
        if (error) {
            callback(error, null);
        } else {
            callback(null, info);
        }
    });
}

function sendChangeStatusResponse(indexParam, callback, error, result) {
    if (indexParam === 3) {
        callback(error, result);
    }
}

function addToTicketOther(req, done1) {

    var dbName = req.headers['db'];
    var loginId = req.headers['id'];
    var dateTime = req.headers['dt'];
    //var dateTime = '2019-06-13 13:17:36';
    var ticketOtherData = req.body;
    ticketOtherData.isNoService__c = 1;
    packObj = req.body.pckgObj;
    try {
        if (req.params.type === 'New') {

            ticketOtherData.isRefund__c = 0;
            ticketOtherData.Status__c = (ticketOtherData.type === 'Online') ? 'Complete' : 'Checked In';
            createAppt(ticketOtherData, dbName, loginId, dateTime, function (err, done) {
                if (err) {
                    logger.error('Error in CheckOut dao - addToTicktOther:', err);
                    done1(err, done)
                } else {
                    ticketOtherData.Ticket__c = done.apptId;
                    createTicketOther(ticketOtherData, packObj, dbName, loginId, dateTime, ticketOtherData.Ticket__c, function (err, result) {
                        done1(err, result)
                    });
                }

            });
        } else {

            createTicketOther(ticketOtherData, packObj, dbName, loginId, dateTime, ticketOtherData.Ticket__c, function (err, result) {
                done1(err, result) //need to change this to send responce
            });
        }
    } catch (err) {
        logger.error('Unknown error in CheckOut dao - addToTicketOther:', err);
        done1(err, { statusCode: '9999' });
    }
}


/**
 * Method to create A Record in appointment Table
 * @param {*} ticketServiceObj required DataObj for Create Record
 * @param {*} done callback
 */
function createAppt(ticketServiceObj, dbName, loginId, dateTime, done) {
    var selectSql = 'SELECT IFNULL(max( cast(Name as unsigned)),0) + 1 Name FROM Appt_Ticket__c';
    execute.query(dbName, selectSql, '', function (err, result) {
        if (err) {
            done(err, result);
        } else {
            apptName = ('00000' + result[0].Name).slice(-6);
        }
        var serviceDur;
        if (ticketServiceObj && ticketServiceObj.serviceDur) {
            serviceDur = ticketServiceObj.serviceDur;
        } else {
            serviceDur = 0;
        }
        var apptObjData = {
            Id: uniqid(),
            OwnerId: loginId,
            IsDeleted: 0,
            Name: apptName,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            Appt_Date_Time__c: ticketServiceObj.Appt_Date_Time__c,
            Client_Type__c: ticketServiceObj.Client_Type__c, // need
            Client__c: ticketServiceObj.Client__c,
            Duration__c: serviceDur,
            Status__c: ticketServiceObj.Status__c,
            isRefund__c: ticketServiceObj.isRefund__c,
            Booked_Online__c: (ticketServiceObj.type === 'Online') ? 1 : 0,
            isTicket__c: 1,
            Is_Booked_Out__c: 0,
            New_Client__c: 0,
            isNoService__c: ticketServiceObj.isNoService__c
        }
        if (ticketServiceObj.Booked_Online__c) {
            apptObjData.Booked_Online__c = ticketServiceObj.Booked_Online__c;
        }
        apptObjData = setDefValApptTkt(apptObjData, ticketServiceObj);
        var insertQuery = 'INSERT INTO Appt_Ticket__c SET ?';
        execute.query(dbName, insertQuery, apptObjData, function (apptDataErr, apptDataResult) {
            if (apptDataErr) {
                // logger.error('Error in CheckOut dao - createAppt:', apptDataErr);
                done(apptDataErr, { statusCode: '9999' });
            } else {
                done(apptDataErr, {
                    'apptId': apptObjData.Id,
                    'apptDate': apptObjData.Appt_Date_Time__c,
                    'clientId': apptObjData.Client__c,
                    'apptName': apptName,
                    'creationDate': apptObjData.CreatedDate,
                    'lastModifiedDate': apptObjData.LastModifiedDate
                })
            }
        });
    });
}
/**
 * To create a record into ticketother Table
 * @param {*} ticketOtherData
 * @param {*} done
 */
function createTicketOther(ticketOtherData, packObj, dbName, loginId, dateTime, apptId, callback) {

    if (packObj && packObj.pckArray && packObj.pckArray.length > 0) {

        var pckData = packObj.pckArray;
        commonClientPackage(pckData, null, packObj.ticketServiceData, dbName, loginId, dateTime,
            apptId,
            function (err, result) {
                callback(err, result);
            });
    } else {

        var ticketOtherDataObj = {
            Id: uniqid(),
            IsDeleted: 0,
            CreatedDate: dateTime,
            CreatedById: loginId,
            LastModifiedDate: dateTime,
            LastModifiedById: loginId,
            SystemModstamp: dateTime,
            LastModifiedDate: dateTime,
            Ticket__c: ticketOtherData.Ticket__c,
            Amount__c: ticketOtherData.Amount__c ? ticketOtherData.Amount__c : ticketOtherData.Amount,
            Transaction_Type__c: ticketOtherData.Transaction_Type__c,
            Package__c: ticketOtherData.Package__c,
            Package_Price__c: ticketOtherData.Package_Price__c,
            Gift_Number__c: ticketOtherData.Gift_Number__c,
            Expires__c: ticketOtherData.Expires__c,
            Issued__c: ticketOtherData.Issued__c ? ticketOtherData.Issued__c : dateTime,
            Worker__c: ticketOtherData.Worker__c,
            Recipient__c: ticketOtherData.Recipient__c ? ticketOtherData.Recipient__c : ticketOtherData.RecipientName,
            Service_Tax__c: ticketOtherData.Service_Tax__c,
            Online__c: ticketOtherData.online_c === 1 ? 1 : 0
        };

        var insertQuery = 'INSERT INTO Ticket_Other__c SET ?';
        execute.query(dbName, insertQuery, ticketOtherDataObj, function (err, result) {
            if (err && err.code === 'ER_DUP_ENTRY') {

                logger.error('Error in CheckOut dao - createTicketOther:', err);
                callback(err, { statusCode: '9996' });
            } else if (err) {

                logger.error('Error in CheckOut dao - createTicketOther:', err);
                callback(err, { statusCode: '9999' });
            } else {
                updateAppt(ticketOtherData, dbName, loginId, dateTime, function (ticketOtherErr, result) {
                    callback(ticketOtherErr, {
                        'apptId': ticketOtherData.Ticket__c,
                        'giftData': ticketOtherData,
                        'apptName': ticketOtherData.apptName,
                        'clientId': ticketOtherData.Client__c,
                        'creationDate': ticketOtherData.creationDate,
                        'lastModifiedDate': ticketOtherData.lastModifiedDate
                    })
                });
            }
        });
    }
}

function commonClientPackage(pckData, isPrepaidPackage, ticketServiceData, dbName, loginId, dateTime, apptId, done) {
    var records = [];
    var indexParams = 0;
    if (pckData && pckData.length > 0) {
        /**
         * here we are getting predifined paymenttype which is with the name of "Prepaid Package"
         */
        paymtTypeSql = 'SELECT Id, Name FROM Payment_Types__c WHERE Name = "Prepaid Package"';
        execute.query(dbName, paymtTypeSql, '', function (paymntErr, paymntResult) {
            if (paymntErr) {
                logger.error('Error in appoitments dao - changeStatus:', paymntErr);
            } else {
                var paymentSql2 = 'SELECT Id, Appt_Ticket__c, Payment_Type__c FROM Ticket_Payment__c WHERE Payment_Type__c = "' + paymntResult[0].Id + '" && Appt_Ticket__c ="' + apptId + '" AND isDeleted=0';
                execute.query(dbName, paymentSql2, '', function (paymntErr1, paymntResult1) {
                    if (paymntErr1) {
                        logger.error('Error in appoitments dao - changeStatus:', paymntErr1);
                    } else {
                        var queries = '';
                        var paymentAmout = 0;
                        var serTax = 0;
                        for (i = 0; i < ticketServiceData.length; i++) {
                            paymentAmout += parseFloat(ticketServiceData[i].netPrice);
                            serTax += parseFloat(ticketServiceData[i].serTax);
                            queries += mysql.format('UPDATE Ticket_Service__c' +
                                ' SET Net_Price__c = "' + ticketServiceData[i].netPrice +
                                '", Price__c = "' + ticketServiceData[i].Price__c +
                                '", Service_Tax__c = "' + ticketServiceData[i].serTax +
                                '", LastModifiedDate = "' + dateTime +
                                '",LastModifiedById = "' + loginId +
                                '",Booked_Package__c = "' + ticketServiceData[i].pckId +
                                '" WHERE Appt_Ticket__c = "' + apptId + '" && Service__c = "' + ticketServiceData[i].serviceId + '" && Id = "' + ticketServiceData[i].tsId + '";');
                        }
                        queries += mysql.format('UPDATE Appt_Ticket__c' +
                            ' SET Service_Sales__c = ' + paymentAmout +
                            ', Service_Tax__c = ' + serTax +
                            ', Has_Booked_Package__c=' + 1 +
                            ', LastModifiedDate = "' + dateTime +
                            '",LastModifiedById = "' + loginId +
                            '" WHERE Id = "' + apptId + '";');
                        if (paymntResult1 && paymntResult1.length > 0) {
                            queries += 'UPDATE Ticket_Payment__c' +
                                ' SET Amount_Paid__c = Amount_Paid__c+"' + (parseFloat(paymentAmout) + parseFloat(serTax)) +
                                '", LastModifiedDate = "' + dateTime +
                                '",LastModifiedById = "' + loginId +
                                '" WHERE Appt_Ticket__c = "' + apptId + '" && Payment_Type__c = "' + paymntResult[0].Id + '" && IsDeleted = 0';
                            indexParams++
                        } else if (ticketServiceData && ticketServiceData.length > 0) {
                            var drawerNumber;
                            if (!pckData[0].Drawer_Number__c) {
                                drawerNumber = null;
                            } else {
                                drawerNumber = pckData[0].Drawer_Number__c;
                            }
                            var ticketPaymentObj = {
                                Id: uniqid(),
                                IsDeleted: 0,
                                CreatedDate: dateTime,
                                CreatedById: loginId,
                                LastModifiedDate: dateTime,
                                LastModifiedById: loginId,
                                SystemModstamp: dateTime,
                                LastModifiedDate: dateTime,
                                Amount_Paid__c: (parseFloat(paymentAmout) + parseFloat(serTax)),
                                Appt_Ticket__c: apptId,
                                Approval_Code__c: '',
                                Drawer_Number__c: drawerNumber,
                                Notes__c: '',
                                Payment_Type__c: paymntResult[0].Id
                            }
                            var insertQuery2 = 'INSERT INTO Ticket_Payment__c SET ?';
                            execute.query(dbName, insertQuery2, ticketPaymentObj, function (err2, result2) {
                                if (err2) {
                                    logger.error('Error in appoitments dao - changeStatus:', err2);
                                } else {
                                    indexParams++
                                    sendChangeStatusResponse(indexParams, done, err2, result2);
                                }
                            });
                        } else {
                            indexParams++
                        }
                        if ((isPrepaidPackage && isPrepaidPackage === false) || (isPrepaidPackage === null)) {
                            for (i = 0; i < pckData.length; i++) {
                                records.push([
                                    uniqid(),
                                    0,
                                    dateTime,
                                    loginId,
                                    dateTime,
                                    loginId,
                                    dateTime,
                                    apptId,
                                    (pckData[i].discountedPackage + pckData[i].pckgtax),
                                    'Package',
                                    pckData[i].pckId,
                                    pckData[i].discountedPackage,
                                    pckData[i].pckgtax
                                ])
                            }
                            // }
                            var insertQuery1 = 'INSERT INTO Ticket_Other__c' +
                                ' (Id, IsDeleted, CreatedDate, CreatedById, LastModifiedDate,  LastModifiedById,' +
                                ' SystemModstamp, Ticket__c, Amount__c, Transaction_Type__c,Package__c, Package_Price__c, Service_Tax__c)VALUES ?';
                            execute.query(dbName, insertQuery1, [records], function (err1, result1) {
                                if (err1) {
                                    logger.error('Error in appoitments dao - changeStatus:', err1);
                                } else {
                                    indexParams++
                                    sendChangeStatusResponse(indexParams, done, err1, result1);
                                }
                            });
                        } else {
                            indexParams++
                            sendChangeStatusResponse(indexParams, done, null, null);

                        }
                        if (queries && queries.length > 0) {
                            execute.query(dbName, queries, '', function (err3, result3) {
                                if (err3) {
                                    logger.error('Error in appoitments dao - changeStatus:', err3);
                                } else {
                                    indexParams++
                                    sendChangeStatusResponse(indexParams, done, err3, result3);
                                }
                            });
                        }
                    }
                });
            }
        });
    } else {
        indexParams = 3;
        sendChangeStatusResponse(indexParams, done, null, []);
    }
}
/**
 *
 * To Update the Record into Appt Table
 * @param {*} miscSaleData
 * @param {*} done
 */
function updateAppt(apptUpdateData, dbName, loginId, dateTime, callback) {

    var qry = `SELECT SUM(Net_Price__c) price, SUM(Service_Tax__c) tax FROM Ticket_Service__c WHERE Appt_Ticket__c ='` + apptUpdateData.apptId + `' AND IsDeleted=0;`
    qry += `SELECT SUM(Amount__c) price FROM Ticket_Other__c WHERE Ticket__c = '` + apptUpdateData.apptId + `' AND IsDeleted=0;`
    qry += `SELECT SUM(Net_Price__c) price, SUM(Product_Tax__c) tax FROM Ticket_Product__c WHERE Appt_Ticket__c = '` + apptUpdateData.apptId + `' AND IsDeleted=0`
    var serSales;
    var prodSales;
    var serTax;
    var prodTax;
    var othrSales;
    execute.query(dbName, qry, function (err, result) {
        if (err) {

            logger.error('error while fecthing Data for update Appt- updateAppt:')
        } else {

            if (result && result[0] && result[0].length > 0) {
                serSales = result[0][0].price;
                serTax = result[0][0].tax
            }
            if (result && result[1] && result[1].length > 0) {
                othrSales = result[1][0].price;
            }
            if (result && result[2] && result[2].length > 0) {
                prodSales = result[2][0].price;
                prodTax = result[2][0].tax
            }
            var tempItem = {
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                Service_Sales__c: serSales,
                Product_Sales__c: prodSales,
                Service_Tax__c: serTax,
                Product_Tax__c: prodTax,
                Other_Sales__c: othrSales
            }
            // tempItem = setDefValApptTkt(tempItem, apptUpdateData);
            var whereCond = {
                Id: apptUpdateData.apptId
            };
            var updateQuery = mysql.format('UPDATE Appt_Ticket__c  SET ? ' +
                ' WHERE ?; ', [tempItem, whereCond]);
            execute.query(dbName, updateQuery, function (err, result) {
                if (err) {

                } else {

                    callback(err, result);
                }
            });
        }
    });
}
function getpromotion(dbName, promotionid, tickettype, ticketprice) {
    var promoquery = "SELECT * FROM `Promotion__c` WHERE Id ='" + promotionid + "' AND IsDeleted= 0";
    execute.query(dbName, promoquery, '', function (err, result) {
        if (tickettype == "Service") {
            if (result[0].Service_Discount__c == 1) {
                if (result[0].Discount_Amount__c != 0) {
                    var netamount = parseInt(ticketprice) - parseInt(result[0].Discount_Amount__c);
                }
            } else {
                var netamount = ticketprice * result[0].Discount_Percentage__c / 100;
            }
        } else if (tickettype == "Product") {
            if (result[0].Product_Discount__c == 1) {
                if (result[0].Discount_Amount__c != 0) {
                    var netamount = parseInt(ticketprice) - parseInt(result[0].Discount_Amount__c);
                }
            } else {
                var netamount = ticketprice * result[0].Discount_Percentage__c / 100;
            }
        } else {
            var netamount = ticketprice;
        }
    })
}

function setDefValApptTkt(dataObj, inputObj) {
    dataObj['Service_Tax__c'] = inputObj['ServiceTaxAmount'] ? inputObj['ServiceTaxAmount'] : 0;
    dataObj['Service_Sales__c'] = inputObj['Price__c'] ? inputObj['Price__c'] : 0;
    dataObj['Product_Sales__c'] = inputObj['ProductAmount'] ? inputObj['ProductAmount'] : 0;
    dataObj['Product_Tax__c'] = inputObj['ProductTaxAmount'] ? inputObj['ProductTaxAmount'] : 0;
    dataObj['Tips__c'] = inputObj['TipsAmount'] ? inputObj['TipsAmount'] : 0;
    dataObj['Other_Sales__c'] = inputObj['OthersAmount'] ? inputObj['OthersAmount'] : 0;
    return dataObj;
}

function createTicketProduct(ticketProductObj, dbName, loginId, dateTime, done) {
    var sqlQuery = 'SELECT * FROM `Ticket_Product__c` WHERE Appt_Ticket__c = "' + ticketProductObj.Appt_Ticket__c + '" && Product__c = "' + ticketProductObj.Product__c + '"' + " && isDeleted = 0";
    execute.query(dbName, sqlQuery, function (err, data) {
        if (err) {
            logger.error('Error in CheckOut dao - addToProduct:', err);
            done(err, { statusCode: '9999' });
        } else {
            if (data.length > 0) {
                // var priceVal = parseInt(data[0].Net_Price__c) + parseInt(ticketProductObj.Price__c);
                var updateQuery = "UPDATE Ticket_Product__c" +
                    " SET Qty_Sold__c = Qty_Sold__c+" + ticketProductObj.Qty_Sold__c // need
                    +
                    ", Product_Tax__c = Product_Tax__c+" + ticketProductObj.Product_Tax__c // need
                    +
                    " WHERE Appt_Ticket__c = '" + ticketProductObj.Appt_Ticket__c + "' && Product__c = '" + ticketProductObj.Product__c + "'"; // need
                execute.query(dbName, updateQuery, '', function (err, result) {
                    if (err) {
                        logger.error('Error in CheckOut dao - addClientToAppt:', err);
                        done(err, { statusCode: '9999' });
                    } else {
                        updateAppt(ticketProductObj, dbName, loginId, dateTime, function (ticketPrdErr, result) {
                            done(ticketPrdErr, ticketPrdResult = { 'apptId': ticketProductObj.Appt_Ticket__c })
                        });
                    }
                });
            } else {
                var ticketProductObjData = {
                    Id: uniqid(),
                    IsDeleted: 0,
                    CreatedDate: dateTime,
                    CreatedById: loginId,
                    LastModifiedDate: dateTime,
                    LastModifiedById: loginId,
                    SystemModstamp: dateTime,
                    LastModifiedDate: dateTime,
                    Appt_Ticket__c: ticketProductObj.Appt_Ticket__c,
                    Client__c: ticketProductObj.Client__c,
                    Net_Price__c: ticketProductObj.netPrice,
                    Price__c: ticketProductObj.price,
                    Product__c: ticketProductObj.Product__c,
                    Promotion__c: ticketProductObj.Promotion__c,
                    Reward__c: ticketProductObj.Reward__c,
                    Qty_Sold__c: ticketProductObj.Qty_Sold__c,
                    Taxable__c: ticketProductObj.Taxable__c,
                    Worker__c: ticketProductObj.Worker__c,
                    Product_Tax__c: ticketProductObj.Product_Tax__c,
                    Redeem_Rule_Name__c: ticketProductObj.redeemName
                };
                var insertQuery = 'INSERT INTO Ticket_Product__c SET ?';
                execute.query(dbName, insertQuery, ticketProductObjData, function (ticketPrdErr, ticketPrdResult) {
                    if (ticketPrdErr) {
                        done(ticketPrdErr, { statusCode: '9999' });
                    } else {
                        updateAppt(ticketProductObj, dbName, loginId, dateTime, function (ticketPrdErr, result) {
                            done(ticketPrdErr, ticketPrdResult = { 'apptId': ticketProductObj.Appt_Ticket__c })
                        });

                    }
                });
            }

        }
    });

}

function createTicketService(ticketServiceObj, dbName, loginId, dateTime, done) {
    var ServiceId = ticketServiceObj.service.Id;
    var sqlQuery = 'SELECT Service_Date_Time__c, Duration__c FROM `Ticket_Service__c` WHERE Appt_Ticket__c = "' + ticketServiceObj.Appt_Ticket__c + '" AND IsDeleted = 0 ORDER BY Service_Date_Time__c DESC'
    execute.query(dbName, sqlQuery, '', function (err, result) {
        if (err) {
            done(err, null);
            logger.error('Error in create Ticket err: ', err);
        } else {
            var apptDate;
            if (result && result.length > 0 && result[0].Service_Date_Time__c !== null) {
                apptDate = dateFns.addMinToDBStr(result[0].Service_Date_Time__c, parseInt(result[0].Duration__c, 10));
            } else {
                apptDate = ticketServiceObj.Appt_Date_Time__c;
            }
            var ticketObjData = {
                Id: uniqid(),
                IsDeleted: 0,
                CreatedDate: dateTime,
                CreatedById: loginId,
                LastModifiedDate: dateTime,
                LastModifiedById: loginId,
                SystemModstamp: dateTime,
                LastModifiedDate: dateTime,
                Service_Date_Time__c: apptDate,
                Visit_Type__c: "",
                Client__c: ticketServiceObj.client.Id,
                Duration_1__c: ticketServiceObj.Duration_1,
                Duration_2__c: ticketServiceObj.Duration_2,
                Duration_3__c: ticketServiceObj.Duration_3,
                Guest_Charge__c: ticketServiceObj.Guest_Charge,
                Buffer_After__c: ticketServiceObj.Buffer_After,
                Service_Tax__c: ticketServiceObj.Service_Tax__c,
                Net_Price__c: ticketServiceObj.netPrice,
                Price__c: ticketServiceObj.netPrice,
                Taxable__c: ticketServiceObj.service.Taxable__c,
                Non_Standard_Duration__c: 0,
                Rebooked__c: 0,
                Service__c: ticketServiceObj.service.id,
                Duration__c: ticketServiceObj.serviceDur,
                Status__c: 'Checked In',
                Is_Booked_Out__c: 0,
                Notes__c: ticketServiceObj.service_notes,
                Appt_Ticket__c: ticketServiceObj.Appt_Ticket__c,
                Worker__c: ticketServiceObj.worker.Id,
                Service_Group_Color__c: ticketServiceObj.service.Service_Group__c,
                reward__c: ticketServiceObj.Reward__c,
                Redeem_Rule_Name__c: ticketServiceObj.redeemName,
            };
            var insertQuery = 'INSERT INTO Ticket_Service__c SET ?';
            execute.query(dbName, insertQuery, ticketObjData, function (ticketServErr, ticketServResult) {
                if (ticketServErr) {
                    done(ticketServErr, null);
                    logger.error('Error in create Ticket ticketServErr: ', ticketServErr);
                } else {
                    // var indexParams = 0;
                    var apptSql = 'Update Appt_Ticket__c SET isNoService__c = 0 where Id= "' + ticketServiceObj.Appt_Ticket__c + '" ';
                    execute.query(dbName, apptSql, '', function (err, result) {
                        if (err) {
                            done(err, null);
                            logger.error('Error in create Ticket err: ', err);
                        }
                        //checking packages
                        pckgquery = 'SELECT Cp.*, Apt.Name as TicketName, P.Name FROM `Client_Package__c` as Cp ' +
                            'LEFT JOIN Package__c as P on P.Id = Cp.Package__c ' +
                            'LEFT JOIN Appt_Ticket__c as Apt on Cp.Ticket__c = Apt.Id ' +
                            'WHERE Cp.Client__c= "' + ticketServiceObj.client.Id + '" and Cp.isDeleted =0'
                        execute.query(dbName, pckgquery, function (error, resultss) {
                            var serviceIds = "'";
                            for (i = 0; i < resultss.length; i++) {
                                for (let k = 0; k < JSON.parse(resultss[i].Package_Details__c).length; k++) {
                                    serviceIds += JSON.parse(resultss[i].Package_Details__c)[k].serviceId + "','";
                                }
                            }
                            if (error) {
                                done(error, null);
                                logger.error('Error in create Ticket error: ', error);
                            } else {
                                if (resultss.length > 0 && serviceIds.slice(0, serviceIds.length - 2)) {
                                    query1 = 'SELECT Id, Name as ServiceName FROM Service__c WHERE IsDeleted = 0 AND Id IN (' + serviceIds.slice(0, serviceIds.length - 2) + ');'
                                    execute.query(dbName, query1, function (error1, results1) {
                                        if (error1) {
                                            done(error1, null);
                                            logger.error('Error in create Ticket error1: ', error1);
                                        } else {
                                            var packageServices = resultss;
                                            if (packageServices.length > 0) {
                                                var packObj = [];
                                                var pckArray = [];
                                                var packd = "true";
                                                for (let i = 0; i < packageServices.length; i++) {
                                                    var packageDetails = JSON.parse(packageServices[i].Package_Details__c);
                                                    packageDetails.filter(function (service) {
                                                        if (service.serviceId == ServiceId && service.used < +service.reps && packd === "true") {
                                                            packObj.push({
                                                                'pckId': packageServices[i].Id,
                                                                'serviceId': ServiceId,
                                                                'netPrice': service.discountPriceEach,
                                                                'Price__c': service.priceEach,
                                                                'serTax': ticketServiceObj.Service_Tax__c
                                                            });
                                                            var sql3 = 'SELECT Discounted_Package__c, Tax__c FROM Package__c WHERE Id = "' + packageServices[i].Package__c + '"';
                                                            execute.query(dbName, sql3, '', function (error, packageDta) {
                                                                pckArray.push({
                                                                    'pckId': packageServices[i].Id,
                                                                    'sumOfDiscountedPrice': packageDta[0].Discounted_Package__c + packageDta[0].Tax__c,
                                                                    'discountedPackage': packageDta[0].Discounted_Package__c,
                                                                    'pckgtax': packageDta[0].Tax__c,
                                                                    'Drawer_Number__c': ''
                                                                });
                                                                commonClientPackage(pckArray, 'true', packObj, dbName, loginId,
                                                                    dateTime, ticketServiceObj.Appt_Ticket__c,
                                                                    function (err, result) {
                                                                        updateAppt(ticketServiceObj, dbName, loginId, dateTime, function (ticketServErr, result) {
                                                                            indexParams++;
                                                                            sendChangeStatusResponse(indexParams, done, ticketServErr, ticketServResult = { 'apptId': ticketServiceObj.Appt_Ticket__c });
                                                                        });
                                                                    });
                                                            });
                                                            packd = "false";
                                                        }
                                                    });
                                                }
                                            } else {
                                                updateAppt(ticketServiceObj, dbName, loginId, dateTime, function (ticketServErr, result) {
                                                    indexParams++;
                                                    sendChangeStatusResponse(indexParams, done, ticketServErr, ticketServResult = { 'apptId': ticketServiceObj.Appt_Ticket__c });
                                                });
                                            }
                                            // done(error1, results = { 'ClientPackageData': resultss, 'ServiceData': results1 });
                                            done(null, { 'apptId': ticketServiceObj.Appt_Ticket__c });
                                        }
                                    });
                                } else {
                                    // done(error, results = { 'ClientPackageData': resultss, 'ServiceData': [] });
                                    done(null, { 'apptId': ticketServiceObj.Appt_Ticket__c });
                                }
                            }
                        });
                    });

                }
            });
        }
    });
}

function createTips(tipsData, dbName, loginId, dateTime, done) {
    var tipsObjData = {
        Id: uniqid(),
        IsDeleted: 0,
        CreatedDate: dateTime,
        CreatedById: loginId,
        LastModifiedDate: dateTime,
        LastModifiedById: loginId,
        SystemModstamp: dateTime,
        LastModifiedDate: dateTime,
        Appt_Ticket__c: tipsData.Appt_Ticket__c,
        Drawer_Number__c: tipsData.Drawer_Number__c,
        Tip_Amount__c: tipsData.Tip_Amount__c,
        Tip_Option__c: tipsData.Tip_Option__c,
        Worker__c: tipsData.Worker__c
    };
    var insertQuery = 'INSERT INTO Ticket_Tip__c SET ?';
    execute.query(dbName, insertQuery, tipsObjData, function (err, result) {
        if (err) {
            logger.error('Error in CheckOut dao - saveTips:', err);
            done(err, { statusCode: '9999' });
        } else {
            updateAppt(tipsData, dbName, loginId, dateTime, function (tipsErr, result) {
                done(tipsErr, { 'apptId': tipsData.Appt_Ticket__c })
            });
        }
    });
}

function getrewards(body, dbName, loginId, dateTime) {
    var amount = 0;
    var clientactiverew = "SELECT * FROM `Contact__c` WHERE Id = '" + body.client.Id + "' ";
    execute.query(dbName, clientactiverew, '', function (err, result) {
        if (result[0].Active_Rewards__c == 1) {
            var queryrwrdc = 'SELECT * from Reward__c where Active__c = "1" and IsDeleted = 0 and Id="' + body.rewards.parent_id + '" ';

            execute.query(dbName, queryrwrdc, '', function (err, resultss) {
                if (result.length > 0) {
                    var temp2 = JSON.parse(resultss[0].Redeem_Rules__c);
                    if (temp2.length > 0) {
                        var today = new Date();
                        for (var i = 0; i < temp2.length; i++) {
                            if (temp2[i].onOneItem == "Products" && temp2[i].redeemName == body.rewards.redeemName) {
                                var startdatee = temp2[i].startDate;
                                var enddatee = temp2[i].endDate;
                                var now = moment().format("YYYY-DD-MM");
                                var startdate = moment(startdatee).format("YYYY-DD-MM");
                                var enddate = moment(enddatee).format("YYYY-DD-MM");
                                if (now > startdate && now < enddate) {
                                    //ticketProductObj.Reward__c = body.rewards.parent_id;  
                                    //ticketProductObj.redeemName = body.rewards.redeemName;
                                    if (temp2[i].discountType != "Percent") {
                                        amount += parseFloat(body.product.price) - parseFloat(temp2[i].discount);
                                    } else {
                                        amount += parseFloat(body.product.price) - parseFloat(body.product.price) * parseFloat(temp2[0].discount) / 100;
                                        return amount;
                                    }

                                } else {
                                    ticketProductObj.Reward__c = "";
                                    ticketProductObj.redeemName = "";
                                }
                            }
                        }
                    }
                } else {
                    ticketProductObj.Reward__c = "";
                    ticketProductObj.redeemName = "";
                }
            })
        } else {
            ticketProductObj.Reward__c = ""; //req.body.rewards.parent_id
            ticketProductObj.redeemName = ""; //req.body.rewards.redeemName 
        }
    })
    setTimeout(function () {
        return amount;
    }, 2000)

}

function getUsrDtStrFrmDBStr(DBDtStr) {
    const dtObj = getDateTmFrmDBDateStr(DBDtStr);
    DBDtStr = (dtObj.getMonth() + 1) + '/' + dtObj.getDate() + '/' + dtObj.getFullYear();
    return [DBDtStr, formatAMPM(dtObj)];
}

function getDateTmFrmDBDateStr(dateStr) {
    const dtTmArry = dateStr.split(' ');
    const dtArry = dtTmArry[0].split('-');
    const tmArry = dtTmArry[1].split(':');
    return new Date(parseInt(dtArry[0], 10), (parseInt(dtArry[1], 10) - 1), parseInt(dtArry[2], 10),
        parseInt(tmArry[0], 10), parseInt(tmArry[1], 10), parseInt(tmArry[2], 10));
}

function formatAMPM(date) {
    let hours = date.getHours();
    const minutes = date.getMinutes();
    const ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    if (hours % 12 === 0) {
        hours = '12';
    }
    hours = ('0' + hours).slice(-2) ? ('0' + hours).slice(-2) : 12;
    return hours + ':' + ('0' + minutes).slice(-2) + ' ' + ampm;
}
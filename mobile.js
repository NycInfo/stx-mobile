var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var path = require('path');
var multer = require('multer');
var app = express();
var sendResponse = require('./common/response').sendResponse;
var tokenService = require('./common/tokenService');
var logger = require('./common/logger');

app.set('env', "development");
var jsonParser = bodyParser.json({
	limit: '150mb'
});
var urlencodedParser = bodyParser.urlencoded({
	extended: false,
	limit: '150mb',
	parameterLimit: 50000
});
var upload = multer({
    dest: 'uploads/'
});
app.use(jsonParser);
app.use(urlencodedParser);

// --- Start of Code for Handling Uncaught Exceptions
process.on('uncaughtException', function (err) {
    logger.error('Un Caught Exception: ', err);
});
// --- End of Code for Handling Uncaught Exceptions

app.use('/uploads', express.static(path.join(__dirname, 'uploads')));
app.use('/templates', express.static(path.join(__dirname, 'templates')));

app.use(function (req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	res.setHeader('Access-Control-Allow-Credentials', true);
	var exUrl = false;
    var excTknURLsArry = [
        '/',
        '/api/users/login',
        '/api/users/forgot_password',
        '/api/users/restpassword'
    ];
    if (req.url.indexOf('/uploads/') > -1) {
        exUrl = true;
    }
    if (!exUrl) {
        for (var i = 0; i < excTknURLsArry.length; i++) {
            if (excTknURLsArry[i] === req.url || req.url.match(/api\/v1\/lookups/g)) {
                exUrl = true;
            } else if (excTknURLsArry[i] === req.url || req.url.match(/api\/v1\/lookups\/states/g)) {
                exUrl = true;
            }
        }
    }
    if (exUrl) {
        next();
    } else {
        var token = req.headers.token;
        if (token && token !== '') {
            tokenService.updateToken(token, res, function (err, tokenData) {
                if (err && err.name === 'JsonWebTokenError') {
                    sendResponse(res, false, '002');
                } else if (err && err.name === 'TokenExpiredError') {
                    sendResponse(res, false, '003');
                } else if (err) {
                    sendResponse(res, false, '003');
                } else {
                    req.headers.db = tokenData.db;
                    req.headers.id = tokenData.id;
                    req.headers.userName = tokenData.userName;
                    req.headers.firstName = tokenData.firstName;
                    req.headers.lastName = tokenData.lastName;
                    req.headers.cid = tokenData.cid;
                    req.headers.cname = tokenData.cname;
                    req.headers.package = tokenData.package;
                    req.headers.workerRole = tokenData.workerRole;
                    next();
                }
            });
        } else {
            sendResponse(res, false, '001');
        }
    }
});

app.disable('etag');

fs.readdirSync('./controllers').forEach(function (file) {
	if (file.substr(-3) == '.js') {
		require('./controllers/' + file).controller(app);
	}
});

app.get('/', function (req, res) {
	res.send('{status: "Server Wroking"}');
	res.end();
});

var debug = require('debug')('ncapp:server');
var http = require('http');
var port = normalizePort('3050');
app.set('port', port);
var server = http.createServer(app);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

function normalizePort(val) {
	var port = parseInt(val, 10);

	if (isNaN(port)) {
		return val;
	}

	if (port >= 0) {
		return port;
	}

	return false;
}

function onError(error) {
	if (error.syscall !== 'listen') {
		throw error;
	}

	var bind = typeof port === 'string'
		? 'Pipe ' + port
		: 'Port ' + port;

	switch (error.code) {
		case 'EACCES':
			console.error(bind + ' requires elevated privileges');
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(bind + ' is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
}

function onListening() {
	var addr = server.address();
	var bind = typeof addr === 'string'
		? 'pipe ' + addr
		: 'port ' + addr.port;
	debug('Listening on ' + bind);
}
module.exports = app;

var mysql = require('mysql');
var config = require('./config');
var logger = require('./logger');

var query = function (db, query, data, done) {
    try {
        mySQLConnection = mysql.createConnection({
            host: config['mySQLhost'],
            user: config['mySQLuser'],
            password: config['mySQLpwd'],
            dateStrings: 'date',
            multipleStatements: true,
            connectTimeout: 30000,
            database: db
        });
        mySQLConnection.connect(function (err) {
            if (err) {
                logger.error('Error in executing query', err);
            }
        });
        if (data === '') {
            mySQLConnection.query(query, function (err, queryResult) {
                done(err, queryResult, db);
            });
        } else {
            mySQLConnection.query(query, data, function (err, queryResult) {
                done(err, queryResult, db);
            });
        }
        mySQLConnection.end();

    } catch (exp) {
        logger.error('Exception in executing query', exp);
    }
}

module.exports.query = query;
var config = require('./config');
var sendGrid = require('@sendgrid/mail');
var logger = require('./logger');

sendGrid.setApiKey(config.sendGridAPIKey);

module.exports = {
    sendemail: function (toAddress, fromAddress, subjectText, htmlMessage, attachment, callback) {
        if (!fromAddress) {
            fromAddress = {
                email: 'info@stxsoftware.com',
                name: 'STX Team'
            };
        }
        if (!subjectText) {
            subjectText = 'STX';
        }
        if (!htmlMessage) {
            htmlMessage = '<div></div>';
        }
        var mailOptions = {
            to: toAddress,
            from: fromAddress,
            subject: subjectText,
            html: htmlMessage
        };
        if (attachment) {
            mailOptions.attachments = attachment;
        }
        sendGrid.send(mailOptions, function (error, info) {
            if (error) {
                logger.error('Error in sending Mail:', error);
                callback(error, null);
            } else {
                callback(null, info);
            }
        });
    }
}

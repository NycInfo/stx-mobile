var config = require('./config');
var jwt = require('jsonwebtoken');
var logger = require('./logger');
var execute = require('../common/dbConnection');

module.exports = {
    generateToken(tokenData, cb) {
        try {
            jwt.sign({
                data: tokenData
            }, config.RSAKeyPassword, { expiresIn: '8h' }, function(err, token) {
                if (token) {
                    // res.header('token', token);
                    cb(null, token)
                } else {
                    cb(err, null)
                }
            });
        } catch (err) {
            logger.error('Error in CommonSRVC - tokenGeneration: ', err);
            return cb(err, null);
        }
    },

    validateToken(token, res, cb) {
        try {
            jwt.verify(token, config.RSAKeyPassword, function(err, decoded) {
                if (err) {
                    cb(err, null);
                } else {
                    cb(null, decoded);
                }
            });
        } catch (err) {
            logger.error('Error in CommonSRVC - tokenValidation: ', err);
            return cb(err, null);
        }
    },

    updateToken(token, res, cb) {
        try {
            jwt.verify(token, config.RSAKeyPassword, function(err, decoded) {
                if (err) {
                    cb(err, null);
                } else {
                    var tokenData = {
                        'id': decoded.data.id,
                        'userName': decoded.data.userName,
                        'firstName': decoded.data.firstName,
                        'lastName': decoded.data.lastName,
                        'db': decoded.data.db,
                        'cid': decoded.data.cid,
                        'cname': decoded.data.cname,
                        'package': decoded.data.package,
                        'workerRole': decoded.data.workerRole
                    };
                    try {
                        jwt.sign({
                            data: tokenData
                        }, config.RSAKeyPassword, { expiresIn: '8h' }, function(err, token) {
                            if (token) {
                                res.setHeader('token', token);
                                cb(null, decoded.data);
                            } else {
                                cb(err, '')
                            }
                        });
                    } catch (err) {
                        logger.error('Error in CommonSRVC - tokenGeneration: ', err);
                        return cb(err, '');
                    }
                }
            });
        } catch (err) {
            logger.error('Error in CommonSRVC - tokenValidation: ', err);
            return cb(err, '');
        }
    },
    getCompanyEmail: function(dbName, callback) {
        execute.query(dbName, 'SELECT Email__c, Name from Company__c where isDeleted = 0', function(err, result) {
            if (err) {
                logger.error('Error in getCompanyEmail CommonSRVC:', err);
                callback('');
            } else {
                callback({
                    'email': result[0]['Email__c'],
                    'name': result[0]['Name']
                });
            }
        });
    },
};